use TestSetup;
initialize_test_globals_ok;

$zs->empty_filestore_ok();
$zs->empty_dir_ok('t/inc/tmp');

remove_test_db_ok();

zs_done_testing();
