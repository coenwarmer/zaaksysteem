package Test::DummyObjectData;
use Moose;

has attributes => (
    is => 'rw',
    isa => 'HashRef[Zaaksysteem::Object::Attribute]',
    default => sub { {} },
);

has source_object => (
    is => 'rw',
);

=head1 NAME

Test::DummyObjectData - Fake ObjectData object

=head1 SYNOPSIS

    $od = Test::DummyObjectData->new();

    $oa = Zaaksysteem::Object::Attribute->new(...);

    $od->add_object_attributes($oa, ...);
    $attr = $od->get_object_attribute('foo');

=head1 METHODS

=head2 add_object_attributes($oa, $oa, ...)

Add the specified L<Zaaksysteem::Object::Attribute> instances to the
internal list of attributes.

=cut

sub add_object_attributes {
    my $self = shift;

    for my $attr (@_) {
        $self->attributes->{ $attr->name } = $attr;
    }

    return $self;
}

=head2 get_object_attribute

Retrieve an attribute by name.

=cut

sub get_object_attribute {
    my $self = shift;

    return $self->attributes->{ $_[0] };
}

=head2 get_source_object

Retrieve the "source object" for this (fake) ObjectData instance.

=cut

sub get_source_object {
    my $self = shift;
    return $self->source_object;
}

__PACKAGE__->meta->make_immutable();



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

