package TestFor::Catalyst::Controller::API::V1::Subject;
use base qw(ZSTest::Catalyst);

use Moose;
use TestSetup;

=head1 NAME

TestFor::Catalyst::Controller:API::V1::Subject - Proves the boundaries of our API: Subject

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/Catalyst/Controller/API/V1/Subject.pm

=head1 DESCRIPTION

These tests prove the interactions between the outside servicebus and our zaaksysteem. This test
uses the version 1 API of zaaksysteem, and proves the C<api/v1/subject> namespace.

=head1 USAGE 

Usage tests, use these if you would like to know how to interact with the API, it's also useful
for extended documentation of the StUF API.

=head2 Create

=head3 create subject

    # curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/subject/create

B<Request>

=begin javascript

{
   "customer_type" : "government",
   "owner" : "betrokkene-bedrijf-91"
}

=end javascript

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-81b8d1-0134cc",
   "result" : {
      "instance" : {
         "customer_type" : "government",
         "id" : "c25d2daf-0e00-4fa5-8bc7-b61fff072234",
         "ipaddresses" : [],
         "owner" : "betrokkene-bedrijf-131",
         "template" : "mintlab"
      },
      "reference" : "c25d2daf-0e00-4fa5-8bc7-b61fff072234",
      "type" : "controlpanel"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_controlpanel_create : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;

        $mech->zs_login;
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/subject/create',
            {
                'subject_type'                      => 'bedrijf',
                'handelsnaam'                       => 'Gemeente Zaakstad',
                'dossiernummer'                     => '25654589',
                'vestigingsnummer'                  => '123456789012',
                'vestiging_landcode'                => '6030',
                'vestiging_straatnaam'              => 'Zaakstraat',
                'vestiging_huisnummer'              => 44,
                'vestiging_huisletter'              => 'a',
                'vestiging_huisnummertoevoeging'    => '1rechts',
                'vestiging_postcode'                => '1234AB',
                'vestiging_woonplaats'              => 'Amsterdam',
            }
        );
    }, 'api/v1/controlpanel/create: create a simple controlpanel');
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
