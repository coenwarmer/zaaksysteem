package TestFor::General::ZTT::MagicDirective;

use base qw[Test::Class];

use TestSetup;

use Zaaksysteem::ZTT::MagicDirective;

sub ztt_magic_directive_parser : Tests {
    my $directive_parser = Zaaksysteem::ZTT::MagicDirective->new;

    is_deeply $directive_parser->parse('case.id'), {
        expression => \'case.id'
    }, 'simple attribute is "parsed"';

    is_deeply $directive_parser->parse('case.startdate | date'), {
        expression => \'case.startdate',
        filter => {
            name => 'date',
            args => []
        }
    }, 'simple attribute + filter parses';

    is_deeply $directive_parser->parse('case.startdate | date(short)'), {
        expression => \'case.startdate',
        filter => {
            name => 'date',
            args => [qw[short]]
        }
    }, 'simple attribute + filter with bareword argument parses';

    is_deeply $directive_parser->parse('case.startdate | date("ABC")'), {
        expression => \'case.startdate',
        filter => {
            name => 'date',
            args => [qw[ABC]]
        }
    }, 'simple attribute + filter with string argument parses';

    is_deeply $directive_parser->parse('case.startdate | date("ABC", case, "herp")'), {
        expression => \'case.startdate',
        filter => {
            name => 'date',
            args => [qw[ABC case herp]]
        }
    }, 'simple attribute + filter with mixed arguments parses';

    is_deeply $directive_parser->parse('itereer:case_relations:case.id'), {
        expression => \'case.id',
        iterate_context => 'case_relations'
    }, 'simple attribute + inline iteration syntax ok';

    is_deeply $directive_parser->parse('itereer:case_relations:case.id | link'), {
        expression => \'case.id',
        iterate_context => 'case_relations',
        filter => {
            name => 'link',
            args => []
        }
    }, 'simple attribute + inline iteration syntax + arg-less filter parses';

    is_deeply $directive_parser->parse('itereer:case_relations:case.id | link(case, herp)'), {
        expression => \'case.id',
        iterate_context => 'case_relations',
        filter => {
            name => 'link',
            args => [qw[case herp]]
        }
    }, 'simple attribute + inline iteration syntax + filter + filter_args parses';

    is_deeply $directive_parser->parse('ITerEER:case_RELATIONS:case.ID | LiNK(CaSe, "AbC")'), {
        expression => \'case.id',
        iterate_context => 'case_relations',
        filter => {
            name => 'link',
            args => [qw[CaSe AbC]]
        }
    }, 'mixed-case attribute name, iterate keyword, iterate context and filtername parses and returns lower-cased';

    is $directive_parser->parse('magic_string_with_ space'), undef, 'syntax error in attribute results in undef parse';
}

sub ztt_mathemagic_directive_parser : Tests {
    my $parser = Zaaksysteem::ZTT::MagicDirective->new;


    is_deeply $parser->parse('1 + 1'), {
        expression => [qw[+ 1 1]]
    }, 'simple expression parses';

    is_deeply $parser->parse('1.1 + 2.2'), {
        expression => [qw[+ 1.1 2.2]]
    }, 'simple decimal expression parses';

    is_deeply $parser->parse('1 + 1 + 1'), {
        # infix: (1 + 1) + 1
        expression => [ '+', [qw[+ 1 1]], 1]
    }, 'left-associative expression parses';

    is_deeply $parser->parse('1 + 1 - 1'), {
        # infix: (1 + 1) - 1
        expression => [ '-', [ '+', 1, 1 ], 1 ]
    }, 'left-associative w/balanced precedence expression parses';

    is_deeply $parser->parse('1 + 2 * 3'), {
        # infix: 1 + (2 * 3)
        expression => [ '+', 1, [ '*', 2, 3] ]
    }, 'left-associative w/unbalanced precendence expression parses';

    is_deeply $parser->parse('(1 + 2) * 3'), {
        expression => [ '*', [ '+', 1, 2 ], 3 ]
    }, 'parenthesised subexpression has precedence over multiplication';

    is_deeply $parser->parse('2 * 3 / 4'), {
        expression => [ '/', [ '*', 2, 3 ], 4 ]
    }, 'left-associative w/balanced higher precedence expression parses';

    is_deeply $parser->parse('pow(2, 2)'), {
        expression => {
            function => 'pow',
            arguments => [ 2, 2 ]
        }
    }, 'function-calling expression parses';

    is_deeply $parser->parse('magic_string + 4'), {
        expression => [ '+', \'magic_string', '4' ]
    }, 'bareword magic string expression parses';

    is_deeply $parser->parse('magic_string / pow(3, 3)'), {
        expression => [
            '/',
            \'magic_string',
            { function => 'pow', arguments => [ 3, 3 ] }
        ]
    }, 'bareword magic string w/function-calling expression';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
