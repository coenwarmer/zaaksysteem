package TestFor::General::ZTT::Script;

use base qw[Test::Class];

use TestSetup;

use Zaaksysteem::ZTT::Script;

sub ztt_script_parser : Tests {
    my $script_parser = Zaaksysteem::ZTT::Script->new;

    is_deeply $script_parser->parse('iterate zaak_relaties'), {
        iterate => 'zaak_relaties'
    }, 'simple iteration command';

    is_deeply $script_parser->parse('show_when { a == b }'), {
        show_when => {
            operator => '==',
            operands => [
                { type => 'bareword', value => 'a' },
                { type => 'bareword', value => 'b' }
            ]
        }
    }, 'simple show_when command';

    is_deeply $script_parser->parse('show_when { a != "b" }'), {
        show_when => {
            operator => '!=',
            operands => [
                { type => 'bareword', value => 'a' },
                { type => 'string', value => 'b' }
            ]
        }
    }, 'simple show_when command with inequality and string arg';

    my ($iterate, $show_when) = $script_parser->parse(
        "iterate zaak_relaties\nshow_when { a == b }"
    );

    is_deeply $iterate, {
        iterate => 'zaak_relaties'
    }, 'multiline script 1/2: iteration command';

    is_deeply $show_when, {
        show_when => {
            operator => '==',
            operands => [
                { type => 'bareword', value => 'a' },
                { type => 'bareword', value => 'b' }
            ]
        }
    }, 'multiline script 2/2: show_when command';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
