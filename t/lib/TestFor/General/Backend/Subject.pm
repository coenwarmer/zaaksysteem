package TestFor::General::Backend::Subject;
use base qw(ZSTest);

use TestSetup;

# use JSON qw/decode_json encode_json/;


=head1 NAME

TestFor::General::Backend::Subject - Zaaksysteem Subject test

=head1 LAYOUT

This file implements all USAGE tests, in other words: if you would like
to use the engine, here is your inspiration.

=head1 USAGE (or end-to-end)

Usage tests: use the below tests when you would like to give this module
a spin.

=head2 subject_usage_create_user

Creates a user

=cut

sub subject_deletion : Tests {
    $zs->zs_transaction_ok(
        sub {
            my $interface = $schema->resultset('Interface')->search_active({ module => 'authldap' })->first;
            my $rs = $schema->resultset('Subject');

            my $active_users = $rs->search_active();
            my $start_active = $active_users->count;
            is($active_users->count, $start_active, "Have users: $start_active");

            my $ue = $zs->create_medewerker_ok(interface => $interface);
            isa_ok($ue, "Zaaksysteem::Model::DB::UserEntity");

            ok($ue->active, "User is active");

            is($active_users->count, $start_active + 1, "Added an active user");

            $ue->update({date_deleted => DateTime->now()});
            ok(!$ue->active, "User is inactive");

            is($active_users->count, $start_active,
                "And the user is not active anymore");

        },
        'Deletion of a user tested'
    );
}

sub subject_usage_create_user : Tests {
    my $self            = shift;

    $zs->zs_transaction_ok(sub {
        my $interface   = $self->_create_authldap_interface;
        my $rs          = $schema->resultset('Subject');

        throws_ok(
            sub {
                $rs->create_user(
                    {
                        interface => $interface
                    }
                );
            },
            qr/profile/,
            'create_user: Check validation'
        );

        throws_ok(
            sub {
                $rs->create_user(
                    {
                        interface       => $interface,
                        sn              => 'Ootjers',
                        givenname       => 'Michiel',
                        initials        => 'M.K.',
                        mail            => 'info@mintlab.nl',
                        telephonenumber => '0207370005',
                        displayname     => 'M.K. Ootjers',
                    }
                );
            },
            qr/profile/,
            'create_user: Check validation, missing keys from primary profile'
        );

        throws_ok(
            sub {
                $rs->create_user(
                    {
                        username        => 'michiel',
                        interface       => $interface,
                        givenname       => 'Michiel',
                        initials        => 'M.K.',
                        email           => 'info@mintlab.nl',
                        telephonenumber => '0207370005',
                        displayname     => 'M.K. Ootjers',
                    }
                );
            },
            qr/profile/,
            'create_user: Check validation, missing keys from secondary profile'
        );

        my $user_entity = $rs->create_user(
            {
                username        => 'michiel',
                interface       => $interface,
                sn              => 'Ootjers',
                givenname       => 'Michiel',
                initials        => 'M.K.',
                mail            => 'info@mintlab.nl',
                telephonenumber => '0207370005',
                displayname     => 'M.K. Ootjers',
            }
        );
        ok($user_entity, 'Found a user_entity');
        ok($user_entity->subject_id, 'Found a subject to this user_entity');
        my $subject     = $user_entity->subject_id;


        is($subject->subject_type, 'employee', 'Found correct subject_type: employee');
        is($subject->username, 'michiel', 'Found correct username');
        ok($subject->last_modified, 'Last modified set for subject');

        is($user_entity->source_interface_id->id, $interface->id, 'Entity: correct interface id');
        is($user_entity->source_identifier, 'michiel', 'Entity: Source identifier got correct username');
        ok($user_entity->active, 'Entity: active');
        is($user_entity->subject_id->id, $subject->id, 'Entity: Got reference to user_entity');
        ok($user_entity->date_created, 'Entity: Got creation date');

        is($subject->username, 'michiel', 'Found correct username');
        is($subject->sn, 'Ootjers', 'Found correct last name');
        is($subject->givenname, 'Michiel', 'Found correct given name');
        is($subject->initials, 'M.K.', 'Found correct initials');
        is($subject->mail, 'info@mintlab.nl', 'Found correct e-mailaddress');
        is($subject->telephonenumber, '0207370005', 'Found correct phone number');

    }, 'subject: Tested create_user');


    $zs->zs_transaction_ok(sub {
        my $interface   = $self->_create_authldap_interface;
        my $rs          = $schema->resultset('Subject');

        my $group       = $schema->resultset('Groups')->search(
            {
                name    => 'Backoffice'
            }
        )->first;

        my $user_entity = $rs->create_user(
            {
                username        => 'michiel',
                interface       => $interface,
                sn              => 'Ootjers',
                givenname       => 'Michiel',
                initials        => 'M.K.',
                mail            => 'info@mintlab.nl',
                telephonenumber => '0207370005',
                displayname     => 'M.K. Ootjers',
                group_id        => $group->id,
            }
        );
        ok($user_entity, 'Found a user_entity');
        ok($user_entity->subject_id, 'Found a subject to this user_entity');
        my $subject     = $user_entity->subject_id;

        ok(($subject->group_ids && scalar @{ $subject->group_ids }), 'Subject has group');

        my $role        = $schema->resultset('Groups')->find($subject->group_ids->[0]);
        is($role->name, 'Backoffice', 'Subject group is "Backoffice"');
    }, 'subject: Tested create_user with group');


    $zs->zs_transaction_ok(sub {
        my $interface   = $self->_create_authldap_interface;
        my $rs          = $schema->resultset('Subject');

        # Delete all roles so we have our testcase.
        $schema->resultset("Roles")->search({})->delete;

        throws_ok(
            sub {
                $rs->create_user(
                    {
                        username        => 'michiel',
                        interface       => $interface,
                        sn              => 'Ootjers',
                        givenname       => 'Michiel',
                        initials        => 'M.K.',
                        mail            => 'info@mintlab.nl',
                        telephonenumber => '0207370005',
                        displayname     => 'M.K. Ootjers',
                        set_behandelaar => 1,
                    }
                );
            },
            qr/Cannot find system role "Behandelaar"/,
            'Correctly throws when no behandelaar role set'
        );

        $zs->create_default_roles_ok();

        my $user_entity = $rs->create_user(
            {
                username        => 'michiela',
                interface       => $interface,
                sn              => 'Ootjers',
                givenname       => 'Michiel',
                initials        => 'M.K.',
                mail            => 'info@mintlab.nl',
                telephonenumber => '0207370005',
                displayname     => 'M.K. Ootjers',
                set_behandelaar => 1,
            }
        );
        ok($user_entity, 'Found a user_entity');
        ok($user_entity->subject_id, 'Found a subject to this user_entity');
        my $subject     = $user_entity->subject_id;

        ok(($subject->role_ids && scalar @{ $subject->role_ids }), 'Subject has role');

        my $role        = $schema->resultset('Roles')->find($subject->role_ids->[0]);
        is($role->name, 'Behandelaar', 'Subject role is "Behandelaar"');
    }, 'subject: Tested create_user with behandelaar');

    $zs->zs_transaction_ok(sub {
        my $interface   = $schema->resultset('Interface')->search_active({module => 'authldap'})->first;
        my $rs          = $schema->resultset('Subject');

        my $user_entity = $rs->create_user(
            {
                username        => 'michiel',
                interface       => $interface,
                sn              => 'Ootjers',
                givenname       => 'Michiel',
                initials        => 'M.K.',
                mail            => 'info@mintlab.nl',
                telephonenumber => '0207370005',
                displayname     => 'M.K. Ootjers',
                set_behandelaar => 1,
                password        => 'Test123'
            }
        );
        ok($user_entity, 'Found a user_entity');
        ok($user_entity->subject_id, 'Found a subject to this user_entity');
        my $subject     = $user_entity->subject_id;

        ok($subject->check_password('Test123'), 'Password succesfully updated');
        ok(!$subject->check_password('Test1234'), 'Fails on incorrect password');


    }, 'subject: Tested create_user with spiffy features');

}

sub subject_roles_and_groups : Tests {
    $zs->zs_transaction_ok(
        sub {

            my $medewerker = $zs->create_medewerker_ok()->subject_id;
            my $bid        = 'betrokkene-medewerker-' . $medewerker->id;

            my $roles  = $medewerker->roles;
            my $groups = $medewerker->groups;

            is(@$roles, 1, "One role");
            is(@$groups, 3, "Three groups");
        },
        "Medewerker roles and groups",
    );
}

sub _create_authldap_interface {
    my ($self, $opts)           = @_;

    my $iface                   = $zs->create_named_interface_ok(
        {
            module              => 'authldap',
            name                => 'LDAP Authenticatie',
            interface_config    => {
                ($opts ? %$opts : ())
            }
        },
    );

    return $iface;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
