package TestFor::General::Backend::Sysin::Modules::AuthInternal;
use base 'Test::Class';

use TestSetup;

use Zaaksysteem::Backend::Sysin::Modules::AuthInternal;

sub sysin_authldap_tests : Tests {

    $zs->txn_ok(
        sub {
            my $interface = $zs->create_named_interface_ok(
                {
                    module              => 'authldap',
                    name                => 'ZS Internal Authentication',
                    interface_config    => { }
                },
            );

            ok($interface->module_object->essential, 'authldap interface is marked "essential"');
        },
        "Essential interfaces"
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
