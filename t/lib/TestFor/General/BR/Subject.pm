package TestFor::General::BR::Subject;

use base qw(ZSTest);

use Moose;
use TestSetup;

use Zaaksysteem::BR::Subject;

=head1 NAME

TestFor::General::BR::Subject - Businessrules for Subject

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/General/BR/Subject.pm


=head1 DESCRIPTION

This is the main object to interfere with the subject subsystem of Zaaksysteem. Newstyle.

=head1 USAGE 

Usage tests, use these if you would like to know how to interact with subjects

=head2 Create

=cut

sub br_subject_usage_create : Tests {
    my $self        = shift;

    $zs->zs_transaction_ok(sub {
        ### USAGE
        my $subject     = Zaaksysteem::BR::Subject->new(
            schema          => $schema,
            log             => $schema->default_resultset_attributes->{log},
            subject_type    => 'bedrijf',
        );

        my $params      = {
            'handelsnaam'                       => 'Gemeente Zaakstad',
            'dossiernummer'                     => '25654589',
            'vestigingsnummer'                  => '123456789012',
            'vestiging_landcode'                => '6030',
            'vestiging_straatnaam'              => 'Zaakstraat',
            'vestiging_huisnummer'              => 44,
            'vestiging_huisletter'              => 'a',
            'vestiging_huisnummertoevoeging'    => '1rechts',
            'vestiging_postcode'                => '1234AB',
            'vestiging_woonplaats'              => 'Amsterdam',

            'password'                          => 'Test123',
        };

        my $rv          = $subject->save($params);

        ### END USAGE

        ok($rv->id, 'Found an ID for created subject');

        my $company = $schema->resultset('Bedrijf')->find($rv->id);
        my $auth    = $schema->resultset('BedrijfAuthenticatie')->find({ gegevens_magazijn_id => $rv->id });


        ok($company, 'Found company in database');
        ok($auth, 'Found authentication credentials in database');

        for my $key (keys %$params) {
            if ($key eq 'password') {
                is($auth->$key, $params->{$key}, 'Given key matches entry in database for ' . $key);
                next;
            }

            is($company->$key, $params->{$key}, 'Given key matches entry in database for ' . $key);
        }

        ### Check subject object
        for my $key (keys %$params) {
            is($subject->$key, $params->{$key}, 'Given key matches entry in object for ' . $key);
        }
    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

