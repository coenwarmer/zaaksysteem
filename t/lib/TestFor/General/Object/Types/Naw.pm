package TestFor::General::Object::Types::Naw;
use base qw(Test::Class);

use TestSetup;
use Zaaksysteem::Tools;
use Zaaksysteem::Object::Types::Naw;

sub zs_object_types_naw : Tests {

    my $model = $zs->object_model;

    $zs->zs_transaction_ok(
        sub {
            my %opts = (
                owner                 => 'betrokkene-bedrijf-1',
                naam                  => "My Name is",
                straatnaam            => "Foostraat",
                huisnummer            => 42,
                huisnummer_letter     => 'C',
                huisnummer_toevoeging => '521',
                postcode              => '1011PZ',
                woonplaats            => 'Amsterdam',
                email                 => 'foo@example.net',
                telefoonnummer        => '0612345678',
            );

            my $objects = $zs->create_object_ok('Naw', \%opts);
        },
        'NAW Object'
    );

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
