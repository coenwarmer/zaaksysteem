package TestFor::General::Object::Iterator;
use warnings;
use strict;
use base 'Test::Class';

use TestSetup;
use Zaaksysteem::Object::Model;
use Zaaksysteem::Object::Iterator;

sub setup : Test(startup) {
    my $self = shift;

    $self->{model} = Zaaksysteem::Object::Model->new(
        schema => $zs->schema
    );
}

sub test_object_iterator : Tests {
    my $self = shift;
    my $model = $self->{model};

    $zs->txn_ok(sub {
        my $new_obj = $model->inflate_from_json('{ "object_class": "object" }');
        my $object1 = $model->save(object => $new_obj);
        my $object2 = $model->save(object => $new_obj);
        my $object3 = $model->save(object => $new_obj);

        my @objects_sorted = sort { $a->id cmp $b->id } ($object1, $object2, $object3);

        my $rs = $model->rs->search(
            {
                'me.object_class' => 'object',
                'me.uuid' => [ $object1->id, $object2->id, $object3->id ],
            },
            { order_by => 'uuid' },
        );

        my $iter = Zaaksysteem::Object::Iterator->new(
            rs    => $rs,
            inflator => sub { $model->inflate_from_row(shift) }
        );

        my $first = $iter->next();
        is(
            $first->id,
            $objects_sorted[0]->id,
            "Calling ->next the first time results in the correct object"
        );

        my $second = $iter->next();
        is(
            $second->id,
            $objects_sorted[1]->id,
            "Calling ->next a second time results in the correct object"
        );

        my $third = $iter->next();
        is(
            $third->id,
            $objects_sorted[2]->id,
            "Calling ->next a third time results in the correct object"
        );

        my $none = $iter->next();
        is($none, undef, "Calling ->next() when there are no more results");

        $iter->reset();

        my $first_again = $iter->next();
        is(
            $first_again->id,
            $objects_sorted[0]->id,
            "Calling ->next after reset yields the first object again"
        );

        is ($iter->count, 3, "Iterator count reports correct value");

        my $first_again_again = $iter->first();
        is($first_again_again->id, $objects_sorted[0]->id, "->first works as expected");

        $iter->reset();

        $iter->set_pager_attributes(
            current_page => 2,
            page_size => 2,
        );

        my $third_again = $iter->next();
        is($third_again->id, $objects_sorted[2]->id, "Setting pager attributes works");

        isa_ok($iter->pager, 'Data::Page', "Retrieving the pager from an iterator works");
    });
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
