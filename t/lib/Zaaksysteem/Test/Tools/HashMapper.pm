package Zaaksysteem::Test::Tools::HashMapper;

use Zaaksysteem::Test;

use Zaaksysteem::Tools::HashMapper;

sub test_hashmap_inflation {
    my $test = shift;

    my @tests = (
        {
            label    => 'level 1, single kvp',
            data     => { a => 1 },
            expect   => { a => 1 }
        },
        {
            label    => 'level 1, single array kvp',
            data     => { a => [ 1 ] },
            expect   => { a => [ 1 ] }
        },
        {
            label    => 'level 1, multi kvp',
            data     => { a => 1, b => 2 },
            expect   => { a => 1, b => 2 }
        },
        {
            label    => 'level 1, multi array kvp',
            data     => { a => [ 1 ], b => [ 2 ] },
            expect   => { a => [ 1 ], b => [ 2 ] }
        },
        {
            label    => 'level 2, nesting single kvp',
            data     => { 'a.b' => 1 },
            expect   => { a => { b => 1 } }
        },
        {
            label    => 'level 2, nesting single array kvp',
            data     => { 'a.b' => [ 1 ] },
            expect   => { a => { b => [ 1 ] } }
        },
        {
            label    => 'level 2, nesting multi kvp',
            data     => { 'a.b' => 1, 'a.c' => 2 },
            expect   => { a => { b => 1, c => 2 } }
        },
        {
            label    => 'level 2, nesting multi kvp',
            data     => { 'a.b' => [ 1 ], 'a.c' => [ 2 ] },
            expect   => { a => { b => [ 1 ], c => [ 2 ] } }
        },
        {
            label    => 'level 2, multi nesting multi kvp',
            data     => { 'a.b' => 1, 'c.d' => 2 },
            expect   => { a => { b => 1 }, c => { d => 2 } }
        },
        {
            label    => 'level 3, conflict kvp',
            data     => { a => 1, 'a.b' => 2 },
            expect   => { a => { _value => 1, b => 2 } },
            deflated => { 'a._value' => 1, 'a.b' => 2 }
        },
        {
            label    => 'level 3, multi conflict kvp',
            data     => { a => 1, 'a.b' => 2, 'a.b.c' => 3 },
            expect   => { a => { _value => 1, b => { _value => 2, c => 3 } } },
            deflated => { 'a._value' => 1, 'a.b._value' => 2, 'a.b.c' => 3 }
        }
    );

    for (@tests) {
        my $inflated = inflate($_->{ data });

        is_deeply $inflated, $_->{ expect }, sprintf(
            'inflate %s',
            $_->{ label }
        );

        my $deflated = deflate($inflated);

        # Test inflation stability
        is_deeply $deflated, $_->{ deflated } || $_->{ data }, sprintf(
            'deflate %s',
            $_->{ label }
        );

        # Test deflation stability
        is_deeply inflate($deflated), $inflated, sprintf(
            're-inflate %s',
            $_->{ label }
        );
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
