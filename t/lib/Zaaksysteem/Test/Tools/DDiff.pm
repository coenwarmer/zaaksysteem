package Zaaksysteem::Test::Tools::DDiff;

use Zaaksysteem::Test;
use Zaaksysteem::Tools::DDiff;

sub test_ddiff {
    my $test = shift;

    my @changes;

    my @tests = (
        {
            args => [ 1, 1 ],
            msg => 'scalar nop',
        },
        {
            args => [ [], [] ],
            msg => 'arrayref nop',
        },
        {
            args => [ [ 1 ], [ 1 ] ],
            msg => 'arrayref[] nop',
        },
        {
            args => [ 1, 2 ],
            num_changes => 1,
            msg => 'scalar update',
            changes => [
                { from => 1, to => 2, op => 'update' }
            ]
        },
        {
            args => [ [ 1 ], [ 2 ] ],
            num_changes => 1,
            msg => 'arrayref[] value update',
            changes => [
                {
                    op => 'update_array_element',
                    index => 0,
                    update => {
                        from => 1,
                        to => 2,
                        op => 'update'
                    }
                }
            ]
        },
        {
            args => [ [ 1 ], [ 1, 2 ] ],
            num_changes => 1,
            msg => 'arrayref[] add value',
            changes => [
                {
                    op => 'update_array_element',
                    index => 1,
                    update => {
                        to => 2,
                        op => 'set'
                    }
                }
            ]
        },
        {
            args => [ [ 1, 2 ], [ 1 ] ],
            num_changes => 1,
            msg => 'arrayref[] remove value',
            changes => [
                {
                    op => 'update_array_element',
                    index => 1,
                    update => {
                        from => 2,
                        op => 'unset'
                    }
                }
            ]
        },
        {
            args => [ {}, {} ],
            msg => 'hashref nop',
        },
        {
            args => [ { a => 1 }, { a => 1 } ],
            msg => 'hashref{} nop',
        },
        {
            args => [ { a => 1 }, { a => 2 } ],
            num_changes => 1,
            msg => 'hashref{} value update',
            changes => [
                {
                    op => 'update_hash_value',
                    key => 'a',
                    update => {
                        from => 1,
                        to => 2,
                        op => 'update'
                    }
                }
            ]
        },
        {
            args => [ { a => 1 }, { b => 1 } ],
            num_changes => 2,
            msg => 'hashref{} rename key',
            changes => [
                {
                    op => 'update_hash_value',
                    key => 'a',
                    update => {
                        op => 'unset',
                        from => 1
                    }
                },
                {
                    op => 'update_hash_value',
                    key => 'b',
                    update => {
                        op => 'set',
                        to => 1
                    }
                }
            ]
        },
        {
            args => [ { a => { b => 1 } }, { a => { b => 2 } } ],
            num_changes => 1,
            msg => 'hashref{}{} value update',
            changes => [
                {
                    op => 'update_hash_value',
                    key => 'a',
                    update => {
                        op => 'update_hash_value',
                        key => 'b',
                        update => {
                            op => 'update',
                            from => 1,
                            to => 2
                        }
                    }
                }
            ]
        },
        {
            args => [ { a => [ { b => 1 } ] }, { a => [ { b => 2 } ] } ],
            num_changes => 1,
            msg => 'hashref{arrayref[hashref{}]} value update',
            changes => [
                {
                    op => 'update_hash_value',
                    key => 'a',
                    update => {
                        op => 'update_array_element',
                        index => 0,
                        update => {
                            op => 'update_hash_value',
                            key => 'b',
                            update => {
                                op => 'update',
                                from => 1,
                                to => 2
                            }
                        }
                    }
                }
            ]
        },
        {
            args => [ { a => { b => { c => undef } } }, { a => { b => { c => 'str' } } } ],
            num_changes => 1,
            msg => 'hashref{hashref{hashref{}}} value update',
            changes => [
                {
                    op => 'update_hash_value',
                    key => 'a',
                    update => {
                        op => 'update_hash_value',
                        key => 'b',
                        update => {
                            op => 'update_hash_value',
                            key => 'c',
                            update => {
                                op => 'set',
                                to => 'str',
                            }
                        }
                    }
                }
            ]
        }
    );

    for (@tests) {
        my @changes = ddiff(@{ $_->{ args } });
        is scalar @changes, $_->{ num_changes } // 0, sprintf('number of changes for %s', $_->{ msg });

        note ddiff_stringify(@changes);
        
        next unless $_->{ changes };

        is_deeply \@changes, $_->{ changes }, sprintf('ddiff update for %s', $_->{ msg });
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
