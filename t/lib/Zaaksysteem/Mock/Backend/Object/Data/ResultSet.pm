package Zaaksysteem::Mock::Backend::Object::Data::ResultSet;

use Moose;

use Mock::Quick;

with qw[
    Zaaksysteem::Search::HStoreResultSet
    Zaaksysteem::Search::TSVectorResultSet
];

=head1 NAME

Zaaksysteem::Mock::Backend::Object::Data::ResultSet - Mocks a ObjectData
resultset

=head1 DESCRIPTION

This mock resultset is built around the
L<Zaaksysteem::Search::ObjectQueryResultSet> role and its unit tests.

=head1 ATTRIBUTES

=head2 _search

Contains the last input to the L</search> call.

=cut

has _search => (
    is => 'rw',
    isa => 'ArrayRef',
    predicate => '_was_searched'
);

=head1 METHODS

=head2 search

Implements a mock L<DBIx::Class::ResultSet/search> method that stores the
input data in L</_search>.

Successive calls to this method destroy previously stashed inputs.

=cut

sub search {
    my $self = shift;

    $self->_search(\@_);

    return $self;
}

=head2 hstore_column

Implements the required C<hstore_column> method for the
L<Zaaksysteem::Search::HStoreResultSet> role. Always returns the string
C<hstore_column>.

=cut

sub hstore_column { 'hstore_column' }

=head2 text_vector_column

Implements the required C<text_vector_column> method for the
L<Zaaksysteem::Search::TSVectorResultSet> role. Always returns the string
C<tsvector_column>.

=cut

sub text_vector_column { 'tsvector_column' }

=head2 result_source

Implements the required C<result_source> method for the
L<Zaaksysteem::Search::ObjectQueryResultSet> role. Always returns a mock
result_source object.

=cut

sub result_source {
    return qobj(
        primary_columns => qmeth { return 'uuid' },
        storage => qobj(
            dbh => qobj(
                quote => qmeth { return $_[1]; }
            )
        )
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
