package Zaaksysteem::Model::ObjectType;

use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

=head1 NAME

Zaaksysteem::Model::ObjectType - Catalyst model wrapper for L<Zaaksysteem::Object::TypeModel>

=cut

__PACKAGE__->config(
    class       => 'Zaaksysteem::Object::TypeModel',
    constructor => 'new',
);

=head1 METHODS

=head2 prepare_arguments

This method overrides the L<Catalyst::Model::Adaptor/prepare_arguments> method
and supplies an empty hash for the L<TypeModel|Zaaksysteem::Object::TypeModel> constructor.

=cut

sub prepare_arguments {
    my ($self, $c) = @_;

    return {};
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
