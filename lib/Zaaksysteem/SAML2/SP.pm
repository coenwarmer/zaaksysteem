package Zaaksysteem::SAML2::SP;

use Moose;

BEGIN { extends 'Net::SAML2::SP'; }

use Zaaksysteem::Profile;
use Zaaksysteem::Exception;

use File::Temp;

use constant SIGNING_BLOCK => qq{
    <ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
      <ds:SignedInfo>
        <ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
        <ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"/>
        <ds:Reference>
          <ds:Transforms>
            <ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
            <ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
          </ds:Transforms>
          <ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/>
          <ds:DigestValue />
        </ds:Reference>
        </ds:SignedInfo>
      <ds:SignatureValue />
      <ds:KeyInfo>
        <ds:KeyName>REPLACE_KEYNAME</ds:KeyName>
      </ds:KeyInfo>
    </ds:Signature>
};

has id => ( is => 'rw', isa => 'Str', required => 1 );

has interface => ( is => 'rw', isa => 'Zaaksysteem::Model::DB::Interface' );

=head1 SAML2::SP

This class wraps L<Net::SAML2::SP> for a bit nicer integration with
L<Zaaksysteem>.

=head1 Example usage

    my $sp = Zaaksysteem::SAML2::SP->new_from_interface(
        $my_interface_object
    );

    my $metadata = $sp->metadata;

=head1 Methods

=head2 new_from_interface

This methods wraps the logic of building a SP object from an interface
definition. Works like the normal C<new> function, except that it requires
an interface object be supplied as the first argument. Remaining parameters
are interpreted as a hash passed directly to C<new>.

=head3 interface

An L<Zaaksysteem::Backend::Sysin::Interface::Component> instance.

=cut

define_profile new_from_interface => (
    required => [qw[interface]],
    typed => {
        interface => 'Zaaksysteem::Model::DB::Interface'
    }
);

sub new_from_interface {
    my ($class, %params) = @_;

    my $interface = assert_profile(\%params)->valid->{ interface };
    my $schema = $interface->result_source->schema;

    my $cert = $schema->resultset('Filestore')->find($interface->jpath('$.sp_cert[0].id'));

    unless($cert) {
        throw('saml2/sp/certificate', 'Certificate configured for SP not found in filestore.');
    }

    my $contact;
    if (my $contact_data = $interface->jpath('$.contact_id')) {
        $contact = $schema->betrokkene_model->get(
            {type => $contact_data->{object_type}},
            $contact_data->{id},
        );
    }

    return $class->new(
        id               => $interface->jpath('$.sp_webservice'),
        url              => $interface->jpath('$.sp_webservice'),
        interface        => $interface,
        cert             => ($cert->get_path || ''),
        cacert           => '',
        org_name         => $interface->jpath('$.sp_application_name'),
        org_display_name => $interface->jpath('$.sp_application_name'),
        org_contact      => ($contact ? $contact->email : ''),
    );
}

sub signed_metadata {
    my $self            = shift;
    my ($options)       = @_;

    my $xml             = $self->metadata(@_);


    ### Sign the XML
    if (-e '/usr/bin/xmlsec1') {
        my $privfile        = $self->cert;
        my $certtext        = $self->_cert_text;

        my $md5cert         = Digest::MD5::md5_hex( $certtext );

        my $replace         = "\n" . SIGNING_BLOCK;
        $replace            =~ s/REPLACE_KEYNAME/$md5cert/;

        $xml                =~ s/(<md:EntityDescriptor(?:[^>]*)>)/$1$replace/sg;

        my $tmph            = File::Temp->new(
            UNLINK => 1
        );

        my $tmpfile         = $tmph->filename . '.' . 'xml';

        open(my $FH, '>:encoding(UTF-8)', $tmpfile) or throw(
            '/auth/saml/sign_error',
            'Failed signing the XML, are you sure our temp environment is writable?'
        );

        print $FH $xml;
        close($FH);

        if ($privfile && $tmpfile) {
            $xml                = `/usr/bin/xmlsec1 --sign --privkey-pem $privfile $tmpfile`;
        }
    }

    return $xml;

}

# around 'metadata' => sub {
#     my $origin          = shift;
#     my $self            = shift;
#     my ($options)       = @_;

#     my $metadata        = $self->$origin(@_);

#     my $xml             = "$metadata";

#     ### Hack for eHerkenning
#     if ($options && $options->{entity_id}) {
#         my $entityid        = $options->{entity_id};
#         $xml                =~ s|entityID=".*?"|entityID="$entityid"|g;
#     }

#     ### Poor mans matching, replace the extra /saml/, e.g.:
#     ### replace http://zaaksysteem.nl/auth/saml/saml/consumer-post
#     ### with http://zaaksysteem.nl/auth/saml/consumer-post
#     $xml                =~ s|/saml/saml/|/saml/|g;

#     ### Replace 0 or 1 values for false or true values
#     $xml                =~ s|WantAssertionsSigned="1"|WantAssertionsSigned="true"|g;
#     $xml                =~ s|WantAssertionsSigned="0"|WantAssertionsSigned="false"|g;

#     $xml                =~ s|AuthnRequestsSigned="1"|AuthnRequestsSigned="true"|g;
#     $xml                =~ s|AuthnRequestsSigned="0"|AuthnRequestsSigned="false"|g;

#     return $xml;
# };

sub metadata {
    my ($self, $options) = @_;

    ### START COPY FROM Net::SAML2::SP
    my $x = XML::Generator->new(':pretty', conformance => 'loose');
    my $md = ['md' => 'urn:oasis:names:tc:SAML:2.0:metadata'];
    my $ds = ['ds' => 'http://www.w3.org/2000/09/xmldsig#'];

    my $md5cert         = Digest::MD5::md5_hex( $self->_cert_text );

    my $metadataobject = $x->EntityDescriptor(
        $md,
        {
            entityID => $self->id },
        $x->SPSSODescriptor(
            $md,
            { AuthnRequestsSigned => '1',
              WantAssertionsSigned => '1',
              errorURL => $self->url . '/saml/error',
              protocolSupportEnumeration => 'urn:oasis:names:tc:SAML:2.0:protocol' },
            $x->KeyDescriptor(
                $md,
                {
                    use => 'signing'
                },
                $x->KeyInfo(
                    $ds,
                    $x->X509Data(
                        $ds,
                        $x->X509Certificate(
                            $ds,
                            $self->_cert_text,
                        ),
                    ),
                    $x->KeyName(
                        $ds,
                        $md5cert
                    )
                ),
            ),
            $x->SingleLogoutService(
                $md,
                { Binding => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
                  Location  => $self->url . '/saml/slo-soap' },
            ),
            $x->SingleLogoutService(
                $md,
                { Binding => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                  Location  => $self->url . '/saml/sls-redirect-response' },
            ),
            $x->AssertionConsumerService(
                $md,
                { Binding => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
                  Location => $self->url . '/saml/consumer-post',
                  index => '1',
                  isDefault => 'true' },
            ),
            $x->AssertionConsumerService(
                $md,
                { Binding => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact',
                  Location => $self->url . '/saml/consumer-artifact',
                  index => '2',
                  isDefault => 'false' },
            ),
        ),
        $x->Organization(
            $md,
            $x->OrganizationName(
                $md,
                {
                    'xml:lang' => 'en' },
                $self->org_name,
            ),
            $x->OrganizationDisplayName(
                $md,
                {
                    'xml:lang' => 'en' },
                $self->org_display_name,
            ),
            $x->OrganizationURL(
                $md,
                {
                    'xml:lang' => 'en' },
                $self->url
            )
        ),
        $x->ContactPerson(
            $md,
            {
                contactType => 'other' },
            $x->Company(
                $md,
                $self->org_display_name,
            ),
            $x->EmailAddress(
                $md,
                $self->org_contact,
            ),
        )
    );
    ### END COPY FROM Net::SAML2::SP


    my $xml             = "$metadataobject";

    ### Hack for eHerkenning
    if ($options && $options->{entity_id}) {
        my $entityid        = $options->{entity_id};
        $xml                =~ s|entityID=".*?"|entityID="$entityid"|g;
    }

    ### Poor mans matching, replace the extra /saml/, e.g.:
    ### replace http://zaaksysteem.nl/auth/saml/saml/consumer-post
    ### with http://zaaksysteem.nl/auth/saml/consumer-post
    $xml                =~ s|/saml/saml/|/saml/|g;

    ### Replace 0 or 1 values for false or true values
    $xml                =~ s|WantAssertionsSigned="1"|WantAssertionsSigned="true"|g;
    $xml                =~ s|WantAssertionsSigned="0"|WantAssertionsSigned="false"|g;

    $xml                =~ s|AuthnRequestsSigned="1"|AuthnRequestsSigned="true"|g;
    $xml                =~ s|AuthnRequestsSigned="0"|AuthnRequestsSigned="false"|g;

    return $xml;
}


1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 SIGNING_BLOCK

TODO: Fix the POD

=cut

=head2 metadata

TODO: Fix the POD

=cut

=head2 signed_metadata

TODO: Fix the POD

=cut

