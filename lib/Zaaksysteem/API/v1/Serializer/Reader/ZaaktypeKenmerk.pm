package Zaaksysteem::API::v1::Serializer::Reader::ZaaktypeKenmerk;

use Moose;

use Zaaksysteem::Tools;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHODS

=head2 class

=cut

sub class { 'Zaaksysteem::DB::Component::ZaaktypeKenmerken' }

=head2 read

=cut

sig read => 'Zaaksysteem::API::v1::Serializer, Object => HashRef';

sub read {
    my ($class, $serializer, $kenmerk) = @_;

    my $attr = $kenmerk->bibliotheek_kenmerken_id;

    return {
        type => $attr->value_type,
        id => $attr->magic_string,
        label => $kenmerk->label || $attr->naam,
        required => $kenmerk->value_mandatory ? \1 : \0,
        multiple_values => $attr->can_have_multiple_values ? \1 : \0,
        values => $attr->options
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
