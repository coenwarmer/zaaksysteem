package Zaaksysteem::API::v1::Serializer::Reader::SubjectBedrijf;

use Moose;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::File - Read document objects

=head1 DESCRIPTION

=head1 METHODS

=head2 class

Implements interface required by
L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub class { 'Zaaksysteem::DB::Component::Bedrijf' }

=head2 read

Implements interface required by
L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub read {
    my ($class, $serializer, $bedrijf) = @_;

    return {
        type => 'subject',
        # reference => $file->filestore_id->uuid,
        instance => {
            # id => $file->filestore_id->uuid,
            subject_identifier => 'betrokkene-bedrijf-' . $bedrijf->id,
            map ({ $_ => ($bedrijf->$_ || undef) } qw/
                dossiernummer
                handelsnaam
                vestigingsnummer
                rechtsvorm
                vestiging_landcode
                vestiging_adres
                vestiging_adres_buitenland1
                vestiging_adres_buitenland2
                vestiging_adres_buitenland3
                vestiging_straatnaam
                vestiging_huisnummer
                vestiging_huisletter
                vestiging_huisnummertoevoeging
                vestiging_postcode
                vestiging_woonplaats
                correspondentie_landcode
                correspondentie_adres
                correspondentie_adres_buitenland1
                correspondentie_adres_buitenland2
                correspondentie_adres_buitenland3
                correspondentie_straatnaam
                correspondentie_huisnummer
                correspondentie_huisletter
                correspondentie_huisnummertoevoeging
                correspondentie_postcode
                correspondentie_woonplaats
            /)
        }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
