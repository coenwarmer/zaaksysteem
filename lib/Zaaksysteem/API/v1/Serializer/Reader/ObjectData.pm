package Zaaksysteem::API::v1::Serializer::Reader::ObjectData;

use Moose;

use Zaaksysteem::Tools;
use Zaaksysteem::Constants qw[ZAAKSYSTEEM_CONSTANTS];

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::ObjectData - Read ObjectData rows

=head1 SYNOPSIS

    my $reader = Zaaksysteem::API::v1::Serializer::Reader::ObjectData->grok($object);

    my $data = $reader->($serializer, $object);

=head1 DESCRIPTION

This class implements a serializer reader for
L<Zaaksysteem::Backend::Object::Data::Component> objects.

This class should not be used outside of the
L<Zaaksysteem::API::v1::Serializer> infrastructure.

=head1 METHODS

=head2 grok

Implements sub required by L<Zaaksysteem::API::v1::Serializer>.

=cut

sub grok {
    my ($class, $object) = @_;

    return unless blessed $object && $object->isa($class->class);

    if ($object->object_class eq 'case') {
        return sub { $class->read_case(@_) };
    }

    if ($object->object_class eq 'casetype') {
        return sub { $class->read_casetype(@_) };
    }

    return;
}

=head2 class

Implements sub required by L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub class { 'Zaaksysteem::Backend::Object::Data::Component' }

=head2 read_case

Implements sub required by L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sig read_case => 'Zaaksysteem::API::v1::Serializer, Object, ?HashRef => HashRef';

sub read_case {
    my ($class, $serializer, $object, $opts) = @_;

    my %attributes;
    my $documents;
    my $with_documents;

    if($opts && exists $opts->{ fields } && scalar @{ $opts->{ fields } }) {
        my @fields =  map { m/^attribute\.(.*)/ } @{ $opts->{ fields } };

        $with_documents = grep { $_ eq 'case.documents' } @{ $opts->{ fields } };

        # Retrieve attributes the hard way to prevent (expensive) case inflations
        my $attrs = $object->result_source->schema->resultset('ZaaktypeKenmerken')->search(
            {
                zaaktype_node_id => $object->get_object_attribute('case.casetype.node.id')->value,
                publish_public => 1,
                bibliotheek_kenmerken_id => { '!=' => undef },
                'bibliotheek_kenmerken_id.magic_string' => { -in => \@fields }
            },
            {
                prefetch => 'bibliotheek_kenmerken_id'
            }
        );

        for my $attr ($attrs->all) {
            my $magic_string = $attr->bibliotheek_kenmerken_id->magic_string;
            my $value        = $object->get_object_attribute("attribute." . $magic_string)->value;
            if ($attr->bibliotheek_kenmerken_id->value_type eq 'file') {
                $attributes{ $magic_string } = [ (@$value ? [ map({ $_->uuid } @$value) ] : undef ) ];
            } else {
                ### Make sure we get the plain text value for values, to prevent DateTime objects to expose to the outside
                my $plain_value = $class->_process_case_attribute_value($serializer, $value);
                $attributes{ $magic_string } = [ $plain_value ];
            }
        }
    }

    if($with_documents) {
        # Yeah, we *should* just dump a Zaaksysteem::Object::Iterator here,
        # however, that package relies on Backend::Object::Data::ResultSet to
        # do some of it's magic, the File RS is not such an object.
        $documents = {
            type => 'set',
            instance => {
                rows => [ map { $serializer->read($_) } $object->documents->all ]
            }
        };
    }

    my @base_attrs = map { $object->get_object_attribute(sprintf('case.%s', $_)) } qw[
        number
        status
        subject_external
        phase
        result
        date_of_registration
        date_target
    ];

    my %kvp = (
        casetype => {
            type => 'casetype',
            reference => $object->get_column('class_uuid')
        }
    );

    for my $attr (@base_attrs) {
        my (undef, $name) = split m[\.], $attr->name;
        my $value = $attr->value;

        $kvp{ $name } = blessed $value ? $serializer->read($value) : $value;
    }

    return {
        type => 'case',
        reference => $object->id,
        instance => {
            id => $object->id,
            %kvp,

            attributes => \%attributes,
        }
    };
}

sub _process_case_attribute_value {
    my $self        = shift;
    my $serializer  = shift;
    my $value       = shift;

    my $processor   = sub {
        my $val         = shift;

        if (blessed($val)) {
            $val = $serializer->read($val);
        }

        return $val;
    };

    if (ref($value) eq 'ARRAY') {
        return [ map { $processor->($_) } @$value];
    }

    return $processor->($value);
}

sig read_casetype => 'Zaaksysteem::API::v1::Serializer, Object => HashRef';

sub read_casetype {
    my ($class, $serializer, $object) = @_;

    my $zaaktype = $object->get_source_object;
    my $node = $zaaktype->zaaktype_node_id;

    my $statussen = $node->zaaktype_statussen->search(undef, {
        order_by => { -asc => 'status' },
    });

    my @phases = map {
        $serializer->read($_)
    } $statussen->all;

    my @results = map {
        $serializer->read($_)
    } $node->zaaktype_resultaten->all;

    return {
        type => 'casetype',
        reference => $object->id,
        instance => {
            id => $object->id,
            title => $node->titel,
            sources => ZAAKSYSTEEM_CONSTANTS->{ contactkanalen },
            results => \@results,
            phases => \@phases
        }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
