package Zaaksysteem::API::v1::Serializer::Reader::ZaaktypeStatus;

use Moose;
use JSON;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

=head1 SYNOPSIS

=head1 METHODS

=head2 class

=cut

sub class { 'Zaaksysteem::DB::Component::ZaaktypeFase' }

=head2 read

=cut

sub read {
    my ($class, $serializer, $status, $extra_options) = @_;
    $extra_options ||= {};

    my $kenmerken = $status->zaaktype_kenmerkens->search(
        {},
        {
            order_by    => 'me.id',
            prefetch => { bibliotheek_kenmerken_id => 'bibliotheek_kenmerken_values' }
        }
    );

    my @fields = map {
        $serializer->read($_)
    } $kenmerken->all;

    ### First phase locked?
    my $is_locked = 0;
    if ($extra_options->{node}) {
        my $node = $extra_options->{node};

        if ($node->properties->{'lock_registration_phase'} && $status->status == 1) {
            $is_locked = 1;
        }
    }

    return {
        name => $status->fase,
        seq => $status->status,
        id     => $status->id,
        fields => \@fields,
        locked => ($is_locked ? JSON::true : JSON::false),
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
