package Zaaksysteem::API::v1::Serializer::Reader::ZaaktypeStatus;

use Moose;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

=head1 SYNOPSIS

=head1 METHODS

=head2 class

=cut

sub class { 'Zaaksysteem::DB::Component::ZaaktypeFase' }

=head2 read

=cut

sub read {
    my ($class, $serializer, $status) = @_;

    my $kenmerken = $status->zaaktype_kenmerkens->search(
        {
            'me.bibliotheek_kenmerken_id' => { '!=' => undef },
            'me.is_group' => undef,
            'me.object_id' => undef
        },
        {
            prefetch => { bibliotheek_kenmerken_id => 'bibliotheek_kenmerken_values' }
        }
    );

    my @fields = map {
        $serializer->read($_)
    } $kenmerken->all;

    return {
        name => $status->fase,
        seq => $status->status,
        fields => \@fields
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
