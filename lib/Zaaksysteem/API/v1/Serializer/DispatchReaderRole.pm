package Zaaksysteem::API::v1::Serializer::DispatchReaderRole;

use Moose::Role;

use List::Util qw[first];

use Zaaksysteem::Tools;

requires qw[dispatch_map];

sig dispatch_map => '=> %CodeRef';

=head1 NAME

Zaaksysteem::API::v1::Serializer::DispatchReaderRole - Convenience role for
readers that support multiple objecttypes.

=head1 SYNOPSIS

    my Zaaksysteem::API::v1::Serializer::Reader::MyReader;

    use Moose;

    with 'Zaaksysteem::API::v1::Serializer::DispatchReaderRole';

    sub dispatch_map {
        return (
            'Zaaksysteem::Zaken::ComponentZaak' => sub { ... },
            'Zaaksysteem::Zaken::ComponentZaakBetrokkenen' => sub { ... }
        );
    }

=head1 DESCRIPTION

This role assists with building reader classes that handle multiple types.
Usually, this means that the class will handle a whole category of related
types.

This role requires that the method C<dispatch_map> is implemented in consuming
classes.

The mapping returned must be key-value-pairs where the key is a classname
which will be used to test for an is-a relation with the provided object.

The values for the map are expected to be code references that are able to
read the object provided. The return value must be a hashref.

This role should not be used outside of the
L<Zaaksysteem::API::v1::Serializer> infrastructure.

=head1 METHODS

=head2 grok

Implements the interface required by L<Zaaksysteem::API::v1::Serializer>.

=cut

sub grok {
    my ($class, $object) = @_;

    return unless blessed $object;

    my %map = $class->dispatch_map;

    my $key = first {
        $object->isa($_)
    } keys %map;

    return unless defined $key;

    return $map{ $key };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
