package Zaaksysteem::Controller;
use Moose;
use namespace::autoclean;

use Encode;
use Zaaksysteem::Tools;

BEGIN {
    extends 'Catalyst::Controller';
    with 'MooseX::Log::Log4perl';
}

# Stub 'begin' controller we can use to hook into in deriving classes
sub begin : Private {
    my ($self, $c) = @_;

    $c->forward('/page/begin');

    $c->stash->{encode_utf8} = \&Encode::encode_utf8;

    if(exists $c->action->attributes->{ Method }) {
        my ($method) = @{ $c->action->attributes->{ Method } };

        unless($c->req->method eq uc($method)) {
            throw('request/method', sprintf(
                "Invalid request method '%s', action '%s' requires '%s'.",
                $c->req->method,
                $c->action,
                $method,
            ));
        }
    }

    $c->forward('_xsrf_checks');
}

sub _xsrf_checks : Private {
    my ($self, $c) = @_;

    my @actions = $c->action->can('chain') ? @{ $c->action->chain } : ($c->action);

    for my $action (@actions) {
        if(exists $action->attributes->{ JSON }) {
            if ($ENV{ZS_NO_XSRF_TOKEN_CHECK}) {
                $c->log->info('XSRF token validation skipped due to developer settings');
                return 1;
            }

            my $token = $c->req->header('X-XSRF-TOKEN');

            throw('security/xsrf', 'XSRF token validation failure. No token set.') if !defined $token;

            if ($c->session->{_xsrf_token} eq $token) {
                $c->log->info('XSRF token validation successful');
                return 1;
            }
            throw('security/xsrf', 'XSRF token validation failure. Token invalid.');
        }
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 begin

TODO: Fix the POD

=cut

