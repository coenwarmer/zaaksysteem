package Zaaksysteem::DB::Component::Adres;
use Moose;
extends 'Zaaksysteem::Backend::Component';

=head1 NAME

Zaaksysteem::DB::Component::Adres - Database component for "adres" rows

=head1 METHODS

=head2 update

When an address is updates, poke all referring C<natuurlijk_persoon> entries to
update their search_term.

=cut

sub update {
    my $self = shift;

    my $rv = $self->next::method(@_);

    for my $np ($self->natuurlijk_persoons->search) {
        $np->update();
    }

    return $rv;
}

=head2 TO_JSON

Returns all data required to create a JSON representation of this address.

Excludes the linked "natuurlijk_persoon_id", to prevent loops.

=cut

sub TO_JSON {
    my $self = shift;

    # Filter out natuurlijk_persoon, so we don't create an infinite loop
    # (natuurlijk_persoon -> adres -> natuurlijk_persoon -> adres, etc.)
    my @serializable = grep {
        $_ ne 'natuurlijk_persoon_id'
    } $self->result_source->columns;

    return {
        map { $_ => $self->get_column($_) } @serializable
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
