package Zaaksysteem::DB::Component::Logging::Case::Subcase;

use Moose::Role;

has subcase => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    $self->rs('Zaak')->find($self->data->{ subcase_id });
});

sub onderwerp {
    my $self = shift;

    my %mapping = (
        deelzaak => 'Deelzaak',
        vervolgzaak => 'Vervolgzaak',
        vervolgzaak_datum => 'Vervolgzaak',
        gerelateerd => 'Gerelateerde zaak'
    );

    my $type = $mapping{ $self->data->{ type }} // 'Zaak';

    return sprintf(
        "%s (%s) aangemaakt",
        $type,
        $self->subcase->zaaktype_node_id->titel
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 onderwerp

TODO: Fix the POD

=cut

