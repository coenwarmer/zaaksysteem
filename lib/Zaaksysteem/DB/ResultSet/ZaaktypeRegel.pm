package Zaaksysteem::DB::ResultSet::ZaaktypeRegel;


#
# The purpose of this module is to execute user defined rules in the input flow. E.g. when subsidy is
# applied for, the general range of subsidy will prompt different questions to be asked. The rules can
# hide questions, show others, pre-fill values, or stop the process altogether if a dead end has been
# reached.
#


use strict;
use warnings;

use Moose;
use Data::Dumper;
use Data::Serializer;

extends 'DBIx::Class::ResultSet', 'Zaaksysteem::Zaaktypen::BaseResultSet';


use constant    PROFILE => {
    optional        => [qw/
        naam
        settings
    /],
};

sub _validate_session {
    my $self            = shift;
    my $profile         = PROFILE;
    my $rv              = {};

    $self->__validate_session(@_, $profile);
}


sub _commit_session {
    my ($self, $node, $element_session_data) = @_;


    foreach my $regel_id (keys %$element_session_data) {

        my $regel = $element_session_data->{$regel_id};

        ### Make sure regel is active when active rule did not exist in old import file
        $regel->{active} = 1 unless exists $regel->{active};

        delete $regel->{settings}; # settings is a json representation of the whole hash.
        $regel->{settings}= $self->_serializer->serialize($regel);
    }

    $self->next::method( $node, $element_session_data );
}


sub _retrieve_as_session {
    my $self            = shift;

    my $rv              = $self->next::method();

    return $rv unless UNIVERSAL::isa($rv, 'HASH');

    foreach my $index (keys %$rv) {
        my $regel = $rv->{$index};

        eval {
            my $deserialized = $self->_serializer->deserialize($regel->{'settings'});
            foreach my $key (keys %$deserialized) {
                $regel->{$key} = $deserialized->{$key};
            }
        };
        if($@) {
            warn 'Could not deserialize regels: ' . $@;
        }
    }

    return $rv;
}


sub _retrieve {
    my $self            = shift;

    my $rv              = $self->next::method();

    return $rv unless UNIVERSAL::isa($rv, 'HASH');
#    warn "retrived lekker";
    return $rv;
}


sub _serializer {
    return Data::Serializer->new(
        serializer => 'JSON',
    );
}


#
# execute the set of rules. traverse and execute one by one, gather results.
#
sub execute {
    my ($self, $opts) = @_;

    die "need kenmerken"        unless $opts->{kenmerken};
    die "need aanvrager"        unless $opts->{aanvrager};
    die "need contactchannel"   unless $opts->{contactchannel};
    die "need payment_status"   unless exists $opts->{payment_status};
    die "need casetype"         unless $opts->{casetype};

    my $result = {};

    while(my $rule = $self->next) {
        next unless $rule->active;

        $rule->execute({
            kenmerken        => $opts->{kenmerken},
            aanvrager        => $opts->{aanvrager},
            contactchannel   => $opts->{contactchannel},
            payment_status   => $opts->{payment_status},
            casetype         => $opts->{casetype},
            result           => $result,
            confidentiality  => $opts->{confidentiality}
        });
    }

    return $result;
}





1;





__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 execute

TODO: Fix the POD

=cut

