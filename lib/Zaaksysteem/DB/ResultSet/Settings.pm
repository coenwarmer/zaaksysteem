package Zaaksysteem::DB::ResultSet::Settings;

use strict;
use warnings;

use Moose;
use Zaaksysteem::Constants;
use Data::Dumper;
use Data::Serializer;
use Digest::MD5::File qw/-nofatals file_md5_hex/;
use Data::UUID;

extends 'DBIx::Class::ResultSet';


=head2 filter

Get all settings key/value pairs matching the supplied filter. Since settings keys
have a format [group]_[subgroup]_[key] an sensible filter would be "[group]_"

Underscore are used because Template Toolkit will interpret dots like sub-hashkeys.

=cut

sub filter {
    my ($self, $opts) = @_;

    my $filter = $opts->{filter} or die "need filter";

    my $rs = $self->search({
        key => {
            ilike => $filter . '%',
        }
    });

    return [$rs->all];
}


sub store {
    my ($self, $opts) = @_;

    my $key     = $opts->{key}      or die "need key";
    my $value   = $opts->{value};
    die "need value" unless defined $value;


    my $row = $self->find({
        key => $key
    });

    if($row) {
        $row->value($value);
        $row->update;
    } else {
        $self->create({
            key     => $key,
            value   => $value,
        });
    }
}
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 store

TODO: Fix the POD

=cut

