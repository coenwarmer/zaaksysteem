package Zaaksysteem::ProcManager;
use strict;

use base 'FCGI::ProcManager::MaxRequests';

our $VERSION = '0.01';

sub new {
    my $proto = shift;
    my $self = $proto->SUPER::new(@_);
    $self->{max_process_size} = $ENV{MAX_PROCESS_SIZE} || 0 unless defined $self->{max_process_size};
    return $self;
}

sub max_process_size { shift->pm_parameter('max_process_size', @_); }

sub pm_post_dispatch {
    my $self = shift;
    if ($self->max_process_size > 0 && $self->max_process_size < (my $memory = $self->_get_process_memory)) {
        $self->pm_exit("safe exit after maximum memory: " . $memory . ' (max allowed:' . $self->max_process_size . ')');
    }
    $self->SUPER::pm_post_dispatch();
}

sub _get_process_memory {
    my $memory;
    open my $mem, '<', '/proc/' . $$ . '/status';
    while (<$mem>) {
        if ($_ =~ /VmRSS/) {
            ($memory) = $_ =~ /VmRSS:\s+(\d+) /;
        }
    }
    close $mem;

    return $memory;
}

1;

__END__

=head1 NAME

Zaaksysteem::ProcManager - restricts maximum amount of memory for processes

=head1 SYNOPSIS

Usage same as FCGI::ProcManager:

    use CGI::Fast;
    use Zaaksysteem::ProcManager;

    my $m = Zaaksysteem::ProcManager->new({
        n_processes => 10,
        max_requests => 100,
        max_process_size => 500000,   # Maximum size in kB
    });
    $m->manage;

    while( my $cgi = CGI::Fast->new() ) {
        $m->pm_pre_dispatch();
        ...
        $m->pm_post_dispatch();
    }

=head1 DESCRIPTION

Zaaksysteem-ProcManager is an extension of FCGI-ProcManager-MaxRequests that allow
restrict fastcgi processes to process only limiting number of requests AND limiting
number of memory usage. This may help avoid growing memory usage and compensate
memory leaks.

This module subclass L<FCGI-ProcManager-MaxRequests>. After server process memory (RSS)
exceeds the max_memory_size, it simple exits, and manager starts another server process.
Maximum number of memory size can be set from MAX_PROCESS_SIZE environment variable.

=head1 OVERLOADED METHODS

=head2 new

    my $pm = Zaaksysteem::ProcManager->new(\%args);

Constructs new proc manager object.

=head2 max_process_size

    $pm->max_process_size($max_process_size);
    my $max_process_size = $pm->max_process_size;

Set/get current max_process_size value (in kB)

=head2 handling_init

Initialize requests counter after new worker process forks.

=head2 pm_post_dispatch

Do all work. Decrements requests counter after each request and exit worker when needed.

=head1 USING WITH CATALYST

At this time, L<Catalyst::Engine::FastCGI> do not allow set any args to FCGI::ProcManager subclass constructor.
Because of this we should use environment MAX_PROCESS_SIZE ;-)

    ## 500 MB
    MAX_PROCESS_SIZE=500000 ./script/myapp_fastcgi.pl -n 10 -l <host>:<port> -d -M Zaaksysteem::ProcManager

=head1 SEE ALSO

L<FCGI::ProcManager::MaxRequests>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut