package Zaaksysteem::Schema;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_classes;


# Created by DBIx::Class::Schema::Loader v0.04006 @ 2013-01-10 07:42:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:R4RHNOmRFCrCmyTFSe08tA

use Moose;
use Zaaksysteem::Tools;
use File::Spec::Functions qw(catdir);

__PACKAGE__->exception_action(sub {
    my $err = shift;

    if(blessed $err) {
        $err->throw if $err->can('throw');
        throw('db', $err->as_string, $err) if $err->can('as_string');
        throw('db', '' . $err, $err);
    }
    throw('db', $err);
});

use constant CLEAR_PER_REQUEST => [qw/
    users
    current_user
    cache
    betrokkene_pager
    catalyst_config
    customer_instance
    current_user_ou_ancestry
    customer_config
    output_path
    storage_path
    tmp_path
/];

sub _clear_schema {
    my $self            = shift;

    for my $attr (@{ CLEAR_PER_REQUEST() }) {
        $self->$attr(undef);
    }
}

sub betrokkene_model {
    my $self            = shift;

    my $dbic            = $self->clone;

    my $stash           = {};

    if ($self->betrokkene_pager) {
        for my $key (keys %{ $self->betrokkene_pager }) {
            $stash->{ $key } = $self->betrokkene_pager->{ $key };
        }
    }

    $dbic->cache($self->cache);

    return Zaaksysteem::Betrokkene->new(
        dbic            => $dbic,
        stash           => $stash,
        config          => $self->catalyst_config,
        customer        => $self->customer_instance,
    );
}

has 'current_user_ou_ancestry'  => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self                = shift;

        return [ map {
            {
                ou_id => $_->id,
                name  => $_->name,
            }
        } @{ $self->current_user->inherited_groups } ];
    }
);

has [qw/users current_user/] => (
    'is'        => 'rw',
    'weak_ref'  => 1,
);


has customer_config => (
    is       => 'rw',
    lazy     => 1,
    isa      => 'Maybe[HashRef]',
    weak_ref => 1,
    default  => sub {
        my $self = shift;
        if ($self->customer_instance && $self->customer_instance->{start_config}) {
            return $self->customer_instance->{start_config};
        }
        return {};
    },
);

has output_path => (
    is      => 'rw',
    isa     => 'Maybe[Str]',
    lazy    => 1,
    default => sub {
        my $self = shift;
        if ($self->customer_config && $self->customer_config->{basedir}) {
            return $self->customer_config->{basedir};
        }
        return '/var/tmp/zs';
    },
);

has storage_path => (
    is      => 'rw',
    isa     => 'Maybe[Str]',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return catdir($self->output_path, 'storage');
    },
);


has tmp_path => (
    is      => 'rw',
    isa     => 'Maybe[Str]',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return catdir($self->output_path, 'tmp');
    },
);

has [qw/cache/] => (
    'is'        => 'rw',
);

has 'log'  => (
    'is'        => 'rw',
    'lazy'      => 1,
    'weak_ref'  => 1,
    'default'   => sub { return Catalyst::Log->new }
);

has [qw/betrokkene_pager catalyst_config customer_instance/]  => (
    'is'        => 'rw',
    'lazy'      => 1,
    'weak_ref'  => 1,
    'default'   => sub { return {} }
);

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 CLEAR_PER_REQUEST

TODO: Fix the POD

=cut

=head2 betrokkene_model

TODO: Fix the POD

=cut

