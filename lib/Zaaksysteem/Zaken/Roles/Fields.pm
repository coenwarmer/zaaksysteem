package Zaaksysteem::Zaken::Roles::Fields;

use Moose::Role;
use Data::Dumper;

sub visible_fields {
    my ($self, $args) = @_;

    my $field_values    = $args->{field_values}     or die "need field_values";
    # my $rules_result    = $args->{rules_result};    # optional;

    my $fase            = $args->{phase};           # required
    my $mandatory       = $args->{mandatory};       # optional
    my $include_docs    = $args->{include_documents};

    my $status_number   = $fase ? $fase->status : $self->milestone;

    ### Todo check change in implementation, we NEED the status
    my $rules           = $self->rules(
        {
            'case.number_status' => $status_number
        }
    );
    my $rules_profile   = $rules->validate_from_case($self, { 'case.number_status' => $status_number });
    my $mapping         = { reverse %{ $rules->_attribute_mapping } };

    my $fields = {};
    for my $attribute (@{ $rules_profile->active_attributes }) {
        my $raw_attribute   = $attribute;
        $raw_attribute      =~ s/^attribute\.//;
        my $bibid           = $mapping->{$raw_attribute};

        my ($zt_kenmerk) = grep (
            {
                $_->bibliotheek_kenmerken_id &&
                $_->bibliotheek_kenmerken_id->id eq $bibid &&
                $_->zaak_status_id->status == $status_number
            } @{ $rules->old_case_attributes }
        );

        next unless $zt_kenmerk;

        ### When NO option include_docs is given, skip files.
        next if (
            !$include_docs &&
            $zt_kenmerk->bibliotheek_kenmerken_id->value_type eq 'file'
        );

        ### When option mandatory is given, only show mandatory fields
        next if ($mandatory && !$zt_kenmerk->value_mandatory);

        $fields->{ $bibid } = 1;
    }

    return $fields;
}

=head2 empty_hidden_attributes

Arguments: $PHASE_OBJECT

    $ok = $case->empty_hidden_attributes($case->zaaktype_statussen->search({status => 1})->first);

Empty invisible fields

=cut

sub empty_hidden_attributes {
    my $self            = shift;
    my $phase           = shift;

    my $rules           = $self->rules(
        {
            'case.number_status' => $phase->status,
            'reload'             => 1,
        }
    );

    my $val_profile     = $rules->validate_from_case($self, { 'case.number_status' => $phase->status });
    my $mapping         = $rules->_attribute_mapping;

    my @empty_fields;
    for my $kenmerk (@{ $rules->old_case_attributes }) {
        my $attribute_name;

        ### Only look at non-systeemkenmerken with a bibliotheek_kenmerken_id
        next unless (
            $kenmerk->bibliotheek_kenmerken_id &&
            $kenmerk->zaak_status_id->status == $phase->status &&
            !$kenmerk->is_systeemkenmerk &&
            $kenmerk->bibliotheek_kenmerken_id->value_type ne 'file'
        );

        next unless $kenmerk->zaak_status_id->id eq $phase->id;

        next unless ($attribute_name = $mapping->{$kenmerk->bibliotheek_kenmerken_id->id});

        ### The problem here is, a hidden field in one phase could be visible in the other. Because
        ### of the hack: show the same attributes in different phases. To prevent this,
        ### check if our visible_fields check tells us the field is visible.
        my $visible_fields = $rules->_get_list_of_visible_attributes_for_number_status($phase->status);

        next if grep { 'attribute.' . $mapping->{$kenmerk->bibliotheek_kenmerken_id->id} eq $_ && $kenmerk->is_systeemkenmerk} @$visible_fields;

        $attribute_name     = 'attribute.' . $attribute_name;

        if (!grep({ $attribute_name eq $_ } @{ $val_profile->active_attributes })) {
            push(@empty_fields, $kenmerk->bibliotheek_kenmerken_id->id);
        }
    }

    # print STDERR 'Deleting fields: ' . join(',', @empty_fields) . "\n";

    ### Delete attributes
    if (@empty_fields) {
        $self->zaak_kenmerken->delete_fields({
            bibliotheek_kenmerken_ids => \@empty_fields,
            zaak_id => $self->id
        });

        ### Remove wijzigingsverzoeken on these fields, because these are not longer visible
        my $change_requests = $self->result_source->schema->resultset('ScheduledJobs')->search_update_field_tasks(
            {
                case_id     => $self->id,
                kenmerken   => \@empty_fields,
            }
        );

        if ($change_requests->count) {
            $change_requests->update({deleted => DateTime->now()});
        }
    }

    return 1;
}


=head2 phase_fields_complete

Checks if all required fields for this phase are filled,
taking into account the fields that are hidden by rules.

=cut

sub phase_fields_complete {
    my ($self, $args) = @_;

    my $fase = $args->{phase} or die "need phase";

    my $given_kenmerken = $args->{custom_fields}; # optional

    $given_kenmerken ||= $self->field_values({ fase => $fase->status });

    my $kenmerken  = { %{ $given_kenmerken } };

    my $rules_result;
    my $required_fields = $self->visible_fields({
        phase           => $fase,
        mandatory       => 1,
        field_values    => $given_kenmerken,
        result          => $rules_result,
    });

    #$kenmerken  = { %{ $given_kenmerken } };

    ### PAUZE?

    if ($rules_result->{pauzeer_aanvraag}) {
        my $key = [ keys %{ $rules_result->{pauzeer_aanvraag} } ]->[0];
        return {
            'succes'    => 0,
            'pauze'     => $rules_result->{pauzeer_aanvraag}->{$key},
        }
    }

    foreach my $bibliotheek_kenmerken_id (keys %$required_fields) {
        my $value = $kenmerken->{$bibliotheek_kenmerken_id};
        $value = ref $value && ref $value eq 'ARRAY' ? join "", @$value : $value;

        next if length($value);

        return {
            'succes'    => 0,
            'required'  => 1,
        };
    }

    return {
        'succes'    => 1
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 visible_fields

TODO: Fix the POD

=cut

