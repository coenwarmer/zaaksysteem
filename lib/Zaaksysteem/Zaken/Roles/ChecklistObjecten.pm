package Zaaksysteem::Zaken::Roles::ChecklistObjecten;

use Moose::Role;
use Zaaksysteem::Exception;

=head1 NAME

Zaaksysteem::Zaken::Roles::ChecklistObjecten - Graft checklist awareness on
L<case|Zaaksysteem::Zaken::ComponentZaak> objects

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHODS

=head2 is_checklist_compleet

This method tests if all checklist items for the current milestone are checked

=cut

sub is_checklist_compleet {
    my $self    = shift;

    my $checklist = $self->checklists->search({
        case_milestone => $self->milestone + 1
    })->first;

    if($checklist) {
        for my $item ($checklist->checklist_items) {
            return 0 unless $item->state;
        }
    }

    return 1;
}

=head2 can_volgende_fase

Wrapper for L<Zaaksysteem::Zaken::ComponentZaak/can_volgende_fase>.

=cut

around can_volgende_fase => sub {
    my $orig    = shift;
    my $self    = shift;

    my $advance_result = $self->$orig(@_);

    if($self->is_checklist_compleet) {
        $advance_result->ok('checklist_complete');
    } else {
        $advance_result->fail('checklist_complete');
    }

    return $advance_result;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
