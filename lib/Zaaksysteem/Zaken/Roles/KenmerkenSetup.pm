package Zaaksysteem::Zaken::Roles::KenmerkenSetup;

use Moose::Role;
use Data::Dumper;

use Zaaksysteem::Exception;

around '_create_zaak' => sub {
    my $orig                = shift;
    my $self                = shift;
    my ($opts)              = @_;
    my ($zaak_kenmerken);

    $self->log->info('Role [KenmerkenSetup]: started');

    my $zaak = $self->$orig(@_);

    ### Mangle kenmerken with defaults
    my $mangled_properties  = $zaak
                            ->zaaktype_node_id
                            ->zaaktype_kenmerken
                            ->mangle_defaults(
                                $opts->{kenmerken} || []
                            );


    # mangle_defaults may return a hashref
    my $ref = ref $mangled_properties;
    if ($ref eq 'ARRAY' && @$mangled_properties) {
        $zaak->zaak_kenmerken->create_kenmerken({
            zaak_id     => $zaak->id,
            kenmerken   => $mangled_properties
        });
    }
    elsif ($ref eq 'HASH') {
        throw('KenmerkenSetup', "Mangled options are a HASHREF, Unable to do something");
    }

    ### Recache field values
    delete($zaak->{cached_field_values});
    $zaak->touch();

    $self->log->debug('Kenmerken toegevoegd');

    return $zaak;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
