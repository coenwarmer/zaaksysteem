package Zaaksysteem::Backend::Object::Data::ResultSet;
use Moose;

use Zaaksysteem::Tools;

extends 'Zaaksysteem::Backend::ResultSet';

with qw/
    Zaaksysteem::Backend::Object::Roles::ObjectResultSet
    Zaaksysteem::Search::HStoreResultSet
    Zaaksysteem::Search::TSVectorResultSet
    Zaaksysteem::Search::Acl
/;

=head1 NAME

Zaaksysteem::Backend::Object::Data::ResultSet - Returns a ResultSet according to the object principals

=head1 SYNOPSIS

    ### ZQL: Retrieve all objects of type case
    my $resultset   = $object->from_zql('select case.id, casetype.title from case');


    ### List of attributes requested
    print join(' , ', @{ $resultset->object_requested_attributes });

    # prints:
    # case.id , casetype.title

    ### Loop over rows
    while (my $row = $resultset->next) {

        ## Prints uuid of object
        print $row->id

        ## prints JSON representation of row
        print Data::Dumper::Dumper( $row->TO_JSON )
    }

=head1 DESCRIPTION

ResultSet returned when called from L<Zaaksysteem::Object::Model>. Every row retrieved from
this resultset, will be blessed with L<Zaaksysteem::Backend::Object::Data::Component>.

=cut

=head1 METHODS

=head2 hstore_column

Implements interface required by the
L<Zaaksysteem::Search::HStoreResultSet> role.

Returns the string C<index_hstore>.

=cut

sub hstore_column { 'index_hstore' }

=head2 text_vector_column

Implements interface required by the the
L<Zaaksysteem::Search::TSVectorResultSet> role.

Returns the string C<text_vector>.

=cut

sub text_vector_column { 'text_vector' }

=head2 find_or_create_by_object_id($object_type, $object_id)

Return value: $row

    resultset('ObjectData')->find_or_create_by_object_id(
        'case', $self->id
    );

=cut

sub find_or_create_by_object_id {
    my $self                        = shift;
    my ($object_class, $object_id)  = @_;

    throw(
        'object/data/resultset/find_or_create_by_object_id/invalid_params',
        'Need at least object_class and object_id'
    ) unless ($object_class && $object_id && $object_id =~ /^\d+$/);

    my $row                         = $self->find(
        {
            object_class        => $object_class,
            object_id           => $object_id
        }
    );

    return $row if $row;

    return $self->create(
        {
            object_class        => $object_class,
            object_id           => $object_id
        }
    );
}

=head2 _construct_object

Overrides L<DBIx::Class::ResultSet>'s C<_construct_object> method to load
the relevant ACL rows per result.

B<NOTE>: C<_construct_object> is a deprecated method of DBIx::Class::ResultSet
and has been replaced in newer versions. This code will break when we upgrade
(to DBIx-Class-0.082820).

=cut

# TODO: refactor usage of _construct_object

sub _construct_object {
    my $self    = shift;

    my ($row, @more) = $self->next::method(@_);

    # Skip loading of ACLs when we cannot possibly check the permissions.
    if ($self->{attrs}{zaaksysteem}{user}) {
        $self->_load_acl_capabilities_on_row($row);
    }

    return ($row, @more);
}


1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
