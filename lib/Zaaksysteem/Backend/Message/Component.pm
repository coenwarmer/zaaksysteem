package Zaaksysteem::Backend::Message::Component;

use strict;
use warnings;

use Zaaksysteem::Exception;
use Zaaksysteem::Profile;

sub TO_JSON {
    my $self = shift;

    my %options;
    $options{ $_ }          = $self->get_column($_) for qw/id message subject_id is_read/;

    $options{logging_id}    = $self->logging->TO_JSON;

    # Tad ugly to do this here, but the alternative is putting this very specific code
    # in the TO_JSON for Logging. (Which is probably worse.)
    if ($self->logging->get_column('zaak_id')) {
        $options{aanvrager}  = ($self->has_column_loaded('aanvrager_name') ? $self->get_column('aanvrager_name') : $self->logging->zaak_id->aanvrager_object->display_name);
        $options{case_type}  = ($self->has_column_loaded('case_type_title') ? $self->get_column('case_type_title') : $self->logging->zaak_id->zaaktype_node->titel);
        $options{created_by} = $self->logging->get_column('created_by_name_cache'),
    }

    return {
        %options,
    }
}

=head2 update_properties

Updates a message with given properties.

=head3 Arguments

=over

=item subject_id [required]

=item is_read [optional]

=back

=head3 Returns

An updated Sysin::Message object.

=cut

Params::Profile->register_profile(
    method  => 'update_properties',
    profile => {
        required => [qw/
            subject_id
        /],
        optional => [qw/
            is_read
        /]
    }
);

sub update_properties {
    my $self = shift;
    my $opts = assert_profile($_[0])->valid;

    # Make sure only the owner of the case can make changes to this message
    if ($opts->{subject_id} ne $self->subject_id) {
        return;
    }
    return $self->update($opts);
}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_JSON

TODO: Fix the POD

=cut

