package Zaaksysteem::Backend::Betrokkene::NatuurlijkPersoon::ResultSet;

use Moose;
use Zaaksysteem::Tools;

use Zaaksysteem::Profiles qw/
    PROFILE_NATUURLIJK_PERSOON
    AUTHENTICATED_PARAM

    PROFILE_NATUURLIJK_PERSOON_ADDRESS_PARAMS
/;

extends qw/Zaaksysteem::Backend::Betrokkene::NatuurlijkPersoon::GenericResultSet/;

with qw/MooseX::Log::Log4perl Zaaksysteem::BR::Subject::ResultSet::Person/;


=head1 NAME

Zaaksysteem::Backend::Betrokkene::NatuurlijkPersoon::ResultSet - Natuurlijk Persoon ResultSet

=head1 SYNOPSIS

    ### Within a different module
    my $instance    = $np->create_natuurlijk_persoon(
        {

        }
    )

=head1 DESCRIPTION

This object handles the searching and creation of NatuurlijkPersonen in zaaksysteem (Gegevensmagazijn Side).
This means that this is the place to insert a natuurlijk persoon.

=head1 METHODS

=head2 create_natuurlijk_persoon

Arguments: \%PARAMS [, \%OPTIONS]

Return value: $ROW_NATUURLIJK_PERSOON

TODO:
authenticated
authenticated_by

voorletters: from 10 to 20

=cut

define_profile 'create_natuurlijk_persoon' => (
    %{ PROFILE_NATUURLIJK_PERSOON() }
);

sub create_natuurlijk_persoon {
    my $self            = shift;
    my $params          = assert_profile(shift || {})->valid;
    my $options         = shift || {};

    ### Remove the damn voorloopnul
    $params->{burgerservicenummer} = int($params->{burgerservicenummer});

    my %db_params;
    if ($options->{authenticated}) {
        throw(
            'betrokkene/natuurlijkpersoon/create_natuurlijk_persoon/invalid_authenticed_param',
            'Authenticated must match regex'
        ) unless $options->{authenticated} =~ AUTHENTICATED_PARAM;

        $db_params{authenticatedby} = $options->{authenticated};
        $db_params{authenticated}   = 1;
    }

    my %address_params;
    for my $param (keys %{ $params }) {
        if (grep { $param eq $_ } @{ PROFILE_NATUURLIJK_PERSOON_ADDRESS_PARAMS() }) {
            $address_params{$param} = $params->{$param};
        } else {
            $db_params{$param}      = $params->{$param};
        }
    }

    # If we already have a natuurlijk persoon, try to update that
    # person.
    my $np = $self->find_by_bsn($params->{burgerservicenummer});
    if ($np) {
        my $address = $np->adres_id;

        foreach (@{$address->get_columns}) {
            $address_params{$_} //= '';
        }
        $address->update(\%address_params);

        foreach (@{$self->get_columns}) {
            $db_params{$_} //= '';
        }
        $np->update(\%db_params);
    }
    else {
        $self->result_source->schema->txn_do(sub {
            my $address = $self->result_source->schema->resultset('Adres')
                ->create(\%address_params);

            $np = $self->create({ %db_params, adres_id => $address, });

            $address->natuurlijk_persoon_id($np->id);
            $address->update;
        });
    }
    return $np;

}

sub _assert_non_existent_bsn {
    my ($self, $bsn) = @_;

    my $np = $self->find_by_bsn($bsn);
    if ($np) {
        throw("natuurlijk_persoon/exists",
            "Natuurlijk Persoon with BSN $bsn found in Zaaksysteem",
        );
    }
    return 1;
}

=head2 get_by_bsn

Get an active natuurlijk persoon based on the BSN.

First checks for an authenticated contact, if that's not found, the search is
repeated for an unauthenticated contact.

Dies if nothing can be found.

=cut

sub get_by_bsn {
    my $self = shift;
    my ($bsn) = @_;

    my $np = $self->find_by_bsn($bsn);
    if (!$np) {
        $np = $self->find_by_bsn(
            $bsn,
            { authenticated => [ 0, undef ] }
        );
    }

    return $np if $np;
    throw("natuurlijk_persoon/BSN/not_found", "Unable to find Natuurlijk Persoon with BSN: $_[0]");
}

=head2 find_by_bsn

Tries to find an active natuurlijk persoon based on the BSN. By default, only
authenticated contacts are returned.

Contrary to L<get_by_bsn> this function returns undef if nothing can be found.

=cut

sub find_by_bsn {
    my ($self, $bsn, $options) = @_;

    my $rs = $self->search(
        {
            'NULLIF(me.burgerservicenummer,\'\')::integer' => int($bsn),
                deleted_on                                 => undef,
                authenticated                              => 1,
                %{ $options // {} },
        },
    );

    my @entries = $rs->all;
    my $c       = @entries;

    if ($c > 1) {
        throw(
            "natuurlijk_persoon/duplicate/bsn",
            sprintf("Multiple entries (%d) found for BSN %s", $c, $bsn)
        );
    }

    return $entries[0];
}

=head2 get_active_authenticated_entry

Arguments: \%params

=cut

sub get_active_authenticated_entry {
    my $self            = shift;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
