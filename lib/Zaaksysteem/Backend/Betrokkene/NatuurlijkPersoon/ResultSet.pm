package Zaaksysteem::Backend::Betrokkene::NatuurlijkPersoon::ResultSet;

use Moose;
use Zaaksysteem::Tools;

use Zaaksysteem::Profiles qw/
    PROFILE_NATUURLIJK_PERSOON
    AUTHENTICATED_PARAM

    PROFILE_NATUURLIJK_PERSOON_ADDRESS_PARAMS
/;

extends qw/Zaaksysteem::Backend::Betrokkene::NatuurlijkPersoon::GenericResultSet/;


=head1 NAME

Zaaksysteem::Backend::Betrokkene::NatuurlijkPersoon::ResultSet - Natuurlijk Persoon ResultSet

=head1 SYNOPSIS

    ### Within a different module
    my $instance    = $np->create_natuurlijk_persoon(
        {

        }
    )

=head1 DESCRIPTION

This object handles the searching and creation of NatuurlijkPersonen in zaaksysteem (Gegevensmagazijn Side).
This means that this is the place to insert a natuurlijk persoon.

=head1 METHODS

=head2 create_natuurlijk_persoon

Arguments: \%PARAMS [, \%OPTIONS]

Return value: $ROW_NATUURLIJK_PERSOON

TODO:
authenticated
authenticated_by

voorletters: from 10 to 20

=cut

define_profile 'create_natuurlijk_persoon' => (
    %{ PROFILE_NATUURLIJK_PERSOON() }
);

sub create_natuurlijk_persoon {
    my $self            = shift;
    my $params          = assert_profile(shift || {})->valid;
    my $options         = shift || {};

    ### Remove the damn voorloopnul
    $params->{burgerservicenummer} = int($params->{burgerservicenummer});

    my %db_params;
    if ($options->{authenticated}) {
        throw(
            'betrokkene/natuurlijkpersoon/create_natuurlijk_persoon/invalid_authenticed_param',
            'Authenticated must match regex'
        ) unless $options->{authenticated} =~ AUTHENTICATED_PARAM;

        $db_params{authenticatedby} = $options->{authenticated};
        $db_params{authenticated}   = 1;
    }

    my %address_params;
    for my $param (keys %{ $params }) {
        if (grep { $param eq $_ } @{ PROFILE_NATUURLIJK_PERSOON_ADDRESS_PARAMS() }) {
            $address_params{$param} = $params->{$param};
        } else {
            $db_params{$param}      = $params->{$param};
        }
    }

    ### Todo, prevent duplicate entries
    $self->_assert_duplicate_entries($params->{burgerservicenummer});

    ### Actual creation
    my $entry;
    $self->result_source->schema->txn_do(sub {
        my $address     = $self->result_source->schema->resultset('Adres')->create(
            \%address_params
        );

        $entry       = $self->create(
            {
                %db_params,
                adres_id => $address->id
            }
        );

        $address->natuurlijk_persoon_id($entry->id);
        $address->update;
    });

    return $entry;
}

sub _assert_duplicate_entries {
    my ($self, $bsn) = @_;
    my $entry = $self->search(
        {
            'NULLIF(me.burgerservicenummer,\'\')::integer' => int($bsn),
            authenticated => 1,
            deleted_on    => undef,
        }
    )->first;

    if ($entry) {
        throw("ZS/NP/duplicate", "Natuurlijk Persoon already found in Zaaksysteem");
    }
    return 1;
}

=head2 get_active_authenticated_entry

Arguments: \%params

=cut

sub get_active_authenticated_entry {
    my $self            = shift;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
