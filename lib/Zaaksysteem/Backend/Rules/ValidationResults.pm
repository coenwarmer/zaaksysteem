package Zaaksysteem::Backend::Rules::ValidationResults;

use Moose;
use Zaaksysteem::Tools;
use Zaaksysteem::Backend::Rules::Integrity;
use JSON::Path;


=head1 NAME

Zaaksysteem::Backend::Rules::ValidationResults - Validation results for given rules

=head1 SYNOPSIS

=head1 ATTRIBUTES

=head2 actions

List of L<Zaaksysteem::Backend::Rules::Rule::Action> active for the given parameters. Loop of the actions to implement the changes

=cut

has 'actions'       => (
    is      => 'rw',
    isa     => 'Maybe[ArrayRef[Zaaksysteem::Backend::Rules::Rule::Action]]',
    default => sub { []; }
);

=head2 active_attributes

Convenient method to find the active attributes (or visible_fields)

=cut

has 'active_attributes'     => (
    is      => 'rw',
    isa     => 'ArrayRef',
    default => sub { []; }
);

=head2 active_groups



=cut

has 'active_groups'     => (
    is      => 'rw',
    isa     => 'ArrayRef',
    default => sub { []; }
);

=head2 is_paused

=cut

has 'is_paused'     => (
    is      => 'rw',
    isa     => 'Boolean',
);

=head2 changes

=cut

has 'changes'       => (
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { {}; },
);

has '_hidden_attributes_metadata' => (
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { {}; },
);

=head1 INTERNAL METHODS

=cut

sub _remove_active_attributes {
    my $self        = shift;
    my @attributes  = @_;

    my @active_attributes = @{ $self->active_attributes };

    for my $attribute (@attributes) {
        @active_attributes = grep { $_ ne $attribute } @active_attributes;
    }

    return $self->active_attributes(\@active_attributes);
}

sub _add_active_attributes {
    my $self        = shift;
    my @attributes  = @_;

    my @active_attributes = @{ $self->active_attributes };

    ### Prevent duplicates
    for my $attribute (@attributes) {
        @active_attributes = grep { $_ ne $attribute } @active_attributes;
    }

    push(@active_attributes, @attributes);
    return $self->active_attributes(\@active_attributes);
}


=head2 return_active_data

=cut

sub return_active_data {
    my $self            = shift;
    my %params          = %{ shift() };
    my $mapping         = shift;

    for my $key (keys %params) {
        my $attr = $mapping->{ $key };

        next if grep { 'attribute.' . $attr eq $_ } @{ $self->active_attributes };

        delete $params{$key};
    }

    return \%params;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
