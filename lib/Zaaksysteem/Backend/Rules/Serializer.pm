package Zaaksysteem::Backend::Rules::Serializer;
use Moose;

=head1 NAME

Zaaksysteem::Backend::Rules::Serializer - A tool to encode/decode the rules.

=head1 DESCRIPTION

The old rules are stored in a non-readable serialized manner which is hard to debug and change.
This module tries to make the transition to actually store the data in the DB as JSON like we do with other bits.

=head1 SYNOPSIS

    use Zaaksysteem::Backend::Rules::Serializer;
    my $s = Zaaksysteem::Backend::Rules::Serializer->new();

    my $hash_ref = $s->decode($data_from_db);
    my $json     = $s->encode($hash_ref);

    my $rule     = $s->decode_to_rule($data_from_db);

=cut

use JSON::XS;
use Data::Serializer;
use Zaaksysteem::Tools;

with 'MooseX::Log::Log4perl';

has json => (
    is => 'ro',
    isa => 'JSON::XS',
    default => sub {
        return JSON::XS->new->convert_blessed(1);
    },
    lazy => 1,
);

has serializer => (
    is => 'ro',
    isa => 'Data::Serializer',
    default => sub {
        return Data::Serializer->new(serializer => 'JSON');
    },
    lazy => 1,
);

=head1 METHODS

=head2 decode

Decode the rule to hash object

=cut

sub decode {
    my ($self, $data) = @_;

    if (!defined $data) {
        $self->log->warn("Decoded rule has no contents");
        return undef;
    }

    if ($data =~ /^\Q^JSON|||hex|\E/) {
        return $self->serializer->deserialize($data);
    }
    else {
        my $e = try {
            return $self->json->utf8(0)->decode($data);
        }
        catch {
            $self->log->warn($_);
        };
        return $e;
    }
}

=head2 decode_to_rule

Decode the data to rule

=cut

sub decode_to_rule {
    my $self = shift;
    my $decoded = $self->decode(@_);
    if (!defined $decoded) {
        return $decoded;
    }
    return { map { $_ => $decoded->{$_} } keys %$decoded };
}

=head2 encode

Encode the data to a JSON string

=cut

sub encode {
    my ($self, $data) = @_;
    $data //= {};
    return $self->json->utf8(1)->encode($data);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
