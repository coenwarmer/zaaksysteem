package Zaaksysteem::Backend::Rules::Rule::Condition::Properties;

use Moose::Role;
use Zaaksysteem::Tools;
use Zaaksysteem::Constants qw/ZAAKSYSTEEM_CONSTANTS/;
use Zaaksysteem::Attributes;

=head1 NAME

Zaaksysteem::Backend::Rules::Rule::Condition::Properties - Handles customer properties of casetype

=head1 SYNOPSIS

See L<Zaaksysteem::Backend::Rules::Rule::Condition>

=head1 DESCRIPTION

Condition role in charge of handling conditions based on current zaaktype values. Like the values
for wkpb and lex_silencio_positivo.

Make sure you can define an action based on this criteria.

=head1 ATTRIBUTES

=head2 casetype_properties

isa: HashRef

    {
        lex_silencio_positivo => 'Nee',
        wkpb => 'Nee',
        [...]
    }

Hashref of casetype properties defined in L<Zaaksysteem::DB::ZaaktypeNode>.

=cut

has 'casetype_properties'   => (
    is      => 'rw',
    isa     => 'HashRef',
);

=head1 CONSTRUCTION

On construction, by adding functionality to C<BUILD>, we translate the old style attributes
to the new style. E.g. lex_silencio_positivo to case.casetype.lex_silencio_positivo.

=cut

after 'BUILD' => sub {
    my $self        = shift;

    my $properties  = ZAAKSYSTEEM_SYSTEM_ATTRIBUTES;

    if ($self->attribute ~~ ZAAKSYSTEEM_CONSTANTS->{CASETYPE_RULE_PROPERTIES}) {
        my ($attr)  = grep { $_->{bwcompat_name} && $_->{bwcompat_name} eq $self->attribute } ZAAKSYSTEEM_SYSTEM_ATTRIBUTES();

        if ($self->attribute eq 'beroep_mogelijk') {
            $self->attribute('case.casetype.objection_and_appeal');
        }

        print STDERR 'Rule::Condition::Properties: did not find system attr for: ' . $self->attribute unless $attr;

        if ($attr) {
            # $self->values($self->casetype_properties ? [$self->casetype_properties->{ $self->attribute }] : []);
            $self->attribute($attr->{name});
        }
    }
};

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules::Rule::Condition> L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Install>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAAKSYSTEEM_CONSTANTS

TODO: Fix the POD

=cut

