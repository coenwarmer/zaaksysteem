package Zaaksysteem::Backend::Rules::Rule::Action::Pause;

use Moose;
use Zaaksysteem::Tools;

with 'Zaaksysteem::Backend::Rules::Rule::Action';

use Zaaksysteem::Types qw(UUID);

=head1 NAME

Z::B::Rules::Rule::Action::Pause - This rule will pause an application.

=head1 SYNOPSIS

=head1 DESCRIPTION

This specific action manage pause actions

=head1 ATTRIBUTES

=head2 copy_attributes

=cut

has 'copy_attributes'    => (
    is      => 'rw',
    isa     => 'Bool',
);

=head2 attribute_name

Because the Pause Action does not work with attribute_names this is override to accept any and isn't required as per
L<Zaaksysteem::Backend::Rules::Rule::Action>.

=cut

has '+attribute_name' => ( isa => 'Any', required => 0 );

=head2 message

=cut

has 'message' => (
    is        => 'rw',
    isa       => 'Str',
    default   => sub { return "De aanvraag is gepauzeerd. Er is geen reden opgegeven." },
);

=head2 want_start_case

Whether the "startzaak" checkbox for this rule action is checked or not.

=cut

has 'want_start_case' => (
    is       => 'rw',
    isa      => 'Bool',
    required => 0,
    default  => 0,
);

=head2 start_case

=cut

has 'start_case'             => (
    is          => 'rw',
    isa         => 'Maybe[HashRef]',
    predicate   => 'has_start_case',
    lazy        => 1,
    default     => sub {
        my $self        = shift;

        return unless $self->want_start_case;

        unless ($self->casetype_id) {
            warn('Need at least a zaaktype to get start_case');
            return;
        }

        return {
            prefill     => {
                aanvrager_type  => $self->requestor_type,
                zaaktype        => {
                    titel           => $self->casetype_title,
                    zaaktype_id     => $self->casetype_id,
                    zaaktype_uuid   => $self->casetype_uuid,
                }
            }
        };
    }
);

has 'casetype_id'       => (
    'is'        => 'rw',
    'isa'       => 'Int',
);

has 'casetype_uuid'     => (
    'is'        => 'rw',
    'isa'       => UUID,
);

has 'casetype_title'    => (
    'is'        => 'rw',
    'isa'       => 'Str',
);

has 'requestor_type'       => (
    'is'        => 'rw',
    'isa'       => 'Str',
);


=head2 _data_attributes

isa: Array

List of attributes to show in data

=cut

has '_data_attributes' => (
    is      => 'rw',
    isa     => 'ArrayRef',
    default => sub {
        my $self            = shift;

        my @keys            = qw/copy_attributes message/;

        if ($self->casetype_id) {
            push(@keys, 'start_case');
        }

        return \@keys;
    }
);

=head2 integrity_verified

Validate if the rule is correct

=cut


sub integrity_verified { return 1 };

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
