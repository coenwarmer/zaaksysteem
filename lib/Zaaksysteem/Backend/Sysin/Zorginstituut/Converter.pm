package Zaaksysteem::Backend::Sysin::Zorginstituut::Converter;
use Moose;

with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Backend::Sysin::Roles::UA
);

use Encode qw(encode_utf8);
use Zaaksysteem::Tools;
use Zaaksysteem::XML::Zorginstituut::Converter;

=head1 NAME

Zaaksysteem::Backend::Sysin::Zorginstituut::Converter - Zorginstituut converter package

=head1 DESCRIPTION

This package deals with the converter from XML to plain ASCII for WMO/JW messages.

=head1 SYNOPSIS

    use Zaaksysteem::Backend::Sysin::Zorginstituut::Converter;

    my $converter = Zaaksysteem::Backend::Sysin::Zorginstituut::Converter->new(
        endpoint => 'https://foo.bar.nl',
        username => 'foo',
        password => 'bar',
    );

    # The ASCII file is called 'EI-bestand'.
    my $ascii = $converter->to_ei($xml);
    my $ascii = $converter->to_ascii($xml);

    my $xml   = $converter->to_xml($ascii);

=head1 ATTRIBUTES

=head2 endpoint

The endpoint of the converter service

=cut

has endpoint => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 soap_envelop

The custom SOAP envelop

=cut

has soap_envelop => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    builder => '_build_soap_envelop',
);

=head2 SEE ALSO

L<Zaaksysteem::Backend::Sysin::Roles::UA>.

=head1 METHODS

=head2 to_ei

See C<to_ascii>

=head2 to_ascii

    my $ascii = $self->to_ascii($xml);

Converts XML to ASCII EI.

=cut

sig to_ascii => 'Str';

sub to_ascii {
    my ($self, $xml) = @_;
    return $self->_call_soap('converteerXmlNaarVastLengte', $xml)->textContent;
}


=head2 to_xml

    my $xml = $self->to_xml($ascii);

Converts ASCII EI to XML

=cut

sig to_xml => 'Str';

sub to_xml {
    my ($self, $ascii) = @_;
    my ($node) = $self->_call_soap('converteerVasteLengteNaarXml', $ascii)->childNodes;
    return $node->serialize;
}

sub _get_converted_data {
    my ($self, $content) = @_;

    my $reader = Zaaksysteem::XML::Zorginstituut::Converter->new(xml => $content);
    return $reader->get_bericht() if $reader->is_valid;

    throw(
        "converter/errors",
        sprintf(
            "Conversion went wrong: %s", dump_terse($reader->show_errors)
        )
    );
}

sub _call_soap {
    my ($self, $type, $data) = @_;


    my $req = $self->_build_soap($type, $data);

    if ($self->log->is_trace()) {
        $self->log->trace($req->as_string);
        #open my $fh, ">", "converter.xml";
        #print $fh $data;
        #close $fh;
    }

    my $res     = $self->ua->request($req);
    my $content = $self->assert_http_response($res);
    return $self->_get_converted_data($content);
}


=head1 PRIVATE METHODS

=head2 _build_endpoints

Builder for the endpoints

=cut

sub _build_endpoints {
    my $self = shift;
    return { converter => $self->endpoint, };
}

sub _build_soap_envelop {
    my $self = shift;
    return q{<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:data="http://www.istandaarden.nl/berichten/conversie/data" xmlns:ist="http://www.istandaarden.nl" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
<SOAP-ENV:Header>
<wsse:Security>
  <wsse:UsernameToken>
    <wsse:Username>%s</wsse:Username>
    <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">%s</wsse:Password>
  </wsse:UsernameToken>
</wsse:Security>
</SOAP-ENV:Header>
<SOAP-ENV:Body>
<data:%s>
  <data:header>
    <ist:NaamOrganisatieAfzender>Zaaksysteem.nl/Mintlab B.V.</ist:NaamOrganisatieAfzender>
    <ist:NaamOrganisatieOntvanger>Zorginstituut Nederland</ist:NaamOrganisatieOntvanger>
    <ist:Verzenddatum>%s</ist:Verzenddatum>
  </data:header>
  <data:bericht>%s</data:bericht>
</data:%s>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>};
}

=head2 _build_soap

Build the SOAP request.

=cut

sub _build_soap {
    my ($self, $type, $data) = @_;

    my $now = DateTime->now->iso8601;

    return HTTP::Request->new(
        POST => $self->endpoint,
        HTTP::Headers->new(
            Content_Type => 'text/xml; charset=UTF-8',
            soapaction   => $type,
        ),
        sprintf($self->soap_envelop, $self->username, $self->password, $type, $now, encode_utf8($data), $type),
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 TESTS

Tests can be found in L<TestFor::General::Backend::Sysin::Zorginstituut::Converter>.

    # You can test this module by running:
    ./zs_prove t/lib/TestFor/General/Backend/Sysin/Zorginstituut/Converter.pm


=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
