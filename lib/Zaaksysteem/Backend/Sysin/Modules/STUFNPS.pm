package Zaaksysteem::Backend::Sysin::Modules::STUFNPS;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use JSON;

use Zaaksysteem::Exception;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::StUFPRSNPS
/;

###
### Interface Properties
###
### Below a list of interface properties, see
### L<Zaaksysteem::Backend::Sysin::Modules> for details.

use constant INTERFACE_ID               => 'stufnps';

use constant INTERFACE_CONFIG_FIELDS    => [];

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'StUF Koppeling NPS',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'incoming',
    manual_type                     => ['text', 'file'],
    is_multiple                     => 0,
    is_manual                       => 1,
    retry_on_error                  => 1,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    trigger_definition              => {
        disable_subscription   => {
            method  => 'disable_subscription',
            #update  => 1,
        },
        search          => {
            method  => 'search_prs',
            #update  => 1,
        },
        import      => {
            method  => 'import_prs',
            #update  => 1,
        },
    },
    attribute_list                  => [
        {
            external_name   => 'bsn-nummer',
            internal_name   => 'burgerservicenummer',
            attribute_type  => 'defined'
        },
    ]
};

has 'stuf_object_type' => (
    'is'        => 'ro',
    'default'   => 'NPS'
);

has 'stuf_subscription_table' => (
    'is'        => 'ro',
    'default'   => 'NatuurlijkPersoon'
);

has 'stuf_version'      => (
    'is'        => 'ro',
    'default'   => '0310'
);

###
### BUILDARGS
###

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

=head1 INTERNAL METHODDS


=head2 $module->_is_verhuisd($object)

Return value: $TRUE_OR_FALSE

Returns whether this person has moved to another city or not. In case of a 0301 xml message,
this would mean the system will check against the bag_woonplaats table, to find the current
woonplaats

=cut

sub _binnen_gemeente {
    my $self                = shift;
    my ($record, $object, $adres)   = @_;

    if ($adres && $adres->landcode && $adres->landcode != 6030) {
        return 0;
    }

    return ($object->verhuisd_against_bag($record->result_source->schema) ? 0 : 1);
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 INTERFACE_CONFIG_FIELDS

TODO: Fix the POD

=cut

=head2 INTERFACE_ID

TODO: Fix the POD

=cut

=head2 MODULE_SETTINGS

TODO: Fix the POD

=cut

