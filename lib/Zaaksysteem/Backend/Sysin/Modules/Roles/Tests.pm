package Zaaksysteem::Backend::Sysin::Modules::Roles::Tests;

use Moose::Role;

use Zaaksysteem::Tools;

use URI;
use IO::Socket::INET;
use IO::Socket::SSL;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Roles::Tests - Add simple server
connection tests capabilities to a sysin module

=head1 SYNOPSIS

    package My::Fancy::Module

    extends 'Zaaksysteem::Backend::Sysin::Modules';

    with 'Zaaksysteem::Backend::Sysin::Modules::Roles::Tests';

    ...

    sub my_test_hook {
        my $self = shift;

        my $socket = $self->test_host_port('some://url/to/some?resource');
    }

    # This test hook runs a HTTPS verification check
    sub my_ssl_test_hook {
        my $self = shift;

        my $socket = $self->test_host_port_ssl('https://someweb.site/path/is?ignored', '/path/to/ca_cert');
    }

=head1 METHODS

=head2 test_host_port

Connection testing. Will get the host and port from the given URL, and tries a simple
connect on the host and port.

    my $socket = $self->test_host_port('some://uyl/to/some?resource');

The L<URI> package will be used to attempt to parse the provided URL for it's
host and port components. Any other URI parts will be ignored.

Returns the L<IO::Socket::INET> instance if a connection is succesful, throws
an exception of type C<sysin/modules/test/error> if the test failed.

=cut

sub test_host_port {
    my $self = shift;
    my $endpoint = shift;

    my $uri = URI->new($endpoint);

    my $host = $uri->host;
    my $port = $uri->port;

    my $sock = IO::Socket::INET->new(
        PeerPort => $port,
        PeerAddr => $host,
        Timeout  => 10,
        Proto    => 'tcp',
    );

    unless ($sock) {
        throw('sysin/modules/test/error', sprintf(
            "Kon geen verbinding met externe server '%s' starten, staat de firewall inkomend verkeer toe en is de URL correct?",
            $endpoint
        ));
    }

    return $sock;
}

=head2 test_host_port_ssl

Connection testing. Will get the host and port from the given url, tries a
simple connect on the host and port and tries to do a TLS handshake, checking
if the server's certificate is signed by the supplied CA certificate chain.

    my $socket = $self->test_host_port_ssl('https://host.tld/path', '/path/to/ca_file');

The L<URI> package will be used to attempt to parse the provided URL for it's
scheme, host, and port components. Any other URL parts will be ignored.

Returns the L<IO::Socket::SLL> instance if the host is connectable and SSL
verification succeeded. If any part of the validation fails, a
C<sysin/modules/test/error> exception will be thrown.

The second parameter (path to the CA file to be used) is optional. If not
provided, the server's base CA store will be used.

=cut

sub test_host_port_ssl {
    my $self = shift;
    my $endpoint = shift;
    my $ca_cert = shift;

    my $uri = URI->new($endpoint);

    unless ($uri->isa('URI::http')) {
        throw(
            'sysin/modules/test/error',
            'Opgegeven URL niet geldig'
        );
    }

    unless ($uri->scheme eq 'https') {
        throw(
            'sysin/modules/test/error',
            "Opgegeven URL is niet beveligd (https://)."
        );
    }

    my $host = $uri->host;
    my $port = $uri->port;

    my %socket_opts = (
        PeerPort => $port,
        PeerAddr => $host,
        Timeout => 10,
        Proto => 'tcp',
        SSL_verify_mode => SSL_VERIFY_PEER,
        SSL_verifycn_scheme => 'www'
    );

    if ($ca_cert) {
        $socket_opts{ SSL_ca_file } = $ca_cert;
    }

    my $sock = IO::Socket::SSL->new(%socket_opts);

    unless ($sock) {
        throw('sysin/modules/test/error', sprintf(
            "Kon geen verbinding met externe server '%s' starten. Controleer de URL, de firewall, of het opgegeven CA certificaat.",
            $endpoint,
            [
                { ssl_error => IO::Socket::SSL::errstr(), }
            ]
        ));
    }

    return $sock;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
