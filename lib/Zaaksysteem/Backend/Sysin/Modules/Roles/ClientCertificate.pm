package Zaaksysteem::Backend::Sysin::Modules::Roles::ClientCertificate;
use Moose::Role;

use Crypt::OpenSSL::X509;
use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Roles::ClientCertificate - Role for interfaces that need to verify a client certificate

=head1 SYNOPSIS

    package Zaaksysteem::Backend::Sysin::Modules::MyAwesomeInterface;
    extends 'Zaaksysteem::Backend::Sysin::Modules';

    with qw/
        Zaaksysteem::Backend::Sysin::Modules::Roles::SOAPGeneric
        Zaaksysteem::Backend::Sysin::Modules::Roles::ClientCertificate
    /;

    # etc.
    # The /sysin/interface/XXX/soap API endpoint uses a configuration field
    # named "(interface_)client_certificate" to find the configured one.

=head1 METHODS

=head2 verify_client_certificate

Verify if the given certificate hash matches the supplied certificate.

=cut

define_profile verify_client_certificate => (
    required => {
        get_certificate_cb => 'CodeRef',
    },
    optional => {
        client_fingerprint => 'Str',
        dn => 'Str',
    },
);

sub verify_client_certificate {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    my $stored_certificate = $params->{get_certificate_cb}->();

    if (!$stored_certificate) {
        $self->log->error("Verification failed: No certificate found to check against.");
        return;
    }

    my $stored_certificate_obj = Crypt::OpenSSL::X509->new_from_file($stored_certificate->get_path);
    $self->log->debug("Stored subject-DN:  " . $stored_certificate_obj->subject());
    $self->log->debug("Stored fingerprint: " . $stored_certificate_obj->fingerprint_sha1());

    $self->log->debug("Received subject-DN:  " . $params->{dn});
    $self->log->debug("Received fingerprint: " . $params->{client_fingerprint});

    if($params->{client_fingerprint} eq $stored_certificate_obj->fingerprint_sha1()) {
        $self->log->debug(
            sprintf(
                "Client certificate SUCCESS: SHA1 match: %s",
                $params->{client_fingerprint},
            ),
        );
        return 1;
    }
    
    $self->log->error(
        sprintf(
            "Client certificate FAILURE: SHA1 mismatch: %s != %s",
            $stored_certificate_obj->fingerprint_sha1(),
            $params->{client_fingerprint},
        ),
    );

    return;
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
