package Zaaksysteem::Backend::Sysin::Auth::Alternative;
use Moose;

use LWP::UserAgent;
use Number::Phone;
use XML::LibXML;
use Zaaksysteem::Tools;
use Zaaksysteem::Types;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Auth::Alternative - Alternative authentication module for Zaaksystem

=head1 DESCRIPTION

Make the logic work for us

=head1 SYNOPSIS

    use Zaaksysteem::Auth::Alternative;

    my $alt = Zaaksysteem::Auth::Alternative->new(
        schema => $cli->schema,
    );

    my $number = $alt->assert_mobile_phone_number($number);
    my $xml = $alt->create_sms_xml(
        phone_number => $number,
        message => "You win the lottery",
    );
    $alt->send_sms(
        xml => $xml,
        record => $transaction_record,
    );


=head1 ATTRIBUTES

=head2 schema

An L<Zaaksysteem::Schema> object. Required.

=cut

has schema => (
    isa      => 'Zaaksysteem::Schema',
    is       => 'ro',
    required => 1,
);

=head2 sms_sender

The SMS sender name. Required.

=cut

has sms_sender => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 sms_product_token

The SMS product token. Required.

=cut

has sms_product_token => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 sms_endpoint. Required.

The SMS HTTP endpoint

=cut

has sms_endpoint => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 ua

An L<LWP::UserAgent> object, override when needed

=cut

has ua => (
    is      => 'ro',
    isa     => 'LWP::UserAgent',
    lazy    => 1,
    default => sub {
        my $ua = LWP::UserAgent->new;
        $ua->timeout(10);
        $ua->ssl_opts(verify_hostname => 1,);
        return $ua;
    },
);

=head2 assert_mobile_phone_number

Checks if the number used is a Dutch mobile phone and/or Dutch pager
number. Dies in case of non-compliancy, returns a number which can be
used for subsequent calls.

=cut

sig assert_mobile_phone_number  => 'Str';

sub assert_mobile_phone_number {
    my ($self, $number) = @_;

    if (substr($number, 0,2) eq '00') {
        $number = "+" . substr($number, 2);
    }

    my $ok_number = Number::Phone->new($number) // Number::Phone->new('NL', $number);

    if (!$ok_number) {
        throw('auth/alternative/phone_number/invalid', "Not a valid phonenumber: $number", { fatal => 1});
    }
    elsif (uc($ok_number->country) ne 'NL') {
        throw('auth/alternative/phone_number/country_code', "Not a supported country", { fatal => 1 });
    }
    # Pagers and mobile phones can receive SMS?
    elsif (!$ok_number->is_mobile() && !$ok_number->is_pager) {
        throw('auth/alternative/phone_number/non_mobile', "Not a mobile phone number", { fatal => 1} );
    }

    return '0031' . $ok_number->format_using('Raw');

}

=head2 send_sms

    $self->send_sms(
        xml => $xml,
        record => "Zaaksysteem::Schema::TransactionRecord",
    );

Sends the XML to the endpoint and logs the output in the transaction records.

=cut

define_profile send_sms => (
    required => {
        xml    => 'Str',
        record => 'Zaaksysteem::Schema::TransactionRecord',
    },
);

sub send_sms {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    my $req = HTTP::Request->new(
        'POST',
        $self->sms_endpoint,
        [ "Content-Type" => "application/xml" ],
        $params->{xml},
    );

    my $res = $self->ua->request($req);

    $params->{record}->input($req->dump(maxlength => 0));
    $params->{record}->output($res->dump(maxlength => 0));

    if (!$res->is_success) {
        throw(
            "twofactor/sms_sending_failed",
            $res->status_line,
            { fatal => 1 }, # Don't retry SMS sending. That's expensive.
        );
    }

    return 1;
}

=head2 create_sms_xml

    $self->create_sms_xml(
        phone_number => $number,
        message      => "When you read this, you've received the SMS",
        reference    => "Reference to something",
    );

Generate the XML which is used to send to the endpoint. You will want to
reference to be a transaction ID but it can be anything.

=cut

define_profile create_sms_xml => (
    required => {
        phone_number => 'Str',
        message      => 'Str',
        reference    => 'Str',
    },
);

sub create_sms_xml {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    my $number = $self->assert_mobile_phone_number($params->{phone_number});

    my $doc = XML::LibXML->createDocument;

    my %nodes;

    $nodes{root} = $doc->createElement("MESSAGES");
    $doc->setDocumentElement($nodes{root});

    $nodes{authentication}  = $nodes{root}->addNewChild(undef, "AUTHENTICATION");
    $nodes{token}           = $nodes{authentication}->addNewChild(undef, "PRODUCTTOKEN");

    $nodes{msg} = $nodes{root}->addNewChild(undef, "MSG");

    $nodes{from}      = $nodes{msg}->addNewChild(undef, "FROM");
    $nodes{to}        = $nodes{msg}->addNewChild(undef, "TO");
    $nodes{body}      = $nodes{msg}->addNewChild(undef, "BODY");
    $nodes{reference} = $nodes{msg}->addNewChild(undef, "REFERENCE");

    $nodes{token}->appendTextNode($self->sms_product_token);
    $nodes{from}->appendTextNode(substr($self->sms_sender, 0, 11));
    $nodes{to}->appendTextNode($number);
    $nodes{body}->appendTextNode(substr($params->{message}, 0, 160));
    $nodes{reference}->appendTextNode($params->{reference});

    my $xml = $doc->toString(1);

    $self->log->trace("SMS XML: " . $xml);

    return $xml;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
