package Zaaksysteem::Controller::Form::Employee;
use Moose;

BEGIN { extends 'Zaaksysteem::Controller' };

=head1 NAME

Zaaksysteem::Controller::Form::Employee - A internal case registration form for employee

=head1 DESCRIPTION

An internal case creation endpoint for employees.

=head1 ACTIONS

=head2 internal_form_base

=cut


sub internal_form_base : Chained('/') : PathPart('form/employee') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{redirect} = $c->req->referer;
}

=head2 create_case_by_casetype

=head3 URI

    C</form/employee/ID/NAME>

=head3 DESCRIPTION

Allows direction creation of a case via a clickable link for logged in users of the organisation.
It redirects to the case creation form with the logged in user as case requestor, the casetype etc.
If the casetype is not I<intern> or I<internextern> you cannot create a case via this endpoint.

=cut

sub create_case_by_casetype : Chained('internal_form_base'): PathPart('') : Args(2) {
    my ($self, $c, $id, $name) = @_;

    if ($id !~ /^\d+$/) {
        $self->_log_and_detach(
            $c,
            error          => "Casetype ID is not a number",
            human_readable => "Ongeldige zaaktype ID opgegeven",
        );
    }

    my $casetype = $c->model('DB::Zaaktype')->search(
        {
            'me.id'                    => $id,
            'me.deleted'               => undef,
            'zaaktype_node_id.trigger' => ['intern', 'internextern'],
            'me.active'                => 1,
        },
        { 'prefetch' => 'zaaktype_node_id', }
    )->first;

    if (!$casetype) {
        $self->_log_and_detach(
            $c,
            error          => "Casetype with id $id is not a internal casetype",
            human_readable => "Ongeldige zaaktype ID opgegeven",
        );
    }

    my $aanvrager_id = $c->user->betrokkene_identifier;

    $c->res->redirect(
        $c->uri_for(
            '/zaak/create/balie',
            {
                aanvrager         => $aanvrager_id,
                bestemming        => 'intern',
                betrokkene_type   => 'natuurlijk_persoon',
                create            => 1,
                create_entry      => 1,
                zaaktype_id       => $id,
                ztc_aanvrager_id  => $aanvrager_id,
                ztc_contactkanaal => 'behandelaar',
                ztc_trigger       => 'intern',
                sessreset         => 1,
            }
        )
    );
    $c->detach;
}

=head1 INTERNAL METHODS

=head2 _log_and_detach

    $self->_log_and_detach(
        $c,
        error => 'logline for log4perl',

        # defaults to 'Helaas, deze zaak kan niet worden aangevraagd',
        title => 'Title for the error page',

        human_readable =>
            'A descriptive message for the user that action is not possible',
    );

Log an error and detach to an error page.

=cut

sub _log_and_detach {
    my ($self, $c, %options) = @_;

    $self->log->error($options{error});

    $c->stash->{template}   = 'general_error.tt';

    $c->stash->{error}      = {
        titel   => $options{titel} // 'Helaas, deze zaak kan niet worden aangevraagd',
        bericht => $options{human_readable},
    };
    $c->detach;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
