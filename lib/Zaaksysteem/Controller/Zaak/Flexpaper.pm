package Zaaksysteem::Controller::Zaak::Flexpaper;

use Moose;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

=head2 flexpaper

Display flexpaper PDF reader

=cut

sub flexpaper : Chained('/zaak/base') : PathPart('flexpaper') : Args() {
    my ($self, $c, $file_id) = @_;

    # find the requested file, check if it's active
    $c->stash->{file} = $c->stash->{zaak}->files->search({
        id => $file_id
    })->active->first or throw('api/case/file', "file $file_id not available");

    $c->forward('annotations');
}


sub annotations : Private {
    my ($self, $c) = @_;

    my $file_id = $c->stash->{file}->id;

    my $rs = $c->model('DB::FileAnnotation')->search({file_id => $file_id});

    my @annotations = $c->model('DB::Config')->get('pdf_annotations_public') ?
        $rs->all :
        $rs->search({subject => 'betrokkene-medewerker-' . $c->user->uidnumber})->all;

    $c->stash->{file_id} = $file_id;
    $c->stash->{annotations} = [map { $_->properties } @annotations];
    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'flexpaper.tt';
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 annotations

TODO: Fix the POD

=cut

