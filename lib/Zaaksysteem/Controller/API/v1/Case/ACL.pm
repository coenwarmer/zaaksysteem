package Zaaksysteem::Controller::API::v1::Case::ACL;

use Moose;
use Clone qw(clone);

use Zaaksysteem::Tools;
use Zaaksysteem::Types qw[UUID Boolean ACLCapability ACLEntityType ACLScope];
use Zaaksysteem::Object::Types::Case::ACL;

use List::MoreUtils qw[any];

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

has api_capabilities => (
    is          => 'ro',
    default     => sub { return [qw/intern/] }
);

has api_control_module_types => (
    is => 'rw',
    default => sub { [ 'api' ] },
);

=head1 NAME

Zaaksysteem::Controller::API::v1::Case::ACL - ACL calls for viewing and altering ACL's

=head1 DESCRIPTION

TODO

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/case/[UUID]/acl> URI namespace.

=cut

sub base : Chained('/api/v1/case/instance_base') : PathPart('acl') : CaptureArgs(0) {
    my ($self, $c) = @_;

}

=head2 list

=head3 URL Path

C</api/v1/case/[case:id]/acl>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c)  = @_;

    $c->forward('_get_and_send_acls');
}

=head2 update

=head3 URL Path

C</api/v1/case/[case:id]/acl/update>

=cut

define_profile update => (
    optional => {
        values  => 'HashRef',
    }
);

sub update : Chained('base') : PathPart('update') : Args(0) : RO {
    my ($self, $c)  = @_;

    $self->assert_post($c);
    my $params      = assert_profile($c->req->params)->valid;
    my @rawacls     = ($params->{values} ? (@{ $params->{values} }) : ());

    ### Validate each given acl, and split the capabilities
    my @acls;
    for my $rawacl (@rawacls) {
        my $rawacl = assert_profile($rawacl, profile => {
            required => {
                capabilities => ACLCapability,
                entity_id    => sub {
                    my $val = shift;

                    return 1 if $val =~ /^\d+|\d+$/;
                    return;
                },
                entity_type  => ACLEntityType,
                scope        => ACLScope,
            }
        })->valid;

        ### Skip types and other than position entity_types
        next unless ($rawacl->{scope} eq 'instance' && $rawacl->{entity_type} eq 'position');

        for my $capability (@{ $rawacl->{capabilities} }) {
            my $acl = clone $rawacl;
            $acl->{capability} = $capability;

            delete($acl->{capabilities});
            push(@acls, $acl);
        }
    }

    ### Retrieve raw case object
    my $zaak;
    $zaak = try {
        $c->stash->{ case }->get_source_object
    } catch {
        $c->log->error($_);

        throw('api/v1/case/retrieval_fault', sprintf(
            'Case retrieval failed, unable to continue.'
        ));
    };

    ### Now push the changes into our database
    try {
        $c->model('DB')->schema->txn_do(
            sub {
                ### We fancy in place updates, let's compare.
                my $current_acls = $c->model('DB::ZaakAuthorisation')->search(
                    {
                        zaak_id => $zaak->id,
                    }
                );

                ### Find entries in de db, and delete the entries we did not find.
                my %got_indexes;
                while (my $dbacl = $current_acls->next) {
                    my $found_acl = 0;

                    for (my $i = 0; $i < @acls; $i++) {
                        my $acl = $acls[$i];

                        next unless (
                            $dbacl->capability eq $acl->{capability} &&
                            $dbacl->entity_id eq $acl->{entity_id} &&
                            $dbacl->entity_type eq $acl->{entity_type} &&
                            $dbacl->scope eq $acl->{scope}
                        );

                        $found_acl++;
                        $got_indexes{$i} = 1;
                    }

                    $dbacl->delete if !$found_acl;
                }

                ### Create the entries not already in the DB
                for (my $i = 0; $i < @acls; $i++) {
                    next if $got_indexes{$i};
                    my $acl = $acls[$i];

                    $c->model('DB::ZaakAuthorisation')->create(
                        {
                            %$acl,
                            zaak_id => $zaak->id,
                        }
                    )
                }
            }
        );
    } catch {
        $c->log->error('Error updating ACLs: ' . $_);

        throw('api/v1/case/acl/update/db_error', sprintf(
            'Problem inserting given data in our database'
        ));
    };

    $zaak->_touch;
    $c->forward('_get_and_send_acls');
}

=head1 PRIVATE METHODS

=head2 _get_and_send_acls

Method for retrieving and sending the ACLS

=cut

sub _get_and_send_acls : Private {
    my ($self, $c) = @_;

    my @acls        = $self->_get_acls($c->stash->{case});

    $c->stash->{ result } = Zaaksysteem::API::v1::ArraySet->new(
        content => [ @acls ],
    );
}


=head2 _get_acls

    my @object_types_case_acls = $self->_get_acls($c->model('ObjectData')->search({object_class => 'case'})->first);

Return a list of L<Zaaksysteem::Object::Types::Case::ACL> objects belonging to this case and type of this case.

=cut

sub _get_acls {
    my ($self, $case)       = @_;

    ### Retrieve ACLs for the object and the type of the object (case (uuid) and casetype (class_uuid)).
    ### Make sure we only retrieve positions
    my @dbacls              = $case->object_acl_entries->result_source->schema->resultset('ObjectAclEntry')->search(
        {
            '-or' => [
                {
                    entity_type => 'position',
                    scope => 'instance',
                    object_uuid => $case->get_column('uuid'),
                },
                {
                    entity_type => 'position',
                    scope => 'type',
                    object_uuid => $case->get_column('class_uuid'),
                },
            ]
        }
    )->all;

    ### Normalize data, so we can combine the capabilities in one array (['read','write',...])
    my %positions;
    for (@dbacls) {
        if ($_->scope eq 'type') {
            # Public:
            if (!$case->acl_groupname || $case->acl_groupname eq 'public') {
                next if $_->groupname && $_->groupname ne 'public';

            # Confidential or "other"
            } else {
                next if !$_->groupname || $_->groupname ne $case->acl_groupname;
            }
        }

        $positions{ $_->scope . ':' . $_->entity_id } //= [];
        push(@{ $positions{ $_->scope . ':' . $_->entity_id } }, $_->capability);
    }

    ### Create ACL Objects from the combined acl rows.
    my @acls;
    for my $key (sort keys %positions) {
        my ($scope, $position) = split(':', $key);

        push(@acls,
            Zaaksysteem::Object::Types::Case::ACL->new(
                entity_type  => 'position',
                capabilities => [sort @{ $positions{$key} }],
                entity_id    => $position,
                scope        => $scope,
                read_only    => ($scope eq 'type' ? 1 : 0)
            )
        );
    }

    return @acls;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
