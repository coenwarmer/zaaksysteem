package Zaaksysteem::Controller::API::v1::Subject;

use Moose;
use Moose::Util::TypeConstraints qw[enum union];

use DateTime;
use JSON qw[decode_json];

use Zaaksysteem::Tools;
use Zaaksysteem::Types qw(SubjectType);

use Zaaksysteem::BR::Subject;


BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::V1::Subject - APIv1 controller for subjects

=head1 DESCRIPTION

This is the controller API class for C<api/v1/subject>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Subject>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Subject>

=head1 ACTIONS

=head2 base

=cut

### XXX TODO (Get inspiration from Controlpanel.pm)
sub base : Chained('/api/v1/base') : PathPart('subject') : CaptureArgs(0) : Scope('subject') {}

=head2 list

TODO

=cut

### XXX TODO (Get inspiration from Controlpanel.pm)
sub list : Chained('base') : PathPart('') : Args(0) : RO {}

=head2 instance_base

TODO

=cut

### XXX TODO (Get inspiration from Controlpanel.pm)
sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {}

=head2 get

TODO

=cut

### XXX TODO (Get inspiration from Controlpanel.pm)
sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {}


=head2 create

=head3 URL Path

C</api/v1/subject/create>

=cut

sub create : Chained('base') : PathPart('create') : Args(0) : RW {
    my ($self, $c) = @_;

    ### Prevent creation of controlpanel object by unknown users
    throw(
        'api/v1/subject/forbidden',
        'Creation of subject objects only allowed for logged in users'
    ) unless $c->user_exists;

    my $subject     = Zaaksysteem::BR::Subject->new(
        schema          => $c->model('DB')->schema,
        log             => $c->log,
        subject_type    => $c->req->params->{subject_type}
    );

    $c->model('DB')->txn_do(sub {
        $subject->save($c->req->params);
    });

    $c->stash->{result} = $c->model('DB::Bedrijf')->find({ id => $subject->id });
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
