package Zaaksysteem::Controller::API::v1;

use Moose;
use namespace::autoclean;

use List::Util qw[first];
use Moose::Util qw[ensure_all_roles];

use Zaaksysteem::Tools;

use Zaaksysteem::API::v1::Message::Pong;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1 - Provides 'base' chain for API v1

=head1 SYNOPSIS

    package Zaaksysteem::API::v1::MyController;

    use Moose;

    BEGIN { extends 'Zaaksysteem::Controller::API::v1' }

    # This action can be called by users configured to have read-only
    # permissions in the API configurations
    sub listing : Chained('/api/v1/base') : PathPart('list') : Args(0) : RO {
        ...
    }

    # This action is implied read-write, and won't be reached by users
    # configured to only have read-only permissions.
    sub my_action : Chained('/api/v1/base') : PathPart('my_action') : Args(0) {
        ...
    }

=head1 DESCRIPTION

=head1 ACTIONS

=head2 base

This base action will verify the client is authorized to access the API
infrastructure, and the specific action being called.

Access via this base action is L<logged|Zaaksysteem::DB::ResultSet::Logging>
under the event type C<api/request>.

The L<interface|Zaaksysteem::Backend::Sysin::Interface::Component> used to
authenticate the client can be found in the L<stash|Catalyst/c-stash> under
the key C<interface>.

The L<user|Zaaksysteem::Backend::Auth::UserEntity::Component> as whom the
client is authorized to access the API can be found under the C<api_user>
stash key.

=cut

sub _get_user_and_interface {
    my ($self, $c)  = @_;
    my ($user, $interface);

    if ($c->user_exists && !$c->user->is_external_api) {
        $user           = $c->user;
    }

    my $interfaces = $c->model('DB::Interface')->search_active({
        module => 'api'
    });

    ### HACKITIHACK: Special case to speed up Dario, PIP is allowed
    if (
        $c->action =~ /controlpanel/ &&
        (
            $c->session->{pip} ||
            ($c->user_exists && !$c->user->is_external_api)
        )
    ) {
        return ();
    }

    if (!$interfaces->count && !$user) {
        throw('api/v1/configuration_incomplete', sprintf(
            'This Zaaksysteem instance does not seem to be configured for public access. Please contact IT.'
        ));
    }

    my @interfaces = $interfaces->all;

    if (scalar @interfaces > 1 && !$c->req->header('API-Interface-Id')) {
        throw(
            'api/v1/multiple_interfaces_found',
            'Multiple API interfaces found and no API-Interface-Id header set, please set'
        );
    }

    if ($user) {
        $c->log->info('Already logged in user: skip DIGEST authentication');
        for my $iface (@interfaces) {
            ### When multiple interfaces are used with the same username, we would like to get
            ### the correct interface by looking at the header API-Interface-Id
            next if ($c->req->header('API-Interface-Id') && $c->req->header('API-Interface-Id') ne $iface->id);

            if ($c->user->username eq $iface->get_interface_config->{medewerker}->{username}) {
                $interface  = $iface;
            }
        }
    } else {
        for my $iface (@interfaces) {
            ### When multiple interfaces are used with the same username, we would like to get
            ### the correct interface by looking at the header API-Interface-Id
            next if ($c->req->header('API-Interface-Id') && $c->req->header('API-Interface-Id') ne $iface->id);

            $user = $c->authenticate({ interface => $iface }, 'api');

            if (defined $user) {
                $interface = $iface;
            }
        }

        # Technically this is a no-op, $c->authenticate with the HTTP digest
        # auth module loops until succesful login, which means we can't find the
        # actual user, which is a problem on our side.
        unless (defined $user) {
            throw('api/v1/forbidden', sprintf(
                'Could not resolve user, unable to authenticate. Please contact IT.'
            ), { http_code => 403 });
        }

        # 'rw' is the intersting bit, any other value is implied read-only.
        if ($interface->name eq 'api' && ($interface->jpath('$.access') || 'ro') ne 'rw') {
            # check the action is explicitly defined to be read-only.
            unless (exists $c->action->attributes->{ RO }) {
                throw('api/v1/forbidden', 'This action has been disabled by the API Security Policy', {
                    http_code => 403
                });
            }
        }
    }

    return ($user, $interface);
}

sub base : Chained('/') : PathPart('api/v1') : CaptureArgs(0) : Scope('global') {
    my ($self, $c) = @_;

    my ($user, $interface)      = $self->_get_user_and_interface($c);
    $c->stash->{ api_user }     = $user;
    $c->stash->{ interface }    = $interface if $interface;

    # Hackityhack, this is because of the Catalyst ActionChain implementation,
    # role application is normally handled by a default actionrole via
    # Catalyst::Controller::ActionRole, but this don't work for ActionChains...
    ensure_all_roles($c->action, 'Zaaksysteem::API::v1::ActionRole');

    my $scope = $c->action->attribute('Scope')->[0];

    unless (defined $scope) {
        throw('api/v1/scope', 'No scope defined for the action being called.');
    }

    ## Only log on 'POST' requests and on external API
    if ($interface && lc($c->req->method) eq 'post') {
        my $body = {};
        if ($c->req->content_type eq 'application/json') {
            $body = $self->_get_body_as_string($c->req->body) || '{}';
        }

        $c->stash->{ request_event } = $interface->process_trigger('log_mutation', {
            client_ip       => $c->req->header('X-Real-IP') || $c->req->address,
            request_id      => $c->stash->{ request_id },
            request_method  => lc($c->req->method),
            request_call    => lc($c->req->path),
            request_body    => $body,
        });
    }
}

=head2 _get_body_as_string

Returns a File::Temp object as string

=cut

sub _get_body_as_string {
    my $self        = shift;
    my $body        = shift;

    return '' unless UNIVERSAL::isa($body, 'File::Temp');

    my $text        = ''; { local $/ = undef; $text = <$body>; }

    return $text;
}

=head2 default

This action matches on any path not used by one of the chained sub-actions
of L</base>. It always triggers an C<api/v1>-typed exception, with a link to
the current API documentation.

=cut

sub default : Chained('base') : PathPart('') {
    my ($self, $c) = @_;

    throw('api/v1', 'Invalid request. For more information, see ' . $c->uri_for('/man/Zaaksysteem::API::V1'), 400);
}

=head2 ping

This action emulates the ping/pong behavior that external systems are expected
to implement.

=cut

sub ping : Chained('/') : PathPart('api/v1/ping') : Args(0) {
    my ($self, $c) = @_;

    unless ($c->req->params->{ api_version } eq 'v1') {
        throw(
            'api/v1/ping/invalid_version',
            'This interface only supports v1 requests'
        );
    }

    my $message = $c->req->params->{ message };

    unless (ref $message eq 'HASH') {
        throw('api/v1/ping/message_expected', sprintf(
            'Unexpected or missing message content, got %s',
            ref $message || $message
        ));
    }

    unless ($message->{ type } eq 'message') {
        throw('api/v1/ping/message_type_invalid', sprintf(
            'Expected instance of message type, got %s',
            $message->{ type }
        ));
    }

    my $instance = $message->{ instance };

    unless ($instance->{ type } eq 'ping') {
        throw('api/v1/ping/instance_of_ping_expected', sprintf(
            'Expected message instance of type ping, got %s',
            $instance->{ type }
        ));
    }

    $c->stash->{ result } = Zaaksysteem::API::v1::Message::Pong->new(
        payload => $instance->{ payload }
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
