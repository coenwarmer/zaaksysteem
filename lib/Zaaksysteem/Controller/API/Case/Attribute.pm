package Zaaksysteem::Controller::API::Case::Attribute;

use Moose;

use Zaaksysteem::Tools;
use Zaaksysteem::Constants qw/ZAAK_CONFIDENTIALITY/;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API::Case::Attribute - Manipulation of case attributes (custom fields)

=head1 CONSTANTS

Defines a dispatch table of attribute name <-> action subs. If an attribute
keyed in this constant hashref is changed, the attached subref is executed.

=head2 SYSTEM_ATTRIBUTE_ACTION_MAP

=over 4

=item case.result

Alter resultaat of this case

=item case.confidentiality

Set the confidentiality of this case

=back

=cut

use constant SYSTEM_ATTRIBUTE_ACTION_MAP => {
    'case.result'   => sub {
        my ($c, $case, $value) = @_;

        throw (
            'api/case/attribute/save/case.result/access_denied',
            'Cannot save confidentiality, insufficient permissions'
        ) unless ($c->check_any_zaak_permission('zaak_beheer') || $c->can_change);

        $case->set_resultaat($value);
        $case->update;
    },
    'case.confidentiality'    => sub {
        my ($c, $case, $value) = @_;

        throw (
            'api/case/attribute/save/case.confidentiality/access_denied',
            'Cannot save confidentiality, insufficient permissions'
        ) unless $c->check_any_zaak_permission(qw[zaak_beheer zaak_edit]);

        throw (
            'api/case/attribute/save/case.confidentiality',
            'Cannot save confidentiality, unknown value given'
        ) unless ZAAK_CONFIDENTIALITY->($value);

        $case->set_confidentiality($value);
        $case->update;
    }
};

=head1 ACTIONS

Contains the API calls for manipulating case attributes.

=head2 base

This base action only reserves the base path for this controller, which is
C</api/case/[ZAAK_NR]/attribute/>.

=cut

sub base : Chained('/api/case/base') : PathPart('attribute') : CaptureArgs(0) { }

=head2 save

Will update a user attribute, calls the rule engine to fix everything, and returns
the output of api/case/ID

=head3 URL

C</api/case/[ZAAK_NR]/attribute/save>

=head3 Parameters

A JSON request body is expected, with the following keys

    {
        fields => {
            'attribute.kenteken' => '44-AB-44',
            'case.result'        => 'Bewaren'
        },

        phases => [ "50" ]
    }

=over 4

=item fields

C<fields> is expected to be a map of attribute names to values which will be set.

=item phases

C<phases> is expected to be an array of phases... that do what exactly?

=back

=head3 Response

See L<Zaaksysteem::Controller::API::Case/get>.

=cut

sub save : Chained('base') : PathPart('save') : Args(0) : ZAPI {
    my ($self, $c)              = @_;

    my $fields                  = $c->req->params->{fields};
    my $phases                  = $c->req->params->{phases};

    throw(
        'api/case/attribute/save/no_post',
        'Method can only be called via a post call'
    ) unless lc($c->req->method) eq 'post';

    throw(
        'api/case/attribute/save/no_permission',
        'Need edit or beheer permissions to edit this case'
    ) unless $c->check_any_zaak_permission(qw[zaak_beheer zaak_edit]);

    throw(
        'api/case/attribute/save/missing_param_fields',
        'Missing parameter: fields'
    ) unless $fields;

    throw(
        'api/case/attribute/save/invalid_param_fields',
        'Invalid parameter: fields, which needs to be a HASH'
    ) unless ref $fields eq 'HASH';

    # Grab the result key, remove from the hash so the can_change checks work
    my $result = delete $fields->{ 'case.result' };

    # Only do can_change if there are attributes to update (almost always)
    if (scalar keys %{ $fields }) {
        # True-ish value indicates can_change not to set a flash message in
        # case of failure. Yeah this is a bad design, but I don't want to break
        # the existing callers.
        unless ($c->can_change(1)) {
            throw('api/case/attribute/save/authorization_failure', sprintf(
                'Update attempt blocked, case might be closed, user may have insufficient authorizations'
            ))
        }

        $self->_update_attributes($c, $fields, $phases);
    }

    if ($result) {
        SYSTEM_ATTRIBUTE_ACTION_MAP->{ 'case.result' }->($c, $c->stash->{ zaak }, $result);
    }

    # Cleanup for the re-dispatch to case/get.
    $c->forward('/touch_delayed_cases');
    $c->go('/api/case/get', [ $c->stash->{ zaak }->id ], []);
}

sub _update_attributes {
    my ($self, $c, $fields, $phases)      = @_;

    my @magic_strings = map { m[^attribute\.(.*)$] } keys %{ $fields };

    my @zaaktype_kenmerken = $c->stash->{zaak}->zaaktype_node_id->zaaktype_kenmerken->search(
        {
            'bibliotheek_kenmerken_id.magic_string' => { -in => [ @magic_strings ] }
        },
        {
            prefetch => 'bibliotheek_kenmerken_id'
        }
    )->all;

    my %attributes;
    my %magic_string_map = map { $_->magic_string => $_->id }
                           map { $_->bibliotheek_kenmerken_id }
                               @zaaktype_kenmerken;

    foreach my $kenmerk_name (keys %{ $fields }) {
        my $value = $fields->{ $kenmerk_name };

        # If we have a specific handler for the requested update, and it's
        # *not* case.result, execute said handler.
        if (SYSTEM_ATTRIBUTE_ACTION_MAP->{$kenmerk_name} && $kenmerk_name ne 'case.result') {
            SYSTEM_ATTRIBUTE_ACTION_MAP->{$kenmerk_name}->($c, $c->stash->{zaak}, $value);

            # No need to try the other generic handlers below.
            next;
        }

        unless (ref $value eq 'ARRAY') {
            $value = [ $value ];
        }

        if($kenmerk_name =~ m|^kenmerk_id_\d+$|) {
            my ($id) = $kenmerk_name =~ m|^kenmerk_id_(\d+)|;

            $attributes{ $id } = $value;
        } elsif (my ($magic_string) = $kenmerk_name =~ /^attribute\.(.*)$/) {
            my $id = $magic_string_map{ $magic_string };

            next unless defined $id;

            $attributes{ $id } = $value;
        }
    }

    if (scalar keys %attributes) {
        $c->stash->{ zaak }->zaak_kenmerken->update_fields({
            new_values => \%attributes,
            zaak => $c->stash->{ zaak }
        });
    }

    my $statussen = $c->stash->{ zaak }->zaaktype_node_id->zaaktype_statussen;
    my @phases = (ref($phases) ? (@$phases) : ($phases));

    for my $status ($statussen->search({ id => { -in => [ @phases ] } })) {
        ### Empty other fields, by finding out which one are visible, and delete the rest
        $c->stash->{ zaak }->empty_hidden_attributes($status);

        ### Moved to "top", to make sure we break when phase does not exist
        my $rules = $c->stash->{ zaak }->execute_rules({
            status => $status->status,
            cache  => 0,
        });
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
