package Zaaksysteem::Controller::API::Authorization;

use Moose;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head2 org_unit

List available org_units, including the top-level
organization element - which typically is not shown, but
it's included for more flexibility and so that the depth
numbering may make sense.

=cut

sub org_unit : Local : ZAPI {
    my ($self, $c) = @_;

    my $tree = $c->model('DB::Groups')->get_as_tree($c->stash);

    $c->stash->{zapi} = [ $self->flatten_group_tree($c, $tree) ];
}

=head2 flatten_group_tree

Takes the group tree structure, and flattens it for easy consumption by the front end.

Walks the tree depth-first (recursively).

=cut

sub flatten_group_tree {
    my ($self, $c, $tree, $depth, $root_id) = @_;

    if (not defined $depth) {
        $depth   = 0;
        $root_id = $tree->[0]{id};
    }

    my @result;

    my $all_roles = $c->model('DB::Roles')->get_all_cached($c->stash);

    for my $node (@$tree) {
        my $node_result = {
            org_unit_id => "" . $node->{id}, # Make sure we send it as string and not integer, bw compatibility
            name        => $node->{name},
            depth       => ($depth),
            roles       => [],
        };

        for my $role (@{ $all_roles }) {
            my $parent_group_id = $role->get_column('parent_group_id');
            next unless ($parent_group_id == $root_id || $parent_group_id == $node->{id});

            push(
                @{ $node_result->{roles} },
                {
                    role_id => "" . $role->id, # Again, backwards compatibility.
                    system  => ($role->system_role ? 1 : 0),
                    name    => $role->name,
                    type    => 'entry',
                }
            );
        }

        push @result, (
            $node_result,
            $self->flatten_group_tree(
                $c,
                $node->{children},
                $depth + 1,
                $root_id,
            )
        );
    }

    return @result;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
