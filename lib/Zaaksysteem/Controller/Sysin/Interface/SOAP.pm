package Zaaksysteem::Controller::Sysin::Interface::SOAP;

use Moose;

use Zaaksysteem::Backend::Sysin::Modules;
use Zaaksysteem::Tools;

use XML::LibXML;

BEGIN { extends 'Zaaksysteem::General::SOAPController' }

sub enter :Chained('/sysin/interface/base'): PathPart('soap'): Args(0): SOAP('DocumentLiteral')  {
    my ( $self, $c) = @_;

    my $interface   = $c->stash->{entry};

    unless ($interface->active) {
        $c->stash->{soap}->fault(
           {
               code     => '404',
               reason   => 'Not found',
               detail   => 'Given interface not active'
           }
        );

        return;
    }

    my $xml             = $c->stash->{soap}->envelope();

    my $transaction     = $interface->process({
        input_data      => $xml,
        transaction_id  => 'unknown'
    });

    unless ($transaction->transaction_records->count) {
        $c->stash->{soap}->fault(
           {
               code     => '500',
               reason   => 'No response from Sysin',
               detail   => 'Error from Sysin, see logfiles for transaction id'
                            . $transaction->id
           }
        );

        return;
    }

    my $record = $transaction->transaction_records->first;

    if ($record->is_error || !$record->output) {
        $c->stash->{soap}->fault(
           {
               code     => '500',
               reason   => 'Failed processing record, see logfile.',
               detail   => 'Transaction id: ' . $transaction->id
           }
        );
    }

    my $responsexml             = XML::LibXML->load_xml(
        string => $record->output
    )->documentElement();

    $c->stash->{soap}->literal_return($responsexml);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 enter

TODO: Fix the POD

=cut

