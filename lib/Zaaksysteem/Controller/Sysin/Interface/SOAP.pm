package Zaaksysteem::Controller::Sysin::Interface::SOAP;
use Moose;

use XML::LibXML;
use Zaaksysteem::Backend::Sysin::Modules;
use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::General::SOAPController' }

=head1 NAME

Zaaksysteem::Controller::Sysin::Interface::SOAP - SOAP API endpoint for interfaces

=head1 USAGE

=over

=item * Create an interface that supports incoming SOAP calls

=item * Use the endpoint C</sysin/interface/ID_HERE/soap> (replace "ID_HERE" with the id of the interface)

=back

=head2 enter

Main SOAP endpoint code. Starts a transaction with the incoming SOAP XML
message as its input on the specified interface.

URL: /sysin/interface/INTERFACE_ID/soap

=cut

sub enter :Chained('/sysin/interface/base'): PathPart('soap'): Args(0): SOAP('DocumentLiteral')  {
    my ($self, $c) = @_;

    my $interface = $c->stash->{entry};
    my $module = $interface->module_object;

    if ($module->does('Zaaksysteem::Backend::Sysin::Modules::Roles::ClientCertificate')) {
        $c->log->debug("Interface requires client certificates");
        $c->forward('assert_authorized_client');
    }
    else {
        $c->log->debug("Interface does not require client certificates");
    }

    unless ($interface->active) {
        $c->stash->{soap}->fault(
           {
               code     => '404',
               reason   => 'Not found',
               detail   => 'Given interface not active'
           }
        );
        return;
    }

    my $xml = $c->stash->{soap}->envelope();

    my $transaction    = $interface->process({
        input_data     => $xml,
        transaction_id => 'unknown'
    });

    unless ($transaction->transaction_records->count) {
        $c->stash->{soap}->fault(
           {
               code   => '500',
               reason => 'No response from Sysin',
               detail => 'Error from Sysin, see logfiles for transaction id: '
                            . $transaction->id
           }
        );

        return;
    }

    my $record = $transaction->transaction_records->first;

    if ($record->is_error) {
        $c->stash->{soap}->fault(
           {
               code   => '500',
               reason => 'Failed processing record, see logfile.',
               detail => 'Transaction id: ' . $transaction->id
           }
        );
    }

    $c->forward('/touch_delayed_cases');

    my $output = $record->output;

    if (defined $output && length($output) > 0) {
        my $responsexml = XML::LibXML->load_xml(
            string => $output
        );
        $c->stash->{soap}->literal_return($responsexml->documentElement());
    }
    else {
        $c->stash->{soap}->string_return("Request processed");
    }

    return;
}

=head2 assert_authorized_client

Asserts that the client certificate supplied by the client is the same as the
one configured in the interface.

=cut

sub assert_authorized_client : Private {
    my ($self, $c) = @_;

    my $interface = $c->stash->{entry};
    my $module = $interface->module_object;

    my $dn                 = $c->engine->env->{SSL_CLIENT_S_DN} || $c->req->header('x-client-ssl-s-dn') // '';
    my $client_fingerprint = $c->engine->env->{SSL_CLIENT_FINGERPRINT} || $c->req->header('x-client-ssl-fingerprint') // '';

    my $certificate_match = try {
        $module->verify_client_certificate(
            client_fingerprint => $client_fingerprint,
            dn                 => $dn,

            get_certificate_cb => sub {
                my $certificate = $c->model('DB::Filestore')->find(
                    $interface->jpath('$.client_certificate[0].id')
                );

                return $certificate;
            },
        );
    } catch {
        $c->stash->{soap}->fault(
            {
                code => 403,
                reason => 'Forbidden',
                detail => "Client certificate verification failed: $_",
            },
        );
        $c->detach();
    };

    if (!$certificate_match) {
        $c->stash->{soap}->fault(
            {
                code => 403,
                reason => 'Forbidden',
                detail => 'Client certificate verification failed.',
            },
        );
        $c->detach();
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
