package Zaaksysteem::Roles::UStore;
use Moose::Role;

use File::UStore;

has ustore => (
    isa     => 'File::UStore',
    lazy    => 1,
    is      => 'ro',
    default => sub {
        my $self = shift;
        return File::UStore->new(
            path   => $self->result_source->schema->storage_path,
            prefix => 'zs_',
            depth  => 5,
        );
    }
);

1;

__END__

=head1 NAME

Zaaksysteem::Roles::UStore

=head1 DESCRIPTION

=head1 SYNOPSIS

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
