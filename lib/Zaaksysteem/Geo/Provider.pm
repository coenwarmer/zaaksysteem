package Zaaksysteem::Geo::Provider;

use Moose::Role;

=head1 NAME

Zaaksysteem::Geo::Provider - Interface for Geo service providers

=head1 DESCRIPTION

This L<role|Moose::Meta::Role> specifies an interface Geo service providers
must implement, so the L<Zaaksysteem::Geo::Model> can use them.

=cut

requires qw[geocode reverse_geocode];

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
