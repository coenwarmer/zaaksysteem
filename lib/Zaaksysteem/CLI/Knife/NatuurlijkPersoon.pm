package Zaaksysteem::CLI::Knife::NatuurlijkPersoon;
use Moose::Role;
use Zaaksysteem::CLI::Knife::Action;
use Zaaksysteem::Tools;

my $knife = 'subject';

=head1 NAME

Zaaksysteem::CLI::Knife::NatuurlijkPersoon - NatuurlijkPersoon CLI actions

=cut

register_knife $knife => (
    description => "Subject related functions"
);

register_category natuurlijk_persoon => (
    knife       => $knife,
    description => "Natuurlijk persoon"
);

register_action list => (
    knife       => $knife,
    category    => 'natuurlijk_persoon',
    description => 'List natuurlijk personen',
    run         => sub {
        my $self = shift;
        my (@params) = @_;

        my $rs = $self->schema->resultset('NatuurlijkPersoon')->search_rs({});
        while (my $np = $rs->next) {
            $self->log->info($np->bsn);
        }
    }
);

register_action delete => (
    knife       => $knife,
    category    => 'natuurlijk_persoon',
    description => 'Search controlpanels',
    run         => sub {
        my $self = shift;
        my (@params) = @_;

        my $params = $self->assert_knife_params(
            bsn => 'Str'
        );

        my $np = $self->_get_np_by_bsn($params->{bsn});
        $self->delete_np($np);

    }
);

sig delete_np => 'Zaaksysteem::Model::DB::NatuurlijkPersoon';

sub delete_np {
    my ($self, $np) = @_;

    my $now = DateTime->now();
    $self->log->info(sprintf("Deleting natuurlijk persoon with BSN %s [%d]", $np->bsn, $np->id));
    if ($np->authenticated) {
        # There should only be one, but just in case.. delete everything in
        # sight. We shouldn't die when deleting with multiple object
        # subscriptions, it is wrong anyway
        my $rs = $self->schema->resultset('ObjectSubscription')->search_rs({
            local_id     => $np->id,
            local_table  => 'NatuurlijkPersoon',
            date_deleted => undef,
        });
        if ($rs->first) {
            $self->log->info("Deleting object subscriptions " . join(", ", $rs->get_column('id')->all));
            $rs->update({date_deleted => $now});
        }
    }
    $np->update({deleted_on => $now});
}

sig _get_np_by_bsn => 'Str';

sub _get_np_by_bsn {
    my ($self, $bsn) = @_;

    my $rs = $self->schema->resultset('NatuurlijkPersoon')->search_rs({burgerservicenummer => $bsn, deleted_on => undef});
    my $entry = $rs->first;
    if ($entry) {
        if ($rs->next) {
            throw("natuurlijk_persoon/multiple", "Multiple entries found for BSN $bsn");
        }
        return $entry;
    }
    else {
        throw('natuurlijk_persoon/not_found', "No entries found for BSN $bsn");
    }
}

1;

=head1 SEE ALSO

L<Zaaksysteem::CLI::Knife>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
