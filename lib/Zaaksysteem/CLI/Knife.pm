package Zaaksysteem::CLI::Knife;

use Moose;

extends qw/Zaaksysteem::CLI::Knife::Init/;

with qw/
    Zaaksysteem::CLI::Knife::Controlpanel
    Zaaksysteem::CLI::Knife::NatuurlijkPersoon
    Zaaksysteem::CLI::Knife::Object
    Zaaksysteem::CLI::Knife::BAG
/;

around run => sub {
    my $orig = shift;
    my $self = shift;

    $self->$orig();

    $self->assert_actions;

    $self->do_transaction(
        sub {
            my ($self, $schema) = @_;

            $self->run_action;
        }
    );
    return 1;
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::CLI::Knife - A handy set of tools for altering zaaksysteem content

=head1 SYNOPSIS

For building a custom Knife, see for example L<Zaaksysteem::CLI::Knife::Controlpanel>, or use the below example:

    package Zaaksysteem::CLI::Knife::Book;

    use Moose::Role;        ## Make sure you place this role in the with statement of L<Zaaksysteem::CLI::Knife>

    register_knife      book => (
        description => "Special knife for book related actions"
    );

    register_category   'author' => (
        knife           => 'book',
        description     => "Author functions below book"
    );

    register_action     'update' => (
        knife           => 'book',
        category        => 'author',
        description     => 'Update the author of this book',
        run             => sub {
            my $self    = shift;
            my @params  = @_;

            $self->book->author($params[0]);
        }
    );

    register_action     'update' => (
        knife           => 'book',
        category        => 'title',
        description     => 'Update the title of this book',
        run             => sub {
            my $self    = shift;
            my @params  = @_;

            $self->book->title($params[0]);
        }
    );

    register_action     'search' => (
        knife           => 'book',
        category        => 'author',
        description     => 'Search authors',
        run             => sub {
            my $self    = shift;
            my @params  = @_;

            my %search  = $self->get_knife_params;

            my $rs      = $self->book->author->search(\%search);

            while (my $author = $rs->next) {
                print $author->name . "\n";
            }
        }
    );

Examples:

    ### Updates
    bin/zsknife --hostname mintlab.zaaksysteem.nl book author update 'Frits de Bie'

    bin/zsknife --hostname mintlab.zaaksysteem.nl book title update 'Question 42'

    ### Search
    bin/zsknife --hostname mintlab.zaaksysteem.nl book author search name="Frits de Bie"

=head1 POSSIBLE USECASES

You could do some funky shell loops for updating various objects. For example: update the attribute customer_type
of all objects of the type controlpanel:

for my UUID in `zsknife object attr search controlpanel uuid`; do
    zsknife object attr update $UUID customer_type=government
done;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
