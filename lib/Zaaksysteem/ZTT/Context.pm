package Zaaksysteem::ZTT::Context;
use Moose::Role;

requires 'get_string_fetchers';

=head1 NAME

Zaaksysteem::ZTT::Context - Role/interface for ZTT contexts

=head1 SYNOPSIS

    package MyZTTContext;
    use Moose;
    with 'Zaaksysteem::ZTT::Context';

    sub get_string_fetchers {
        # return string fetcher s
    }

    sub get_context_iterators {
        # Return "sub-contexts" for iterating parts
    }

=cut

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
