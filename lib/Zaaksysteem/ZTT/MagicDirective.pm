package Zaaksysteem::ZTT::MagicDirective;

use Moose;

use Parse::RecDescent;

use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::ZTT::MagicDirective - Parse augmented magic-string syntax

=head1 DESCRIPTION

This package abstracts the syntax used in ZTT templates to render
'magic strings'.

    my $parser = Zaaksysteem::ZTT::MagicDirective->new;

    $parser->parse('case.id');
    # {
    #   expression => \'case.id'
    # }

    $parser->parse('case.id | link_to_case');
    # {
    #   expression => \'case.id',
    #   filter => {
    #     name => 'link_to_case',
    #     args => []
    #   }
    # }

    $parser->parse('case.id | link_to(case, "Zaak %d")');
    # {
    #   expression => \'case.id',
    #   filter => {
    #     name => 'link_to',
    #     args => [ 'Zaak %d' ]
    #   }
    # }

For a better description of the return value of this call, see L</parse>.

=head1 ATTRIBUTES

=head2 parser

This attribute holds a reference to the L<Parse::RecDescent> object used to
parse directives.

=cut

has parser => (
    is => 'rw',
    isa => 'Parse::RecDescent',
    default => sub {
        # $::RD_HINT = 1;
        # $::RD_WARN = 1;
        # $::RD_TRACE = 1;

        return Parse::RecDescent->new(MAGIC_DIRECTIVE_GRAMMAR->());
    }
);

=head1 CONSTANTS

=head2 MAGIC_DIRECTIVE_GRAMMAR

This constant is the grammar that will be fed to L<Parse::RecDescent>

=cut

use constant MAGIC_DIRECTIVE_GRAMMAR => <<'EOG';
{
    # Treebuilder helper. This sub will be available only in the grammer
    # below. If someone has a non-nasty way of embedding this
    # programmatically, have at it.
    sub treeify {
        my $t = shift;

        $t = [ shift, $t, shift ] while scalar @_;

        return $t;
    }
}

# Main production of magic directive grammar
magic_directive : expression filter(?) /\Z/ {
    $return = {
        # Unpack matching expression, prevent needless wrapping
        %{ $item{ expression } },

        filter => $item{ 'filter(?)' }[0],
    };
}

expression : /iter(?:eer|ate)\:/i bareword /\:/ bareword {
    $return = {
        # Singular string by itself is a valid expression, that evaluates
        # to itself.
        expression => \lc($item[4]),
        iterate_context => lc($item[2])
    };
}

expression : binary_expr {
    $return = {
        expression => $item{ binary_expr }
    };
}

binary_expr : <leftop: term /([+-])/ term> {
    $return = treeify(@{ $item[1] });
}

term : <leftop: factor /([*\/])/ factor> {
    $return = treeify(@{ $item[1] });
}

factor : number | call | string | binary_sub_expr

factor : bareword {
    $return = \$item[1];
}

binary_sub_expr : '(' <commit> binary_expr ')' {
    $return = $item{ binary_expr };
}

call : bareword '(' binary_expr(s? /,/) ')' {
    $return = {
        function => lc($item[1]),
        arguments => $item[3]
    }
}

number : /[+-]?\d+(?:\.\d+)?/

bareword : /[\w\.]+/

# A filter begins with a pipe, followed by a filtername and perhaps arguments
filter : "|" bareword filter_args(?) {
    $return = {
        name => lc($item{ bareword }),
        args => $item{ 'filter_args(?)'}[0] || []
    }
}

# Arguments appear between the '(' and ')' grouping chars, returns an
# arrayref of the arguments
filter_args : '(' arg(s? /,/) ')' {
    $return = $item[2]
}

# An argument can be bareword, or a quoted string (for space inclusion)
# Arguments are positional, no key-value-pair crud here pls
arg : bareword | string

# Non-greedily match all chars between two '"' chars, skip over escaped '\"'
# return the matched string sans quote chars
string : /"(.*?)(?<!\\)"/ {
    $return = $1
}
EOG

=head1 METHODS

=head2 parse

This method takes a string and attempts to parse the string as a magic
directive. The return-value is a hashref with the sanitized data.

    my $directive = Zaaksysteem::ZTT::MagicDirective->parse('...');

The returned hashref will look something like this:

    {
        attribute => '...',

        # Other keys are optional
        iterate_context => '...',

        filter => {
            name => '...'
            args => [       # Args will always be strings
                '...',
                '...'
            ]
        }
    }

=cut

sub parse {
    my $self = shift;
    my $text = shift;

    my $data;

    # No need to catch, on failure return implicit undef.
    try {
        $data = $self->parser->magic_directive($text);
    };

    # Early silent return on parse-errors
    unless (defined $data && $data->{ expression }) {
        return;
    }

    # Strip empty fields for API compliance
    for my $key (keys %{ $data }) {
        delete $data->{ $key } unless $data->{ $key };
    }

    return $data;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 KNOWN BUGS

May summon the Dark Lord, Cthulhu

     Ph'nglui mglw'nafh Cthulhu R'lyeh wgah'nagl fhtagn

                            .....
                       .d$$$$*$$$$$$bc
                    .d$P"    d$$    "*$$.
                   d$"      4$"$$      "$$.
                 4$P        $F ^$F       "$c
                z$%        d$   3$        ^$L
               4$$$$$$$$$$$$$$$$$$$$$$$$$$$$$F
               $$$F"""""""$F""""""$F"""""C$$*$
              .$%"$$e    d$       3$   z$$"  $F
              4$    *$$.4$"        $$d$P"    $$
              4$      ^*$$.       .d$F       $$
              4$       d$"$$c   z$$"3$       $F
               $L     4$"  ^*$$$P"   $$     4$"
               3$     $F   .d$P$$e   ^$F    $P
                $$   d$  .$$"    "$$c 3$   d$
                 *$.4$"z$$"        ^*$$$$ $$
                  "$$$$P"             "$$$P
                    *$b.             .d$P"
                      "$$$ec.....ze$$$"
                          "**$$$**""

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
