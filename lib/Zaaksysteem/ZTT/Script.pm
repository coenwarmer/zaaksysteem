package Zaaksysteem::ZTT::Script;

use Moose;

use Parse::RecDescent;

use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::ZTT::Script - Parse zttscripts

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 parser

This attribute holds a reference to the L<Parse::RecDescent> object used to
parse directives.

=cut

has parser => (
    is => 'rw',
    isa => 'Parse::RecDescent',
    default => sub {
        # $::RD_HINT = 1;
        # $::RD_WARN = 1;
        # $::RD_TRACE = 1;

        return Parse::RecDescent->new(ZTT_SCRIPT_GRAMMAR->());
    }
);

=head1 CONSTANTS

=head2 ZTT_SCRIPT_GRAMMAR

This constant is the grammar that will be fed to L<Parse::RecDescent>

=cut

use constant ZTT_SCRIPT_GRAMMAR => <<'EOG';
# Main production of ztt script grammar
script : command /\Z/ {
    $return = $item { command }
}

command : cmd_iterate
        | cmd_show_when

cmd_iterate : /iterate/i bareword {
    $return = {
        iterate => lc($item{ bareword }{ value })
    }
}

cmd_show_when : /show_when/i '{' conditional '}' {
    $return = {
        show_when => $item{ conditional }
    }
}

conditional : operand infix_operator operand {
    $return = {
        operands => [ $item[1], $item[3] ],
        operator => $item[2]
    }
}

operand : bareword
        | string

infix_operator : "=="
               | "!="

bareword : /[\w\.]+/ {
    $return = {
        type => 'bareword',
        value => $item[1]
    }
}

# Non-greedily match all chars between two '"' chars, skip over escaped '\"'
# return the matched string sans quote chars
string : /"(.*?)(?<!\\)"/ {
    $return = {
        type => 'string',
        value => $1
    }
}
EOG

=head1 METHODS

=head2 parse

This method takes a string and attempts to parse the string as a ztt script.
The return-value is a list of hashrefs with the sanitized data.

    my @productions = Zaaksysteem::ZTT::Script->parse('...');

The returned hashref will look something like this:

    (
        # script line: iterate zaak_relaties
        { iterate => 'zaak_relaties' },

        # script line: show_when { aanvrager_type == "natuurlijk_persoon" }
        {
            show_when => {
                operator => '==',
                operands => [
                    { type => 'bareword', value => 'aanvrager_type' },
                    { type => 'string', value => 'natuurlijk_persoon' }
                ]
            }
        }
    )
    

=cut

sub parse {
    my $self = shift;
    my $text = shift;

    my @productions;

    for my $line (split m[\n], $text) {
        push @productions, try {
            return $self->parser->script($line);
        } catch {
            return {
                error => sprintf("Could not parse line '%s'", $line)
            }
        };
    }

    return @productions;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
