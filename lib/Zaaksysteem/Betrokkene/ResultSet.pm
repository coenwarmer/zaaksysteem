package Zaaksysteem::Betrokkene::ResultSet;

use strict;
use warnings;

use Moose;

has opts    => (
    is      => 'rw',
);

has dbic_rs => (
    is      => 'rw',
);

has ldap_rs => (
    'is'    => 'rw',
);

has class   => (
    'is'    => 'rw',
);

has [qw/prod log dbic stash config customer/] => (
    'weak_ref' => 1,
    'is'    => 'ro',
);

has '_dispatch_options' => (
    'is'    => 'ro',
    'lazy'  => 1,
    'default'   => sub {
        my $self    = shift;

        my $dispatch = {
            prod    => $self->prod,
            log     => $self->log,
            dbic    => $self->dbic,
            stash   => $self->stash,
            config  => $self->config,
            customer => $self->customer,
        };

        Scalar::Util::weaken($dispatch->{stash});

        return $dispatch;
    }
);

has pointer => (
    'is'    => 'rw',
);


sub next {
    my ($self) = @_;

    return $self->_next_dbic;
}

sub first {
    my ($self) = @_;

    $self->reset;
    return $self->next;
}

sub reset {
    my ($self) = @_;

    if (
        $self->opts->{type} eq 'org_eenheid'
    ) {
        return $self->pointer(0);
    } else {
        return $self->dbic_rs->next;
    }
}

sub count {
    my ($self) = @_;

    if (
        $self->opts->{type} eq 'org_eenheid'
    ) {
        return scalar(@{ $self->ldap_rs });
    } else {
        return $self->dbic_rs->count;
    }
}

sub _next_dbic {
    my ($self) = @_;

    my $bclass = $self->class;

    my $record = $self->dbic_rs->next;

    return unless $record;

    my $record_id;
    if (
        $self->opts->{type} eq 'natuurlijk_persoon'
    ) {
        my $first_person;
        if (
            !$record->natuurlijk_persoons ||
            !($first_person = $record->natuurlijk_persoons->first)
        ) {
            return $self->_next_dbic;
        }

        if ($first_person) {
            $record_id = $first_person->id if $first_person;
            $record    = $first_person;
        }
    } else {
        $record_id = $record->id;
    }

    my $object;

    eval {
        $object = $bclass->new(
            trigger => 'get',
            id      => $record_id,
            record  => $record,
            %{ $self->opts },
            %{ $self->_dispatch_options },
        );
    };

    if ($@) {
        my $errmsg = 'Error opening this record, looping to next: ' . $@;
        $self->c->log->error($errmsg);
        return $self->_next_dbic;
    }

    return $object;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 count

TODO: Fix the POD

=cut

=head2 first

TODO: Fix the POD

=cut

=head2 next

TODO: Fix the POD

=cut

=head2 reset

TODO: Fix the POD

=cut

