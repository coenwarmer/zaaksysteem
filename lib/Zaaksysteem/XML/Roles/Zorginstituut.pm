package Zaaksysteem::XML::Roles::Zorginstituut;
use Moose::Role;

around _build_predefines => sub {
    my $orig = shift;
    my $self = shift;

    my $retval = $self->$orig;

    $retval->{convert_boolean}    = \&convert_to_boolean;
    $retval->{convert_dt_to_date} = \&convert_dt_to_date;
    $retval->{convert_dt_to_time} = \&convert_dt_to_time;
    return $retval;
};

=head2 convert_to_boolean

Convert the perl-like boolean to a WMO like boolean.

=cut

sub convert_to_boolean {
    my $val = shift;
    return 2 if !$val;
    return 1;
}

=head2 convert_dt_to_date

Convert a datetime object or a scalar to a WMO date.

=cut


sub convert_dt_to_date {
    return _convert_dt_to_custom('%F', shift);
}

=head2 convert_dt_to_time

Convert a datetime object or a scalar to a WMO timestamp.
WMO does not like twelve o' clock, so we adjust it to twelve o' clock
plus one second.

=cut

sub convert_dt_to_time {
    my $str = _convert_dt_to_custom('%T', shift);
    # 00:00:00 is not allowed for some reason.
    if ($str eq '00:00:00') {
        return '00:00:01';
    }
    return $str;
}

sub _convert_dt_to_custom {
    my ($pattern, $val) = @_;

    if (!ref $val) {
        my $dtf = DateTime::Format::Strptime->new(pattern => '%d-%m-%Y');
        $val = $dtf->parse_datetime($val);
    }
    my $dtf = DateTime::Format::Strptime->new(
        pattern   => $pattern,
        locale    => 'nl_NL',
        time_zone => 'Europe/Amsterdam',
    );
    return $dtf->format_datetime($val);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
