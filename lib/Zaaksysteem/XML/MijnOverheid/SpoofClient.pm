package Zaaksysteem::XML::MijnOverheid::SpoofClient;
use Moose;
use namespace::autoclean;

use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::XML::MijnOverheid::SpoofClient - MijnOverheid fake client for testing

=head1 SYNOPSIS

    my $client = Zaaksysteem::XML::MijnOverheid::SpoofClient->new();

    $client->has_berichtenbox(bsn => '012345678');
    # Always returns true values/success for every call.

=head2 has_berichtenbox

Fake a "has_berichtenbox" call. Always returns true.

=cut

sub has_berichtenbox {
    my $self = shift;

    print STDERR "has_berichtenbox called in SPOOF MODE\n";

    return {
        has_berichtenbox => 1,
    };
}

=head2 lopende_zaak

Fake a "lopende zaak" update call. Always succeeds.

=cut

sub lopende_zaak {
    my $self = shift;

    print STDERR "lopende_zaak called in SPOOF MODE\n";

    return (
        { accepted => 1 },
        "Return XML goes here (lopende zaak)"
    );
}

=head2 berichtenbox_message

Fake a "berichtenbox" update call. Always succeeds.

=cut

sub berichtenbox_message {
    my $self = shift;

    print STDERR "berichtenbox_message called in SPOOF MODE\n";

    return (
        { accepted => 1 },
        "Return XML goes here (berichtenbox)"
    );
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
