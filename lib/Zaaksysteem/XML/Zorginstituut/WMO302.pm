package Zaaksysteem::XML::Zorginstituut::WMO302;
use Moose;

extends 'Zaaksysteem::XML::Zorginstituut::Reader';

=head1 NAME

Zaaksysteem::XML::Zorginstituut::WMO302 - An Zorginstituut WMO302 reader for ZS

=head1 SYNOPSIS

    use Zaaksysteem::XML::Zorginistituut::WMO302;

    my $reader = Zaaksysteem::XML::Zorginistituut::WMO302->new(
        xml => $xml_string,
    );

    $reader->is_valid();
    my $datastructure = $reader->show_invalid();

=head1 DESCRIPTION

This module will read a WMO 302 messages and tell you if the message says
"All ok" or "Something is wrong". If the message is incorrect, you will
need to resend the WMO301 with the correct data.

=cut

has '+ns' => (default => 'wmo302');

around _xpath_builder => sub {
    my $orig = shift;
    my $self = shift;

    my $xp = $self->$orig(@_);

    $xp->registerNs('wmo',
        'http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0');
    $xp->registerNs('wmo302',
        'http://www.istandaarden.nl/iwmo/2_0/wmo302/schema/2_0');

    return $xp;
};


sub _build_message_mapping {

    return {
        header  => { xpath => '/wmo302:Bericht/wmo302:Header', optional => 1 },
        clienten => { xpath => '/wmo302:Bericht/wmo302:Clienten', optional => 1 },
        client  => { xpath => '/wmo302:Bericht/wmo302:Clienten/wmo302:Client' },
        relatie => {
            xpath => '/wmo302:Bericht/wmo302:Clienten/wmo302:Client/wmo302:Relaties/wmo302:Relatie',
            optional => 1,
        },
        relatie_adres => {
            xpath => '/wmo302:Bericht/wmo302:Clienten/wmo302:Client/wmo302:Relaties/wmo302:Relatie/wmo302:Adres',
            optional => 1,
        },
        adres_adres => {
            xpath => '/wmo302:Bericht/wmo302:Clienten/wmo302:Client/wmo302:Adressen/wmo302:Adres',
        },
        beschikking => {
            xpath => '/wmo302:Bericht/wmo302:Clienten/wmo302:Client/wmo302:Beschikking',
        },
        beperking => {
            xpath    => '/wmo302:Bericht/wmo302:Clienten/wmo302:Client/wmo302:Beschikking/wmo302:Beperkingen/wmo302:Beperking',
            optional => 1,
            multiple => 1,
        },
        beperkingscore => {
            xpath => '/wmo302:Bericht/wmo302:Clienten/wmo302:Client/wmo302:Beschikking/wmo302:Beperkingen/wmo302:Beperking/wmo302:BeperkingScores/wmo302:BeperkingScore',
            optional => 1,
            multiple => 1,
        },
        beperkingscore_commentaarregels => {
            xpath => '/wmo302:Bericht/wmo302:Clienten/wmo302:Client/wmo302:Beschikking/wmo302:Beperkingen/wmo302:Beperking/wmo302:BeperkingScores/wmo302:BeperkingScore/wmo302:Commentaarregels',
            optional => 1,
            multiple => 1,
        },
        beperking_commentaarregels => {
            xpath => '/wmo302:Bericht/wmo302:Clienten/wmo302:Client/wmo302:Beschikking/wmo302:Beperkingen/wmo302:Beperking/wmo302:Commentaarregels',
            optional => 1,
            multiple => 1,
        },
        beschiktproduct => {
            xpath => '/wmo302:Bericht/wmo302:Clienten/wmo302:Client/wmo302:Beschikking/wmo302:BeschiktProducten/wmo302:BeschiktProduct',
            multiple => 1,
        },
        beschiktproduct_commentaarregels => {
            xpath => '/wmo302:Bericht/wmo302:Clienten/wmo302:Client/wmo302:Beschikking/wmo302:BeschiktProducten/wmo302:BeschiktProduct/wmo302:Commentaarregels',
            optional => 1,
            multiple => 1,
        },
        toegewezenproduct => {
            xpath => '/wmo302:Bericht/wmo302:Clienten/wmo302:Client/wmo302:Beschikking/wmo302:ToegewezenProducten/wmo302:ToegewezenProduct',
            multiple => 1,
        },
        beschikking_commentaarregels => {
            xpath => '/wmo302:Bericht/wmo302:Clienten/wmo302:Client/wmo302:Beschikking/wmo302:Commentaarregels',
            optional => 1,
            multiple => 1,
        },
        client_commentaarregels => {
            xpath => '/wmo302:Bericht/wmo302:Clienten/wmo302:Client/wmo302:Commentaarregels',
            optional => 1,
            multiple => 1,
        },
    };
}

=head2 beschikkingsnummer

Returns value for the C<beschikkingsnummer> node of the embedded message.

=cut

sub beschikkingsnummer {
    my $self = shift;
    return $self->xpath->findvalue('/wmo302:Bericht/wmo302:Clienten/wmo302:Client/wmo302:Beschikking/wmo302:Beschikkingnummer');
}


=head2 berichtidentificatienummer

Returns value for the C<berichtidentificatienummer> node of the embedded message.

=cut

sub berichtidentificatienummer {
    my $self = shift;
    return $self->xpath->findvalue('/wmo302:Bericht/wmo302:Header/wmo302:Berichtidentificatie/wmo:Identificatie');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
