package Zaaksysteem::XML::Generator::iJW::2_0;
use Moose;

with qw(
    Zaaksysteem::XML::Generator
    Zaaksysteem::XML::Roles::Zorginstituut
);


use Zaaksysteem::Tools;
use DateTime::Format::Strptime;

=head1 NAME

Zaaksysteem::XML::Generator::iJW::2_0 - Definitions for iJW 2.0 XML templates

=head1 SYNOPSIS

    use Zaaksysteem::XML::Generator::iJW::2_0;

    my $generator = Zaaksysteem::XML::Generator::iJW::2_0->new();

    # Returns and accepts character strings, not byte strings!
    $xml_data = $generator->build_ijw301(\%arguments);

    # One method is created for every .xml file in the template directory.

=head1 METHODS

=head2 name

Short name for this module, for use as the accessor name in
L<Zaaksysteem::XML::Compile::Backend>.

=cut

sub name { return 'ijw2_0' }

=head2 path_prefix

Path (under share/xml-templates) to search for . "ijw2_0".

=cut

sub path_prefix {
    return 'ijw2_0';
}

__PACKAGE__->build_generator_methods();

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
