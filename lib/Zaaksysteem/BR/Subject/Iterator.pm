package Zaaksysteem::BR::Subject::Iterator;

use Moose;

extends qw/Zaaksysteem::Object::Iterator/;

with qw/
    Zaaksysteem::BR::Subject::Utils
    Zaaksysteem::BR::Subject::Iterator::Person
    Zaaksysteem::BR::Subject::Iterator::Company
    Zaaksysteem::BR::Subject::Iterator::ObjectSubscription
/;

=head1 NAME

Zaaksysteem::BR::Subject::Iterator - Simple Subject-Object inflating Iterator.

=head1 SYNOPSIS

    my $iterator    = Zaaksysteem::BR::Subject::Iterator->new(
        rs                      => $rs,
        inflator                => sub {
            shift->as_object
        },
        cache_addresses         => 1,
        cache_below_num_rows    => 50,
    );

    $iterator->next; $iterator->next;
    
    $iterator->first;
    $iterator->search({}, {});
    $iterator->reset;

=head1 ATTRIBUTES

=head2 rs

A L<DBIx::Class> abstracted iterator. See L<DBIx::Class::ResultSet> for more documentation about
the possibilies. Methods implemented are C<reset,count,pager,is_paged,all,search,first,next>

=cut

has rs => (
    is       => 'rw',
    isa      => 'DBIx::Class::ResultSet',
    required => 1,
    handles => {
        count => 'count',
        reset => 'reset',
        pager => 'pager',
        is_paged => 'is_paged',
    }
);

=head2 cache_addresses

When using a different table for address (C<Adres>), then cache these results so we can reduce the
number of database queries when a pager is set.

=cut

has cache_related_rows => (
    is      => 'rw',
    isa     => 'Bool',
    default => 1,
);


=head2 cache_below_num_rows

B<Default>: 50

Do not cache addresses when the pager size is greater than than C<cache_below_num_rows> rows.

=cut

has cache_below_num_rows => (
    is      => 'rw',
    isa     => 'Int',
    default => 50,
);


=head2 _in_cache_loop

Private attr for determining whether the cache is already filled

=cut

has _in_cache_loop => (
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { {} },
    clearer => '_clear_cache',
);



=head1 METHODS

=head2 search

Will call the L<DBIx::Class::ResultSet->search> method, and throws this iterator around it

=cut

sub search {
    my $self            = shift;
    my $params          = shift;

    my $rs              = $self->_search_via_preferred($self->rs, $params, @_);

    return $self->clone(rs => $rs);
}

=head2 search_rs

Will call the L<DBIx::Class::ResultSet->search> method, and throws this iterator around it

=cut

sub search_rs {
    my $self            = shift;

    return $self->search(@_);
}

=head2 next

Will call the L<DBIx::Class::ResultSet->next> method, and inflates it according to the subref set in
attribute C<inflator>

=cut

sub next {
    my $self = shift;

    $self->_cache_related_rows();

    my $next = $self->rs->next;

    if (!$next) {
        ### Reset cache
        $self->_clear_cache;
        return;
    }

    $self->_apply_cache_on_row($next);

    return $self->inflate($next);
}

=head2 first

Will call the L<DBIx::Class::ResultSet->first> method, and inflates it according to the subref set in
attribute C<inflator>

=cut


sub first {
    my $self    = shift;
    $self->rs->reset;

    return $self->next;
}

=head1 PRIVATE METHODS

=head2 _cache_is_setup

Will return true when this iterator is able to cache addresses

=cut

sub _cache_is_setup {
    my $self            = shift;

    return unless (
        $self->cache_related_rows &&
        $self->rs->{attrs}->{rows} &&
        $self->rs->{attrs}->{rows} <= $self->cache_below_num_rows
    );

    return 1;
}

=head2 clone

This method returns a deep-copy clone of the iterator instance.

Parameters to C<new> will default to the values of the invocant, but can be
overridden by passing them to clone directly.

=cut

sub clone {
    my ($self, %params) = @_;

    $params{ rs }       //= $self->rs->search;

    $params{ $_ }       //= $self->$_ for qw/cache_related_rows cache_below_num_rows inflator/;

    return $self->meta->new_object(%params);
}


=head2 _cache_addresses

=cut

sub _cache_related_rows {}

=head2 _inflate_address_in_row

Will inflate the given L<DBIx::Class::Row> with cached addresses

=cut

sub _apply_cache_on_row {}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

