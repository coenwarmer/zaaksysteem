package Zaaksysteem::BR::Subject;

use Moose;
use Scalar::Util qw/blessed/;

use Zaaksysteem::Tools;
use Zaaksysteem::Profile;
use Data::FormValidator;
use Zaaksysteem::Types qw(SubjectType NLZipcode);

=head1 NAME

Zaaksysteem::BR::Subject - This is a bridge between new and old style creation of subjects.

=head1 DESCRIPTION

This module is an abstraction between old and new style creation. This module is in use by the 
JSON API for subject creation.

At this time using the old Betrokkene.pm infrastructure, but can be rewritten to use a new future
basis.

NOTE: This module is designed to be used outside of catalyst, make sure it stays this way...

=head1 SYNOPIS

    ### Creating a subject
    my $subject     = Zaaksysteem::BR::Subject->new(
        schema          => $schema,
        log             => Log::Log4perl->get_logger('Zaaksysteem::BR::Subject'),
        subject_type    => 'bedrijf',
    );

    my $params      = {
        'handelsnaam'                       => 'Gemeente Zaakstad',
        'dossiernummer'                     => '25654589',
        'vestigingsnummer'                  => '123456789012',
        'vestiging_landcode'                => '6030',
        'vestiging_straatnaam'              => 'Zaakstraat',
        'vestiging_huisnummer'              => 44,
        'vestiging_huisletter'              => 'a',
        'vestiging_huisnummertoevoeging'    => '1rechts',
        'vestiging_postcode'                => '1234AB',
        'vestiging_woonplaats'              => 'Amsterdam',

        'password'                          => 'Test123',
    };

    my $rv          = $subject->save($params);

    ### Editing a subject

=head1 ATTRIBUTES

=head2 schema

A L<Zaaksysteem::Schema> object. Required.

=cut

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

=head2 log

Central logger, make sure you set this to a log instance which has at least the following
accessors:

    debug
    info
    error
    warn

=cut

has log => (
    is          => 'rw',
    required    => 1,
);

=head2 subject_type

Type: SubjectType

The subjecttype, required.

e.g.: natuurlijk_persoon / bedrijf / medewerker

=cut

has subject_type => (
    is          => 'rw',
    isa         => SubjectType,
    required    => 1,
);

=head2 id

The identifier for the row, when there is one.

=cut

has id          => (
    is          => 'rw',
    isa         => 'Int',
);

=head2 authenticated

Whether this subject is an authenticated, validated by GBA or DigiD, subject

=cut

has authenticated => (
    is          => 'rw',
    isa         => 'Boolean',
    predicate   => 'has_authenticated',
);

=head2 authenticatedby

How this subject is authenticated, e.g.: "gba", "digid", "behandelaar"

=cut

has authenticatedby => (
    is          => 'rw',
    isa         => 'Str',
    predicate   => 'has_authenticatedby',
);


=head2 deleted

Date this subject became a deleted subject

=cut

has deleted          => (
    is          => 'rw',
    isa         => 'DateTime',
    predicate   => 'has_deleted',
);


=head2 password

Subjecttype "bedrijf" only, bedrijvenID

=cut

has password    => (
    is          => 'rw',
    isa         => 'Str',
);

=head2 telephonenumber

=cut

has telephonenumber    => (
    is          => 'rw',
    isa         => 'Str',
    predicate   => 'has_telephonenumber',
);

=head2 mobilenumber

=cut

has mobilenumber       => (
    is          => 'rw',
    isa         => 'Str',
    predicate   => 'has_mobilenumber',
);

=head2 email

=cut

has email              => (
    is          => 'rw',
    isa         => 'Str',
    predicate   => 'has_email',

);

=head2 subject_type_settings

Defines the different profiles for every subject type. Only company supported right now.

Also contains capabilities, like bedrijf_authenticatie, contact settings etc.

=cut
use constant SUBJECT_TYPE_SETTINGS => {
    bedrijf     => {
        profile     => {
            required    => {
                dossiernummer                           => sub {
                    my ($dfv, $val) = @_;

                    return unless $val =~ /^\d{7,9}$/;

                    ### Check database for existing entry
                    my $rv = ($dfv->{__INPUT_DATA}->{_schema}->resultset('Bedrijf')->search(
                        {
                            "NULLIF(dossiernummer,'')::bigint" => $val,
                            $dfv->get_filtered_data->{vestigingsnummer} ? (vestigingsnummer => $dfv->get_filtered_data->{vestigingsnummer}) : (),
                            deleted_on => undef,
                        }
                    )->count) ? 0 : 1;

                    if (!$rv) {
                        $dfv->{_custom_messages}->{dossiernummer} = 'Dossiernumber already in use';
                        $dfv->{_custom_messages}->{vestigingsnummer} = 'Vestigingsnumber already in use';
                    }

                    return $rv;
                },
                handelsnaam                             => 'Str',
                vestigingsnummer                        => 'Int',
            },
            optional    => {
                rechtsvorm                              => 'Int',
                vestiging_landcode                      => 'Int', ### TODO Restrict these
                vestiging_adres                         => 'Str',
                vestiging_adres_buitenland1             => 'Str',
                vestiging_adres_buitenland2             => 'Str',
                vestiging_adres_buitenland3             => 'Str',
                vestiging_straatnaam                    => 'Str',
                vestiging_huisnummer                    => 'Int',
                vestiging_huisletter                    => 'Str', ### TODO Restrict these
                vestiging_huisnummertoevoeging          => 'Str', ### TODO Restrict these
                vestiging_postcode                      => NLZipcode,
                vestiging_woonplaats                    => 'Str',
                correspondentie_landcode                => 'Int', ### TODO Restrict these
                correspondentie_adres                   => 'Str',
                correspondentie_adres_buitenland1       => 'Str',
                correspondentie_adres_buitenland2       => 'Str',
                correspondentie_adres_buitenland3       => 'Str',
                correspondentie_straatnaam              => 'Str',
                correspondentie_huisnummer              => 'Int',
                correspondentie_huisletter              => 'Str', ### TODO Restrict these
                correspondentie_huisnummertoevoeging    => 'Str', ### TODO Restrict these
                correspondentie_postcode                => NLZipcode,
                correspondentie_woonplaats              => 'Str',
            },
            dependencies => {
                dossiernummer => ['vestigingsnummer'],
                vestiging_landcode => sub {
                    my $dfv     = shift;
                    my $code    = shift;

                    if ($code eq '6030') {
                        return [
                            'vestiging_postcode',
                            'vestiging_straatnaam',
                            'vestiging_huisnummer',
                            'vestiging_woonplaats',
                            'dossiernummer',
                        ];
                    } else {
                        return ['vestiging_adres_buitenland1'];
                    }
                },
                correspondentie_landcode => sub {
                    my $dfv     = shift;
                    my $code    = shift;

                    if ($code eq '6030') {
                        return ['correspondentie_postcode','correspondentie_straatnaam','correspondentie_huisnummer','correspondentie_woonplaats'];
                    } else {
                        return ['correspondentie_buitenland1'];
                    }
                },
            },
            msgs    => sub {
                my $dfv     = shift;
                my $rv      = {};

                return $rv unless $dfv->{_custom_messages};

                for my $key (keys %{ $dfv->{_custom_messages} }) {
                    $rv->{$key} = $dfv->{_custom_messages}->{$key};
                }

                return $rv;
            }
        },
        capabilities    => [qw/authentication contactdata/],
    }
};

has '_subject_type_settings' => (
    is      => 'ro',
    isa     => 'HashRef',
    default => sub {
        return SUBJECT_TYPE_SETTINGS()
    }
);

has 'possible_params' => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self    = shift;

        my $profile = $self->_subject_type_settings->{$self->subject_type}->{profile};
        return [ 'email', 'password', 'telephonenumber', 'mobilenumber', keys %{ $profile->{required} }, keys %{ $profile->{optional} } ];
    },
);

=head1 METHODS

=head2 save

Arguments: [ \%PARAMS ]

Saves this subject into database

=cut

sub save {
    my $self            = shift;
    my $params          = shift;

    ### Save given parameters into object
    if ($params) {
        throw('br/subject/invalid_params', 'Make sure params is a valid HASH when given')
            unless ref $params eq 'HASH';

        for my $key (keys %$params) {
            $self->$key($params->{$key});
        }
    }

    ### Update or create subject
    my $subject;
    if ($self->id) {
        $subject = $self->schema->resultset(ucfirst($self->subject_type))->find($self->id);

        throw('br/subject/not_found_by_id', 'Cannot find subject by id ' . $self->id) unless $subject;

        unless ($subject->update($self->_gen_db_params)) {
            throw('br/subject/update_error', 'Did not receive a true update from company save');
        }
    } else {
        ### On new object, validate
        $self->log->debug('Validating subject parameters');
        $self->_validate_subject;

        $subject = $self->schema->resultset(ucfirst($self->subject_type))->create($self->_gen_db_params);

        unless ($subject) {
            throw('br/subject/create_error', 'Could not save company');
        }

        $self->id($subject->id);
    }

    ### Run over capability hooks
    for my $capability (@{ $self->_subject_type_settings->{$self->subject_type}->{capabilities} }) {
        if (my $method = $self->can('_save_' . $capability)) {
            $method->($self, $subject);
        }
    }

    $self->inflate;

    return $self;
}

=head1 INTERNAL METHODS

=head2 BUILD

Makes sure we generate the necessary attributes according to the given parameters

=cut

sub BUILD {
    my $self        = shift;

    $self->_generate_accessors;
}

=head2 _generate_accessors

Generate accessors for the subject type

=cut

sub _generate_accessors {
    my $self        = shift;

    if ($self->subject_type ne 'bedrijf') {
        throw('br/subject/unimplemented_subject_type', 'Subject type: ' . $self->subject_type . ' not implemented yet');
    }

    ### Required and optional accessors, all set as optional, final checking on "save" call
    my $profile = $self->_subject_type_settings->{$self->subject_type}->{profile};
    for my $attr (keys %{ $profile->{required} }, keys %{ $profile->{optional} }) {
        my $isa         = $profile->{required}->{ $attr } || $profile->{optional}->{ $attr };

        $self->meta->add_attribute(
            $attr,
            blessed($isa) ? (isa => $isa) : (),
            is          => 'rw',
            predicate   => 'has_' . $attr,
        );
    }
}

=head2 _validate_subject

Validates the object before saving against subject profile

=cut

sub _validate_subject {
    my $self            = shift;

    my $params          = $self->_external_params;

    assert_profile(
        {
            %$params,
            _schema => $self->schema,
        }, 
        profile => $self->_subject_type_settings->{$self->subject_type}->{profile}
    );
}

=head2 _gen_db_params

Reads the profile settings, and generates a list of params from this object ready to insert into
the database

=cut

sub _external_params {
    my $self            = shift;


    ### Required params
    my $profile = $self->_subject_type_settings->{$self->subject_type}->{profile};

    my $db_hash = {};
    for my $attr (keys %{ $profile->{required} }, keys %{ $profile->{optional} }) {
        my $checker = $self->can('has_' . $attr);
        if ($checker->($self)) {
            $db_hash->{$attr} = $self->$attr;
        }
    }

    return $db_hash;
}

sub _gen_db_params {
    my $self            = shift;

    my %params          = (
        $self->id ? (id => $self->id) : (),
        $self->has_authenticated ? (authenticated => $self->authenticated) : (),
        $self->has_authenticatedby ? (authenticatedby => $self->authenticatedby) : (),

        ### Undelete
        $self->has_deleted ? (deleted_on  => $self->deleted || undef) : (),
    );

    ### Generate a hash for validation
    my $db_hash = $self->_external_params;

        ### Required params
    my $profile = $self->_subject_type_settings->{$self->subject_type}->{profile};

    ### Get valid parameters via profile
    my $valid_params = Data::FormValidator->check(
        {
            %$db_hash,
            _schema => $self->schema,
        }, Zaaksysteem::Profile::build_profile(%$profile)
    )->valid;

    return { %params, %$valid_params };
}


=head2 _save_authentication

Saves authentication parameters when given.

=cut

sub _save_authentication {
    my $self            = shift;
    my $subject         = shift;

    throw(
        'br/subject/_save_authentication/no_company',
        'Authentication capability only possible for subject_type = "bedrijf"'
    ) unless ($self->subject_type eq 'bedrijf');

    unless ($self->password) {
        $self->log->debug('Creating subject without password');
        return;
    }

    my $authparams      = {
        gegevens_magazijn_id    => $subject->id,
        login                   => $self->dossiernummer,
        password                => $self->password,
    };

    my $auth;
    if ($auth = $self->schema->resultset('BedrijfAuthenticatie')->find($self->id)) {
        unless ($auth->update($authparams)) {
            throw('br/subject/_save_authentication/update_error', 'Did not receive a true update from authentication save');
        }
    } else {
        my $auth = $self->schema->resultset('BedrijfAuthenticatie')->create($authparams);

        unless ($auth) {
            throw('br/subject/_save_authentication/create_error', 'Could not save authentication params');
        }
    }

    return 1;
}

=head2 _save_contactdata

Saves authentication parameters when given.

=cut

sub _save_contactdata {
    my $self            = shift;
    my $subject         = shift;

    throw(
        'br/subject/_save_contactdata/no_company',
        'Authentication capability only possible for subject_type = "bedrijf"'
    ) unless ($self->subject_type eq 'bedrijf');

    unless ($self->has_mobilenumber && $self->has_telephonenumber && $self->has_email) {
        $self->log->debug('Creating subject without contactdata');
        return;
    }

    my $authparams      = {
        gegevens_magazijn_id    => $subject->id,
        betrokkene_type         => $self->subject_type,
        ($self->has_mobilenumber ? (mobiel => $self->mobilenumber) : ()),
        ($self->has_telephonenumber ? (telefoonnummer => $self->telephonenumber) : ()),
        ($self->has_email ? (email => $self->email) : ()),
    };

    my $auth;
    if ($auth = $self->schema->resultset('ContactData')->find($self->id)) {
        unless ($auth->update($authparams)) {
            throw('br/subject/_save_contactdata/update_error', 'Did not receive a true update from contactdata save');
        }
    } else {
        my $auth = $self->schema->resultset('ContactData')->create($authparams);

        unless ($auth) {
            throw('br/subject/_save_contactdata/create_error', 'Could not save contactdata params');
        }
    }

    return 1;
}

=head2 inflate

Arguments: none

Return value: none

    $obj->inflate;

Inflates this object according to the given subject id with the neccessary values.

=cut

sub inflate {
    my $self        = shift;

    my $contact_map = {
        mobilenumber    => 'mobiel',
        email           => 'email',
        telephonenumber => 'telefoonnummer'
    };

    return unless $self->id;

    my $company     = $self->schema->resultset('Bedrijf')->find($self->id);
    my $contact     = $self->schema->resultset('ContactData')->find(
        {
            gegevens_magazijn_id => $self->id,
            betrokkene_type => ($self->subject_type eq 'bedrijf' ? 2 : 1)
        }
    );

    for my $param (@{ $self->possible_params }) {
        next if $param eq 'password';

        if ($param =~ /^(?:mobilenumber|telephonenumber|email)$/) {
            next unless $contact;

            my $dbkey = $contact_map->{$param};

            next unless defined($contact->$dbkey);
            $self->$param($contact->$dbkey);
            next;
        }

        next unless defined($company->$param);
        $self->$param($company->$param);
    }
}

=head2 serialize

Arguments: none

Return value: %OBJECT_AS_HASHREF

    $params = $obj->serialize;

Returns the object as hashref.

=cut

sub serialize {
    my $self        = shift;

    my $rv = {};
    for my $param (@{ $self->possible_params }) {
        $rv->{$param} = $self->$param;
    }

    return $rv;
}

## Impossible to make immutable, due to generating accessors
# __PACKAGE__->meta->make_immutable;

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
