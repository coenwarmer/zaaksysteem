package Zaaksysteem::Tools::HashMapper;

use warnings;
use strict;

use Exporter 'import';

our @EXPORT = qw[inflate deflate];

=head1 NAME

Zaaksysteem::Tools::HashMapper - Inflate/deflate object/hash structures

=head1 DESCRIPTION

Easily convert between the following types of data structures:

    {
        'key' => 'value',
        'nest.key' => 'value',
        'other.deeper.key' => 'value'
    }

    {
        'key' => 'value',
        'nest' => {
            'key' => 'value'
        },
        'other' => {
            'deeper' => {
                'key' => 'value'
            }
        }
    }

=head1 FUNCTIONS

=head2 inflate

Inflate a flat hash to the structured form

=cut

sub inflate {
    my $data = shift;
    my $options = _get_options(@_);
    my $retval = {};

    my $separator = quotemeta $options->{ nesting_separator };

    for my $flat_key (keys %{ $data }) {
        my $pointer = $retval;
        my @nests = split m[$separator], $flat_key;
        my $key = pop @nests;

        # Dive into the existing data structure, autovifify hashrefs where
        # needed and set the pointer/cursor ref.
        for my $nest (@nests) {
            if (exists $pointer->{ $nest }) {
                my $value = $pointer->{ $nest };

                if (ref $value eq 'HASH') {
                    $pointer = $value;
                } else {
                    $pointer = $pointer->{ $nest } = {
                        _value => $value
                    };
                }
            } else {
                $pointer = $pointer->{ $nest } = {};
            }
        }

        # If 'foo.bar' key has already been set, and we encounter key 'foo'
        # then this guard will move the value into foo._value instead of
        # writing over the existing data.
        if (exists $pointer->{ $key } && ref $pointer->{ $key } eq 'HASH') {
            $pointer->{ $key }{ _value } = $data->{ $flat_key };
        } else {
            $pointer->{ $key } = $data->{ $flat_key };
        }
    }

    return $retval;
}

=head2 deflate

Deflate a structured hash to the flat form

=cut

sub deflate {
    my $data = shift;
    my $options = _get_options(@_);

    return { map {
        _deflate($data->{ $_ }, $_, $options->{ nesting_separator })
    } keys %{ $data } };
}

sub _deflate {
    my $data = shift;
    my $prefix = shift;
    my $separator = shift;

    return ($prefix, $data) unless ref $data eq 'HASH';

    return map {
        _deflate($data->{ $_ }, join($separator, $prefix, $_), $separator)
    } keys %{ $data };
}

sub _get_options {
    return {
        nesting_separator => '.',
        @_
    };
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
