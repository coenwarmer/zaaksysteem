=head1 NAME

Zaaksysteem::Manual::API::V1::Case - Case retrieval and mutation API.

=head1 Description

This API-document describes the usage of our JSON Case API. Via the Case API it is possible
to retrieve, create and edit cases. It also supports adding files to a case.

=head2 API

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head2 URL

The base URL for this API is:

    /api/v1/case

Make sure you use the HTTP Method C<GET> for retrieving, and C<POST> for change requests.

=head1 Retrieve data

=head2 get

   /api/v1/case/d7c73f04-5fcc-4c03-b089-b5fc760a08d5

Retrieving an object from our database is as simple as calling the URL C</api/v1/case/[UUID]>. You will get
the response in the C<result> parameter. It will only contain one single result, and as always, the C<type>
will tell you what kind of object you received. The C<instance> property will contain the contents of this
object.

B<Example call>

    curl \
        -H "API-Interface-Id: 42" \
        -H "Content-Type: application/json" \
        --digest -u "username:password" \
        https://localhost/api/v1/case/d7c73f04-5fcc-4c03-b089-b5fc760a08d5

B<Request JSON>

An empty request body suffices

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-6e212f-20ef20",
   "development" : false,
   "result" : {
      "instance" : {
         "attributes" : {
            "checkbox_agreed" : [
               null
            ],
            "test_comment" : [
               "This is a comment"
            ],
            "test_conclusion" : [
               null
            ],
            "test_decision" : [
               null
            ],
            "test_paspoort" : [
               null
            ]
         },
         "casetype" : {
            "reference" : "4a8904ef-0920-40a8-a160-4a814273890d",
            "type" : "casetype"
         },
         "date_of_registration" : "2015-03-04T11:49:53Z",
         "date_target" : "2015-03-09T11:49:53Z",
         "id" : "1f972123-a6d3-4d7b-85cd-1526e87a6525",
         "number" : 579,
         "phase" : "afhandelfase",
         "result" : null,
         "status" : "new",
         "subject_external" : null
      },
      "reference" : "1f972123-a6d3-4d7b-85cd-1526e87a6525",
      "type" : "case"
   },
   "status_code" : 200
}

=end javascript

=head2 list

   /api/v1/case

Retrieving multiple objects from our database is as simple as calling the URL C</api/v1/case> without arguments. The
C<result> property will contain an object of type B<set>, which allows us to page the data.

B<Example call>

    curl \
        -H "API-Interface-Id: 42" \
        -H "Content-Type: application/json" \
        --digest \
        -u "username:password" \
        https://localhost/api/v1/case

B<Request JSON>

An empty request body suffices

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-d9db3d-199919",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 2,
            "total_rows" : 2
         },
         "rows" : [
            {
               "instance" : {
                  "attributes" : {
                     "checkbox_agreed" : [
                        null
                     ],
                     "test_comment" : [
                        null
                     ],
                     "test_conclusion" : [
                        null
                     ],
                     "test_decision" : [
                        null
                     ],
                     "test_paspoort" : [
                        null
                     ]
                  },
                  "casetype" : {
                     "reference" : "5767c5e1-e666-4acc-908e-a61693ef96f8",
                     "type" : "casetype"
                  },
                  "date_of_registration" : "2015-03-04T11:49:03Z",
                  "date_target" : "2015-03-09T11:49:03Z",
                  "id" : "17276cbd-505d-405b-b89b-c37c72d47ed3",
                  "number" : 577,
                  "phase" : "afhandelfase",
                  "result" : null,
                  "status" : "new",
                  "subject_external" : null
               },
               "reference" : "17276cbd-505d-405b-b89b-c37c72d47ed3",
               "type" : "case"
            },
            {
               "instance" : {
                  "attributes" : {
                     "checkbox_agreed" : [
                        null
                     ],
                     "test_comment" : [
                        null
                     ],
                     "test_conclusion" : [
                        null
                     ],
                     "test_decision" : [
                        null
                     ],
                     "test_paspoort" : [
                        null
                     ]
                  },
                  "casetype" : {
                     "reference" : "5767c5e1-e666-4acc-908e-a61693ef96f8",
                     "type" : "casetype"
                  },
                  "date_of_registration" : "2015-03-04T11:49:03Z",
                  "date_target" : "2015-03-09T11:49:03Z",
                  "id" : "db5a29b6-7bc0-4149-b646-77a2e5e39861",
                  "number" : 578,
                  "phase" : "afhandelfase",
                  "result" : null,
                  "status" : "new",
                  "subject_external" : null
               },
               "reference" : "db5a29b6-7bc0-4149-b646-77a2e5e39861",
               "type" : "case"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=head1 Mutate data

Mutations via our API will have to be send via the HTTP C<POST> method. The inputdata for these mutations
are one single JSON object containing the parameters. When no input is needed, make sure you send an empty
object (C< {} >)

=head2 create

   /api/v1/case/create

It is possible to create a case in zaaksysteem.nl, complete with files, attributes and a requestor. Before
you start, it is advisable to look at the documentation of casetypes, L<Zaaksysteem::Manual::API::V1::Casetype>, to find out the
mandatory C<casetype_id> property.

The input (request) data for this call is a JSON object containing the following properties

B<Properties>

=over 4

=item casetype_id [required]

B<TYPE>: UUID

Every case belongs to a casetype. You will need the id of the casetype when you would like to create a case
via our API. Please look at the documentation of the casetype API: L<Zaaksysteem::Manual::API::V1::Casetype>

=item source [required]

B<TYPE>: String

This item contains one of the different sources, choose one of: C<webformulier>, C<behandelaar>, C<post>, C<balie>,
C<telefoon>, C<email>.

Although it is a required field, it is normally only used for users of zaaksysteem.nl to know where this case
is coming from. Most of the time you would like to use C<webformulier>

=item requestor [required]

B<TYPE>: Object

Every case needs a requestor. An identifier of the person or company requesting this case. For now, we implemented
two requestors: C<person> and C<company>.

When using C<person>, make sure you provide the B<burgerservicenummer> as the C<id> field.

=begin javascript

{
    ...,
    "requestor" : {
        "id" : "12345678",
        "type" : "person"
    }
}

=end javascript

When using C<company>, make sure you provide the B<KVK-nummer> as the C<kvk_number> field, and the B<vestigingsnummer>
as the C<branch_number> field.

=begin javascript

{
    ...,
    "requestor" : {
        "type" : "company"
        "id": {
            "kvk_number" : "51902672",
            "branch_number": "000021881022"
        },
    }
}

=end javascript

=item values

B<TYPE>: Object

A key-value pair of attributes you would like to create the case with. Make sure every value is wrapped in an array, like
you see in the example below. For more information about the format of providing values, please look at our
general API documentation: L<Zaaksysteem::Manual::API::V1>

=begin javascript

{
    ...,
    "values" : {
        "test_comment" : [
            "This is telling you: welcome to our api"
        ]
    }
}

=end javascript

=item assignee_id

B<TYPE>: Int (local table reference)

When creating the case object, automatically sets the provided subject as
assignee.

=item open

B<TYPE>: Boolean (default C<false>)

If an assignee is set, and C<open> is C<true>, the case will be created with
C<status> set to 'open'. Effectually, the case will not show up in any intake,
and is automatically accepted by the assignee.

B<CAVEAT>: This parameter does nothing if no assignee is provided in the
request, either via C<assignee_id> or by providing a subject of type
C<assignee>

=begin javascript

{
    ...,
    "open": true
}

=end javascript

=item recipient

B<TYPE>: Object

A case may optionally have a recipient, which can be set using this parameter.

The expected data is structurally the same as for the C<requestor> parameter.

=begin javascript

{
    ...,
    "recipient": {
        "type": "person",
        "id": 123456789
    }
}

=end javascript

=item route

B<TYPE>: Object

Allows the creation request to override the default route for the new case.

The route dictates which L<Zaaksysteem::Object::Types::Group|group> /
L<Zaaksysteem::Object::Types::Role|role> combination (position) receives the
case in their intake.

This parameter will be ignored if L</assignee_id> is specified in the same
request.

=begin javascript

{
    ...,
    "route": {
        "group_id": 37,
        "role_id": 21
    }
}

=end javascript

=item contact_details

B<TYPE>: Object

Allows the caller to set specific contact details to be used within the
context of the case.

=begin javascript

{
    ...,
    "contact_details": {
        "phone_number": "+31204420587", // or any valid phone number
        "mobile_number": "+31641752906", // must be a mobile phone number
        "email_address": "my.name@domain.tld"
    }
}

=end javascript

=item confidentiality

B<TYPE>: String (enumerated values)

Sets the level of confidentiality for the case. Can be any of C<public>
(default), C<internal>, or C<confidential>.

The C<public> and C<internal> values are currently functionally equivalent,
but this behavior may change in the future. It is recommended to only use the
C<public> and C<confidential> values.

Setting a confidentiality value will change which entities have which
permissions/capabilities on the case, the exact effect is defined in the
management interface for C<casetype> objects.

=item subjects

B<TYPE>: Array | Object

Adds a subject to the case after creation.

This parameter can also be used to set an C<assignee>, C<recipient>, or
C<requestor>.

=begin javascript

{
    ...,
    "subjects": [
        {
            // Required fields
            "subject": {
                "type": "subject",
                "reference": "6fcc1bdd-d6b7-4166-acc5-bc2fa88ebf6a"
            },

            "role": "Auditor",
            "magic_string_prefix": "auditor",

            // Optional fields
            "pip_authorized": true,
            "send_auth_notification": false
        }
    ]
}

=end javascript

Alternative call-style (for single subject additions):

=begin javascript

{
    ...,
    "subjects": {
        "subject": {
            "type": "subject",
            "reference": "f9c7cb11-3006-458b-a762-76e0b247999f"
        },

        "role": "Auditor",
        "magic_string_prefix": "auditor",
    }
}

=end javascript

Use the subjects to set a requestor:

=begin javascript

{
    ...,
    "subjects": [
        {
            "subject": {
                "type": "subject",
                "reference": "789277b3-8e66-4efa-8dfa-e50cadc27c68"
            },

            "role": "Requestor",
            "magic_string_prefix": "requestor",

            // Type field is normally omitted
            "type": "requestor"
        }
    ]
}

=end javascript

=item files

B<TYPE>: Array | Object

Allows the caller to set metadata on files preloaded via C<prepare_file>. Make sure the "name" (filename) of
the file does not contain an extension.

=begin javascript

{
    ...,
    "files": [
        {
            "reference": "e4d6bd10-f3fd-4cbb-bfee-559a21f72f4b",
            "name": "Agreement",
            "metadata": {
                    "origin": "Uitgaand",
                    "description": "Besluit van de gemeente over de aanvraag",
                    "document_category": "Besluit",
                    "trust_level": "Zaakvertrouwelijk",
                    "origin_date": "2016-05-05T00:00:00",
                    "pronom_format": "doc/2042",
                    "appearance": "As document",
                    "structure": "Hard"
            }
        }
    ]
}

=end javascript

=back

B<Example call>

    curl \
        -H "API-Interface-Id: 42" \
        -H "Content-Type: application/json" \
        --data @file_with_request_json \
        --digest \
        -u "username:password" \
        https://localhost/api/v1/case/create

B<Request JSON>

=begin javascript

{
   "casetype_id" : "a8466f23-1bd3-4dd7-943c-38b23d648fb0",
   "requestor" : {
      "id" : "12345678",
      "type" : "person"
   },
   "source" : "webformulier",
   "values" : {
      "test_comment" : [
         "This is telling you: welcome to our api"
      ]
   }
}

=end javascript

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-f0a833-ea87dc",
   "development" : false,
   "result" : {
      "instance" : {
         "attributes" : {
            "checkbox_agreed" : [
               null
            ],
            "test_comment" : [
               "This is telling you: welcome to our api"
            ],
            "test_conclusion" : [
               null
            ],
            "test_decision" : [
               null
            ],
            "test_paspoort" : [
               null
            ]
         },
         "casetype" : {
            "reference" : "a8466f23-1bd3-4dd7-943c-38b23d648fb0",
            "type" : "casetype"
         },
         "date_of_registration" : "2015-03-04T11:52:24Z",
         "date_target" : "2015-03-09T11:52:24Z",
         "id" : "2773480d-bc45-4dca-b351-490f5660b469",
         "number" : 580,
         "phase" : "afhandelfase",
         "result" : null,
         "status" : "new",
         "subject_external" : null
      },
      "reference" : "2773480d-bc45-4dca-b351-490f5660b469",
      "type" : "case"
   },
   "status_code" : 200
}

=end javascript

=head2 update

Now that you have created a case in zaaksysteem.nl, you probably want to update the attributes, because the
above C<test_comment> is rather mellow. Say, you would like to the C<test_conclusion> to "This is telling you: you are awesome", then
please pay attention to this API call.

The input (request) data for this call is a JSON object containing the following properties

B<Properties>

=over 4

=item values [required]

A key-value pair of attributes you would like to create the case with. Make sure every value is wrapped in an array, like
you see in the example below. For more information about the format of providing values, please look at our
general API documentation: L<Zaaksysteem::Manual::API::V1>

=begin javascript

{
    ...,
    "values" : {
        "test_conclusion" : [
            "This is telling you: you are awesome"
        ],
    }
}

=end javascript

=back

B<Example call>

    curl \
        -H "API-Interface-Id: 42" \
        -H "Content-Type: application/json" \
        --data @file_with_request_json \
        --digest \
        -u "username:password" \
        https://localhost/api/v1/case/9e7ab012-ffee-45e2-a6ae-974c91312967/update

B<Request JSON>

=begin javascript

{
    ...,
    "values" : {
        "test_conclusion" : [
            "This is telling you: you are awesome"
        ],
    }
}

=end javascript

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-62ff11-10ed03",
   "development" : false,
   "result" : {
      "instance" : {
         "attributes" : {
            "checkbox_agreed" : [
               null
            ],
            "test_comment" : [
               "This is telling you: welcome to our api"
            ],
            "test_conclusion" : [
               "This is telling you: you are awesome"
            ],
            "test_decision" : [
               null
            ],
            "test_paspoort" : [
               null
            ]
         },
         "casetype" : {
            "reference" : "92ccea09-62b0-4a36-9af7-8fbbf807918f",
            "type" : "casetype"
         },
         "date_of_registration" : "2015-03-04T10:53:43Z",
         "date_target" : "2015-03-09T10:53:43Z",
         "id" : "c0209d5e-0918-4336-b533-0a2b8a0207d2",
         "number" : 584,
         "phase" : "afhandelfase",
         "result" : null,
         "status" : "new",
         "subject_external" : null
      },
      "reference" : "c0209d5e-0918-4336-b533-0a2b8a0207d2",
      "type" : "case"
   },
   "status_code" : 200
}

=end javascript

=head2 transition

   /api/v1/case/00cd9b9c-5535-4ab8-9ac0-e44e544e4f12/transition

Because cases follow a process, sometimes it is needed to transition a case to the next "phase". With this command
you can accomplish this. This can be a little tricky, because it is not always possible a case "out of the box". Make
sure you design a casetype which does not hang on incomplete checklists, incomplete fields, missing documents, or more
importantly, a missing case-handler. Make sure you define a default case-handler for API calls in your casetype configuration within
zaaksysteem.nl.

The input (request) data for this call is a JSON object containing the following properties. There is one optional "result" field, which contains
the result id when you try to finish this case.

B<Properties>

=over 4

=item result_id

This field contains the ID of one of the possible results for this case. This id can be found in the C<results>
property in the casetype API call: L<Zaaksysteem::Manual::API::V1::Casetype>

=item result

In addition to the result_id, you could also just give the name of the result, e.g. "afgehandeld"...

=cut

=begin javascript

{
   "result_id" : 747
}

=end javascript

B<OR>

=begin javascript

{
   "result" : "afgehandeld"
}

=end javascript

=back

B<Example call>

    curl \
        -H "API-Interface-Id: 42" \
        -H "Content-Type: application/json" \
        --data @file_with_request_json \
        --digest \
        -u "username:password" \
        https://localhost/api/v1/case/9e7ab012-ffee-45e2-a6ae-974c91312967/transition

B<Request JSON>

=begin javascript

{
   "result_id" : 1299
}

=end javascript

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-9af744-ddec6e",
   "development" : false,
   "result" : {
      "instance" : {
         "attributes" : {
            "checkbox_agreed" : [
               null
            ],
            "test_comment" : [
               "This is telling you: welcome to our api"
            ],
            "test_conclusion" : [
               null
            ],
            "test_decision" : [
               null
            ],
            "test_paspoort" : [
               null
            ]
         },
         "casetype" : {
            "reference" : "af086984-ae66-49c3-b241-0d041dc8932f",
            "type" : "casetype"
         },
         "confidentiality" : "public",
         "date_of_registration" : "2015-03-04T10:55:12Z",
         "date_target" : "2015-03-09T10:55:12Z",
         "id" : "5a977270-1372-46e2-9598-24398c3d131f",
         "number" : 586,
         "phase" : null,
         "result" : "afgebroken",
         "status" : "resolved",
         "subject_external" : null
      },
      "reference" : "5a977270-1372-46e2-9598-24398c3d131f",
      "type" : "case"
   },
   "status_code" : 200
}

=end javascript

=head2 take

It is possible to assign the case to the current logged in user. You cannot use this call if you are using the API via Digest
authentication. You will need to C<POST> towards this endpoint. In case the case is already owned by another user you will be able
to assign the case to yourself. If the case is closed an error is thrown with a 422 HTTP code.

=head3 URI

    C</api/v1/case/[UUID]/take>

=head2 reject

When a case is assigned incorrectly, you can reject it. The case will be send to the group and role configured for redistributing cases.
You cannot use this call if you are using the API via Digest authentication. You will need to C<POST> towards this endpoint. In case the case
is not in C<new> state, or there is no distribution setup, it will fail with an error.

=head3 URI

    C</api/v1/case/[UUID]/reject>

=head2 prepare file

It is also possible to use this API to upload files and attach them to a case
during creation.

As it is not possible to create a file and a case in the same call, you can use
the "prepare_file" call to create a temporary reference to use when creating a
case.

B<Example call>

    curl \
        -H "API-Interface-Id: 42" \
        -H "Content-Type: multipart/form-data" \
        --digest \
        -u "username:password" \
        -F upload=@FilestoreTest.txt \
        https://localhost/api/v1/case/prepare_file

B<Request>

The request body should be a normal C<multipart/form-data> POST body, with one
part, containing the file contents (and the correct C<Content-Tye> header, etc.).

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-21bf98-e068c3",
   "development" : false,
   "result" : {
      "instance" : {
         "references" : {
            "86a9485f-40cf-4b26-88f8-db2875cb9628" : "FilestoreTest.txt"
         }
      },
      "reference" : null,
      "type" : "filebag"
   },
   "status_code" : 200
}

=end javascript

In the JSON response body, you will find a list of references to the uploaded
files, and the names of the files represented (as specified in the
C<Content-Disposition> header of the relevant part of the request).

You can use the returned UUIDs in the C<values> property of a field of type
C<file> in a subsequent C<create_case> call, as follows:

B<Example /api/v1/case/create call>

=begin javascript

{
   "casetype_id" : "62efcaef-e0d4-4083-9325-892f09cd8121",
   "requestor" : {
      "id" : "12345678",
      "type" : "person"
   },
   "source" : "webformulier",
   "values" : {
      "test_comment" : [
         "This is telling you: welcome to our api"
      ],
      "test_uploaded_file" : [
         "86a9485f-40cf-4b26-88f8-db2875cb9628"
      ]
   }
}

=end javascript

=head1 Objects

=head2 Case

Most of the calls in this document return an instance of type C<case>. Below we provide more information about
the contents of this object.

B<Properties>

=over 4

=item id

B<TYPE>: UUID

The unique identifier of this case. You can use this identifier to request more information about the
related objects of this case, like the download API call.

=item casetype_id

B<TYPE>: UUID

The unique identifier of the casetype this case is derived from. You could use one the C<api/v1/casetype/get/UUID>
to get more information about this casetype.

=item confidentiality

B<TYPE>: String (one of "public", "internal" or "confidential")

String identifying the level of confideniality for the case. Determines which
set of ACLs is used to determine access rights.

=item number

B<TYPE>: Number

The unique number of this case, the human readable version which you will also find on your dashboard from
within zaaksysteem.nl

=item subject_external

B<TYPE>: String

Will return the human readable description of this case. This is a generated string derived from the content
of this case. It is human defined in the casetype configuration, and with the clever use of magic strings, a
casetype creator could return something like: "Parkeervergunning afgerond op 12-02-2015, kenteken 44-24-AB"

=item phase

B<TYPE>: String

This defines the phase this case is currently in. For a two-phase casetype this would be B<afhandelfase>.

=item status

B<TYPE>: ENUM String

This defines the status this case is currently in, a status does not provide the step in a process, like phase,
but defines whether this case is closed, open, new or stalled.

Possible options are: C<open>, C<new>, C<resolved>, C<stalled>, C<overdragen>

=item result

B<TYPE>: String

The result of this case, when closed, it will show you the result. Like "verwerkt" or "afgebroken"

=item date_of_registration

B<TYPE>: ISO8601 Date representation in UTC

The date this case has been created.

=item date_target

B<TYPE>: ISO8601 Date representation in UTC

According to the belonging Casetype, this case should be finished at the target date in this property.

=item child_relations

B<TYPE>: SET of case relations

Returns a SET object containing the rows of different child cases.

=item parent_relation

B<TYPE>: Case reference

Returns an object of type C<case> containing the reference uuid of the parent case.

=item relations

B<TYPE>: SET of case relations

Returns a SET object containing the rows of different child cases.

=item attributes

B<TYPE>: Object

An object containing the attribute ids as a key. This could get a little bit tricky, so let us explain how
to use this attribute.

In zaaksysteem.nl it is possible to define multiple values for a single attribute. This way it is possible
to create a listing of text fields. But to prevent a collision with a single attribute which could contain
multiple values (think of a checkbox where you could select multiple values) a simple Array would not suffice.

Let me explain this with a couple of examples. Let's say we would like to give some feedback about us.

B<single value>

We have one suggestion for a feature

=begin javascript

"attributes" : {
   "feedback" : [
      "Please add an easter egg, so zaaksysteem.nl is really awesome"
   ],
},

=end javascript

B<multiple text values>

We have a lot of ideas, so we place a second idea.

=begin javascript

"attributes" : {
   "feedback" : [
      "Please add an easter egg, so zaaksysteem.nl is really awesome",
      "Please add some chuck norris quotes"
   ],
},

=end javascript

B<selectbox>

We make a selection of features

=begin javascript

"attributes" : {
   "features" : [
      [ "easter egg", "document uploading" ]
   ],
},

=end javascript

B<multiple selectboxes>

We would like to give a list of priority features, the first line of checkboxes for "priority features" is
priority 1, the second list of these checkboxes is priority 2.

=begin javascript

"attributes" : {
   "features" : [
      [ "easter egg", "document uploading" ],
      [ "chuck norris quotes", "jack bauer quotes" ]
   ],
},

=end javascript

=back

=head2 Filebag

The special object filebag just contains a listing of UUIDs and filenames.

B<Properties>

=over 4

=item references

This property contains a key/value pair of UUIDs and filenames.

=begin javascript

"references" : {
   "d6d02038-d460-4a0e-a5ff-6dea0f7503ac" : "FilestoreTest.txt"
}

=end javascript

=back

=head1 Support

The data in this document is supported by the following test. Please make sure you use the API as described
in this test. Any use of this API outside the scope of this test is B<unsupported>

L<TestFor::Catalyst::API::V1::Case>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
