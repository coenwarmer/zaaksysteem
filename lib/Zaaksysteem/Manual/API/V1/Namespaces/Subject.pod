=head1 NAME

Zaaksysteem::Manual::API::V1::Namespaces::Subject - Subject API reference
documentation

=head1 NAMESPACE URL

    /api/v1/subject

=head1 DESCRIPTION

This page documents the endpoints within the C<subject> API namespace.

=head1 ENDPOINTS

=head2 Core endpoints

=head3 C<POST />

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<subject|Zaaksysteem::Manual::API::V1::Types::Subject> instances.

=head3 C<POST /remote_search>

Returns a L<set|Zaaksysteem::Manual::API::V1::Types::Set> of
L<subject|Zaaksysteem::Manual::API::V1::Types::Subject> instances.

=head3 C<POST /remote_import>

Returns a L<subject|Zaaksysteem::Manual::API::V1::Types::Subject> instance.

=head3 C<GET /[subject:id]>

Returns a L<subject|Zaaksysteem::Manual::API::V1::Types::Subject> instance.

=head3 C<POST /create>

Returns a L<subject|Zaaksysteem::Manual::API::V1::Types::Subject> instance.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
