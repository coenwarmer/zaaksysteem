=head1 NAME

Zaaksysteem::Manual::API::V1::Map - MAP related API functions

=head1 Description

This API-document describes the usage of our JSON Map API. Via the Map API it is
possible to retrieve various settings for the MAP functionality of zaaksysteem.nl.

=head1 URL

The base URL for this API is:

    /api/v1/map

Make sure you use the HTTP Method C<GET> for retrieving, and C<POST> for change requests.

=head1 API

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head2 ol_settings

   /api/v1/map/ol_settings

Retrieving the configured OpenLayer settings for the WMS/PDOK system, is as
easy as calling C<ol_settings> on this API. It will return the available
layers.

B<Example call>

 https://localhost/api/v1/map/ol_settings

B<Request JSON>

An empty request body suffices

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-ab6cde-3418ef",
   "development" : false,
   "result" : {
      "instance" : {
         "map_center" : "52.278,5.163",
         "wms_layers" : [
            {
               "instance" : {
                  "active" : true,
                  "id" : "hoogtenl",
                  "label" : "Hoogtebestand Nederland",
                  "url" : "https://geodata.nationaalgeoregister.nl/ahn1/wms",
               },
               "reference" : null,
               "type" : "ol_layer_wms"
            },
            {
               "instance" : {
                  "active" : false,
                  "id" : "adrnl",
                  "label" : "Adressen Nederland",
                  "url" : "https://geodata.nationaalgeoregister.nl/inspireadressen/wms",
               },
               "reference" : null,
               "type" : "ol_layer_wms"
            }
         ]
      },
      "reference" : null,
      "type" : "ol_settings"
   },
   "status_code" : 200
}

=end javascript

=head1 Objects

=head2 ol_settings

The ol_settings is the configuration object for a OpenLayer Map. 

B<Properties>

=over

=item map_center

Type: String (lat,lng)

Contains the lat/lng coordinates for centering a map. The lat/lng data comes from the configured settings
in "Configuratie"

=item wms_layers

Type: Array of ol_layer_wms

Contains a list of objects of type C<ol_layer_wms>. See below

=back

=head2 ol_layer_wms

The ol_layer_wms is a OpenLayer WMS Layer representation

B<Properties>

=over

=item id

B<TYPE>: String

Name of the layer to use. This can generally be found in the "GetCapabilities"
XML response of the WMS service.

=item url

B<TYPE>: URL

The base URL of the PDOK (or other WMS) service. This should always be an B<https://> URL, without.

=item label [required]

B<TYPE>: String

The configured label of the layer

=item active

B<TYPE>: Boolean

The configured status of the layer. When set to true, you should include this layer.

=back

=head1 Support

The data in this document is supported by the following test. Please make sure you use the API as described
in this test. Any use of this API outside the scope of this test is B<unsupported>

L<TestFor::Catalyst::API::V1::Map>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
