=head1 NAME

Zaaksysteem::Manual::API::V1::Config - Configuration item retrieval

=head1 Description

Get a list of configuration items.

=head2 API

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head2 URL

The base URL for this API is:

    /api/v1/general/config

Make sure you use the HTTP Method C<GET> for retrieving.

You need to be logged in to Zaaksysteem to speak to this API.

=head1 Retrieve data

=head2 List all configuration items

   /api/v1/general/config

This API differs from most APIs as the configuration items do not have a UUID.
The unique key to retrieve them is their name.

B<Response JSON>

=begin javascript

{
   "status_code" : 200,
   "api_version" : 1,
   "request_id" : "vagrant-1277e6-7e10f3",
   "development" : false,
   "result" : {
      "type" : "set",
      "instance" : {
         "pager" : {
            "next" : null,
            "rows" : 1,
            "pages" : 1,
            "prev" : null,
            "page" : 1,
            "total_rows" : 1
         },
         "rows" : [
            {
               "reference" : "allocation_notification_template_id",
               "instance" : {
                   "parameter" : "allocation_notification_template_id",
                   "value" : "3"
               },
               "type" : "config",
            }
         ]
      },
      "reference" : null
   }
}

=end javascript

Configuration items are a special kind of object. The reference (unique
identifier) is its name, and the "value" is set to the configuration item's
value.

=head2 get

   /api/v1/general/config/allocation_notification_template_id

In our API you should only retreive objects based on their 'reference' as found
in the JSON. Because configuration items do no have a UUID, you cannot get one
by using a UUID.

You can however get one by using its name. You are however not advised to use
this, as we may change this behaviour in the future.

=begin javascript

{
   "status_code" : 200,
   "request_id" : "vagrant-1277e6-a9ad1b",
   "development" : false,
   "api_version" : 1,
   "result" : {
      "reference" : "allocation_notification_template_id",
      "instance" : {
        "parameter" : "allocation_notification_template_id",
        "value" : "3"
      },
      "type" : "config"
   }
}

=end javascript

=head1 Mutate data

Mutations for configuration items are not supported at this moment.

=head1 Support

The data in this document is supported by the following test. Please make sure
you use the API as described in this test. Any use of this API outside the
scope of this test is B<unsupported>

L<TestFor::Catalyst::API::V1::General::Config>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
