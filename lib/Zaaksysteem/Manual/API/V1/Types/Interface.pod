=head1 NAME

Zaaksysteem::Manual::API::V1::Types::Interface - Type definition for interface
objects

=head1 DESCRIPTION

This page documents the serialization of C<interface> objects.

=head1 JSON

=begin javascript

{
    "type": "interface",
    "reference": "5e9a6bf2-6f9a-43b8-9643-0144ef1df3a9",
    "instance": {
        "id": 1,
        "name": "Foo interface",
        "module": "foo",
        "interface_config": { ... }
    }
}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 id

This attribute holds the internal ID of the interface, which some API calls
rely on.

=head2 name

Name of the interface.

=head2 module

Module name of the interface.

=head2 interface_config

This attribute holds the configuration of the interface. The structure of this
data is specific per L</module>.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
