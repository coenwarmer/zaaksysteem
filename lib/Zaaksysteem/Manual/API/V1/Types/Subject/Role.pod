=head1 NAME

Zaaksysteem::Manual::API::V1::Types::Subject::Role - Type definition for
subject roles

=head1 DESCRIPTION

This page documents the serialization for C<subject/role> objects.

=head1 JSON

=begin javascript

{
    "reference": null,
    "type": "subject/role"
    "instance": {
        "date_created": "2016-09-13T17:27:04Z",
        "date_modified": "2016-09-13T21:13:23Z",
        "label": "A label goes here",
        "is_builtin": true
    }
}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 date_created E<raquo> L<C<datetime>|Zaaksysteem::Manual::API::V1::ValueTypes/datetime>

Object creation timestamp.

=head2 date_modified E<raquo> L<C<datetime>|Zaaksysteem::Manual::API::V1::ValueTypes/datetime>

Object modification timestamp.

=head2 label E<raquo> L<C<text>|Zaaksysteem::Manual::API::V1::ValueTypes/text>

Human-readable name of the subject role.

=head2 is_builtin E<raquo> L<C<boolean>|Zaaksysteem::Manual::API::V1::ValueTypes/boolean>

Indicates whether the referenced subject role is built into Zaaksysteem. If
false, the value was added manually.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
