=head1 NAME

Zaaksysteem::Manual::API::V1::Types::Case - Type definition for case objects

=head1 DESCRIPTION

This page documents the serialization of C<case> objects.

=head1 JSON

=begin javascript

{
    "type": "case",
    "reference": "215f5f54-8c5f-454e-8317-4a7bd883f825",
    "instance": {
    }
}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
