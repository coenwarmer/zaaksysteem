package Zaaksysteem::Schema::BagLigplaatsNevenadres;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::BagLigplaatsNevenadres - koppeltabel voor nevenadressen bij ligplaats

=cut

__PACKAGE__->table("bag_ligplaats_nevenadres");

=head1 ACCESSORS

=head2 identificatie

  data_type: 'varchar'
  is_nullable: 0
  size: 16

58.01 : de unieke aanduiding van een ligplaats.

=head2 begindatum

  data_type: 'varchar'
  is_nullable: 0
  size: 14

58.91 : de begindatum van een periode waarin een of meer gegevens die worden bijgehouden over een ligplaats een wijziging hebben ondergaan.

=head2 nevenadres

  data_type: 'varchar'
  is_nullable: 0
  size: 16

58.11 : de identificatiecodes nummeraanduiding waaronder nevenadressen van een ligplaats, die in het kader van de basis gebouwen registratie als zodanig zijn aangemerkt, zijn opgenomen in de basis registratie adressen.

=head2 correctie

  data_type: 'varchar'
  is_nullable: 0
  size: 1

het gegeven is gecorrigeerd.

=cut

__PACKAGE__->add_columns(
  "identificatie",
  { data_type => "varchar", is_nullable => 0, size => 16 },
  "begindatum",
  { data_type => "varchar", is_nullable => 0, size => 14 },
  "nevenadres",
  { data_type => "varchar", is_nullable => 0, size => 16 },
  "correctie",
  { data_type => "varchar", is_nullable => 0, size => 1 },
);
__PACKAGE__->set_primary_key("identificatie", "begindatum", "correctie", "nevenadres");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-03-18 11:06:59
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:1VRu9jH6by8avWgp2Mhs0w





# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

