package Zaaksysteem::Schema::ObjectData;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::ObjectData

=cut

__PACKAGE__->table("object_data");

=head1 ACCESSORS

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 object_id

  data_type: 'integer'
  is_nullable: 1

=head2 object_class

  data_type: 'text'
  is_nullable: 0

=head2 properties

  data_type: 'text'
  default_value: '{}'
  is_nullable: 0

=head2 index_hstore

  data_type: 'hstore'
  is_nullable: 1

=head2 date_created

  data_type: 'timestamp'
  is_nullable: 1

=head2 date_modified

  data_type: 'timestamp'
  is_nullable: 1

=head2 text_vector

  data_type: 'tsvector'
  is_nullable: 1

=head2 class_uuid

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 1
  size: 16

=head2 acl_groupname

  data_type: 'text'
  is_nullable: 1

=head2 invalid

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "object_id",
  { data_type => "integer", is_nullable => 1 },
  "object_class",
  { data_type => "text", is_nullable => 0 },
  "properties",
  { data_type => "text", default_value => "{}", is_nullable => 0 },
  "index_hstore",
  { data_type => "hstore", is_nullable => 1 },
  "date_created",
  { data_type => "timestamp", is_nullable => 1 },
  "date_modified",
  { data_type => "timestamp", is_nullable => 1 },
  "text_vector",
  { data_type => "tsvector", is_nullable => 1 },
  "class_uuid",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 1, size => 16 },
  "acl_groupname",
  { data_type => "text", is_nullable => 1 },
  "invalid",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("uuid");
__PACKAGE__->add_unique_constraint(
  "object_data_object_class_object_id_key",
  ["object_class", "object_id"],
);

=head1 RELATIONS

=head2 loggings

Type: has_many

Related object: L<Zaaksysteem::Schema::Logging>

=cut

__PACKAGE__->has_many(
  "loggings",
  "Zaaksysteem::Schema::Logging",
  { "foreign.object_uuid" => "self.uuid" },
  {},
);

=head2 object_acl_entries

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectAclEntry>

=cut

__PACKAGE__->has_many(
  "object_acl_entries",
  "Zaaksysteem::Schema::ObjectAclEntry",
  { "foreign.object_uuid" => "self.uuid" },
  {},
);

=head2 object_bibliotheek_entries

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectBibliotheekEntry>

=cut

__PACKAGE__->has_many(
  "object_bibliotheek_entries",
  "Zaaksysteem::Schema::ObjectBibliotheekEntry",
  { "foreign.object_uuid" => "self.uuid" },
  {},
);

=head2 class_uuid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ObjectData>

=cut

__PACKAGE__->belongs_to(
  "class_uuid",
  "Zaaksysteem::Schema::ObjectData",
  { uuid => "class_uuid" },
);

=head2 object_datas

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectData>

=cut

__PACKAGE__->has_many(
  "object_datas",
  "Zaaksysteem::Schema::ObjectData",
  { "foreign.class_uuid" => "self.uuid" },
  {},
);

=head2 object_mutation_object_uuids

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectMutation>

=cut

__PACKAGE__->has_many(
  "object_mutation_object_uuids",
  "Zaaksysteem::Schema::ObjectMutation",
  { "foreign.object_uuid" => "self.uuid" },
  {},
);

=head2 object_mutation_lock_object_uuids

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectMutation>

=cut

__PACKAGE__->has_many(
  "object_mutation_lock_object_uuids",
  "Zaaksysteem::Schema::ObjectMutation",
  { "foreign.lock_object_uuid" => "self.uuid" },
  {},
);

=head2 object_relationships_object2_uuids

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectRelationships>

=cut

__PACKAGE__->has_many(
  "object_relationships_object2_uuids",
  "Zaaksysteem::Schema::ObjectRelationships",
  { "foreign.object2_uuid" => "self.uuid" },
  {},
);

=head2 object_relationships_object1_uuids

Type: has_many

Related object: L<Zaaksysteem::Schema::ObjectRelationships>

=cut

__PACKAGE__->has_many(
  "object_relationships_object1_uuids",
  "Zaaksysteem::Schema::ObjectRelationships",
  { "foreign.object1_uuid" => "self.uuid" },
  {},
);

=head2 zaaktype_kenmerkens

Type: has_many

Related object: L<Zaaksysteem::Schema::ZaaktypeKenmerken>

=cut

__PACKAGE__->has_many(
  "zaaktype_kenmerkens",
  "Zaaksysteem::Schema::ZaaktypeKenmerken",
  { "foreign.object_id" => "self.uuid" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2015-03-19 00:35:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:16f0Y5LQin6D9GS2hVg7fw

use JSON qw/to_json from_json/;
use Pg::hstore;
use Zaaksysteem::JSON::Serializer;

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Object::Data::ResultSet');

__PACKAGE__->load_components(
    "+Zaaksysteem::Backend::Object::Data::Component",
    __PACKAGE__->load_components()
);

__PACKAGE__->add_columns('date_modified',
    { %{ __PACKAGE__->column_info('date_modified') },
    set_on_update => 1,
    set_on_create => 1,
});

__PACKAGE__->add_columns('date_created',
    { %{ __PACKAGE__->column_info('date_created') },
    set_on_create => 1,
});

__PACKAGE__->inflate_column('properties', {
    inflate => sub {
        Zaaksysteem::JSON::Serializer->from_json(shift);
    },
    deflate => sub {
        Zaaksysteem::JSON::Serializer->to_json(shift);
    }
});

__PACKAGE__->inflate_column('index_hstore', {
    inflate => sub {
        Pg::hstore::decode(shift);
    },
    deflate => sub {
        Pg::hstore::encode(shift);
    }
});

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

