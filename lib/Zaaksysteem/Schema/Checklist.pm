package Zaaksysteem::Schema::Checklist;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::Checklist

=cut

__PACKAGE__->table("checklist");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'checklist_id_seq'

=head2 case_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 case_milestone

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "checklist_id_seq",
  },
  "case_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "case_milestone",
  { data_type => "integer", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 case_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Zaak>

=cut

__PACKAGE__->belongs_to("case_id", "Zaaksysteem::Schema::Zaak", { id => "case_id" });

=head2 checklist_items

Type: has_many

Related object: L<Zaaksysteem::Schema::ChecklistItem>

=cut

__PACKAGE__->has_many(
  "checklist_items",
  "Zaaksysteem::Schema::ChecklistItem",
  { "foreign.checklist_id" => "self.id" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-03-18 11:07:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:+PFaLgCyzg+uzdbcTCLa8Q

__PACKAGE__->load_components(
    '+Zaaksysteem::DB::Component::Checklist',
    __PACKAGE__->load_components()
);

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::Checklist');

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

