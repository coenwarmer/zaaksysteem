package Zaaksysteem::Search::ZQL::Literal::String;

use Moose;

extends 'Zaaksysteem::Search::ZQL::Literal';

has '+value' => (
    isa => 'Str'
);

around new_from_production => sub {
    my $orig = shift;
    my $class = shift;

    my $string = substr shift, 1, -1;

    $string =~ s[\\][];

    $class->$orig($string);
};

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

