package Zaaksysteem::Object::SecurityIdentity::Group;

use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object::SecurityIdentity';

=head1 NAME

Zaaksysteem::Object::SecurityIdentity::Group - Group specific security
identity data-wrapper

=head1 DESCRIPTION

This class extends L<Zaaksysteem::Object::SecurityIdentity>, and adds behavior
specific to the conceptual 'group' within the groups-tree

=head1 ATTRIBUTES

=head2 ou_id

This attribute holds the string representation of the organizational unit's
C<l> property.

=cut

has ou_id => (
    is => 'rw',
    isa => 'Int',
    required => 1
);

=head2 entity_id

This attribute is inherited from L<Zaaksysteem::Object::SecurityIdentity>. We
extend it to add a default value hook based on the L</ou_id> and L</role_id>
attributes, effectively making the attribute optional.

=cut

has '+entity_id' => (
    lazy => 1,
    default => sub {
        return shift->ou_id;
    }
);

=head2 entity_type

This attribute is inherited from L<Zaaksysteem::Object::SecurityIdentity>. We
extend it to add a default value of 'group', effectively making it
optional.

=cut

has '+entity_type' => (
    default => 'group'
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

