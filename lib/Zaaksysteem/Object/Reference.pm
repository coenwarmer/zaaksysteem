package Zaaksysteem::Object::Reference;

use Moose::Role;

use Zaaksysteem::Types qw[UUID];
use Zaaksysteem::Tools;

requires qw[_instantiable _instance];

=head1 NAME

Zaaksysteem::Object::Reference - Moose role with behavior for objects with
an identity

=head1 DESCRIPTION

=head1 REQUIRED INTERFACES

=head2 _instantiable

Consuming classes must implement the C<_instantiable> method, which return
value will be used to determine if the reference instance is capable of
producing the referent instance (thing being pointed to).

=head2 _instance

Consuming classes must implement the C<_instance> method, which must return
an instance of L<Zaaksysteem::Object> when called. This method will not be
called unless L</_instantiable> returns true-ish.

=cut

sig _instance => '=> Zaaksysteem::Object';

=head1 ATTRIBUTES

=head2 id

This attribute may hold a L<UUID|Zaaksysteem::Types/UUID> value, uniquely
identifying the instance this role is applied on.

Handles: C<has_id>, C<clear_id>.

=cut

has id => (
    is => 'rw',
    isa => UUID,
    predicate => 'has_id',
    clearer => 'clear_id'
);

=head1 METHODS

=head2 _ref

This convenience method returns a new
L<reference instance|/"Reference instance"> based on the data stored in the
current instance (C<$self>).

It may fail if the method is called on a plain object instance with no C<id>
value, since C<id> is a required field for the reference instance.

=cut

sub _ref {
    my $self = shift;

    return Zaaksysteem::Object::Reference::Instance->new(
        id => $self->id,
        type => $self->type
    );
}

=head2 _as_string

Generic stringification method available to all consumers. Produces a short
string which can be used to identify the object by the last 6 hexchars of it's
L</id>. Returns strings like C<my_type(...1b2b3c)> or C<my_type(unsynched)>,
depending on the definedness of the L</id> attribute.

=cut

sub _as_string {
    my $self = shift;

    my $id = 'unsynched';

    if ($self->has_id) {
        $id = sprintf('...%s', substr($self->id, -6));
    }

    return sprintf('%s(%s)', $self->type, $id);
}

=head1 Reference instance

This package also declares the C<Zaaksysteem::Object::Reference::Instance>
class, which is a minimal consumer of the role described above. It's meant to
be seen as a passable value that stands in for an actual object instance. This
allows a form of DB round-trip optimization if used correctly.

For instance, say there exists an object A, which has a relation with object
B. If I want to assign the related object B to another object (say, C),
without actually doing something with object B, it would be needless to
retrieve B from the database.

The reference instance is used mainly by the Object model itself, which will
usually return reference instances unless the relation had an embedded object
serialization. The reference instance is provided with an instantiator, which
allows the model to inflate the object at a later time, if required.

Reference instances can be assigned to attributes meant for object instances,
and the model will figure out the rest.

=cut

package Zaaksysteem::Object::Reference::Instance;

use Moose;

use Zaaksysteem::Tools;

has type => (
    is => 'ro',
    isa => 'Str',
    required => 1
);

has instantiator => (
    is => 'rw',
    isa => 'CodeRef',
    traits => [qw[Code]],
    predicate => '_instantiable',
    handles => { _instance => 'execute_method' }
);

before _instance => sub {
    my $self = shift;

    return if $self->_instantiable;

    throw('object/ref/instantiator', sprintf(
       'Cannot instantiate object from reference, no instantiator was provided for object %s',
       $self->_as_string
    ));
};

# Usually, role application directives are located at the top of the class
# definition, but in this case we need to have had the attributes applied
# before the reference role requires the instantiator predicate and delegates,
# so Moose won't except when compiling, because those methods don't exist yet.
with 'Zaaksysteem::Object::Reference';

# This attribute is also out-of-place, we require the applied reference role
# because it declares the attribute we're modifying here.
has '+id' => (
    required => 1
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
