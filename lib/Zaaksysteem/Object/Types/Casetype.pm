package Zaaksysteem::Object::Types::Casetype;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';
with 'Zaaksysteem::Object::Roles::Security';

=head1 NAME

Zaaksysteem::Object::Types::Casetype - Shim for "zaaktype"

=head1 DESCRIPTION

Automatically created (and kept in sync by) writing a Zaaktype entry.

=head1 ATTRIBUTES

=head2 casetype.id

The old-style id of the case type.

=cut

has 'casetype.id' => (
    is       => 'rw',
    isa      => 'Int',
    required => 1,
    traits   => [qw(OA)],
    accessor => 'casetype_id',
);

=head2 casetype.name

The user provided name of the case type.

=cut

has 'casetype.name' => (
    is       => 'rw',
    isa      => 'Str',
    label    => 'Title',
    required => 1,
    traits   => [qw(OA)],
    accessor => 'casetype_name',
);

has 'casetype.public_url_path' => (
    is => 'rw',
    isa => 'HashRef[Str]',
    label => 'Public URLs',
    required => 0,
    traits => [qw[OA]],
    accessor => 'casetype_public_url_path'
);

=head2 TO_STRING

Stringification of the casetype. Uses the "casetype.name" attribute.

=cut

override TO_STRING => sub {
    my $self = shift;

    return $self->casetype_name;
};

__PACKAGE__->meta->make_immutable;
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
