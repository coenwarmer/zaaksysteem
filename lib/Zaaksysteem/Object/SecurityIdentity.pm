package Zaaksysteem::Object::SecurityIdentity;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::Object::SecurityIdentity - Bare implementation of the Object
security infrastructure C<security_identity> interface.

=head1 DESCRIPTION

This package serves two possible purposes, as interface-compliant
data-wrapper, or as base for child classes that want to have the security
identity as the main purpose.

Internally this class is instantiated for light-weight data wrapping, in cases
where getting at the real object is expensive, this can be used in place.

=head1 ATTRIBUTES

=head2 entity_id

This attribute holds a string representing the entity's 'remote' identifier.
Expect integer, UUID or composit values.

=cut

has entity_id => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

=head2 entity_type

This attribute holds a string representing the entity's type. This attribute
functions like namespacing for the remote identifiers.

=cut

has entity_type => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

=head1 METHODS

=head2 security_identity

This method implements the security identity interface used by the
L<Zaaksysteem::Object> infrastructure.

=cut

sub security_identity {
    my $self = shift;

    return ($self->entity_type, $self->entity_id);
}

=head2 compare

This method compares the identity instance to another instance and returns
true if either is a non-proper subset of the other.

=cut

sub compare {
    my ($a, $b) = @_;

    for my $sid ($a, $b) {
        unless (blessed $sid && $sid->isa(__PACKAGE__)) {
            return;
        }
    }

    return $a->entity_type eq $b->entity_type &&
           $a->entity_id eq $b->entity_id;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

