package Zaaksysteem::Object::Queue::Model::NatuurlijkPersoon;
use Moose::Role;

use Zaaksysteem::Tools qw(sig dump_terse);
with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Object::Queue::Model::NatuurlijkPersoon - NatuurlijkPersoon queue item handler

=head1 DESCRIPTION

=head1 METHODS

=head2 disable_natuurlijk_persoon

Disable natuurlijk personen

=cut

sig disable_natuurlijk_persoon => 'Zaaksysteem::Backend::Object::Queue::Component';

sub disable_natuurlijk_persoon {
    my ($self, $item) = @_;

    my $schema = $item->result_source->schema;

    my $np = $schema->resultset('NatuurlijkPersoon')
        ->find($item->data->{natuurlijk_persoon_id});

    $np->disable_natuurlijk_persoon;

    return 1;
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
