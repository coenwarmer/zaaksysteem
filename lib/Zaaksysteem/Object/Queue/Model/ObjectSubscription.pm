package Zaaksysteem::Object::Queue::Model::ObjectSubscription;
use Moose::Role;

use Zaaksysteem::Tools qw(sig dump_terse);
with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Object::Queue::Model::ObjectSubscription - Object subscription queue item handler

=head1 DESCRIPTION

=head1 METHODS

=head2 enable_object_subscription

Enable object subscriptions

=cut

sig enable_object_subscription => 'Zaaksysteem::Backend::Object::Queue::Component';

sub enable_object_subscription {
    my ($self, $item) = @_;

    my $schema = $item->result_source->schema;

    my $subscription = $schema->resultset('ObjectSubscription')
        ->find($item->data->{subscription_id});

    $subscription->interface_id->process_trigger('enable_object_subscription', $item->data);

    return 1;
}

=head2 disable_object_subscription

Disable object subscriptions

=cut

sig disable_object_subscription => 'Zaaksysteem::Backend::Object::Queue::Component';

sub disable_object_subscription {
    my ($self, $item) = @_;

    my $schema = $item->result_source->schema;

    my $subscription = $schema->resultset('ObjectSubscription')
        ->find($item->data->{subscription_id});

    $subscription->interface_id->process_trigger('disable_subscription', $item->data);

    return 1;
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
