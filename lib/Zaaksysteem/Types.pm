package Zaaksysteem::Types;

use Zaaksysteem::Constants qw(VALID_FQDN SUBJECT_TYPES);

use MooseX::Types -declare => [qw(
    ArrayRefFQDN
    ArrayRefIPs
    ArrayRefOfHashRefs
    Betrokkene
    BetrokkeneBedrijf
    Boolean
    CustomerType
    FQDN
    Host
    IPv4
    IPv6
    IntervalStr
    JSONBoolean
    JSONNum
    JSON_XS_Boolean
    NonEmptyStr
    Otap
    PackageElementStr
    RelatedCaseType
    RelatedCaseTypes
    Timestamp
    UUID
    ZSNetworkACL
    ZSNetworkACLs
    HashedPassword
    SubjectType

    NLZipcode
)];

use MooseX::Types::Moose qw(Str Int Num Bool ArrayRef HashRef Item);

use DateTime::Format::ISO8601 qw[];

=head1 NAME

Zaaksysteem::Types - Custom types for Zaaksysteem

=head1 SYNOPSIS

    package MyClass;
    use Moose;
    use Zaaksysteem::Types qw(TYPE1 TYPE2);

    has attr => (
        isa => TYPE1,
        is => 'ro',
    );

=head1 AVAILABLE TYPES

=head2 ZSNetworkACL

A ZSNetworkACL type

=cut

subtype ZSNetworkACL, as Str,
    where {
        my $str = shift;
        # IPv4/IPv6 regexp are coming from https://github.com/waterkip/regexp-ip which is not yet on CPAN. I should probably do that one day.
        my $ipv4_re = '(?^:([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5])))';
        my $ipv6_re = '(?^:(?::(?::[0-9a-fA-F]{1,4}){0,5}(?:(?::[0-9a-fA-F]{1,4}){1,2}|:(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5])))|[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}|:)|(?::(?:[0-9a-fA-F]{1,4})?|(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))))|:(?:(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|[0-9a-fA-F]{1,4}(?::[0-9a-fA-F]{1,4})?|))|(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|:[0-9a-fA-F]{1,4}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){0,2})|:))|(?:(?::[0-9a-fA-F]{1,4}){0,2}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){1,2})|:))|(?:(?::[0-9a-fA-F]{1,4}){0,3}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){1,2})|:))|(?:(?::[0-9a-fA-F]{1,4}){0,4}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){1,2})|:))|::))';
        my $re = qr/^(?:$ipv4_re|$ipv6_re)(?:|\s+(?:.+))?$/;
        return $str =~ /$re/;
    };

=head2 ZSNetworkACLs

An array ref of L<ZSNetworkACL> types

=cut

subtype ZSNetworkACLs, as ArrayRef[ZSNetworkACL];

=head2 FQDN

A type (subtype of Str) that only allows syntactically valid fully qualified domain names.

=cut

subtype FQDN,
    as Str,
    where {
        return VALID_FQDN->($_);
    };

=head2 ArrayRefFQDN

A type subtype which defines an array reference of FQDN types.

=cut

subtype ArrayRefFQDN, as ArrayRef[FQDN];

=head2 HOST

A hostname, the ones before a 'dot'

=cut

subtype Host,
    as Str,
    where {
        return $_ =~ /^[a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9]$/;
    };

=head2 CustomerType

A type (subtype of Str) that only allows syntactically valid Customer Types

=over

=item * commercial

=item * government

=item * commercieel

=item * overheid

=back

=cut

subtype CustomerType, as Str,
    where {
        return $_ =~ /^(?:commercial|government|overheid|commercieel)$/;
    },
    message { "'$_' is not a valid Customer Type" };

=head2 Betrokkene

A type (subtype of Str) that only allows syntactically valid betrokkene ID's

=cut

subtype Betrokkene, as Str,
    where {
        return $_ =~ /betrokkene-(?:natuurlijk_persoon|medewerker|bedrijf)-\d+/;
    },
    message { "'$_' is not a valid Betrokkene" };


=head2 BetrokkeneBedrijf

A type (subtype of Str) that only allows syntactically valid betrokkene ID's for companies

=cut

subtype BetrokkeneBedrijf, as Str,
    where {
        return $_ =~ /betrokkene-bedrijf-\d+/;
    },
    message { "'$_' is not a valid BetrokkeneBedrijf" };

=head2 Otap

A type (subtype of Str) that only allows syntactically valid OTAP environments.

=over

=item * development (D)

=item * testing (T)

=item * accept (A)

=item * production (P)

=back

=cut

subtype Otap, as Str,
    where {
        return $_ =~ /^(?:development|testing|accept|production)$/;
    },
    message { "'$_' is not a valid OTAP type" };

=head2 HashedPassword

A type (subtype of Str) that only allows hashed passwords, which start with their hashing algorithm in curly brackets {}

Allowed algorithms:

=over 4

=item SSHA

Salted HASH

=back

=cut

subtype HashedPassword, as Str,
    where {
        return $_ =~ /^\{SSHA\}.+$/;
    },
    message { "'$_' is not a valid hashed password" };

=head2 IPv4

A type (subtype of Str) that only allows syntactically valid IPv4 addresses.

=cut

subtype IPv4, as Str,
    where {
        my ($ip, $range) = split(/\//, shift);
        return 0 if !$ip;
        my $ipv4_re = '(?^:([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5])))';

        $ipv4_re = qr/^$ipv4_re$/;

        if ($ip =~ /$ipv4_re/ && ( !defined $range or $range =~ /^\d+/ && $range >= 1 && $range <= 32)) {
            return 1;
        }
        return 0;
    },
    message { "$_ is not a valid IPv4 CIDR address" };

=head2 IPv6

A type (subtype of Str) that only allows syntactically valid IPv6 addresses.

=cut

subtype IPv6, as Str,
    where {
        my ($ip, $range) = split(/\//, shift);
        return 0 if !$ip;
        my $ipv6_re = '(?^:(?::(?::[0-9a-fA-F]{1,4}){0,5}(?:(?::[0-9a-fA-F]{1,4}){1,2}|:(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5])))|[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}:(?:[0-9a-fA-F]{1,4}|:)|(?::(?:[0-9a-fA-F]{1,4})?|(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))))|:(?:(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|[0-9a-fA-F]{1,4}(?::[0-9a-fA-F]{1,4})?|))|(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|:[0-9a-fA-F]{1,4}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){0,2})|:))|(?:(?::[0-9a-fA-F]{1,4}){0,2}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){1,2})|:))|(?:(?::[0-9a-fA-F]{1,4}){0,3}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){1,2})|:))|(?:(?::[0-9a-fA-F]{1,4}){0,4}(?::(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\.(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))|(?::[0-9a-fA-F]{1,4}){1,2})|:))|::))';

        $ipv6_re = qr/$ipv6_re/;
        if ($ip =~ /$ipv6_re/ && ( !defined $range or $range =~ /^\d+/ && $range >= 1 && $range <= 64)) {
            return 1;
        }
        return 0;
    },
    message { "$_ is not a valid IPv6 CIDR address" };

=head2 ArrayRefIPs

An array ref of IPv4 and/or IPv6 addresses

=cut

subtype ArrayRefIPs, as ArrayRef[IPv4|IPv6];

=head2 UUID

A type (subtype of Str) that only allows syntactically valid UUIDs.

=cut

subtype UUID,
    as Str,
    where {
        my $str = shift;
        return $str =~ /^[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}$/i;
    };

=head2 NonEmptyStr

A type (subtype of Str) that defines a label

=cut

subtype NonEmptyStr,
    as Str,
    where {
        my $str = shift;
        return $str =~ /^\p{XPosixSpace}*(?:\p{XPosixAlnum}|\p{XPosixPunct})+/;
    };

=head2 Boolean

Unfortunatly, the C<Bool> type of Moose will stringify your input, and modules like JSON will see it as a string.

B<example>

    has disabled => (
        is     => 'rw',
        isa    => 'Bool',
        default => 1,
    );

    ### $object->disabled becomes "disabled: '1'"

Except, when using Boolean, it will work as you would expect. It has the EXACT same functionality
as Bool, but will also return the right value to JSON.

    has disabled => (
        is     => 'rw',
        isa    => 'Boolean',
        default => 1,
    );

    ### $object->disabled becomes "disabled: 1"

=cut

## !ref($_) added to correctly find out JSON::XS::Boolean, because it normally stringifies to 0 or 1. But
## when send to JSON, it will inflate to "true" or "false". This could be a good implementation, but we
## chose to send back 1 of 0.

subtype Boolean, as Item,
    where { !ref($_) && (!defined($_) || $_ eq '1' || $_ eq '0' || "$_" eq '1'|| "$_" eq '0' || $_ eq "") };

class_type JSON_XS_Boolean, { class => 'JSON::XS::Boolean' };
coerce Boolean, from JSON_XS_Boolean, via { if (JSON::XS::is_bool($_)) { return ($_ == 1 ? 1 : 0) } };

=head2 JSONBoolean

As a bonus, we extended the C<Boolean> function. This one will send back proper true and false values. It will
coerce everything into JSON::XS::true or JSON::XS::false.

B<example>

    has disabled => (
        is     => 'rw',
        isa    => 'JSONBoolean',
        default => 1,
        coerce => 1,
    );

    ### $object->disabled becomes "disabled: true"

    has disabled => (
        is     => 'rw',
        isa    => 'JSONBoolean',
        default => { JSON::XS::true },
        coerce => 1,
    );

    ### $object->disabled becomes "disabled: true"

    has disabled => (
        is     => 'rw',
        isa    => 'JSONBoolean',
        default => 0,
        coerce => 1,
    );

    ### $object->disabled becomes "disabled: false"

    has disabled => (
        is     => 'rw',
        isa    => 'JSONBoolean',
        default => { JSON::XS::false },
        coerce => 1,
    );

    ### $object->disabled becomes "disabled: false"


=cut

subtype JSONBoolean, as Item,
    where { JSON::XS::is_bool($_); };

coerce JSONBoolean, from Bool, via { ($_ ? JSON::XS::true : JSON::XS::false) };

subtype JSONNum, as Num,
    where { my $num = $_; return length( do { no warnings "numeric"; $num & "" }); };

coerce JSONNum, from Num, via { my $rv = $_; $rv += 0; return $rv; };

=head2 Timestamp

The C<Timestamp> type wraps L<DateTime> objects, and adds C<Str> => C<DateTime>
coercion (via L<DateTime::Format::ISO8601/parse_datetime>.

=cut

class_type Timestamp, { class => 'DateTime' };
coerce Timestamp, from Str, via { DateTime::Format::ISO8601->parse_datetime($_) };

=head2 IntervalStr

A string representing an interval "type".

Allowed values are:

=over

=item * years

=item * months

=item * weeks

=item * days

=item * once

=back

=cut

subtype IntervalStr, as Str,
    where { $_ =~ /^(?:years|months|weeks|days|once)$/ };

=head2 PackageElementStr

A valid element of a Perl package name.

Currently, we only allow ASCII (even though Perl allows Unicode in its package
names).

=cut

subtype PackageElementStr, as Str,
    where { $_ =~ /^[a-zA-Z0-9_]+$/ };


=head1 Subject restrictions

=head2 SubjectType

One of three possible subject types, see constants 

=cut

subtype SubjectType, as Str,
    where {
        my $type = $_;       
        return (grep ({ $_ eq $type } @{ SUBJECT_TYPES() }) ? 1 : 0);
    },
    message { "'$_' is not a valid subject type" };

=head2 NLZipcode

One of three possible subject types, see constants 

=cut

subtype NLZipcode, as Str,
    where { return $_ =~ /^[1-9][0-9]{3}[A-Z]{2}/ },
    message { "'$_' is not a valid dutch zipcode, remove space?" };



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

