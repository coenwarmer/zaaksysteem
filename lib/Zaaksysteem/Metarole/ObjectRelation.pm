package Zaaksysteem::Metarole::ObjectRelation;

use Moose::Role;
use namespace::autoclean;

use Zaaksysteem::Object::Attribute;

use Zaaksysteem::Tools;

use Moose::Meta::TypeConstraint;
use Moose::Util::TypeConstraints qw[enum role_type subtype];

use List::MoreUtils qw[all];
use List::Util qw[first];

=head1 NAME

Zaaksysteem::Metarole::ObjectAttribute - Hijack L<Moose::Meta::Attribute>s for
L<Zaaksysteem::Object::Relation> infrastructure

=head1 DESCRIPTION

This L<role|Moose::Role> is meant to be applied on Moose attributes in classes
that are intended to behave like Objects.

    package Zaaksysteem::Object::Types::SomeType;

    use Moose;

    extends 'Zaaksysteem::Object';

    has attr => (
        is => 'ro',
        isa => 'Str',
        traits => [qw[OR]],

        label => 'Relation',
        documentation => 'Relation description',
        required => 1,

        cardinality => 'one-to-one'
    );

=cut

# OR as initialism, we're probably going to be using this one all the thyme,
# so it'd better be short.
Moose::Util::meta_attribute_alias('OR');

=head1 ATTRIBUTES

=head2 label

=cut

has label => (
    isa => 'Str',
    is => 'ro',
    required => 1,
    default => sub {
        shift->name
    }
);

=head2 isa_set

=cut

has isa_set => (
    is => 'ro',
    isa => 'Bool',
    required => 1,
    default => 0
);

=head2 embed

=cut

has embed => (
    is => 'ro',
    isa => 'Bool',
    required => 1,
    default => 0
);

=head1 METHODS

=cut

before _process_options => sub {
    my ($self, $name, $options) = @_;

    return if exists $options->{ isa };

    my $role_constraint = role_type('Zaaksysteem::Object::Reference');
    my $type_constraint = $role_constraint;

    my $type = delete $options->{ type };

    if ($type) {
        $type_constraint = subtype({
            as => $role_constraint,
            where => sub { $_->type eq $type }
        });
    }

    if ($options->{ isa_set }) {
        # Crude ArrayRef[$type_constraint] implementation, no reasonable way
        # to do this with the current TypeConstraint::Parameterizable
        # implementation. So we don't have native value type delegation either
        # and the 'handles' is thus unavailable. You'll have to roll your own
        # set accessors in the attached metaclass.
        $options->{ isa } = subtype({
            as => 'ArrayRef',
            where => sub { all { $type_constraint->check($_) } @{ $_ } },
            message => sub {
                return sprintf(
                    'Value not a %s reference: %s',
                    $type // 'object',
                    first { $type_constraint->validate($_) } @{ $_ }
                );
            }
        });

        return;
    }

    $options->{ isa } = $type_constraint;

    return;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

