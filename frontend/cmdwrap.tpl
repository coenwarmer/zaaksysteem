@IF EXIST "%~dp0\node.exe" (
  "%~dp0\node.exe"  "%~dp0\..\PACKAGE_NAME\bin\PACKAGE_NAME" %*
) ELSE (
  @SETLOCAL
  @SET PATHEXT=%PATHEXT:;.JS;=;%
  node  "%~dp0\..\PACKAGE_NAME\bin\PACKAGE_NAME" %*
)
