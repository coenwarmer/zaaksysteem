/*global angular*/
(function ( ) {
	
	angular.module('MintJS.core')
		.animation('.ml-animate-auto-height', [ 'animationService', function ( animationService ) {
			
			function getRealHeight ( element ) {
				return element[0].scrollHeight + (element[0].marginTop || 0) + (element[0].marginBottom || 0);
			}
			
			function getCurrentHeight ( element ) {
				return element[0].offsetHeight;
			}
			
			function hasTransitions ( element ) {
				var key = '$$ngAnimateCSS3Data',
					data = element.data(key);
				
				return !!(data && data.timings.transitionPropertyStyle !== 'none');
			}
			
			return {
				enter: function ( element, done ) {
					done();
				},
				leave: function ( element, done ) {
					done();
				},
				move: function ( element, done ) {
					done();
				},
				beforeAddClass: function ( element, className, done ) {

					if(className === 'ng-hide') {
						element.css('max-height', getCurrentHeight(element) + 'px');
					}

					done();
				},
				addClass: function ( element, className, done ) {
					var unbind;

					if(className === 'ng-hide') {
						if(hasTransitions(element)) {
							element.css('max-height', '0px');
							unbind = animationService.onEnd(element, function ( ) {
								element.css('max-height', '');
								unbind();
							});
						} else {
							element.css('max-height', '');
						}
					}

					done();
				},
				beforeRemoveClass: function ( element, className, done ) {

					if(className === 'ng-hide') {
						element.css('max-height', getCurrentHeight(element) + 'px');
					}

					done();
				},
				removeClass: function ( element, className, done ) {
					var unbind;

					if(className === 'ng-hide') {
						if(hasTransitions(element)) {
							unbind = animationService.onEnd(element, function ( ) {
								element.css('max-height', '');
								unbind();
							});
							element.css('max-height', getRealHeight(element) + 'px');
						} else {
							element.css('max-height', '');
						}
					}

					done();
				}
			};
			
		}]);
	
})();
