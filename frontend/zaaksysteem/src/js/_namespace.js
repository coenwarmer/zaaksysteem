/*global window*/
(function ( ) {
	
	var registered = {};
	var resolved = {};
	
	// define functions in the global namespace
	
	window.define = function ( alias, factory ) {
		if(registered[alias]) {
			throw new Error('cannot redefine alias ' + alias);
		}
		registered[alias] = factory;
	};
	
	window.fetch = function ( alias ) {
		if(!resolved[alias]) {
			resolved[alias] = registered[alias]();
		}
		return resolved[alias];
	};
	
})();