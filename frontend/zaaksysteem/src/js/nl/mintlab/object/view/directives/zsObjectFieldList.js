/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.object.view')
		.directive('zsObjectFieldList', [ '$sce', 'zqlService', 'systemMessageService', 'translationService', 'zqlEscapeFilter', function ( $sce, zqlService, systemMessageService, translationService, zqlEscapeFilter ) {
			
			return {
				require: [ 'zsObjectFieldList', '^zsObjectView' ],
				controller: [ function ( ) {
					
					var ctrl = {},
						fields,
						zsObjectView,
						loading = true;
					
					ctrl.getFields = function ( ) {
						return fields;
					};
					
					ctrl.link = function ( controllers ) {
						zsObjectView = controllers[0];
						
						zsObjectView.loadObject()
							.then(function ( ) {
								
								var object = zsObjectView.getObject();
								
								zqlService.query('SELECT WITH DESCRIPTION {} FROM ' + object.type + ' WHERE object.uuid = ' + zqlEscapeFilter(object.id))
									.success(function ( response ) {
										var obj = response.result[0],
											type = zsObjectView.getObjectType();
										
										fields = _.map(obj.describe, function ( attr ) {
											
											var order = _.findIndex(type.values.attributes, { name: 'attribute.' + attr.name });
											
											return {
												label: attr.human_label,
												human_value: attr.human_value,
												value: attr.value,
												type: attr.attribute_type,
												order: order
											};
										});
										
										fields.unshift({
											label: translationService.get('Object ID'),
											human_value: object.id,
											value: object.id,
											type: 'text',
											order: -1
										});
										
										fields = _.sortBy(fields, 'order');
										
									})
									.error(function ( /*response*/ ) {
										systemMessageService.emitLoadError('de gegevens');
									})
									['finally'](function ( ) {
										loading = false;
									});
							});
						
					};
					
					ctrl.getFieldTemplate = function ( field ) {
						var tplUrl = '/html/object/view/field-';
						
						switch(field.type) {
							default:
							tplUrl += 'generic';
							break;
							
							case 'file':
							case 'subject':
							case 'date':
							tplUrl += field.type;
							break;
							
							case 'url':
							case 'image_from_url':
							tplUrl += 'url';
							break;
						}
						
						return tplUrl + '.html';
					};
					
					ctrl.getTrustedHtml = function ( field ) {
						var humanValue = field.human_value,
							html = '';
							
						if(humanValue !== undefined) {
							html = $sce.trustAsHtml(String(humanValue));
						}
						
						return html;
					};
					
					ctrl.isLoading = function ( ) {
						return loading;
					};
					
					return ctrl;
					
				}],
				controllerAs: 'objectFieldList',
				link: function ( scope, element, attrs, controllers ) {
					controllers[0].link(controllers.slice(1));
				}
			};
			
		}]);
	
})();
