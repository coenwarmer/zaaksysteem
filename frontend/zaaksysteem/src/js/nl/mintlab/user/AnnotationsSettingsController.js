/*global angular*/
(function () {

    angular.module('Zaaksysteem.user')
        .controller('nl.mintlab.user.AnnotationsSettingsController', [ '$scope', 'smartHttp', 'translationService', function ( $scope, smartHttp, translationService ) {
            
            $scope.clearAnnotations = function ( ) {
                smartHttp.connect( {
                    method: 'POST',
                    url: '/api/user/annotations/clear',
                    data: { confirm: true }
                })
                    .success(function ( data ) {
                        var count = data.result[0].count;

                        $scope.$emit('systemMessage', {
                            type: 'info', 
                            content: translationService.get('Alle opgeslagen annotaties (' + count + ') zijn gewist.')
                        });
                    })
                    .error(function ( /*data*/ ) {
                        var errorObj = {
                                content: 'Annotaties konden niet worden gewist',
                                type: 'error'
                            };
                                        
                        $scope.$emit('systemMessage', errorObj);
                    });
            };
            

        }]);
}());