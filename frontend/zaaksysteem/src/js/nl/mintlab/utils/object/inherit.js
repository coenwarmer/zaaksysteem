(function ( ) {
	
	// Holy Grail inherit function, via Stoyan Stefanov's JavaScript Patterns
	
	define('nl.mintlab.utils.object.inherit', function ( ) {
		return function ( C, P ) {
			var F = function (){};
			F.prototype = P.prototype;
			C.prototype = new F();
			C.uber = P.prototype;
			C.prototype.constructor = C;
		};
	});
	
})();