/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.filter('truncate', [ function ( ) {
			
			return function ( str, max ) {
				if(!str || max === undefined) {
					return str;
				}
				
				if(str.length > max) {
					str = str.substr(0, max) + '…';
				}
				
				return str;
				
			};
			
		}]);
	
})();