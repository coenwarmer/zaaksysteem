/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsSystemMessageComponent', [ '$document', function ( $document ) {
			
			var safeApply = window.zsFetch('nl.mintlab.utils.safeApply'),
				contains = window.zsFetch('nl.mintlab.utils.dom.contains');
			
			return {
				link: function ( scope, element/*, attrs*/ ) {
					
					function onMouseDown ( event ) {
						var el = element[0],
							target = event.target;
							
						if(!(el === target || contains(el, target))) {
							safeApply(scope, function ( ) {
								scope.closeMessage(scope.message);
							});	
						}
					}
					
					$document.bind('mousedown', onMouseDown);
					
					scope.$on('$destroy', function ( ) {
						$document.unbind('mousedown', onMouseDown);
					});
					
				}
			};
			
		}]);
	
})();
