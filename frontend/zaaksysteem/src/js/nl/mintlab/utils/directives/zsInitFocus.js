/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsInitFocus', [ function ( ) {
			
			return function ( scope, element/*, attrs*/ ) {
				
				element[0].focus();
				
			};
			
		}]);
	
})();