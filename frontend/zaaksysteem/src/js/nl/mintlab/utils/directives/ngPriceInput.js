/*global angular,fetch,console*/
(function () {
    "use strict";

    angular.module('Zaaksysteem')
        .directive('ngPriceInput', [ '$parse', '$window', '$document', function ($parse, $window, $document) {
            return {
                require: 'ngModel',
                link: function (scope, element, attr, ngModelCtrl) {
                    function fromUser(text) {
                        var transformedInput = text.replace(/[^\d\.]/g, '');
                        if (transformedInput !== text) {
                            ngModelCtrl.$setViewValue(transformedInput);
                            ngModelCtrl.$render();
                        }
                        return transformedInput;
                    }
                    ngModelCtrl.$parsers.push(fromUser);
                }
            };
        }]);
}());