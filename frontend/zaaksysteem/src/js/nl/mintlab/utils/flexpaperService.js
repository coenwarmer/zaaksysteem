/*global angular,$,navigator*/
(function () {
    "use strict";

/*

Flexpaper plugin displays PDFs in a in-page viewer.
This service deals with its browser dependencies.

It's possible to use it with HTML4 browsers, but that would require
a PDF to JS converter in the backend. This is currently not implemented.

The browser universe for Flexpaper consists of three segments:
- IE < 10: No support, download the PDF instead
- IPad: Open the viewer in a new tab
- everything else: Display in an iframe.

The intended flow is:
- somebody clicks on a flexpaper enabled button
- check if flexpaper is supported at all, if not download PDF
   -> canDisplay
- check if iframe-viewing is supported, if not open new window
    -> canDisplayInIFrame
- otherwise render the fancy version.

Pseudocode:
    if (canDisplay) {
        if (canDisplayInIFrame) {
            // fancy version
        } else {
            // new tab/window version
        }
    } else {
        // download
    }

*/
    angular.module('Zaaksysteem')
        .factory('flexpaperService', [function () {

            return {
                // The $.browser functionality will die with JQuery 1.9.
                canDisplay: function () {
                    return !($.browser.msie && $.browser.version < 10);
                },

                // in IPad annotations don't work within the iframe
                canDisplayInIFrame: function () {
                    return navigator.userAgent.match(/iPad/i) === null;
                }
            };

        }]);
}());