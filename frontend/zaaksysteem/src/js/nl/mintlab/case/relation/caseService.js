/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.factory('caseService', [ '$q', '$http', function ( $q, $http ) {
			
			var caseService = {};
			
			caseService.resolveCase = function ( caseId, result, reason ) {
				var deferred = $q.defer();
				
				$http({
					method: 'POST',
					url: '/zaak/' + caseId + '/update/afhandelen',
					data: {
						reden: reason,
						system_kenmerk_resultaat: result,
						update: 1
					}
				})
					.success(function ( /*response*/ ) {
						deferred.resolve();
					})
					.error(function ( /*response*/ ) {
						deferred.reject();
					});
				
				return deferred.promise;
			};
			
			return caseService;
			
		}]);
	
})();
