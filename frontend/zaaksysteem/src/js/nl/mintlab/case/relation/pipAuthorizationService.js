/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.case.relation')
		.factory('pipAuthorizationService', [ '$q', function ( $q ) {
			
			var pipAuthorizationService = {},
				rolePromise;
				
			function getRolePromise ( ) {
				var deferred = $q.defer(),
					roles;
					
				roles = _.map([
					'Advocaat',
					'Auditor',
					'Aannemer',
					'Bewindvoerder',
					'Familielid',
					'Gemachtigde',
					'Mantelzorger',
					'Ouder',
					'Verzorger',
					'Ontvanger',
					'Anders'
				], function ( str ) {
					return {
						value: str,
						label: str
					};
				});
					
				Object.freeze(roles);
				
				deferred.resolve(roles);
				
				return deferred.promise;
			}
			
			pipAuthorizationService.getRoles = function ( ) {
				if(!rolePromise) {
					rolePromise = getRolePromise();
				}
				return rolePromise;
			};
			
			pipAuthorizationService.getApiParams = function ( authorization ) {
				var data = _.pick(authorization, 'role', 'magic_string_prefix', 'pip_authorized', 'betrokkene_identifier', 'notify_subject');
				return data;
			};
			
			return pipAuthorizationService;
			
		}]);
	
})();
