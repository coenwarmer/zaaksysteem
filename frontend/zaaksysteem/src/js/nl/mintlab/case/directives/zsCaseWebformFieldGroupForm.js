/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.directive('zsCaseWebformFieldGroupForm', [ function ( ) {
			
			var cancelEvent = fetch('nl.mintlab.utils.events.cancelEvent');
			
			return {
				require: [ 'zsCaseWebformFieldGroupForm', 'zsCaseWebformFieldGroup' ],
				controller: [  '$scope', '$element', function ( $scope, $element ) {
					
					var ctrl = {},
						zsCaseWebformFieldGroup,
						anchor,
						el;
					
					ctrl.link = function ( controllers ) {
						zsCaseWebformFieldGroup = controllers[0];
					};
					
					function onClick ( event ) {
						if(!zsCaseWebformFieldGroup.isVisible()) {
							cancelEvent(event, true);
						}
					}
					
					el = $element[0].querySelectorAll('a');
					
					anchor = angular.element(el);
					
					anchor.bind('click', onClick);
					
					$scope.$on('$destroy', function ( ) {
						anchor.unbind('click', onClick);
					});
					
					return ctrl;
					
				}],
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].link(controllers.slice(1));
					
				}
			};
			
		}]);
	
	
})();
