/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsCaseWebformInner', [ function ( ) {
			
			return {
				scope: true,
				require: [ 'zsCaseWebformInner', '^zsCaseWebform' ],
				controller: [ '$scope', '$element', function ( $scope, $element ) {
					
					var ctrl = {},
						zsCaseWebform;
					
					ctrl.setControls = function ( ) {
						zsCaseWebform = arguments[0];
						zsCaseWebform.setWebformInner(ctrl);
					};
					
					ctrl.destroy = function ( ) {
						while($scope.$$childHead) {
							$scope.$$childHead.$destroy();
						}
						$element[0].innerHTML = '';
					};
					
					return ctrl;
					
				}],
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].setControls.apply(controllers[0], controllers.slice(1));
					
				}
			};
			
		}]);
	
})();