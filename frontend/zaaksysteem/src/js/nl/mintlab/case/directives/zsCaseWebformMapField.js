/*global angular,$*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.directive('zsCaseWebformMapField', [ function ( ) {
			
			return {
				require: [ '^zsCaseWebformField'],
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].setSetter(function ( value ) {
						var el = element[0].querySelector('.ezra_map-autocomplete');
						if(el.value !== value) {
							el.value = value;
							// FIXME: this fails on mobile, wrap in try/catch block to prevent
							// other issues
							try {
								$(element[0]).ezra_map('setAddress', {
									value : value,
									no_update: 1
								});	
							} catch ( error ) {
								console.log(error);
							}
							
						}
					});
					
				}

			};
			
		}]);
	
})();