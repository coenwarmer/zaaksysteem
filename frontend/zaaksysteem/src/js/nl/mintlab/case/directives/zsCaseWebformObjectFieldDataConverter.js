/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.directive('zsCaseWebformObjectFieldDataConverter', [ 'bagService', function ( bagService ) {
			
			return {
				require: [ 'zsCaseWebformObjectFieldDataConverter', 'zsCaseWebformObjectField' ],
				controller: [ function ( ) {
					
					var ctrl = {},	
						zsCaseWebformObjectField,
						converted;
						
					ctrl.setControls = function ( ) {
						zsCaseWebformObjectField = arguments[0];
						if(converted) {
							zsCaseWebformObjectField.setList(converted);
						}
					};
					
					ctrl.setObjects = function ( objects ) {
						converted = _.map(objects, function ( obj ) {
							return {
								bag_id: obj.id,
								human_identifier: bagService.parseLabel(obj),
								address_data: obj
							};
						});
						
						if(zsCaseWebformObjectField) {
							zsCaseWebformObjectField.setList(converted);
						}
					};
					
					return ctrl;
				}],
				controllerAs: 'caseWebformObjectFieldDataConverter',
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].setControls.apply(controllers[0], controllers.slice(1));
					
				}
				
			};
			
		}]);
			
	
})();