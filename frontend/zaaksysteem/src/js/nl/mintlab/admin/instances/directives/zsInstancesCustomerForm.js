/*global angular,_*/
(function ( ) {

	angular.module('Zaaksysteem.admin.instances')
		.directive('zsInstancesCustomerForm', [ 'systemMessageService', 'api', 'formService', function ( systemMessageService, api, formService ) {

		return {
			scope: {
				'customer': '&',
				'onSubmit': '&',
				'onClose': '&'
			},
			template: '<div zs-form-template-parser data-config="instancesCustomerForm.getFormConfig()" zs-form-submit="instancesCustomerForm.handleSubmit($values)">/div>',
			controller: [ '$scope', function ( $scope ) {

				var ctrl = this,
					config = {
						name: 'customerForm'
					};

				config.fields = [
					{
						name: 'ipaddresses',
						label: 'IP-adressen',
						type: 'text',
						template: '/html/admin/instances/instances.html#form-field-ipaddresses',
						'default': [],
						required: false
					},
					{
						name: 'template',
						label: 'Template',
						type: 'select',
						data: {
							options: _.map($scope.customer().instance.available_templates, function ( tplId ) {
								return {
									value: tplId,
									label: tplId
								};
							})
						},
						required: true
					},
					{
						name: 'shortname',
						label: 'Short name',
						type: 'text',
						required: true
					},
					{
						name: 'allowed_instances',
						label: 'Maximum aantal omgevingen',
						type: 'number',
						required: true
					},
					{
						name: 'allowed_diskspace',
						label: 'Maximum diskpace (GB)',
						type: 'number',
						required: true
					},
					{
						name: 'read_only',
						label: 'Read-only',
						type: 'checkbox',
						data: {
							checkboxLabel: 'Ja'
						}
					}
				];

				config.actions = [
					{ 
						name: 'submit',
						type: 'submit',
						label: 'Opslaan'
					}
				];

				ctrl.getFormConfig = function ( ) {
					return config;
				};

				ctrl.handleSubmit = function ( values ) {

					var promise = $scope.onSubmit({
							$values: _(values)
								.extend({ ipaddresses: _.filter(values.ipaddresses, _.identity) } )
								.mapValues(function ( value ) {
									if(_.isArray(value) && value.length === 0) {
										value = null;
									}
									return value;
								})
								.value()
						});

					if(promise) {
						promise.then(function ( ) {
							systemMessageService.emitSave();
							$scope.onClose();
						})
						.catch(function ( error ) {
							var form = formService.get(config.name);
							
							if(error && error.type === 'validationexception') {
								form.setValidity(api.getLegacyFormValidations(config.fields, error));
								systemMessageService.emitValidationError();
							} else {
								systemMessageService.emitSaveError();
							}
						});
					} else {
						$scope.onClose();
					}
				};

				_.each($scope.customer().instance, function ( value, key ) {
					var field = _.find(config.fields, { name: key });
					if(field && value !== undefined) {
						field.value = angular.copy(value);
					}
				});

				if($scope.customer().instance.shortname.indexOf('betrokkene-bedrijf-') !== 0) {
					config.fields = _.reject(config.fields, { name: 'shortname' });
				}

			}],
			controllerAs: 'instancesCustomerForm'
		};

	}]);

})();
