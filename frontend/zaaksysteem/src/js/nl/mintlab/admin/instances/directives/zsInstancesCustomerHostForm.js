/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.admin.instances')
		.directive('zsInstancesCustomerHostForm', [ 'systemMessageService', 'api', function ( systemMessageService, api ) {
			
			return {
				restrict: 'E',
				template: 
					'<div data-zs-form-template-parser data-config="getFormConfig()" zs-form-submit="handleFormSubmit($values)">' + 
					'</div>',
				scope: {
					'host': '&',
					'instances': '&',
					'onSubmit': '&',
					'onComplete': '&',
					'ips': '&'
				},
				controller: [ '$scope', '$element', function ( $scope, $element ) {
					
					var config = {
							name: 'customerHostForm',
							fields: [
								{
									name: 'label',
									label: 'Titel',
									type: 'text',
									required: true
								},
								{
									name: 'fqdn',
									label: 'Host',
									type: 'text',
									required: true
								},
								{
									name: 'ip',
									label: 'IP',
									type: 'select',
									required: true,
									data: {
										options: getIpOptions()
									}
								},
								{
									name: 'ssl_cert',
									label: 'SSL Certificaat',
									type: 'textarea'
								},
								{
									name: 'ssl_key',
									label: 'SSL Private Key',
									type: 'textarea'
								},
								{
									name: 'instance',
									type: 'select',
									label: 'Omgeving',
									data: {
										options: []
									}
								}
							],
							actions: [
								{
									name: 'submit',
									type: 'submit',
									label: 'Opslaan',
									disabled: 'isSubmitDisabled()'
								}
							]
						},
						loading;
						
					function getIpOptions ( ) {
						
						return _($scope.ips() || [])
								.push($scope.host() ? $scope.host().instance.ip : null)
								.filter(_.identity)
								.unique()
								.map(function ( ip ) {
									return {
										value: ip,
										label: ip
									};
								})
								.value();
					}
						
					function getForm ( ) {
						var ctrl = $element.children().eq(0).inheritedData('$zsFormTemplateParserController'),
							form;
							
						if(ctrl) {
							form = ctrl.getForm();
						}
						
						return form;
					}
					
					$scope.getFormConfig = function ( ) {
						return config;	
					};
					
					$scope.handleFormSubmit = function ( $values ) {
						
						
						var host = $scope.host() || { type: 'host' },
							promise;
							
						host = 
							_.extend(host, {
								instance: angular.copy($values)
							});
							
						promise = $scope.onSubmit({ 
							$host: host
						});
							
						if(promise) {
							
							loading = true;
							
							promise
								.then(function ( ) {
									$scope.onComplete();
								})
								['catch'](function ( error ) {
									
									var form = getForm();
									
									if(error && error.type === 'validationexception') {
										form.setValidity(api.getLegacyFormValidations(config.fields, error));
										systemMessageService.emitValidationError();
									} else {
										systemMessageService.emitSaveError();
									}
									
									
								})
								['finally'](function ( ) {
									loading = false;
								});
								
						} else {
							$scope.onComplete();
						}
					};
					
					$scope.isSubmitDisabled = function ( ) {
						var form = getForm(),
							disabled = true;
							
						if(form) {
							disabled = !form.$valid && !loading;
						}
						
						return loading;
					};
					
					if($scope.host()) {
						_.each($scope.host().instance, function ( value, key ) {
							var field = _.find(config.fields, { name: key });
							
							if(field) {
								
								if(field.name === 'instance' && value && value.reference !== undefined) {
									value = value.reference;
								}
								
								field.value = value;
							}
							
						});
					}
					
					_.find(config.fields, { name: 'instance' }).data.options = 
						_($scope.instances())
							.map(function ( instance ) {
								return {
									label: instance.instance.label + ' (' + instance.instance.fqdn + ')',
									value: instance.reference
								};
							})
							.value().concat({
								label: 'Geen',
								value: null
							});
				}]
			};
			
		}]);
	
})();
