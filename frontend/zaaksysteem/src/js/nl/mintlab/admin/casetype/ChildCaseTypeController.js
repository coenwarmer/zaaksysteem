/*global angular,_*/
(function ( ) {
	angular.module('Zaaksysteem.admin.casetype')
		.controller('nl.mintlab.admin.casetype.ChildCaseTypeController', [ '$scope', 'smartHttp', 'translationService', function ( $scope, smartHttp, translationService ) {
				
			function save ( ) {
				smartHttp.connect({
					method: 'POST',
					url: '/api/casetype/' + $scope.caseTypeId + '/save_child_casetypes',
					data: {
						child_casetypes: JSON.stringify($scope.children)
					}
				})
					.error(function ( /*response*/ ) {
						$scope.$emit('systemMessage', {
							type: 'error'
						});
					});
			}
			
			$scope.addCaseType = function ( casetype ) {
				var children = $scope.children || [],
					isAdded = !!_.find(children, function ( child ) {
						return child.casetype.id.toString() === casetype.id.toString();
					});

				if(isAdded) {
					$scope.$emit('systemMessage', {
						type: 'error',
						content: translationService.get('Kindzaaktype "%s" al aanwezig.', casetype.zaaktype_node_id.titel)
					});
					return;
				}

				$scope.$emit('systemMessage', {
					type: 'info',
					content: translationService.get('Kindzaaktype "%s" toegevoegd.', casetype.zaaktype_node_id.titel)
				});
				
				children.push({
					"casetype": {
						"id": casetype.id,
						"title": casetype.zaaktype_node_id.titel
					},
					"import": true,
					"definitie": true,
					"authorisaties": true,
					"betrokkenen": true,
					"actions": true,
					"first_phase": false,
					"middle_phases": false,
					"last_phase": false,
					"_collapsed": false
				});
			};
			
			$scope.removeRelation = function ( relation, $event ) {
				$scope.children = _.reject($scope.children, function ( r ) {
					return relation.casetype.id === r.casetype.id;
				});
				$event.stopPropagation();
			};
			
			$scope.onImportToggleClick = function ( $event ) {
				$event.stopPropagation();	
			};
			
			$scope.$watch('children', function ( nwVal/*, oldVal, scope*/ ) {
				if(nwVal) {
					save();
				}
			}, true);
			
			$scope.$watch('newCaseType', function ( nwVal/*, oldVal, scope*/ ) {
				if(nwVal) {
					$scope.addCaseType(nwVal);
					$scope.newCaseType = null;
				}
			});
		
		}]);
	
})();