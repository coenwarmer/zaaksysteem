/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.admin')
		.directive('zsObjectTypeEdit', [ function ( ) {
			
			return {
				controller: [ '$scope', function ( $scope ) {					
					
					var ctrl = this;
					
					ctrl.getObjectValues = $scope.getObjectValues;
					
					ctrl.getObjectTypeId = function ( ) {
						return $scope.objectTypeId;	
					};
					
				}]
			};
			
		}]);
	
})();
