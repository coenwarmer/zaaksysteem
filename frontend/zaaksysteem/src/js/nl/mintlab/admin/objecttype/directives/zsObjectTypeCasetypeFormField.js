(function ( ) {
	
	angular.module('Zaaksysteem.admin.objecttype')
		.directive('zsObjectTypeCasetypeFormField', [ function ( ) {
			
			return {
				controller: [ function ( ) {
					
					var ctrl = {};

					ctrl.transform = function ( $object ) {
						return {
							'related_object_title': $object.values['casetype.name'],
							'related_object': null,
							'related_object_id': $object.id,
							'related_object_type': 'casetype'
						};
					};

					return ctrl;
					
				}],
				controllerAs: 'objectTypeCasetypeFormField'
				
			};
			
		}]);
	
})();
