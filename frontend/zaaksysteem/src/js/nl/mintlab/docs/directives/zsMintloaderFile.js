/*global angular*/
(function ( ) {

	angular.module('Zaaksysteem.docs')
		.directive('zsMintloaderFile', [ '$window', 'flexpaperService', '$http', 'systemMessageService',  function ( $window, flexpaperService, $http, systemMessageService ) {

			return {
				scope: true,
				controller: [ '$scope', '$element', function ( $scope, $element ) {

					var ctrl = this;

					ctrl.handleDocClick = function ( event ) {
						if (flexpaperService.canDisplay()) {
							if (flexpaperService.canDisplayInIFrame()) {
								$scope.openPopup();
							} else {
								$window.open($scope.flexpaperUrl);
							}
						} else {
							$window.open($scope.downloadUrl);
							event.stopPropagation();
						}
					};

					ctrl.handleRemoveClick = function ( event ) {

						var fileId = Number($scope.fileId),
							listItem = $element.parent(),
							files;
						
						event.stopPropagation();
						event.preventDefault();
						
						listItem.css('display', 'none');

						files = {};
						files[fileId] = 
							{
								action: 'update_properties',
								data: {
									deleted: true
								}
							};

						$http({
							url: '/api/bulk/file/update',
							method: 'POST',
							data: {
								files: files
							}
						})
							.success(function ( ) {
								listItem.remove();
								$scope.$destroy();
							})
							.error(function ( ) {
								listItem.css('display', '');
								
								systemMessageService.emitError('Het bestand kon niet worden verwijderd. Probeer het later opnieuw.');
							});

					};

				}],
				controllerAs: 'mintloaderFile'
			};

		}]);
}());
