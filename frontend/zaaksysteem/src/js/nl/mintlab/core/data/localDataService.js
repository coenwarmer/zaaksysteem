/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.core.data')
		.factory('localDataService', [ 'localStorageService', function ( localStorageService ) {
			
			var localDataService = {};
			
			localDataService.set = function ( key, value ) {
				localStorageService.set(key, { 'key': key, 'value': value } );
			};
			
			localDataService.get = function ( key ) {
				var stored = localStorageService.get(key);
				return stored ? stored.value : null;
			};
			
			localDataService.unset = function ( key ) {
				localStorageService.remove(key);
			};
			
			return localDataService;
			
		}]);
	
})();