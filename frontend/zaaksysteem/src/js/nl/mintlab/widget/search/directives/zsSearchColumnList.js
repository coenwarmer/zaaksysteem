/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.directive('zsSearchColumnList', [ function ( ) {
			
			return {
				controller: [ '$scope', '$element', '$attrs', function ( $scope, $element, $attrs ) {
					
					var ctrl = {};
					
					ctrl.getItems = function ( ) {
						var list = $scope.$eval($attrs.list),
							items;
							
						if(_.isArray(list)) {
							items = list;
						} else if(list) {
							items = [ list ];
						}
						
						return items;
					};
					
					return ctrl;
					
				}],
				controllerAs: 'searchColumnList'
			};
			
		}]);
	
})();