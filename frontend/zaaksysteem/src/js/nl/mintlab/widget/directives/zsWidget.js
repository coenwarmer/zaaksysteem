/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.widget')
		.directive('zsWidget', [ function ( ) {
			
			return {
				templateUrl: '/html/widget/widget.html',
				transclude: true,
				scope: true,
				link: function ( scope, element, attrs ) {
					
					attrs.$observe('zsWidgetTitle', function ( ) {
						scope.widgetTitle = attrs.zsWidgetTitle;
					});
				}
			};
			
		}]);
	
})();