#!/usr/bin/perl -w

use Data::Dumper;

use Cwd qw[realpath];
use FindBin;
use lib "$FindBin::Bin/../../lib";

use Catalyst qw[ConfigLoader];
use Catalyst::Log;
use Catalyst::Model::DBIC::Schema;

my $log = Catalyst::Log->new;

error("USAGE: $0 [dsn] [user] [password] [commit]") unless @ARGV && scalar(@ARGV) >= 3;

my ($dsn, $user, $password, $commit) = @ARGV;
my $dbic = database($dsn, $user, $password);

sub info {
    $log->info(sprintf shift, @_);
    $log->_flush;
}

sub error {
    $log->error(shift);
    $log->_flush;

    exit 1;
}

sub database {
    my ($dsn, $user, $password) = @_;

    Catalyst::Model::DBIC::Schema->config(
        schema_class => 'Zaaksysteem::Schema',
        connect_info => {
            dsn => $dsn,
            user => $user,
            password => $password,
            pg_enable_utf8 => 1
        }
    );

    Catalyst::Model::DBIC::Schema->new->schema;
}

$dbic->txn_do(sub {
    my $templates = $dbic->resultset('BibliotheekSjablonen');

    while(my $template = $templates->next) {
        my $filestore = $template->filestore_id;

        next unless $filestore->original_name =~ m[\.odf$];

        info('Processing "%s"', $template->naam);

        $new_name = $filestore->original_name;
        $new_name =~ s[\.odf$][.odt];

        info('Renaming %s to %s', $filestore->original_name, $new_name);

        $filestore->original_name($new_name);

        $filestore->update;
    }
});
