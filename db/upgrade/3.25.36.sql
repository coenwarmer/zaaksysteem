BEGIN;
    /* db/upgrade/3.25.35/1000-messages.sql */

    ALTER TABLE message ADD is_archived BOOLEAN NOT NULL DEFAULT FALSE;
    CREATE INDEX message_subject_archived_idx ON message(subject_id, is_archived);
    CREATE INDEX message_subject_read_idx ON message(subject_id, is_read);

    /* db/upgrade/3.25.35/1001-queue_items.sql */

    ALTER TABLE queue ADD COLUMN priority INTEGER DEFAULT 1000 NOT NULL;

COMMIT;
