BEGIN;

    CREATE TABLE object_relation (
        id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
        name TEXT NOT NULL,
        object_type TEXT NOT NULL,
        object_uuid UUID REFERENCES object_data (uuid),
        object_embedding TEXT
    );

    ALTER TABLE object_relation ADD CONSTRAINT object_relation_ref_xor_embed CHECK (
        object_uuid IS NULL != object_embedding IS NULL
    );

COMMIT;
