-- Add indexes when they do not exist yet. Long but safe version.

DO $$
BEGIN

IF NOT EXISTS (
    SELECT 1
    FROM   pg_class c
    JOIN   pg_namespace n ON n.oid = c.relnamespace
    WHERE  c.relname = 'bibliotheek_sjablonen_interface_id_idx'
    ) THEN

    CREATE INDEX bibliotheek_sjablonen_interface_id_idx ON bibliotheek_sjablonen (interface_id);
END IF;

IF NOT EXISTS (
    SELECT 1
    FROM   pg_class c
    JOIN   pg_namespace n ON n.oid = c.relnamespace
    WHERE  c.relname = 'bibliotheek_sjablonen_template_uuid_idx'
    ) THEN

    CREATE INDEX bibliotheek_sjablonen_template_uuid_idx ON bibliotheek_sjablonen (template_uuid);
END IF;

IF NOT EXISTS (
    SELECT 1
    FROM   pg_class c
    JOIN   pg_namespace n ON n.oid = c.relnamespace
    WHERE  c.relname = 'transaction_uuid_idx'
    ) THEN

    CREATE INDEX transaction_uuid_idx ON transaction (uuid);
END IF;

IF NOT EXISTS (
    SELECT 1
    FROM   pg_class c
    JOIN   pg_namespace n ON n.oid = c.relnamespace
    WHERE  c.relname = 'transaction_record_uuid_idx'
    ) THEN

    CREATE INDEX transaction_record_uuid_idx ON transaction_record (uuid);
END IF;

IF NOT EXISTS (
    SELECT 1
    FROM   pg_class c
    JOIN   pg_namespace n ON n.oid = c.relnamespace
    WHERE  c.relname = 'interface_uuid_idx'
    ) THEN

    CREATE INDEX interface_uuid_idx ON interface (uuid);
END IF;

END$$;
