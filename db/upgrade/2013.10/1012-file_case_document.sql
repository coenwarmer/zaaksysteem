BEGIN;

CREATE TABLE file_case_document (
    id SERIAL PRIMARY KEY,
    file_id INTEGER NOT NULL REFERENCES file(id),
    case_document_id INTEGER NOT NULL REFERENCES zaaktype_kenmerken(id)
);

ALTER TABLE file DROP COLUMN case_type_document_id;

COMMIT;