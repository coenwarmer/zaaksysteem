BEGIN;

-- This fixes entries like:
-- 
--  zaak_id | bibliotheek_kenmerken_id |      value       |  id
--  ---------+--------------------------+------------------+-------
--     2412 |                       24 | ARRAY(0xfc67160) | 65914
--
-- It has already been rollback tested on all production databases.

DELETE FROM zaak_kenmerk WHERE value ~ 'ARRAY\(0x.*\)$';

COMMIT;
