BEGIN;

ALTER TABLE transaction ADD COLUMN date_deleted timestamp without time zone;

ALTER TABLE transaction_record ADD COLUMN date_deleted timestamp without time zone;

ALTER TABLE transaction_record_to_object ADD COLUMN date_deleted timestamp without time zone;

COMMIT;