BEGIN;

    ALTER TABLE scheduled_jobs ADD COLUMN uuid UUID UNIQUE DEFAULT uuid_generate_v4();
    ALTER TABLE scheduled_jobs ADD FOREIGN KEY (case_id) REFERENCES zaak(id);

COMMIT;
