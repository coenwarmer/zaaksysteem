BEGIN;

    DROP TABLE IF EXISTS object_type_bibliotheek_entry;
    ALTER SEQUENCE object_type_bibliotheek_entry_id_seq RENAME TO object_bibliotheek_entry_id_seq;

COMMIT;
