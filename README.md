# Zaaksysteem.nl

This is the main code repository for the Zaaksysteem framework.

For more information, visit our [website](http://www.zaaksysteem.nl/).

## Short technical introduction

All development for Zaaksysteem follows the following procedure when it comes
to getting the code in this codebase. Since this is open source software, you
are free to fork, modify, redistribute and open pull-requests, under the
limitations of the
[EUPL license](http://joinup.ec.europa.eu/software/page/eupl).

### Branches

* master
    The master branch contains the latest *stable* and released version of
    Zaaksysteem. This is the version most people want.

* quarterly
    The quarterly branch has a common ancestor at the current master branch,
    and contains all new features, bugfixes and other modifications done in
    our three-monthly release cycle. This branch is considered *stable*, as it
    has been tested internally, but it has not seen a production environment
    yet.

* sprint
    This branch contains active and ongoing development modifications, it is
    *unstable* as it may contain modifications that have not been tested fully.
    It is part of our bi-weekly sprint cycle, and is named according to the ISO
    weeknumber of the week the sprint started in.

### Contributing

You are free to submit pull-requests for improvements to Zaaksysteem. Please
target those requests to our most recent sprint branch, and describe
liberally what the change does, why it does so, and what you believe the impact
will be.

### Running the development environment

Zaaksysteem uses [vagrant](http://www.vagrantup.com/) to manage development
environments that are the same for every developer. To create a new
environment, run `vagrant up` in the source directory. This will create a
virtual machine capable of running Zaaksysteem. For more information on how
this works, please refer to the Vagrant documentation. Vagrant 1.4.3 (the
version that comes with Ubuntu 14.04) is known to be compatible.

To run Zaaksysteem in the Vagrant VM, first open a connection to it using
`vagrant ssh`, then go to the source by typing `cd /vagrant` and finally, run
the development server with `./script/dev_fastcgi.pl`

You can now connect to Zaaksysteem on [https://10.44.0.11](https://10.44.0.11).
The default username is "admin", with password "admin".

### Support

We only support the community version of the software through the
[wiki](http://wiki.zaaksysteem.nl/). For professional support, please contact
[Mintlab](http://www.mintlab.nl/).

-- 
The [Mintlab](http://www.mintlab.nl/) Team
