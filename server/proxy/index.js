'use strict';
 
let http = require('http'),
	url = require('url'),
	pick = require('lodash/object/pick'),
	minimist = require('minimist'),
	fs = require('fs'),
	fetcherFactory = require('./fetcher');

let argv = minimist(process.argv.slice(2)),
	fetcher;

fs.readFile(argv.config || '', ( err, data ) => {

	let contents = data ? data.toString() : '',
		match = contents.match(/\s+class\s*=\s*Cache::Memcached[\s\S]*servers\s*=\s*\[\s*([\w\.:,\s]+)\s*]/),
		servers = [];

	if (match) {
		servers = match[1].split(',')
			.map(location => location.trim());
	}

	fetcher = fetcherFactory({
		memcachedAddress: servers
	});

});

let preflight = ( res ) => {
	res.writeHead(200, {
		'Access-Control-Allow-Methods': 'GET',
		'Access-Control-Allow-Headers': 'Content-Type, Authorization, ZS-User-Name, ZS-User-Password, API-Interface-ID'
	});
	res.end();
};

let generateManifest = ( req, res ) => {

	let params = url.parse(req.url, true).query,
		manifest = JSON.parse(params.manifest);

	res.writeHead(200, {
		'Content-Type': 'application/json'
	});

	res.end(
		JSON.stringify(
			pick(manifest, 'icons', 'short_name', 'name', 'start_url', 'background_color', 'theme_color', 'display', 'orientation')
		)
	);

};

let proxyRequest = ( req, res, options ) => {
	
	fetcher.proxy(req, res, options);

};

let flush = ( req, res ) => {

	fetcher.flush()
		.catch( ( ) => {
			res.writeHead(500);
			res.end('Could not flush proxy cache');
		})
		.then(( ) => {
			res.writeHead(200);
			res.end('Flushed proxy cache');
		});
};

let retry = ( req, res ) => {

	fetcher.retry()
		.catch( ( ) => {
			res.writeHead(500);
			res.end('Could not retry all requests');
		})
		.then(( ) => {
			res.writeHead(200);
			res.end('Reloaded all cached requests');
		});
};

http.createServer( ( req, res ) => {

	let domain = req.headers.host.split(':')[0],
		target = `https://${domain}`;

	res.setHeader('Access-Control-Allow-Origin', target);

	req.url = req.url.replace('/clientutil/proxy', '');

	if (req.method === 'OPTIONS') {
		preflight(res);
	} else if (req.url.indexOf('/manifest') === 0) {
		generateManifest(req, res);
	} else if (req.url.indexOf('/flush') === 0) {
		flush(req, res);
	} else if (req.url.indexOf('/retry') === 0) {
		retry(req, res);
	} else {
		proxyRequest(req, res);
	}

})
	.listen(1025);
