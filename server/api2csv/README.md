
api2csv - by Mintlab
--------------------------------------
Run server: node index.js --parameters 
--------------------------------------
Get output: curl --header "API-Interface-Id: 2" --header "API-Key: YOUR_ZS_API_KEY" "https://development.zaaksysteem.nl/app/api2csv"
--------------------------------------

Allows saving of Zaaksysteem JSON api outputs in a CSV format.
It runs as a server, responding to requests with the converted
CSV output.

It defaults to the result.instance.rows key when parsing the 
data. When it can't find this key, it will return an error 
message.

It flattens nested api outputs by default, but shows the path
of the nested keys in the first row of the csv output.
E.g.: result.instance.message


CLI parameters for api2csv server:
--------------------------------------
--port=<integer> (sets the port api2csv will listen to. Defaults to 1030.)
--server=<domain.of.server> (sets the server of the api that will be queried. Defaults to hostname of host environment)
--path=/path/to/api/call (sets path of the api that will be queried. Defaults to /api/v1/case)
--interfaceid=<integer> (sets a ZS interface ID in the header when doing requests to api. defaults to null)
--apikey=<string> (sets a api key in the header when doing requests to the api. defaults to null)
--user=<username> (sets a ZS username in the header when doing requests to api. defaults to null)
--password=<password> (sets a ZS password in the header when doing requests to api. defaults to null)
--help (shows this message)


Header keys when doing request to api2csv server:
--------------------------------------
You can pass variables to api2csv by using header keys in the request to the 
api2csv server. If these are set, these will override command line variables when
querying the api. 

* 'Api-Key'
* 'Api-Interface-Id'


Query parameters when doing request to server:
--------------------------------------
Any request parameters that are included in the request to api2csv server are passed 
along.

E.g.: curl --header "API-Interface-Id: 62" --header "API-Key: YOUR_ZS_API_KEY" https://YOUR_ZAAKSYSTEEM_ENVIRONMENT/app/api2csv?rows_per_page=30" will lead to a call to the api
you want to query with query parameters rows_per_page=30
