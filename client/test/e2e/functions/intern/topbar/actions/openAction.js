import toggleActionMenu from './toggleActionMenu';

export default ( action ) => {

	let	expandIcon = element.all(by.css('zs-contextual-setting-menu [icon-type="chevron-down"]')),
		actionButtons = element.all(by.css('zs-contextual-setting-menu li'));

	toggleActionMenu();

	expandIcon.isDisplayed().then( (isDisplayed) => {
		if (isDisplayed) {
			expandIcon.click();
		}
	});

	actionButtons.filter((elm) => {
		return elm.getText().then((text) => {
			return text === `${action}`;
		});
	}).first().click();

};
