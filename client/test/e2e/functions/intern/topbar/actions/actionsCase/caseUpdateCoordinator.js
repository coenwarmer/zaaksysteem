import openAction from './../openAction';
import inputSelectFirst from './../../../../common/input/inputSelectFirst';

export default ( username ) => {

	let form = $('zs-case-admin-view form');

	openAction('Coordinator wijzigen');

	inputSelectFirst(form, 'coordinator', username);

	form.submit();

};
