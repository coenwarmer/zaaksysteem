import openAction from './../openAction';

export default ( status ) => {

	let form = $('zs-case-admin-view form');

	openAction('Status wijzigen');

	$('[data-name="status"] select').sendKeys(status);

	form.submit();

};
