import openAction from './../openAction';
import inputDate from './../../../../common/input/attribute/inputDate';

export default ( day, month, year ) => {

	let form = $('zs-case-admin-view form'),
		dateField = $('[data-name="new_term_date"]');

	openAction('Termijn wijzigen');

	$('[data-name="reason"] input').sendKeys('Reason');
	$('[value="fixedDate"]').click();

	inputDate(dateField, day, month, year);

	form.submit();

};
