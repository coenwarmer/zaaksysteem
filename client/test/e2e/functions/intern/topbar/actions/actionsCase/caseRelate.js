import openAction from './../openAction';
import inputSelectFirst from './../../../../common/input/inputSelectFirst';

export default ( caseToRelateTo ) => {

	let form = $('zs-case-admin-view form');

	openAction('Zaak relateren');

	inputSelectFirst(form, 'case_to_relate', caseToRelateTo);

	form.submit();

};
