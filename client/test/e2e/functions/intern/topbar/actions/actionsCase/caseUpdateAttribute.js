import openAction from './../openAction';
import inputSelectFirst from './../../../../common/input/inputSelectFirst';

export default ( attribute, value ) => {

	let form = $('zs-case-admin-view form');

	openAction('Kenmerken wijzigen');

	inputSelectFirst(form, 'attributes', attribute);

	$('zs-case-admin-attribute-list .vorm-control').sendKeys(value);

	form.submit();

};
