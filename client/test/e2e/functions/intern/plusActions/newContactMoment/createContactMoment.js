import openPlusAction from './../openPlusAction';
import inputSelectFirst from './../../../common/input/inputSelectFirst';

export default ( newContactMoment ) => {

	let form = $('zs-contextual-action-form form');

	openPlusAction('contact-moment');

	form.$(`[data-name="subject_type"] [value="${newContactMoment.type}"]`).click();

	inputSelectFirst(form, 'subject', '123456789');

	inputSelectFirst(form, 'case', '43');

	form.$('[data-name="message"] textarea').sendKeys(newContactMoment.message);

	form.$('[type="submit"]').click();

};
