import getRandomBsn from './../../../common/getValue/getRandomBsn';
import getRandomKvk from './../../../common/getValue/getRandomKvk';
import openPlusAction from './../openPlusAction';

export default ( newContact ) => {

	let form = $('zs-contextual-action-form form'),
		fields = {};

	openPlusAction('contact');

	fields.type = {
			type: 'radio',
			fieldname: 'betrokkene_type',
			value: newContact.type === undefined ? undefined : newContact.type
		};
		
	if (newContact.type === 'natuurlijk_persoon') {

		fields.bsn = {
			type: 'text',
			fieldname: 'np-burgerservicenummer',
			value: newContact.bsn === undefined ? getRandomBsn() : newContact.bsn
		};
		fields.firstName = {
			type: 'text',
			fieldname: 'np-voornamen',
			value: newContact.firstName
		};
		fields.surnamePrefix = {
			type: 'text',
			fieldname: 'np-voorvoegsel',
			value: newContact.surnamePrefix
		};
		fields.lastName = {
			type: 'text',
			fieldname: 'np-geslachtsnaam',
			value: newContact.lastName === undefined ? 'lastName' : newContact.lastName
		};
		fields.sex = {
			type: 'radio',
			fieldname: 'np-geslachtsaanduiding',
			value: newContact.sex === undefined ? 'M' : newContact.sex
		};
		fields.country = {
			type: 'list',
			fieldname: 'np-landcode',
			value: newContact.country
		};

		if ( newContact.country === undefined ) {

			fields.correspondenceAddress = {
				type: 'checkbox',
				fieldname: 'briefadres',
				value: newContact.correspondenceAddress
			};
			fields.streetName = {
				type: 'text',
				fieldname: 'np-straatnaam',
				value:	newContact.streetName !== undefined ?
						newContact.streetName
						: newContact.correspondenceAddress === undefined ? 'streetName' : undefined
			};
			fields.houseNumber = {
				type: 'text',
				fieldname: 'np-huisnummer',
				value:	newContact.houseNumber !== undefined ?
						newContact.houseNumber
						: newContact.correspondenceAddress === undefined ? '1' : undefined
			};
			fields.houseNumberExtra = {
				type: 'text',
				fieldname: 'np-huisnummertoevoeging',
				value: newContact.houseNumberExtra
			};
			fields.zipcode = {
				type: 'text',
				fieldname: 'np-postcode',
				value:	newContact.zipcode !== undefined ?
						newContact.zipcode
						: newContact.correspondenceAddress === undefined ? '1111AA' : undefined
			};
			fields.city = {
				type: 'text',
				fieldname: 'np-woonplaats',
				value:	newContact.city !== undefined ?
						newContact.city
						: newContact.correspondenceAddress === undefined ? 'city' : undefined
			};
			fields.withinCity = {
				type: 'checkbox',
				fieldname: 'np-in-gemeente',
				value: newContact.withinCity
			};

			if ( newContact.correspondenceAddress !== undefined ) {

				fields.correspondenceStreetName = {
					type: 'text',
					fieldname: 'np-correspondentie_straatnaam',
					value:	newContact.correspondenceAddress === undefined ?
							undefined
							: newContact.correspondenceStreetName === undefined ? 'streetName' : newContact.correspondenceStreetName
				};
				fields.correspondenceHouseNumber = {
					type: 'text',
					fieldname: 'np-correspondentie_huisnummer',
					value:	newContact.correspondenceAddress === undefined ?
							undefined
							: newContact.correspondenceHouseNumber === undefined ? '1' : newContact.correspondenceHouseNumber
				};
				fields.correspondenceHouseNumberExtra = {
					type: 'text',
					fieldname: 'np-correspondentie_huisnummertoevoeging',
					value:	newContact.correspondenceHouseNumberExtra
				};
				fields.correspondenceZipcode = {
					type: 'text',
					fieldname: 'np-correspondentie_postcode',
					value:	newContact.correspondenceAddress === undefined ?
							undefined
							: newContact.correspondenceZipcode === undefined ? '1111AA' : newContact.correspondenceZipcode
				};
				fields.correspondenceCity = {
					type: 'text',
					fieldname: 'np-correspondentie_woonplaats',
					value:	newContact.correspondenceAddress === undefined ?
							undefined
							: newContact.correspondenceCity === undefined ? 'city' : newContact.correspondenceCity
				};
			}

		} else {

			fields.foreignAddress1 = {
				type: 'text',
				fieldname: 'np-adres_buitenland1',
				value: newContact.foreignAddress1 === undefined ? 'foreignAddress1' : newContact.foreignAddress1
			};
			fields.foreignAddress2 = {
				type: 'text',
				fieldname: 'np-adres_buitenland2',
				value: newContact.foreignAddress2
			};
			fields.foreignAddress3 = {
				type: 'text',
				fieldname: 'np-adres_buitenland3',
				value: newContact.foreignAddress3
			};

		}

	} else {

		fields.country = {
			type: 'list',
			fieldname: 'vestiging_landcode',
			value: newContact.country
		};
		fields.tradeName = {
				type: 'text',
				fieldname: 'handelsnaam',
				value: newContact.tradeName === undefined ? 'tradeName' : newContact.tradeName
		};

		if ( newContact.country === undefined ) {

			fields.typeOfBusinessEntity = {
				type: 'list',
				fieldname: 'rechtsvorm',
				value: newContact.typeOfBusinessEntity === undefined ? 'Eenmanszaak' : newContact.typeOfBusinessEntity
			};
			fields.coc = {
				type: 'text',
				fieldname: 'dossiernummer',
				value: newContact.coc === undefined ? getRandomKvk() : newContact.coc
			};
			fields.establishmentNumber = {
				type: 'text',
				fieldname: 'vestigingsnummer',
				value: newContact.establishmentNumber
			};
			fields.streetName = {
				type: 'text',
				fieldname: 'vestiging_straatnaam',
				value: newContact.streetName === undefined ? 'streetName' : newContact.streetName
			};
			fields.houseNumber = {
				type: 'text',
				fieldname: 'vestiging_huisnummer',
				value: newContact.houseNumber === undefined ? '1' : newContact.houseNumber
			};
			fields.houseLetter = {
				type: 'text',
				fieldname: 'vestiging_huisletter',
				value: newContact.houseLetter
			};
			fields.houseNumberExtra = {
				type: 'text',
				fieldname: 'vestiging_huisnummertoevoeging',
				value: newContact.houseNumberExtra
			};
			fields.zipcode = {
				type: 'text',
				fieldname: 'vestiging_postcode',
				value: newContact.zipcode === undefined ? '1111AA' : newContact.zipcode
			};
			fields.city = {
				type: 'text',
				fieldname: 'vestiging_woonplaats',
				value: newContact.city === undefined ? 'city' : newContact.city
			};

		} else {

			fields.foreignAddress1 = {
				type: 'text',
				fieldname: 'vestiging_adres_buitenland1',
				value: newContact.foreignAddress1 === undefined ? 'foreignAddress1' : newContact.foreignAddress1
			};
			fields.foreignAddress2 = {
				type: 'text',
				fieldname: 'vestiging_adres_buitenland2',
				value: newContact.foreignAddress2
			};
			fields.foreignAddress3 = {
				type: 'text',
				fieldname: 'vestiging_adres_buitenland3',
				value: newContact.foreignAddress3
			};

		}

	}

	Object.keys(fields).forEach( ( key ) => {

		if ( fields[key].value !== undefined ) {

			switch (fields[key].type) {
				case 'radio':
					form.$(`[data-name="${fields[key].fieldname}"] [value="${fields[key].value}"]`).click();
					break;
				case 'list':
					form.$(`[data-name="${fields[key].fieldname}"] select`).sendKeys(fields[key].value);
					break;
				case 'checkbox':
					form.$(`[data-name="${fields[key].fieldname}"] input`).click();
					break;
				default:
					form.$(`[data-name="${fields[key].fieldname}"] input`).sendKeys(fields[key].value);
					break;
			}

		}

	});

	form.submit();

};
