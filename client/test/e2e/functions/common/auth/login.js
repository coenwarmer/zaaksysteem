import attemptLogin from './attemptLogin';
import getPassword from './getPassword';

export default ( ) => {
	return attemptLogin('admin', getPassword('admin'));
};
