module.exports =
	( username, password ) => {

		element(by.css('#id_username')).sendKeys(username);
		element(by.css('#id_password')).sendKeys(password);

		return element(by.css('form input[type=submit]')).click();

	};
