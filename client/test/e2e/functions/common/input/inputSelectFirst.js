import selectFromSuggest from './../selectFromSuggest';
import getFieldSelector from './../getFieldSelector';
import first from 'lodash/first';

export default ( parentElement, dataName, searchValue ) => {

	selectFromSuggest(
			parentElement.$(getFieldSelector(dataName)),
			searchValue,
			first
		);

};
