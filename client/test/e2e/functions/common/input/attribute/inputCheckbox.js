import random from 'lodash/random';

export default ( attribute, checkboxesToClick ) => {

	let checkboxes = attribute.all(by.css('[type="checkbox"]')),
		randomCheckboxes = [],
		inputCheckbox,
		i,
		j;

		checkboxes.count().then((checkboxCount) => {

			for (i = 0; i < checkboxCount; i++) {

				if ( random(0, 1) ) {

					randomCheckboxes.push(i + 1);

				}

			}

			if ( randomCheckboxes.length === 0 ) {
				randomCheckboxes.push(random(1, checkboxCount));
			}

			inputCheckbox = checkboxesToClick ? checkboxesToClick : randomCheckboxes;

			for ( j = 0; j < inputCheckbox.length; j++) {
				attribute.$(`label:nth-child(${inputCheckbox[j]}) [type="checkbox"]`).click();
			}

		});

};
