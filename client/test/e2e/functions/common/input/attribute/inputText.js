export default ( attribute, textToInput ) => {

	attribute.getAttribute('class').then((attributeClass) => {

		let inputText,
			i;

		if (attributeClass.includes('multiple')) {

			inputText = textToInput ? textToInput : ['this', 'is', 'random', 'text'];

		} else {

			inputText = textToInput ? textToInput : ['this is random text'];

		}

		for (i = 0; i < inputText.length; i++) {

			attribute.$(`li:nth-child(${i + 1}) input`).sendKeys(inputText[i]);

			if ( inputText.length - 1 > i ) {

				attribute.$('.vorm-field-add-button').click();

			}

		}
		
	});

};
