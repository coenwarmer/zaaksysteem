export default ( attribute, textToInput ) => {

	let inputText = textToInput ? textToInput : 'this is random text\nand another line';

	attribute.$('.rich-text-editor.dummy').click();

	browser.sleep(2000);

	attribute.$('.ql-editor').sendKeys(inputText);

};
