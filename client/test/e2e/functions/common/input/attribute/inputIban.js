export default ( attribute, ibanToInput ) => {

	let inputIban = ibanToInput ? ibanToInput : 'GB82WEST12345698765432';

	attribute.$('input').sendKeys(inputIban);

};
