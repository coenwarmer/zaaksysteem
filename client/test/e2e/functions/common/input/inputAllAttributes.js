import inputBag from './attribute/inputBag';
import inputIban from './attribute/inputIban';
import inputCheckbox from './attribute/inputCheckbox';
import inputDate from './attribute/inputDate';
import inputEmail from './attribute/inputEmail';
import inputFile from './attribute/inputFile';
import inputMap from './attribute/inputMap';
import inputImageUrl from './attribute/inputImageUrl';
import inputNumeric from './attribute/inputNumeric';
import inputOption from './attribute/inputOption';
import inputRichtext from './attribute/inputRichtext';
import inputSelect from './attribute/inputSelect';
import inputText from './attribute/inputText';
import inputTextarea from './attribute/inputTextarea';
import inputUrl from './attribute/inputUrl';
import inputValuta from './attribute/inputValuta';

export default ( ) => {

	let allAttributes = element.all(by.css('vorm-field')),
	i,
	attributeType;

	allAttributes.each((attribute) => {

		attribute.getAttribute('class').then((classes) => {

			let classArray = classes.split(' ');

			for (i = 0; i < classArray.length; i++) {

				if ( classArray[i].includes('attribute-type')) {

					attributeType = classArray[i].replace('attribute-type-', '');

				}

			}

			switch (attributeType) {

				case 'bag_adres':
				case 'bag_openbareruimte':
				case 'bag_straat_adres':
				case 'bag_adressen':
				case 'bag_openbareruimtes':
				case 'bag_straat_adressen':
				inputBag(attribute);
				break;

				case 'bankaccount':
				inputIban(attribute);
				break;

				case 'checkbox':
				inputCheckbox(attribute);
				break;

				case 'date':
				inputDate(attribute);
				break;

				case 'email':
				inputEmail(attribute);
				break;

				case 'file':
				inputFile(attribute);
				break;

				case 'geolatlon':
				case 'googlemaps':
				inputMap(attribute);
				break;

				case 'image_from_url':
				inputImageUrl(attribute);
				break;

				case 'numeric':
				inputNumeric(attribute);
				break;

				case 'option':
				inputOption(attribute);
				break;

				case 'richtext':
				inputRichtext(attribute);
				break;

				case 'select':
				inputSelect(attribute);
				break;

				case 'text':
				case 'text_uc':
				inputText(attribute);
				break;

				case 'textarea':
				inputTextarea(attribute);
				break;

				case 'url':
				inputUrl(attribute);
				break;

				case 'valuta':
				case 'valutaex':
				case 'valutaex21':
				case 'valutaex6':
				case 'valutain':
				case 'valutain21':
				case 'valutain6':
				inputValuta(attribute);
				break;

				default:
				break;
			}

		});

	});

};
