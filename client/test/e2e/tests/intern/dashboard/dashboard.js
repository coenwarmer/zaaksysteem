import login from './../../../functions/common/auth/login';
import logout from './../../../functions/common/auth/logout';
import loginAs from './../../../functions/common/auth/loginAs';

describe('when viewing the dashboard', ( ) => {

	beforeAll(( ) => {

		logout();

		loginAs('dashboardfull');

		browser.waitForAngular();

	});

	it('there should be an intake widget with case 44', ( ) => {

		expect($('[data-name="intake"] [href="/intern/zaak/44"]').isPresent()).toBe(true);

	});

	it('there should be a my open cases widget with case 45', ( ) => {

		expect($('[data-name="mine"] [href="/intern/zaak/45"]').isPresent()).toBe(true);

	});

	it('there should be a personal search widget with case 46', ( ) => {

		expect($('[data-name="3ed1d737-b205-4619-9495-c9574b73a9d6"] [href="/intern/zaak/46"]').isPresent()).toBe(true);

	});

	it('there should be a favorite casetype widget with casetype dashboard', ( ) => {

		expect($('[data-name=""] .widget-favorite-link').getText()).toEqual('Dashboard');

	});

	afterAll(( ) => {

		logout();

		login();

	});

});
