import openPlusAction from './../../../../functions/intern/plusActions/openPlusAction';
import getFieldSelector from './../../../../functions/common/getFieldSelector';
import inputSelectFirst from './../../../../functions/common/input/inputSelectFirst';

describe('when starting a case registration', ( ) => {

	beforeAll(( ) => {

		let form = $('zs-contextual-action-menu form');

		openPlusAction('zaak');

		inputSelectFirst(form, 'casetype', 'basic casetype');

		form.$(getFieldSelector('requestor_type'))
			.$('input[type=radio][value=natuurlijk_persoon')
			.click();

		inputSelectFirst(form, 'requestor', '123456789');

		form.submit();

	});

	it('should redirect to zaak/create', ( ) => {

		expect(browser.getCurrentUrl()).toContain('/zaak/create/balie');

	});

	describe('and completing the steps', ( ) => {

		beforeAll(( ) => {

			$('[name=submit_to_next]').click();

			browser.driver.wait(( ) => {
				return browser.driver.getCurrentUrl().then((url) => {
					return url.indexOf('submit_to_next=1') !== -1;
				});
			}, 5000);

		});

		it('should move to the next phase', ( ) => {

			expect(browser.getCurrentUrl()).toContain('submit_to_next=1');

		});

		describe('and submitting the form', ( ) => {

			beforeAll(( ) => {

				$('[name=submit_to_next]').click();

			});

			it('should redirect to the case', ( ) => {

				browser.driver.wait(( ) => {
					return browser.driver.getCurrentUrl().then((url) => {
						return /intern\/zaak/.test(url);
					});
				}, 5000);

				expect(browser.getCurrentUrl()).toContain('/intern/zaak');

			});

		});

	});

	afterAll(( ) => {

		browser.get('');

	});

});
