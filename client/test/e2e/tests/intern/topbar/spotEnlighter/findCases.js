import searchFor from './../../../../functions/intern/topbar/spotEnlighter/searchFor';
import getResultElements from './../../../../functions/intern/topbar/spotEnlighter/getResultElements';
import closeSpotEnlighter from './../../../../functions/intern/topbar/spotEnlighter/closeSpotEnlighter';

describe('when searching for cases', ( ) => {

	it('and query is "zaak"', ( ) => {

		searchFor('melding');

		expect(getResultElements().count()).toBeGreaterThan(0);

	});

	afterEach(( ) => {

		closeSpotEnlighter();
		
	});

});
