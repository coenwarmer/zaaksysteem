import login from './../../../../../functions/common/auth/login';
import logout from './../../../../../functions/common/auth/logout';
import loginAs from './../../../../../functions/common/auth/loginAs';
import resetDashboard from './../../../../../functions/intern/topbar/actions/actionsDashboard/resetDashboard';

describe('when viewing the dashboard', ( ) => {

	beforeAll(( ) => {

		logout();

		loginAs('dashboardempty');

		resetDashboard();

	});

	it('there should be three widgets', ( ) => {

		let widgets = element.all(by.css('.widget'));

		expect(widgets.count()).toBe(3);

	});

	it('there should be an intake widget with case 44', ( ) => {

		expect($('[data-name="intake"] [href="/intern/zaak/44"]').isPresent()).toBe(true);

	});

	it('there should be a my open cases widget with case 47', ( ) => {

		expect($('[data-name="mine"] [href="/intern/zaak/47"]').isPresent()).toBe(true);

	});

	it('there should be a favorite casetype widget with casetype dashboard', ( ) => {

		expect($('[data-name=""] .widget-favorite-link').getText()).toEqual('Dashboard');

	});

	afterAll(( ) => {

		let widgetCloseButtons = element.all(by.css('.widget-header-remove-button'));

		widgetCloseButtons.each((closeButton) => {
			closeButton.click();
			browser.waitForAngular();
		});

		logout();

		login();

	});

});
