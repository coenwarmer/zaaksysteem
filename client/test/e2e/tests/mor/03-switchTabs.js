describe('when opening the app', ( ) => {

	beforeAll(( ) => {

		browser.get('/mor');

	});

	it('should contain a header', ( ) => {

		let nav = element.all(by.css('mor-nav'));
		
		expect(nav.count()).toBeGreaterThan(0);

	});


	it('should contain tabs', ( ) => {

		let tabs = element.all(by.css('.mor-nav__tabs a'));
		
		expect(tabs.count()).toBe(2);

	});

	it('when clicking on a tab it should navigate to that tab', ( ) => {

		let tab = element.all(by.css('.mor-nav__tabs a')).last();

		tab.click();

		expect(browser.getCurrentUrl()).toMatch('/mor/zaken/afgehandeld');
		
	});

	afterAll(( ) => {

		browser.get('/intern');

	});

});
