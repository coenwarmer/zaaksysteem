import login from './../../functions/common/auth/login';
import logout from './../../functions/common/auth/logout';
import loginAs from './../../functions/common/auth/loginAs';

describe('when opening a case and adding a note', ( ) => {

	beforeAll(( ) => {

		logout();

		loginAs('burgemeester');

		browser.get('/vergadering/bbv');

		let firstProposal = element.all(by.css('.proposal-item-table tbody tr')).first(),
			noteBar = $('.proposal-detail-view__notecontent'),
			noteTextarea = $('textarea'),
			noteSave = $('.proposal-note-view__header-button--save');

		firstProposal.click();

		browser.waitForAngular();

		noteBar.click();

		noteTextarea.sendKeys('this is my note');

		noteSave.click();

	});

	it('the note should have the given content', ( ) => {

		let notePreview = $('.proposal-detail-view__noteplaceholder');

		expect(notePreview.getText()).toEqual('this is my note');

	});

	afterAll(( ) => {

		logout();

		login();

		browser.get('/intern');

	});

});
