describe('when opening the app', ( ) => {

	beforeAll(( ) => {

		browser.get('/vergadering/bbv');

	});

	it('should contain a header', ( ) => {

		let nav = element.all(by.css('meeting-nav'));

		expect(nav.count()).toBeGreaterThan(0);

	});

	it('should contain a meetingList with two proposals', ( ) => {

		let openProposals = element.all(by.css('.proposal-item-table tbody tr'));

		expect(openProposals.count()).toBe(2);

	});

	it('should contain an unsorted meetingList with three proposals', ( ) => {

		let unfilteredOpenProposals = element.all(by.css('.proposal-item-table tbody tr'));

		$('.meeting-nav__button-bar button:nth-child(2)').click();

		expect(unfilteredOpenProposals.count()).toBe(3);

	});

	it('should contain a meetingList with two proposals', ( ) => {

		$('.meeting-nav__tabs a:nth-child(2)').click();

		let closedProposals = element.all(by.css('.proposal-item-table tbody .closed'));

		expect(closedProposals.count()).toBe(2);

	});

	afterAll(( ) => {

		browser.get('/intern');

	});

});
