describe('when clicking on an object in the objectList', ( ) => {

	beforeAll(( ) => {

		browser.get('/pdc');

	});

	it('should navigate to the detailView', ( ) => {

		let objectItem = element.all(by.css('object-list-item-list-item')).first();
		
		objectItem.click();

		expect(browser.getCurrentUrl()).toContain('/!');

	});

	it('should contain a detailView', ( ) => {

		let objectDetailView = element(by.css('object-detail-view'));
		
		expect(objectDetailView.isPresent());

	});

	it('should display at least one piece of information present in the object', ( ) => {

		let objectValues = element.all(by.css('.object-detail-view__list li'));
		
		expect(objectValues.count()).toBeGreaterThan(0);

	});


	afterAll(( ) => {

		browser.get('/intern');

	});

});
