import angular from 'angular';
import template from './template.html';
import zsObjectSuggestModule from '../../../shared/object/zsObjectSuggest';
import './styles.scss';

export default
		angular.module('Zaaksysteem.officeaddin.word.caseSearchView', [
			zsObjectSuggestModule
		])
		.directive('caseSearchView', [ ( ) => {
			return {
				restrict: 'E',
				template,
				bindToController: true,
				controller: [ '$scope', '$state', function ( scope, $state ) {

					let ctrl = this;

					ctrl.handleCaseSuggest = ( caseItem ) => {
						$state.go('caseDetail', { caseId: caseItem.id });
					};

				}],
				controllerAs: 'vm'
			};

		}
		])
		.name;
