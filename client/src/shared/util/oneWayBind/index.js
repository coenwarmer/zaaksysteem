import propCheck from './../propCheck';

let defaultGetter = ( ) => undefined;

export default ( ) => {

	let getter = defaultGetter;

	return ( fn ) => {

		if (fn) {
			propCheck.throw(
				propCheck.func,
				fn
			);

			getter = fn;

			return ( ) => {
				getter = defaultGetter;
			};
		}

		return getter();

	};

};
