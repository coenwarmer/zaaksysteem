import angular from 'angular';
import breakpoints from '!raw!!./../../styles/_breakpoints.scss';
import appUnloadModule from './../appUnload';
import parseSassVariables from './../parseSassVariables';
import 'match-media/matchMedia';
import each from 'lodash/each';
import includes from 'lodash/includes';

export default
	angular.module('rwdService', [
		appUnloadModule
	])
		.provider('rwdService', [ ( ) => {

			let breakpointVars = {};

			let parse = ( sass ) => {
				breakpointVars = parseSassVariables(sass);
			};

			parse(breakpoints);

			return {
				parse,
				$get: [ '$rootScope', '$window', '$document', 'appUnload', ( $rootScope, $window, $document, appUnload ) => {

					let activeViews = [];

					let determineActiveViews = ( ) => {

						let views = [];

						each(breakpointVars, ( variable, key ) => {

							let isActive = $window.matchMedia(variable).matches;

							if (isActive) {
								views = views.concat(key);
							}

						});

						if (!angular.equals(views, activeViews)) {

							$rootScope.$evalAsync(( ) => {
								activeViews = views;
							});

						}
						
					};

					let getActiveViews = ( ) => activeViews;

					let isActive = ( type ) => includes(activeViews, type);

					let handleResize = ( ) => {
						determineActiveViews();
					};

					$window.addEventListener('resize', handleResize);

					appUnload.onUnload(( ) => {
						$window.removeEventListener('resize', handleResize);
					});

					determineActiveViews();

					return {
						isActive,
						getActiveViews,
						parse
					};

				}]
			};

		}])
		.name;
