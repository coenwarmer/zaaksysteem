import angular from 'angular';
import zsStorageModule from './';
import SomaEvents from 'soma-events';

describe('zsStorage', ( ) => {

	let zsStorage,
		$rootScope,
		$window;

	beforeEach(angular.mock.module(zsStorageModule));

	beforeEach(angular.mock.inject([ '$window', '$rootScope', 'zsStorage', ( ...rest ) => {

		[ $window, $rootScope, zsStorage ] = rest;

	}]));

	it('should store the value if set', ( ) => {

		zsStorage.set('foo', 'bar');

		expect(zsStorage.get('foo')).toBe('bar');

	});

	it('should clear the storage', ( ) => {

		zsStorage.set('foo', 'bar');

		expect(zsStorage.get('foo')).toBe('bar');

		zsStorage.clear();

		expect(zsStorage.get('foo')).not.toEqual('bar');

	});

	it('should return the prefix', ( ) => {

		expect(zsStorage.prefix()).toBeDefined();

	});

	it('should call the update listeners when set through zsStorage', ( ) => {

		let spy = jasmine.createSpy('update'),
			key = 'foo';

		zsStorage.onUpdate.push(spy);

		zsStorage.set(key, 'foo');

		expect(spy).toHaveBeenCalledWith(key);

	});

	it('should sync to local storage after a digest', ( ) => {

		let key = 'foo',
			data = { foo: 'bar' };

		let getStorageValue = ( ) => {
			return JSON.parse($window.localStorage.getItem(`${zsStorage.prefix()}.${key}`));
		};

		zsStorage.set(key, data);

		expect(getStorageValue()).not.toEqual(data);

		$rootScope.$digest();

		expect(getStorageValue()).toEqual(data);

	});

	it('should call the update listeners when storage event fires after flushed', ( ) => {

		let spy = jasmine.createSpy('update'),
			event = new SomaEvents.Event('storage'),
			key = 'foo';

		event.key = `${zsStorage.prefix()}.${key}`;

		zsStorage.onUpdate.push(spy);

		$window.dispatchEvent(event);

		expect(spy).not.toHaveBeenCalled();

		zsStorage.flush();

		expect(spy).toHaveBeenCalledWith(key);

	});

	afterEach( ( ) => {

		zsStorage.clear();

	});

});
