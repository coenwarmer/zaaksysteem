import angular from 'angular';

export default
	angular.module('stateRegistrar', [
	])
		.provider('stateRegistrar', [ '$stateProvider', ( $stateProvider ) => {

			let states = [],
				statesByName = {};

			$stateProvider.decorator('data', ( state, parent ) => {

				if (!statesByName[state.self.name]) {

					states = states.concat(state);

					statesByName[state.self.name] = state;

				}

				return parent(state.self);

			});

			return {
				$get: [ ( ) => {

					return {
						getState: ( name ) => statesByName[name],
						getStates: ( ) => states
					};
				}]
			};

		}])
		.name;
