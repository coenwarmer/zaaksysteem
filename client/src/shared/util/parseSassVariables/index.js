import each from 'lodash/each';

let regex = '\\$([A-Za-z0-9\-]+):\s*(.*?);',
	multi = new RegExp(regex, 'gm'),
	single = new RegExp(regex);

export default ( source = '' ) => {

	let matches = source.match(multi),
		vars = {};

	each(matches, ( line ) => {

		let match = line.match(single),
			key = match[1],
			value = match[2]
				.replace(/\s*!default$/, '')
				.replace(/\"(.*)\"/, '$1')
				.replace(/#{\$([A-Za-z0-9\-]+)}/, ( m, ref, offset, string ) => {

					if (!(ref in vars)) {
						throw new Error(`Unable to parse string: reference ${ref} not found in ${string}`);
					}

					return vars[ref];

				})
				.trim();

		vars[key] = value;

	});

	return vars;
};
