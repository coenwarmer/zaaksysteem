import get from 'lodash/get';
import map from 'lodash/map';
import identity from 'lodash/identity';

export default
	( attribute, source ) => {

		let value = source;

		switch (attribute.type) {

			case 'bag_adres':
			case 'bag_straat_adres':
			case 'bag_openbareruimte':
			value = value ? get(value, 'bag_id') : null;
			break;

			case 'bag_adressen':
			case 'bag_straat_adressen':
			case 'bag_openbareruimtes':
			value = map(value, ( val ) => {
				return val ? get(val, 'bag_id') : null;
			})
				.filter(identity);
			break;

			case 'valuta':
			case 'valutaex':
			case 'valutaex21':
			case 'valutaex6':
			case 'valutain':
			case 'valutain21':
			case 'valutain6':
			value = value && isNaN(value) ? value.toFixed(2) : value;
			break;

			case 'date':
			value = value || value === 0 ? new Date(value) : null;

			if (!value || isNaN(value.getTime())) {
				value = null;
			} else {
				value = `${value.getDate()}-${value.getMonth() + 1}-${value.getFullYear()}`;
			}
			
			break;
		}

		return value;

	};
