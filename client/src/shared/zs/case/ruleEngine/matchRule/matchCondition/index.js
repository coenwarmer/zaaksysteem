import some from 'lodash/some';
import isEqual from 'lodash/isEqual';
import isArray from 'lodash/isArray';
import includes from 'lodash/includes';

export default
	( condition, values = {}, hidden = {}) => {

		let attrName = condition.attribute_name,
			attrValue = hidden[attrName] ? null : values[attrName],
			matches = false,
			isValArray = isArray(attrValue);

		if (condition.validates_true !== undefined) {
			matches = !!condition.validates_true;
		} else {
			matches = some(condition.values, val => {
				return isValArray ?
					includes(attrValue, val)
					: isEqual(val, attrValue);
			});
		}

		return matches;

	};
