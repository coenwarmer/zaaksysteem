import isArray from 'lodash/isArray';

export default
	( formula, values ) => {

		let val,
			expression;

		expression = formula.replace(/([a-zA-Z]{1,}(?:[a-zA-Z0-9_\.]+))/g, ( match, key ) => {
			let value,
				name = key;

			if (name.indexOf('.') === -1) {
				name = `attribute.${name}`;
			}

			value = values[name];

			if (isArray(value)) {
				value = value[0];
			}

			return (isNaN(value) || value === undefined || value === null || value === '') ? 0 : Number(value);
		});

		try {
			val = new Function('return ' + expression)(); //eslint-disable-line
			
			if (isNaN(val) || val === Number.POSITIVE_INFINITY || val === Number.NEGATIVE_INFINITY) {
				val = null;
			} else {
				val = Math.round(val * 100) / 100;
			}
		} catch ( e ) {
			val = null;
		}

		return val;

	};
