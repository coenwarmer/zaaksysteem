import evaluateFormula from '.';

describe('evaluateFormula', ( ) => {

	it('should return the result of the formula', ( ) => {

		expect(evaluateFormula('foo * 5', { foo: 3 })).toBe(15);

		expect(evaluateFormula('foo * bar', { foo: 3, bar: 4 })).toBe(12);

		expect(evaluateFormula('foo / bar', { foo: 4, bar: 2 })).toBe(2);

		expect(evaluateFormula('foo - bar', { foo: 4, bar: 1 })).toBe(3);

		expect(evaluateFormula('bar - foo', { foo: 4, bar: 1 })).toBe(-3);

	});

	it('should normalize non-numeric values to 0', ( ) => {

		expect(evaluateFormula('bar - foo', { foo: 4, bar: NaN })).toBe(-4);

		expect(evaluateFormula('bar - foo + blah', { foo: 2, bar: NaN })).toBe(-2);

	});

	it('should normalize infinity to null', ( ) => {

		expect(evaluateFormula('3 / 0')).toBe(null);

		expect(evaluateFormula('-3 / 0')).toBe(null);

	});

	it('should round off to two decimals', ( ) => {

		expect(evaluateFormula('4 / 3')).toBe(1.33);

	});

	it('should catch evaluation errors and normalize to null', ( ) => {

		expect(evaluateFormula('3-2.2.2')).toBe(null);

	});

});
