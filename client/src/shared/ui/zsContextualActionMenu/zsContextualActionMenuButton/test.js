import angular from 'angular';
import 'angular-mocks';
import zsContextualActionMenuButtonModule from '.';

describe('zsContextualActionMenu', ( ) => {

	let contextualActionService,
		scope,
		el,
		ctrl;

	beforeEach(angular.mock.module(zsContextualActionMenuButtonModule));

	beforeEach(angular.mock.inject([ '$compile', '$rootScope', ( ...rest ) => {

		let $compile,
			$rootScope;

		[ $compile, $rootScope ] = rest;

		el = angular.element('<zs-contextual-action-menu-button/>');
		scope = $rootScope.$new();

		$compile(el)(scope);

		ctrl = el.controller('vm');

	}]));

	xit('should list the same actions as the service when open', ( ) => {

		ctrl.openMenu();

		expect(ctrl.options()).toEqual(contextualActionService.getActions());

		contextualActionService.add(
			{
				name: 'foo',
				label: 'foo',
				click: ( ) => { }
			},
			scope
		);

		expect(ctrl.options()).toEqual(contextualActionService.getActions());

	});

	xit('should return no actions when the menu is closed', ( ) => {
		
		ctrl.closeMenu();

		contextualActionService.add(
			{
				name: 'foo',
				label: 'foo',
				click: ( ) => { }
			},
			scope
		);

		expect(ctrl.options()).not.toEqual(contextualActionService.getActions());

	});

	xit('should call the click handler when clicked', ( ) => {

		let spy = jasmine.createSpy('click');

		ctrl.openMenu();

		contextualActionService.add({
			name: 'foo',
			label: 'foo',
			click: spy
		}, scope);

		scope.$digest();

		angular.element(el[0].querySelector('li > button')).eq(0).triggerHandler('click');

		expect(spy).toHaveBeenCalled();

	});


});
