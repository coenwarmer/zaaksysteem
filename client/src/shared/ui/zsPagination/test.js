import zsPagination from './index.js';
import angular from 'angular';
import 'angular-mocks';

describe('zsPagination', ( ) => {
	let $rootScope,
		$compile,
		ctrl,
		$document,
		scope,
		zsPaginationEl;

	beforeEach(angular.mock.module(zsPagination));

	beforeEach(angular.mock.inject([ '$rootScope', '$compile', '$document', ( ...rest ) => {

		[ $rootScope, $compile, $document ] = rest;

		zsPaginationEl = angular.element(`
			<zs-pagination
				current-page="getCurrentPage()"
				on-page-change="handlePageChange($page)"
				has-next-page="hasNextPage()"
				has-prev-page="hasPrevPage()"
				on-limit-change="onLimitChange()"
				limit="limit"
			>
			</zs-pagination>
		`);

		scope = $rootScope.$new();
		
		$compile(zsPaginationEl)(scope);
		$document.find('body').append(zsPaginationEl);

		ctrl = zsPaginationEl.controller('zsPagination');

		scope.limit = 10;
		scope.getCurrentPage = ( ) => 1;
		scope.hasPrevPage = ( ) => false;
		scope.hasNextPage = ( ) => true;

	}]));
	it('should have called the function on click next', ( ) => {
		
		let spy = jasmine.createSpy('click');

		scope.handlePageChange = spy;

		angular.element(zsPaginationEl[0].querySelector('button')).eq(0).triggerHandler('click');

		expect(spy).toHaveBeenCalled();

	});

	it('should have called the function on click prev', ( ) => {
		
		let spy = jasmine.createSpy('click');

		scope.handlePageChange = spy;

		angular.element(zsPaginationEl[0].querySelectorAll('button')[1]).eq(0).triggerHandler('click');

		expect(spy).toHaveBeenCalled();

	});

	it('should have called the onLimitChange function', ( ) => {
		
		let spy = jasmine.createSpy('click');

		scope.onLimitChange = spy;
		ctrl.onLimitChange({ $limit: 12 });
		
		expect(spy).toHaveBeenCalled();
		
	});

	it('should have called the onLimitChange function thru the change event of the limit select', ( ) => {

		let spy = jasmine.createSpy('click');
		
		scope.onLimitChange = spy;
		angular.element(zsPaginationEl[0].querySelector('select')).eq(0).triggerHandler('change');
		
		expect(spy).toHaveBeenCalled();

	});

	it('should have been changed the displayedLimit value to 25', ( ) => {

		scope.limit = 25;
		scope.$digest();

		expect(ctrl.displayedLimit).toBe(25);

	});


});
