import angular from 'angular';
import zsSuggestionList from '.';
import 'angular-mocks';

describe('zsSuggestionList', ( ) => {

	let $compile,
		$timeout,
		$q,
		el,
		scope,
		input,
		ctrl;

	let getSuggestions = ( ) => [ 1, 2, 3 ].map( n => ({ id: n, label: n }));

	beforeEach(angular.mock.module(zsSuggestionList));

	beforeEach(angular.mock.inject([ '$rootScope', '$compile', '$timeout', '$q', ( $rootScope, ...rest ) => {

		[ $compile, $timeout, $q ] = rest;

		scope = $rootScope.$new();

		el = angular.element(`
			<div>
				<input type="text" ng-model="values.query"/>
				<zs-suggestion-list
					key-input-delegate="input"
					suggestions="filter()"
					on-select="onSelect($suggestion)"
				/>
			</div>
		`);

		input = el.find('input');

		scope.input = { input };

	}]));

	describe('without fixed position', ( ) => {

		beforeEach(( ) => {

			$compile(el)(scope);

			ctrl = el.find('zs-suggestion-list').controller('zsSuggestionList');

		});

		it('should have a controller', ( ) => {

			expect(ctrl).toBeDefined();

		});

		it('should return the input as key delegate', ( ) => {
			expect(ctrl.getKeyInputDelegate().input[0]).toBe(input[0]);
		});

		it('should store the result of the filter call in suggestions', ( ) => {

			let suggestions = getSuggestions();

			scope.filter = ( ) => suggestions;

			scope.$digest();

			expect(ctrl.getSuggestions().length).toBe(suggestions.length);


		});

		it('should handle promises', ( ) => {

			let items = getSuggestions(),
				promise = $q(( resolve/*, reject*/ ) => {
				$timeout(( ) => {
					resolve(items);
				}, 500);
			});

			scope.filter = ( ) => {
				return promise;
			};

			scope.query = 'foo';

			scope.$digest();

			expect(ctrl.isLoading()).toBe(true);

			expect(ctrl.getSuggestions().length).toBe(0);

			$timeout.flush();

			expect(ctrl.isLoading()).toBe(false);

			expect(ctrl.getSuggestions()).toBe(items);

		});

		it('should handle a promise rejection', ( ) => {

			let promise = $q(( resolve, reject ) => {
				$timeout(( ) => {
					reject(new Error('foo'));
				}, 500);
			});

			scope.filter = ( ) => {
				return promise;
			};

			scope.query = 'foo';

			scope.$digest();

			expect(ctrl.isLoading()).toBe(true);

			expect(ctrl.getSuggestions().length).toBe(0);

			$timeout.flush();

			expect(ctrl.isLoading()).toBe(false);

			expect(ctrl.getSuggestions().length).toBe(0);

		});

		describe('when suggestions are supplied', ( ) => {

			let suggestions = getSuggestions();
			
			beforeEach( ( ) => {

				scope.filter = ( ) => suggestions;

				scope.$digest();
			});


			it('should trigger the onSelect handler when a button is clicked', ( ) => {

				expect(ctrl.getSuggestions().length).toBe(suggestions.length);

				scope.onSelect = jasmine.createSpy('onSelect');

				ctrl.handleSuggestionClick(suggestions[0]);

				expect(scope.onSelect).toHaveBeenCalledWith(suggestions[0]);

			});

			it('should trigger on select if a key commit happens', ( ) => {

				scope.onSelect = jasmine.createSpy('onSelect');
				
				ctrl.handleKeyCommit(suggestions[0]);

				expect(scope.onSelect).toHaveBeenCalledWith(suggestions[0]);

			});

			it('should change the highlighted suggestion when it should', ( ) => {

				expect(ctrl.isHighlighted(suggestions[0])).toBe(false);
				
				ctrl.handleHighlight(suggestions[0]);
				
				expect(ctrl.isHighlighted(suggestions[0])).toBe(true);

			});

		});

	});

	afterEach( ( ) => {

		scope.$destroy();

	});

});
