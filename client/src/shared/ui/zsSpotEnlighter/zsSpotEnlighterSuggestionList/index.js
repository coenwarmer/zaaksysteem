import angular from 'angular';
import composedReducerModule from './../../../api/resource/composedReducer';
import zsKeyboardNavigableListModule from './../../zsKeyboardNavigableList';
import controller from './../../zsSuggestionList/controller';
import suggestionTemplate from './suggestion-template.html';
import groupBy from 'lodash/groupBy';
import flatten from 'lodash/flatten';
import map from 'lodash/map';
import uniq from 'lodash/uniq';
import take from 'lodash/take';
import template from './template.html';

const NAME = 'vm';

export default
	angular.module('shared.ui.zsSpotEnlighter.zsSpotEnlighterSuggestionList', [
		zsKeyboardNavigableListModule,
		composedReducerModule
	])
		.directive('zsSpotEnlighterSuggestionList', [ 'composedReducer', ( composedReducer ) => {

			let el = angular.element(template)[0];

			angular.element(el.querySelector('.suggestion')).replaceWith(suggestionTemplate);

			return {
				restrict: 'E',
				template: el.outerHTML,
				scope: {
					keyInputDelegate: '&',
					suggestions: '&',
					onSelect: '&',
					label: '@',
					objectType: '&',
					onObjectTypeChange: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						groupReducer,
						loading = false;

					controller.call(ctrl, $scope);

					// we don't use ctrl.objectType() as a dependency for the reducer
					// because we want the groups to change only when the data does
					groupReducer = composedReducer( { scope: $scope }, ctrl.suggestions)
						.reduce(( suggestions ) => {

							let grouped = groupBy(suggestions, 'type');

							return uniq(flatten(map(suggestions, 'type')))
								.map( type => {

									let items = grouped[type],
										truncated = ctrl.objectType() === 'all' && type !== 'saved_search' && items.length > 3;

									if (truncated) {
										items = take(items, 3);
									}

									return {
										type,
										items,
										truncated,
										click: ( ) => {

											loading = true;

											groupReducer.subscribe(( ) => {

												loading = false;

											}, { once: true });

											ctrl.onObjectTypeChange({ $type: type });
										}
									};

								});

						});

					ctrl.getGroups = groupReducer.data;

					ctrl.isLoading = ( ) => loading;

					ctrl.handleActionClick = ( suggestion, action, event ) => {
						action.click(event);
						event.stopPropagation();
					};

				}],
				controllerAs: NAME
			};

		}])
		.name;
