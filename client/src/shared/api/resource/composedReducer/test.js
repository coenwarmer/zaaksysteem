import angular from 'angular';
import 'angular-mocks';
import composedReducerModule from '.';
import resourceModule from '../';
import apiCacherModule from './../../cacher';

describe('composedReducer', ( ) => {

	let $rootScope,
		$httpBackend,
		apiCacher,
		resource,
		composedReducer,
		scope,
		combiner,
		spy;

	beforeEach(angular.mock.module(composedReducerModule, resourceModule, apiCacherModule));

	beforeEach(angular.mock.inject([ '$rootScope', '$httpBackend', 'apiCacher', 'resource', 'composedReducer', ( ...rest ) => {

		[ $rootScope, $httpBackend, apiCacher, resource, composedReducer ] = rest;

		apiCacher.clear();

	}]));

	describe('when combining resources', ( ) => {

		let resourceConfig =
			[
				{
					url: '/foo',
					data: [ 'foo' ]
				},
				{
					url: '/bar',
					data: [ 'bar' ]
				}
			],
			resources;

		beforeEach( ( ) => {

			scope = $rootScope.$new();

			resources = [];

		});

		describe('when waiting until all are resolved', ( ) => {

			beforeEach( ( ) => {

				resourceConfig.forEach( ( options ) => {
					
					let { url, data } = options;

					$httpBackend.expectGET(url)
						.respond(data);

					resources.push(resource({ url }, { scope }));

				});

				combiner = composedReducer(...resources);

				spy = jasmine.createSpy('reduce');

				combiner.reduce(spy);

			});

			it('should call the reducer only when all resources are resolved', ( ) => {

				combiner.data();

				expect(spy).not.toHaveBeenCalled();

				$httpBackend.flush(1);

				combiner.data();

				expect(spy).not.toHaveBeenCalled();

				$httpBackend.flush(1);

				combiner.data();

				expect(spy).toHaveBeenCalled();

			});

			it('should call the reducer with the results of the resources in given order', ( ) => {

				$httpBackend.flush();

				combiner.data();

				expect(spy).toHaveBeenCalled();

				let args = spy.calls.argsFor(0);

				expect(args[0]).toEqual(resourceConfig[0].data);

			});

			describe('when checking state', ( ) => {

				describe('when some are rejected', ( ) => {

					beforeEach( ( ) => {

						spyOn(resources[0], 'state').and.returnValue('rejected');
						spyOn(resources[1], 'state').and.returnValue('pending');

					});

					it('should be rejected', ( ) => {

						expect(combiner.state()).toBe('rejected');

					});

				});

				describe('when some are pending and none are rejected', ( ) => {

					beforeEach( ( ) => {

						spyOn(resources[0], 'state').and.returnValue('pending');
						spyOn(resources[1], 'state').and.returnValue('resolved');

					});

					it('should be pending', ( ) => {

						expect(combiner.state()).toBe('pending');

					});

				});

				describe('when all are resolved', ( ) => {

					beforeEach( ( ) => {

						spyOn(resources[0], 'state').and.returnValue('resolved');
						spyOn(resources[1], 'state').and.returnValue('resolved');

					});

					it('should be resolved', ( ) => {

						expect(combiner.state()).toBe('resolved');

					});

				});

				afterEach( ( ) => {

					$httpBackend.flush();

				});

			});


		});

		describe('when not waiting until all are resolved', ( ) => {

			beforeEach( ( ) => {

				resourceConfig.forEach( ( options, index ) => {
					
					let { url, data } = options;

					$httpBackend.expectGET(url)
						.respond(index === 0 ? data : 400);

					resources.push(resource({ url }, { scope }));

				});

				combiner = composedReducer({ waitUntilResolved: false }, ...resources);

				spy = jasmine.createSpy('reduce');

				combiner.reduce(spy);

			});

			it('should be pending when all are pending', ( ) => {

				expect(combiner.state()).toBe('pending');

				$httpBackend.flush();

			});

			it('should be resolved when at least one is', ( ) => {

				$httpBackend.flush(1);

				expect(combiner.state()).toBe('resolved');

				$httpBackend.flush();

			});

			it('should be rejected when at least one is', ( ) => {

				$httpBackend.flush();

				expect(combiner.state()).toBe('rejected');

			});

		});

	});

	describe('when using functions as resource proxies', ( ) => {

		let reducer,
			fn;

		let createComposed = ( options = {} ) => {

			fn = jasmine.createSpy('fn');

			reducer = composedReducer(options, fn);

		};

		it('should err when scope is not supplied', ( ) => {

			expect( ( ) => {
				createComposed({ });
			}).toThrow();

			scope = $rootScope.$new();

			expect( ( ) => {
				createComposed({ scope });
			}).not.toThrow();

		});

		it('should revalidate the function after a digest', ( ) => {

			scope = $rootScope.$new();

			createComposed({ scope });

			reducer.data();

			expect(fn).toHaveBeenCalled();

			fn.calls.reset();

			scope.$digest();

			reducer.data();

			expect(fn).toHaveBeenCalled();

		});

		it('should invalidate when the result of the function changes', ( ) => {

			let reduceSpy = jasmine.createSpy('reduce');

			scope = $rootScope.$new();

			createComposed({ scope });

			reducer.reduce(reduceSpy);

			reducer.data();

			reduceSpy.calls.reset();

			reducer.data();

			expect(reduceSpy).not.toHaveBeenCalled();

			fn.and.returnValue('foo');

			scope.$digest();

			reducer.data();

			expect(reduceSpy).toHaveBeenCalled();
			expect(reduceSpy.calls.argsFor(0)[0]).toBe('foo');

		});

	});

	describe('when using a composed reducer as proxy', ( ) => {

		let fooFn,
			barFn,
			reducer,
			fooReduce,
			fooReducer;

		beforeEach( ( ) => {

			scope = $rootScope.$new();

			fooFn = jasmine.createSpy('foo');
			barFn = jasmine.createSpy('bar');

			fooReducer = composedReducer({ scope }, fooFn, 'bar');

			fooReduce = jasmine.createSpy('fooreduce');

			fooReducer.reduce(fooReduce);

			reducer = composedReducer(
				fooReducer,
				composedReducer({ scope }, 'foo', barFn)
			);

		});

		it('should revalidate if the given reducer is updated', ( ) => {

			let reduceFn = jasmine.createSpy('reduce');

			reducer.reduce(reduceFn);

			reducer.data();

			reduceFn.calls.reset();

			reducer.data();

			expect(reduceFn).not.toHaveBeenCalled();

			fooFn.and.returnValue('foo');

			fooReduce.and.returnValue('bar');

			reduceFn.calls.reset();

			scope.$digest();

			reducer.data();

			expect(reduceFn).toHaveBeenCalled();

			expect(reduceFn.calls.argsFor(0)[0]).toEqual(fooReducer.data());

		});

		it('should always return resolved as state', ( ) => {

			expect(reducer.state()).toBe('resolved');

		});

	});

	describe('when using data as resource proxies', ( ) => {

		it('should call the reducer fn with the data given', ( ) => {

			let data = { foo: 'bar' },
				reducer = composedReducer(null, data),
				reduceSpy = jasmine.createSpy('reduce');

			reducer.reduce(reduceSpy);

			reducer.data();

			expect(reduceSpy.calls.argsFor(0)[0]).toEqual(data);

			expect(reducer.state()).toBe('resolved');

		});

	});

	describe('when compare is true', ( ) => {

		it('should only invoke the listeners when the new result is not equal to the previous', ( ) => {

			let dataMocker = jasmine.createSpy('data'),
				reducer = composedReducer({ scope: $rootScope, compare: true }, dataMocker)
					.reduce(data => data),
				onUpdate = jasmine.createSpy('update');

			dataMocker.and.returnValue({ a: 'bar' });

			reducer.onUpdate(onUpdate);

			$rootScope.$digest();

			onUpdate.calls.reset();

			expect(onUpdate).not.toHaveBeenCalled();

			dataMocker.and.returnValue({ a: 'bar' });

			$rootScope.$digest();

			expect(onUpdate).not.toHaveBeenCalled();

			dataMocker.and.returnValue({ a: 'baz' });

			$rootScope.$digest();

			expect(onUpdate).toHaveBeenCalled();


		});

	});

	describe('when compare is a function', ( ) => {

		it('should use it to determine whether the listeners should be invoked', ( ) => {

			let dataMocker = jasmine.createSpy('data'),
				compareMocker = jasmine.createSpy('compare'),
				reducer = composedReducer({ scope: $rootScope, compare: compareMocker }, dataMocker)
					.reduce(data => data),
				onUpdate = jasmine.createSpy('update'),
				first = { a: 'bar' },
				second = { a: 'baz' };

			dataMocker.and.returnValue(angular.copy(first));

			compareMocker.and.returnValue(true);

			reducer.onUpdate(onUpdate);

			$rootScope.$digest();

			onUpdate.calls.reset();

			expect(onUpdate).not.toHaveBeenCalled();

			dataMocker.and.returnValue(first);

			$rootScope.$digest();

			expect(onUpdate).not.toHaveBeenCalled();

			dataMocker.and.returnValue(second);

			compareMocker.calls.reset();

			compareMocker.and.returnValue(false);

			$rootScope.$digest();

			expect(onUpdate).toHaveBeenCalled();

			expect(compareMocker).toHaveBeenCalledWith(first, second);

		});

	});

	afterEach( ( ) => {

		apiCacher.clear();

	});

});
