import BaseReducer from './BaseReducer';

export default ( scope, eventName ) => {

	let reducer = new BaseReducer(),
		unwatcher;

	unwatcher = scope.$on(eventName, ( event, ...rest ) => {

		reducer.setSrc(rest);

		reducer.$setState('resolved');
		
	});

	reducer.onDestroy(( ) => {
		unwatcher();
	});

	return reducer;

};
