import angular from 'angular';
import template from './template.html';
import resourceModule from './../../../../../../../../shared/api/resource';
import savedSearchesServiceModule from './../../../../../../../../shared/ui/zsSpotEnlighter/savedSearchesService';
import zsResourcePaginationModule from './../../../../../../../../shared/ui/zsPagination/zsResourcePagination';
import zsDashboardWidgetSearchSelectListGroupModule from './zsDashboardWidgetSearchSelectListGroup';
import merge from 'lodash/merge';
import './styles.scss';

export default angular.module('zsDashboardWidgetSearchSelectList', [
		resourceModule,
		savedSearchesServiceModule,
		zsResourcePaginationModule,
		zsDashboardWidgetSearchSelectListGroupModule
	])
		.directive('zsDashboardWidgetSearchSelectList', [ '$timeout', 'resource', 'savedSearchesService', ( $timeout, resource, savedSearchesService ) => {
			return {
				restrict: 'E',
				scope: {
					onSearchSelect: '&',
					widgetTitle: '&',
					filterQuery: '&'
				},
				template,
				bindToController: true,
				controller: [ '$scope', '$element', function ( $scope ) {
					let ctrl = this,
						listData;
					
					listData = resource(
						( ) => {
							return merge({},
								savedSearchesService.getRequestOptions(ctrl.filterQuery()),
								{ params: { zapi_num_rows: 5 } }
							);
						},
						{ scope: $scope }
					);

					ctrl.handleItemSelect = ( item ) => {
						ctrl.onSearchSelect({
							$search: item
						});
					};

					listData.reduce(( requestOptions, data ) => {
						return (data || [])
							.map(savedSearchesService.parseLegacyFilter);
					});

					ctrl.widgetTitle({
						$getter: ( ) => 'Kies een zoekopdracht'
					});

					ctrl.getResource = ( ) => listData;

					ctrl.getPredefinedSearches = ( ) => savedSearchesService.filter([], ctrl.filterQuery());
					ctrl.getUserDefinedSearches = listData.data;

					return ctrl;
				}],
				controllerAs: 'zsDashboardWidgetSearchSelectList'
			};
		}])
		.name;
