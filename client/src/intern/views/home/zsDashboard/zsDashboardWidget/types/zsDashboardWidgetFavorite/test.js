import angular from 'angular';
import 'angular-mocks';
import zsDashboardWidgetFavoriteModule from '.';
import actionsModule from './actions';
import { mockObject } from './../../../../../../../shared/object/mock';
import apiCacherModule from './../../../../../../../shared/api/cacher';
import shortid from 'shortid';
import immutable from 'seamless-immutable';
import last from 'lodash/last';
import cloneDeep from 'lodash/cloneDeep';
import find from 'lodash/find';

describe('zsDashboardWidgetFavorite', ( ) => {

	const TEST_TYPE = 'casetype';

	let $rootScope,
		$compile,
		$httpBackend,
		$timeout,
		apiCacher,
		scope,
		el,
		ctrl;

	beforeEach(angular.mock.module(zsDashboardWidgetFavoriteModule, apiCacherModule, actionsModule));

	beforeEach(angular.mock.inject([ '$rootScope', '$compile', '$httpBackend', '$timeout', 'apiCacher', ( ...rest ) => {

		[ $rootScope, $compile, $httpBackend, $timeout, apiCacher ] = rest;

		el = angular.element(`<zs-dashboard-widget-favorite data-type="'${TEST_TYPE}'"/>`);

		scope = $rootScope.$new();

		$compile(el)(scope);

		ctrl = el.controller('zsDashboardWidgetFavorite');

	}]));

	describe('when successful', ( ) => {

		let favoriteData =
			'foo bar'.split(' ')
				.map( ( label, index ) => {
					return mockObject({
						type: 'favourite',
						values: {
							label,
							reference_type: TEST_TYPE,
							reference_id: shortid(),
							order: (index + 1) * 10
						}
					});
				});

		beforeEach(( ) => {

			$httpBackend.expectGET(`/api/v1/dashboard/favourite/${TEST_TYPE}`)
				.respond(immutable(favoriteData));

			$httpBackend.flush();

		});


		it('should have a controller', ( ) => {

			expect(ctrl).toBeDefined();

		});

		it('should list the favorites', ( ) => {

			expect(ctrl.getFavorites().length).toBe(favoriteData.length);

		});

		describe('when mutating', ( ) => {

			let actions;

			beforeEach(angular.mock.inject([ 'zsDashboardWidgetFavoriteActions', ( ...rest ) => {

				[ actions ] = rest;

			}]));


			it('should add a favorite when suggested', ( ) => {

				let object =
					{
						id: shortid(),
						label: 'foo'
					};

				ctrl.handleObjectSuggest(object);

				expect(ctrl.getFavorites().length).toBe(favoriteData.length + 1);

				$httpBackend.expectPOST(/.*?/)
					.respond(200,
						find(actions, { type: 'add_favorite' })
							.reduce(
								immutable(cloneDeep(favoriteData)),
								{
									reference_type: TEST_TYPE,
									reference_id: object.id,
									label: object.label
								}
							)
						);

				$timeout.flush();

				$httpBackend.flush();

				expect(ctrl.getFavorites().length).toBe(favoriteData.length + 1);

			});

			it('should not add a favorite when object is already in list', ( ) => {

				let object =
					{
						id: favoriteData[0].instance.reference_id,
						label: 'foo'
					};

				ctrl.handleObjectSuggest(object);

				expect(ctrl.getFavorites().length).not.toBe(favoriteData.length + 1);

			});

			it('should remove a favorite', ( ) => {

				let fav = ctrl.getFavorites()[0];

				ctrl.removeFavorite(fav);

				expect(ctrl.getFavorites().length).toBe(favoriteData.length - 1);

				$httpBackend.expectPOST(/.*?/)
					.respond(200,
						favoriteData.filter( ( favorite ) => favorite.reference !== fav.id)
					);

				$timeout.flush();

				$httpBackend.flush();

				expect(ctrl.getFavorites().length).toBe(favoriteData.length - 1);

			});

			it('should reorder favorites', ( ) => {

				let fav = ctrl.getFavorites()[0],
					order = Math.max(...ctrl.getFavorites().map(favorite => favorite.order)) + 5;

				ctrl.reorderFavorite(fav, order);

				expect(last(ctrl.getFavorites()).id).toBe(fav.id);

				$httpBackend.expectPOST(/.*?/)
					.respond(200,
						find(actions, { type: 'update_favorite' })
							.reduce(
								immutable(cloneDeep(favoriteData)),
								{
									uuid: fav.id,
									order
								}
							)
					);

				$timeout.flush();

				$httpBackend.flush();

				expect(last(ctrl.getFavorites()).id).toBe(fav.id);

			});

			it('should only allow move when the handle is clicked', ( ) => {

				expect(ctrl.canMove(null, null, el[0])).toBe(false);

				expect(ctrl.canMove(null, null, el[0].querySelector('.widget-favorite-drag-handle'))).toBe(true);

			});


			describe('when dropping', ( ) => {

				let dropElement = ( index ) => {
					let droppedEl = angular.element(el[0].querySelectorAll('.widget-favorite')[index]);

					scope.$broadcast(`${ctrl.getDragulaBagId()}.drop`, droppedEl);

					return droppedEl.scope().favorite;

				};

				describe('at the top or lower position', ( ) => {

					it('should call reorderFavorite', ( ) => {

						let favorite;

						spyOn(ctrl, 'reorderFavorite');

						favorite = dropElement(0);

						expect(ctrl.reorderFavorite.calls.argsFor(0)[0]).toBe(favorite);

					});

				});

				describe('at the bottom position', ( ) => {

					it('should call reorderFavorite', ( ) => {

						let favorite;

						spyOn(ctrl, 'reorderFavorite');

						favorite = dropElement(1);

						expect(ctrl.reorderFavorite.calls.argsFor(0)[0]).toBe(favorite);

					});

				});


			});


		});

	});

	afterEach(( ) => {

		scope.$destroy();

		apiCacher.clear();

	});

});
