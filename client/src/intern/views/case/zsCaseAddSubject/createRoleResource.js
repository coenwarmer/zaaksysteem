import seamlessImmutable from 'seamless-immutable';

export default ( resource, scope ) => {

	return resource(
		( ) => {
			
			return {
				url: '/api/v1/subject/role'
			};
		},
		{ scope }
	)
	.reduce( ( requestOptions, data ) => {
		return data || seamlessImmutable([]);
	});

};
