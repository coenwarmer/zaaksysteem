import isArray from 'lodash/isArray';

export default ( val ) => {

	let normalized = val;

	if (!isArray(normalized)) {
		normalized = [ normalized ];
	}

	return normalized.map(
		value => {
			return value === undefined ? null : String(value);
		}
	).filter(value => value !== null);

};
