import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import ocLazyLoadModule from 'oclazyload/dist/ocLazyLoad';
import assign from 'lodash/assign';
import template from './index.html';

let assignToScope = [ '$scope', '$stateParams', function ( $scope, $stateParams ) {

	assign($scope, $stateParams);
	
}];

export default
	angular.module('Zaaksysteem.intern.register.route', [
		angularUiRouterModule,
		ocLazyLoadModule
	])
		.config([ '$stateProvider', '$urlMatcherFactoryProvider', ( $stateProvider, $urlMatcherFactoryProvider ) => {

			$urlMatcherFactoryProvider.strictMode(false);

			$stateProvider
				.state('register', {
					url: '/aanvragen/zaaktype/:casetypeId',
					controller: assignToScope,
					template,
					resolve: {
						module: [ '$rootScope', '$ocLazyLoad', '$q', ( $rootScope, $ocLazyLoad, $q ) => {

							return $q(( resolve/*, reject*/ ) => {

								require([ './' ], ( ) => {

									let load = ( ) => {

										$ocLazyLoad.load({
											name: 'Zaaksysteem.intern.register'
										});

										resolve();
									};

									if (!$rootScope.$$phase) {
										$rootScope.$apply(load);
									} else {
										load();
									}

								});

							});
						}]
					}
				});
		}])
		.name;
