import angular from 'angular';
import mutationServiceModule from './../../../shared/api/resource/mutationService';
import propCheck from '../../../shared/util/propCheck';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';

export default
	angular.module('Zaaksysteem.meeting.voteActions', [
		mutationServiceModule,
		snackbarServiceModule
	])
	.run([ 'snackbarService', 'mutationService', ( snackbarService, mutationService ) => {

		mutationService.register( {
			type: 'VOTE_FOR_PROPOSAL',
			request: ( mutationData ) => {

				propCheck.throw(
					propCheck.shape({
						proposalId: propCheck.string,
						voteData: propCheck.object,
						interfaceId: propCheck.number
					}),
					mutationData
				);

				return {
					url: `/api/v1/app/app_meeting/case/${mutationData.proposalId}/update_attributes`,
					data: mutationData.voteData,
					headers: {
						'API-Interface-Id': mutationData.interfaceId
					}
				};
			},
			reduce: ( data, mutationData ) => {

				return data.map(
					proposal => {

						if (proposal.reference === mutationData.proposalId) {

							return proposal.merge({
								instance: {
									attributes: mutationData.voteData.values
								}
							}, { deep: true });
						}

						return proposal;

					}
				);

			},
			error: ( /*data*/ ) => {

				snackbarService.error('Er ging iets mis bij het opslaan van uw stem. Neem contact op met uw beheerder voor meer informatie.');

			}

		} );

	}])
	.name;
