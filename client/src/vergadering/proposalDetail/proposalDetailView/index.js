import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import template from './template.html';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import proposalNoteBarModule from './proposalNoteBar';
import proposalVoteBarModule from './proposalVoteBar';
import rwdServiceModule from '../../../shared/util/rwdService';
import auxiliaryRouteModule from '../../../shared/util/route/auxiliaryRoute';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';
import sessionServiceModule from '../../../shared/user/sessionService';
import appServiceModule from '../../shared/appService';
import getAttributes from '../../shared/getAttributes';
import actionsModule from './actions';
import shortid from 'shortid';
import get from 'lodash/get';
import includes from 'lodash/includes';
import find from 'lodash/find';
import isArray from 'lodash/isArray';
import uniqBy from 'lodash/uniqBy';
import first from 'lodash/head';
import sortBy from 'lodash/sortBy';
import identity from 'lodash/identity';

import './styles.scss';

export default
		angular.module('Zaaksysteem.meeting.proposalDetailView', [
			composedReducerModule,
			angularUiRouterModule,
			proposalNoteBarModule,
			rwdServiceModule,
			auxiliaryRouteModule,
			sessionServiceModule,
			proposalVoteBarModule,
			actionsModule,
			snackbarServiceModule,
			appServiceModule
		])
		.directive('proposalDetailView',
			[ '$sce', '$window', '$timeout', '$document', '$http', '$compile', '$state', 'dateFilter', 'composedReducer', 'auxiliaryRouteService', 'rwdService', 'sessionService', 'snackbarService', 'appService',
			( $sce, $window, $timeout, $document, $http, $compile, $state, dateFilter, composedReducer, auxiliaryRouteService, rwdService, sessionService, snackbarService, appService ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					proposal: '&',
					documents: '&',
					casetype: '&',
					onSaveNote: '&',
					appConfig: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						fieldReducer,
						styleReducer,
						proposalReducer,
						titleReducer,
						userResource = sessionService.createResource($scope),
						casetypeAttributesReducer,
						voteAttributesReducer,
						voteBarConfigReducer,
						voteBarConfigValueReducer,
						voteBarDataReducer,
						proposalResultsReducer,
						appConfigReducer;

					appConfigReducer = composedReducer({ scope: $scope }, ctrl.appConfig )
						.reduce( config => config);

					casetypeAttributesReducer = composedReducer({ scope: $scope }, ctrl.casetype() )
						.reduce( casetype => {
							return uniqBy(casetype.instance.phases.flatMap(phase => phase.fields), 'magic_string');
						});

					proposalReducer = composedReducer({ scope: $scope }, ctrl.proposal() )
						.reduce( proposal => {
							return proposal;
						});

					voteAttributesReducer = composedReducer({ scope: $scope }, appConfigReducer, casetypeAttributesReducer )
						.reduce( ( config, casetypeAttributes ) => {

							let attributes = config.instance.interface_config.magic_strings_accept ? config.instance.interface_config.magic_strings_accept.concat(config.instance.interface_config.magic_strings_comment) : null;

							return attributes.map( configAttribute => {
								return find( casetypeAttributes, ( attribute ) => {
									return attribute.magic_string === configAttribute.object.column_name.replace('attribute.', '');
								});
							});

						});

					voteBarConfigReducer = composedReducer({ scope: $scope }, voteAttributesReducer, userResource)
						.reduce( ( attributes, user ) => {

							let orgUnits = get(user, 'instance.logged_in_user.legacy.parent_ou_ids').concat( get(user, 'instance.logged_in_user.legacy.ou_id')),
								roles = get(user, 'instance.logged_in_user.legacy.role_ids');

							let findAttr = ( type ) => {
								return find(attributes, ( attribute ) => {

									if (attribute.type === type && attribute.permissions.length ) {
										return includes( orgUnits, first(attribute.permissions).group.instance.id)
											&& includes(roles, first(attribute.permissions).role.instance.id);
									}

									return false;

								});
							};

							return {
								choice: findAttr('select') || findAttr('option') || undefined,
								comment: findAttr('textarea')
							};

						});

					voteBarConfigValueReducer = composedReducer({ scope: $scope }, voteBarConfigReducer)
						.reduce( attributes => {

							return {
								choice: {
									magic_string: attributes.choice.magic_string,
									values: sortBy(
										attributes.choice.values
											.map( value => {
												return value.active ? value : null;
											})
											.filter(identity),
										( value ) => value.sort_order
									)
								},
								comment: attributes.comment
							};

						});

					voteBarDataReducer = composedReducer({ scope: $scope }, voteBarConfigReducer, proposalReducer )
						.reduce( ( attributes, proposal ) => {

							return attributes.choice !== undefined && attributes.comment !== undefined ? {
								choice: first(get(proposal, `instance.attributes.${attributes.choice.magic_string}`)) || '',
								comment: first(get(proposal, `instance.attributes.${attributes.comment.magic_string}`)) || ''
							} : null;
						});

					fieldReducer = composedReducer({ scope: $scope }, proposalReducer, appConfigReducer, casetypeAttributesReducer )
						.reduce( ( proposal, config, casetypeAttributes ) => {

							let fieldConfig = getAttributes(config).voorstel,
								caseData = ctrl.proposal(),
								values =
								fieldConfig.filter( ( field ) => field.external_name.indexOf('voorstelbijlage') === -1)
								.asMutable().map( ( field ) => {

									let attribute = get(field, 'internal_name.searchable_object_id'),
										label = field.internal_name.searchable_object_label_public || field.internal_name.searchable_object_label,
										value = caseData.data().instance.attributes[field.internal_name.searchable_object_id] || '',
										tpl = String(value) || '-',
										type = get(find(casetypeAttributes, ( casetypeAttribute ) => casetypeAttribute.magic_string === attribute), 'type');

									if ( isArray(value) ) {

										if (value.length > 1) {
											tpl = '<ul>';

											value.map( ( el ) => {
												tpl += `<li>${el}</li>`;
											});

											tpl += '</ul>';
										} else {
											tpl = `${value.join()}`;
										}
									}

									if (label === 'zaaknummer') {
										value = proposal.instance.number;
										tpl = String(value);
									}

									if (label === 'zaaktype') {
										tpl = String(proposal.instance.casetype.instance.name || '-');
									}

									if (label === 'vertrouwelijkheid') {
										tpl = String(proposal.instance.confidentiality.mapped);
									}

									if (label === 'aanvrager_naam') {
										label = 'Aanvrager naam';
										tpl = String(`${proposal.instance.requestor.instance.subject.instance.first_names} ${proposal.instance.requestor.instance.subject.instance.surname}`);
									}

									if (label === 'resultaat') {
										tpl = String(proposal.instance.result || '-');
									}

									if (type === 'date') {
										tpl = String(dateFilter(first(value), 'dd MMM yyyy') || '-');
									}

									return {
										id: shortid(),
										label: label.charAt(0).toUpperCase() + label.slice(1),
										value,
										template: $sce.trustAsHtml(tpl)
									};
								});

							return values;

						});

					styleReducer = composedReducer( { scope: $scope }, appConfigReducer )
						.reduce( config => {

							return {
								'background-color': get(config, 'instance.interface_config.header_bgcolor', '#FFF')
							};

						});

					titleReducer = composedReducer({ scope: $scope }, proposalReducer, appConfigReducer )
						.reduce( ( proposal, config ) => {

							let columns = getAttributes( config ).voorstel,
								descriptionCol = find(columns, ( col ) => col.external_name === 'voorstelonderwerp' ),
								value = proposal.instance.attributes[descriptionCol.internal_name.searchable_object_id] || proposal.instance.casetype.instance.name || '-';

							return `${proposal.instance.number}: ${value}`;
						});

					proposalResultsReducer = composedReducer({ scope: $scope }, proposalReducer, voteAttributesReducer)
						.reduce( ( proposal, attributes ) => {

							return attributes ?
								attributes
									.asMutable()
									.map( ( attribute ) => {

										let attributeName = attribute.magic_string,
											value = first(proposal.instance.attributes[attributeName]) || '',
											tpl = String(value) || '-';

											if ( isArray(value) ) {

												if (value.length > 1) {
													tpl = '<ul>';

													value.map( ( el ) => {
														tpl += `<li>${el}</li>`;
													});

													tpl += '</ul>';
												} else {
													tpl = `${value.join()}`;
												}
											}

										return {
											id: shortid(),
											label: attribute.public_label || attribute.label,
											value,
											template: $sce.trustAsHtml(tpl)
										};

							}) : null;

						});

					ctrl.proposalResults = proposalResultsReducer.data;

					ctrl.voteBarData = voteBarDataReducer.data;

					ctrl.voteBarConfig = voteBarConfigValueReducer.data;

					ctrl.getFields = fieldReducer.data;

					ctrl.getAttachments = ctrl.documents;

					ctrl.getStyle = styleReducer.data;

					ctrl.getTitle = titleReducer.data;

					ctrl.proposalId = ctrl.proposal().data().instance.number;

					ctrl.proposalRef = ctrl.proposal().data().reference;

					ctrl.isWriteModeEnabled = ( ) => appConfigReducer.data().instance.interface_config.access === 'rw' && userResource.state() === 'resolved';

					ctrl.isVotingAllowed = ( ) => appConfigReducer.data().instance.interface_config.access === 'rw' && userResource.state() === 'resolved' && voteBarConfigValueReducer.data();

					ctrl.isCaseClosed = ( ) => proposalReducer.data().instance.status === 'resolved';

					ctrl.getFileUrl = ( documentId ) => `/api/v1/case/${ctrl.proposal().data().reference}/document/${documentId}/download`;

					ctrl.handleVote = ( proposalId, voteData ) => {

						return ctrl.proposal().mutate('VOTE_FOR_PROPOSAL', {
							proposalId,
							voteData,
							interfaceId: appConfigReducer.data().instance.id
						})
							.asPromise();

					};

					ctrl.handleSaveNote = ( proposalId ) => {
						appService.dispatch('proposal_note_save', proposalId);
					};

					ctrl.handleFileClick = ( file, event ) => {

						event.preventDefault();

						let URL,
							blobUrl,
							anchor;

						snackbarService.wait(
							'Het bestand wordt gedownload',
							{
								promise:
									$http({
										url: ctrl.getFileUrl(file.reference),
										responseType: 'blob'
									})
										.then( ( response ) => {

											URL = 'URL' in $window ? $window.URL : $window.webkitURL;

											blobUrl = URL.createObjectURL(response.data, { type: 'text/bin' });
											anchor = angular.element('<a></a>');

											anchor.attr('href', blobUrl);
											anchor.attr('download', file.instance.name);

											$document.find('body').append(anchor);

											anchor[0].click();

											$timeout( ( ) => {
	
												anchor.remove();
												URL.revokeObjectURL(blobUrl);
	
											}, 300);

										}),
								then: ( ) => '',
								catch: ( ) => 'Het bestand kon niet worden gedownload. Neem contact op met uw beheerder voor meer informatie.'
							}
						);
						

					};

					ctrl.getViewSize = ( ) => {
						if ( includes( rwdService.getActiveViews(), 'small-and-down') ) {
							return 'small';
						}
						return 'wide';
					};

					ctrl.goBack = ( ) => {
						$state.go('^');
					};

				}],
				controllerAs: 'proposalDetailView'

			};
		}
		])
		.name;
