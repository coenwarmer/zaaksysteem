import angular from 'angular';
import 'angular-mocks';
import meetingNavModule from '.';
import configData from '../mock/config.json';
import data from '../mock/data.json';
import nGramFilter from '../nGramFilter';
import appServiceModule from '../appService';
import cacherModule from '../../../shared/api/cacher';
import uniq from 'lodash/uniq';
import without from 'lodash/without';

describe('meetingNav', ( ) => {

	let meetingNav,
		apiCacher,
		$httpBackend,
		el;


	beforeEach(angular.mock.module(meetingNavModule, cacherModule, appServiceModule, [ 'appServiceProvider', ( appServiceProvider ) => {

		appServiceProvider.setDefaultState({ expanded: true, filters: [ ], grouped: true })
			.reduce('filter_add', 'filters', ( filters, filterToAdd ) => uniq(filters.concat(filterToAdd)))
			.reduce('filter_remove', 'filters', ( filters, filterToRemove ) => without(filters, filterToRemove))
			.reduce('filter_clear', 'filters', ( ) => [ ])
			.reduce('toggle_grouped', 'grouped', ( isGrouped ) => !isGrouped)
			.reduce('toggle_expand', 'expanded', ( isExpanded ) => !isExpanded);

	}]));

	beforeEach(angular.mock.inject([ '$httpBackend', 'apiCacher', ( ...rest ) => {

		[ $httpBackend, apiCacher ] = rest;

		$httpBackend.expectGET(/\/api\/v1\/app\/meetingapp/)
			.respond([ configData ]);

		apiCacher.clear();

	}]));

	beforeEach(angular.mock.inject([ '$rootScope', '$compile', '$document', ( ...rest ) => {

		let $rootScope, $compile, $document;

		[ $rootScope, $compile, $document ] = rest;

		el = angular.element('<meeting-nav/>');

		$document.find('body').append(el);

		$compile(el)($rootScope.$new());

		meetingNav = el.controller('meetingNav');

		$httpBackend.flush();

	}]));


	it('should have a controller', ( ) => {

		expect(meetingNav).toBeDefined();

	});

	it('should be able to return the current state', ( ) => {

		expect( meetingNav.getState() ).toBeDefined();

	});

	it('should be able to list all filters that have been set', ( ) => {

		expect( meetingNav.getActiveFilters ).toBeDefined();

	});

	it('should be able to set a new filter', ( ) => {

		expect( meetingNav.addFilter ).toBeDefined();

	});

	describe('when data is added to the filter', ( ) => {

		beforeEach( ( ) => {

			nGramFilter.addToIndex(data);

		});

		it('should list a filter once it has been set', ( ) => {

			meetingNav.addFilter('diemerbos');

			expect( meetingNav.getActiveFilters() ).toContain('diemerbos');

		});

		it('should be able to remove a filter', ( ) => {

			meetingNav.addFilter('diemerbos');

			meetingNav.removeFilter('diemerbos');

			expect( meetingNav.getActiveFilters() ).not.toContain('diemerbos');

		});

		describe('when a query is entered that returns multiple suggestions', ( ) => {

			let keyPress = (key) => {
				
				let event = document.createEvent('Event');
				
				event.keyCode = key;
				event.initEvent('keydown');
				document.dispatchEvent(event);
			};

			beforeEach( ( ) => {

				meetingNav.searchQuery = 'di'; // this should generate a list of suggestions

			});

			it('should handle an arrow down keyboard press to go through a list of suggestions', ( ) => {

				// Check to see if an element is selected

				// Press arrow down
				keyPress(40);

				// Check if the first element in the ng-repeat is now selected

			});

			it('should handle an arrow up keyboard press to go through a list of suggestions', ( ) => {

				// Check to see if any item in the suggestions list is selected

				// Press arrow up
				keyPress(38);

				// Check if the last element in the ng-repeat is now selected

			});

		});

	});

	it('should list a filter once it has been set', ( ) => {

		meetingNav.handleSuggestionSelect( { id:'diemerbos' } );

		expect( meetingNav.getActiveFilters() ).toContain('diemerbos');
	});


	it('it should have a function to toggle the filter bar', ( ) => {

		expect( meetingNav.handleSearchToggle ).toBeDefined();

	});


	it('when using the toggle filter bar function it should toggle a variable', ( ) => {
		
		let toggleState = meetingNav.searchActive;
		
		meetingNav.handleSearchToggle();

		expect( meetingNav.searchActive ).not.toEqual( toggleState );

	});

	it('should have a function that returns a container style', ( ) => {

		expect( meetingNav.getContainerStyle ).toBeDefined();

	});


	it('should have a function that returns a bg style', ( ) => {

		expect( meetingNav.getBgContainerStyle ).toBeDefined();

	});


	it('should have a function that reports whether meetings are expanded', ( ) => {

		expect( meetingNav.isExpanded ).toBeDefined();

	});

	it('should have a function to toggle expand items', ( ) => {

		expect( meetingNav.toggleExpand ).toBeDefined();

	});

	it('should be able to report whether meetings are expanded or not in the form of a boolean', ( ) => {

		let initialState = meetingNav.isExpanded;

		meetingNav.toggleExpand();

		expect( meetingNav.isExpanded() ).not.toEqual( initialState );

	});

	it('should be able to return the title that should be displayed relevant to the view currently active and the size of the viewport', ( ) => {

		expect( meetingNav.getViewportTitle ).toBeDefined();

	});

	it('should have function that reports whether items are grouped', ( ) => {

		expect( meetingNav.isGrouped() ).toBeDefined();

	});

	it('should be able to report whether items are grouped or not in the form of a boolean', ( ) => {

		expect( typeof meetingNav.isGrouped() ).toEqual('boolean');

	});

	it('should have a function defined to toggle whether items are grouped', ( ) => {

		expect( meetingNav.groupItems ).toBeDefined();

	});

	it('should be able to toggle whether items are grouped or not', ( ) => {

		let initialState = meetingNav.isGrouped;

		meetingNav.groupItems();

		expect( meetingNav.isGrouped() ).not.toEqual( initialState );

	});


	it('should have a function to trigger a return to a previous state', ( ) => {

		expect( meetingNav.goBack ).toBeDefined();

	});

});
