#!/bin/bash

# Usage: [CATALYST_DEBUG=0] ./script/dev_fastcgi.sh [hostname [port]]

# Default parameters for hostname:port combination.
HOSTNAME=${1:-'127.0.0.1'};
PORT=${2:-'3333'};

echo -n "Syncing FS... "
sync
echo "done"

# grep over all processes, find fcgi + zaaksysteem process, select PID column, dispatch all PIDs to kill for termination
echo -n "Cleansing machine of lingering instances... "
ps aux | grep perl-fcgi-pm | grep Zaaksysteem | awk '{ print $2; }' | xargs -r kill
echo "done"

echo "Starting on ${HOSTNAME}:${PORT}... "

# we exec the perl script, to prevent this file from lingering as a process.
# default CATALYST_DEBUG to 1, unless it already exists in the environment
CATALYST_DEBUG=${CATALYST_DEBUG:-1} \
ZS_DISABLE_STUF_PRELOAD=${ZS_DISABLE_STUF_PRELOAD:-1} \
PM_MAX_REQUESTS=10 \
exec script/zaaksysteem_fastcgi.pl -e -l $HOSTNAME:$PORT -n 2 -M FCGI::ProcManager::MaxRequests

# COPYRIGHT and LICENSE
# =============
#
# Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
# Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
