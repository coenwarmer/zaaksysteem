#
# Cookbook Name:: zaaksysteem
# Recipe:: development
#
# Copyright 2013, Example Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

### Development tools
%w{subversion w3m curl screen git}.each do |pkg|
    apt_package pkg do
        action :install
    end
end

### NodeJS
apt_repository "nodejs" do
  uri "https://deb.nodesource.com/node"
  distribution node['lsb']['codename']
  components ["main"]
  keyserver "keyserver.ubuntu.com"
  key "68576280"
end

%w{nodejs}.each do |pkg|
    apt_package pkg do
        action :upgrade
        version '0.10.33-2nodesource1~precise1'
    end
end

# windows users have to upgrade manually
# see http://stackoverflow.com/a/27464115
execute "npm_install" do
    cwd "/"
    user "root"
    command 'npm install -g npm@2.1.12'
end

execute "npm_configure" do
    cwd "/"
    user "root"
    command 'npm config set ca ""'
end

execute "install_frontend" do
    command "su vagrant -l -c '/vagrant/dev-bin/frontend_install.sh --cache-min=999999999'"
    only_if 'test -f /vagrant/frontend/package.json'
end
