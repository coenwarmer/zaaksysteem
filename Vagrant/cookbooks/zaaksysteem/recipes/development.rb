#
# Cookbook Name:: zaaksysteem
# Recipe:: development
#
# Copyright 2013, Example Com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

### Development tools
%w{subversion w3m curl screen git}.each do |pkg|
    apt_package pkg do
        action :install
    end
end

apt_repository "nodejs" do
  uri "https://deb.nodesource.com/node_4.x"
  distribution node['lsb']['codename']
  components ["main"]
  keyserver "keyserver.ubuntu.com"
  key "68576280"
end

apt_package 'nodejs' do
    action :upgrade
end

if !node.key?('skip_frontend_install') || node['skip_frontend_install'] != true
  execute "install_frontend" do
      command "su vagrant -l -c '/vagrant/dev-bin/frontend_install.sh --cache-min=999999999'"
  end
end
