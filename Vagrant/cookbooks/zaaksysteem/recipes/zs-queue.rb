# Installs zs-queue application from git repo

bash "install_zs_queue_dependencies" do
    user "root"
    code <<-EOS
        cpanm Module::Install MooseX::App
    EOS
end

git "#{Chef::Config['file_cache_path']}/zs-queue-repo" do
    repository node['zaaksysteem']['queue']['repository']
    action :sync
    notifies :run, "bash[zs_queue_make_install]", :immediately
end

bash "zs_queue_make_install" do
    user "root"
    cwd "#{Chef::Config['file_cache_path']}/zs-queue-repo"
    code <<-EOS
        [[ -f Makefile ]] && make clean
        perl Makefile.PL
        make install
    EOS
    action :nothing
end
