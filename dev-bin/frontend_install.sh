#!/usr/bin/env bash

cd $(readlink -m $(dirname $0)/../frontend)
npm install $@
[ -x "./cmdwrap" ] && ./cmdwrap
