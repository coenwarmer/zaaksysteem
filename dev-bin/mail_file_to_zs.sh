#!/usr/bin/env bash

if [ -z "$1" ] ; then
    echo "Please supply a mail file" >&2
    exit 1;
fi

if [ ! -f "$1" ] ; then
    echo "$1 does not exist!" >&2
    exit 1;
fi

cat $1 | ./bin/mail-gate --url https://10.44.0.11/api/mail

