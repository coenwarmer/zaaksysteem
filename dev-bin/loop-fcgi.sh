loop=1;
while [ $loop -eq 1 ] ; do
    ./script/dev_fastcgi.sh $@
    read -p "Do you want to restart fcgi? " answer
    case $answer in
        'no') loop=0;;
        'n')  loop=0;;
        *)    loop=1;;
    esac
done
