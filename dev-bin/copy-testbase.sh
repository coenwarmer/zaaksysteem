#! /bin/bash

if [ -z "$1" ]; then
    echo "Usage: $0 hostname.of.database.server.nl"
    exit 1
fi

DBHOST="$1"
DUMP_PATH="${2:-$HOME}/testbase"
mkdir -p "${DUMP_PATH}"

echo "Copying database to ${DUMP_PATH}/db.sql.gz"
ssh testdbsync@"${DBHOST}" db > "${DUMP_PATH}/db.sql.gz"

echo "Copying filestore to ${DUMP_PATH}/filestore.tar.xz"
ssh testdbsync@"${DBHOST}" filestore > "${DUMP_PATH}/filestore.tar.xz"
