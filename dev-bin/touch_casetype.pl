#!/usr/bin/perl

use strict;
use warnings;

use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use Getopt::Long;
use Zaaksysteem::Config;
use Pod::Usage;
use File::Spec::Functions;
use File::Basename;

use Time::HiRes qw(gettimeofday tv_interval);

use Zaaksysteem::Object::Model;

my %opt = (
    help       => 0,
    config     => '/etc/zaaksysteem/zaaksysteem.conf',
    n          => 0,
);

{
    local $SIG{__WARN__};
    my $ok = eval {
        GetOptions(
            \%opt, qw(
                help
                n
                config=s
                customer_d=s
                hostname=s
                casetype=s@
                offset=i
                )
        );
    };
    if (!$ok) {
        pod2usage(1) ;
    }
}

pod2usage(0) if ($opt{help});

foreach (qw(config hostname)) {
    if (!defined $opt{$_}) {
        warn "Missing option: $_";
        pod2usage(1) ;
    }
}

if ($opt{casetype} && $opt{offset}) {
    warn "Conflicting options: casetype and offset";
    pod2usage(1) ;
}

if (!defined $opt{customer_d}) {
    $opt{customer_d} = catdir(dirname($opt{config}), 'customer.d');
}

my $ZS = Zaaksysteem::Config->new(
    zs_customer_d => $opt{customer_d},
    zs_conf       => $opt{config},
);

my $schema = $ZS->get_customer_schema($opt{hostname}, 1);

initialize_hstore($schema);

sub initialize_hstore {
    my $dbic = shift;

    my $args = {};
    if (exists $opt{casetype}) {
        $args = { 'me.id' => $opt{casetype} };
    }

    my $rs = $dbic->resultset('Zaaktype')->search(
        $args,
        {
            order_by => { -desc => 'me.id' },
            $opt{offset} ? (offset => $opt{offset}) : (),
        }
    );


    my $count = $rs->count();
    if (!$count) {
        print "No case types found\n";
        return;
    }

    my $starttime = [gettimeofday];
    my $casetypes_done = 0;
    while(my $zaaktype = $rs->next) {
        $casetypes_done++;
        do_transaction(
            $dbic,
            sub {
                my $action = $zaaktype->deleted ? "Deleting" : "Updating";
                printf(
                    "%s hstore for zaaktype %d (%d of %d, %.3f/second)\n",
                    $action,
                    $zaaktype->id,
                    $casetypes_done, $count,
                    $casetypes_done / tv_interval($starttime, [gettimeofday]),
                );
                $zaaktype->_sync_object(Zaaksysteem::Object::Model->new(
                    table => 'ObjectData',
                    schema => $dbic
                ));
            }
        );
    }
}

sub do_transaction {
    my ($dbic, $sub) = @_;

    $dbic->txn_do(
        sub {
            $sub->();
            if ($opt{n}) {
                $dbic->txn_rollback;
            }
        }
    );
}

1;

__END__

=head1 NAME

touch_casetype.pl - A casetype hstore fixer

=head1 SYNOPSIS

touch_case.pl OPTIONS [ [ --casetypes 1234 ] [--casetypes 12345 ] ]

=head1 OPTIONS

=over

=item * config

The Zaaksysteem configuration defaults to /etc/zaaksysteem/zaaksysteem.conf

=item * customer_d

The customer_d dir, defaults to the relative directory where zaaksysteem.conf is found.

=item * hostname

The hostname you want to touch cases for

=item * casetype

You can use this to update a specific case type, not supplying this will touch all case types.

=item * offset

In case you want to touch all casetypes but want to start with a certain offset. This is handy in case the process get's killed and you want to continue the run.
This option conflicts with the casetype option.

=item * n

Dry run, run it, but don't

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
