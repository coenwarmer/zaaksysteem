#!/usr/bin/perl

use strict;
use warnings;

use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use Getopt::Long;
use Zaaksysteem::Config;
use Pod::Usage;
use File::Spec::Functions;
use Text::CSV;

use Time::HiRes qw(gettimeofday tv_interval);

my %opt = (
    help       => 0,
    config     => '/etc/zaaksysteem/zaaksysteem.conf',
    customer_d => '/etc/zaaksysteem/customer.d',
    n          => 0,
);

{
    local $SIG{__WARN__};
    my $ok = eval {
        GetOptions(\%opt, qw(
            help
            n
            skip_user_entities
            config=s
            customer_d=s
            hostname=s
            csv=s
        ));
    };
    if (!$ok) {
        pod2usage(1) ;
    }
}

pod2usage(0) if ($opt{help});

foreach (qw(config customer_d hostname csv)) {
    if (!defined $opt{$_}) {
        warn "Missing option: $_";
        pod2usage(1) ;
    }
}

my $ZS = Zaaksysteem::Config->new(
    zs_customer_d => $opt{customer_d},
    zs_conf       => $opt{config},
);

my $schema = $ZS->get_customer_schema($opt{hostname}, 1);
my $parser = Text::CSV->new;

open(my $csv, '<', $opt{ csv }) || die "Could not open CSV file: $!";

process_subject_rename_usernames($schema);

sub process_subject_rename_usernames {
    my $dbic = shift;

    my $subjects = $dbic->resultset('Subject');

    while (not eof $csv) {
        my ($current_username, $new_username) = @{ $parser->getline($csv) };

        printf "Processing user %s... ", $current_username;

        my $subjecters = $subjects->employees->search({
            username => $current_username
        });

        unless ($subjecters->count) {
            printf "not found, skipping\n\n";
            next;
        }

        if ($subjecters->count > 1) {
            printf "multiple found, skipping\n\n";
            next;
        }

        my $subject = $subjecters->first;
        my @user_entities;

        unless ($opt{ skip_user_entities }) {
            @user_entities = $subject->user_entities->all;
        }

        printf "id = %d, user_entity_ids = (%s), new_username = %s\n",
            $subject->id,
            join(', ', map { $_->id } @user_entities),
            $new_username;

        do_transaction($dbic, sub {
            my $properties = $subject->properties;

            if (ref $properties eq 'HASH' && exists $properties->{ cn }) {
                printf " * update subject properties->{ cn }\n";

                $properties->{ cn } = $new_username;
            }

            printf " * update subject username\n";

            $subject->properties($properties);
            $subject->username($new_username);

            for my $entity (@user_entities) {
                printf " * update subject->user_entity(%d)->source_identifier\n", $entity->id;

                $entity->source_identifier($new_username);
            }

            printf " * saving changes to db\n\n";

            $_->update for @user_entities;
            $subject->update;
        });
    }
}

sub do_transaction {
    my ($dbic, $sub) = @_;

    $dbic->txn_do(sub {
        printf " * starting transaction\n";

        $sub->();

        if ($opt{n}) {
            printf " * rolling back transaction\n";
            $dbic->txn_rollback;
        }
    });
}

1;

__END__

=head1 NAME

rename_usernames_from_csv.pl - Rename usernames from CSV

=head1 SYNOPSIS

    $ cd /path/to/source;
    $ ./dev-bin/rename_usernames_from_csv.pl [OPTIONS]

=head1 OPTIONS

=over

=item * config (default: /etc/zaaksysteem/zaaksysteem.conf)

The Zaaksysteem configuration.

=item * customer_d (default: /etc/zaaksysteem/customer.d)

The customer_d dir.

=item * hostname

The hostname you want to update users for.

=item * skip_user_entities (default: don't skip)

Don't update user entities associated with the found subjects.

=item * n (default: don't dry-run)

Pretend to run the migration (wrap everything in a transaction that rolls back
any changes).

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
