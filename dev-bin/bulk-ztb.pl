#!/usr/bin/perl

use warnings;
use strict;
use FindBin qw/$Bin/;

our $VERSION = 1_0;

use lib "$Bin/../lib";

use autodie;
use LWP::UserAgent;
use File::Spec::Functions qw(catfile);
use Pod::Usage;
use HTTP::Cookies;

use Getopt::Long qw{ :config no_ignore_case };
my %opt = (
    directory      => '',
    base_url       => 'https://10.44.0.11',
    timeout        => 500,
);

GetOptions \%opt => qw{
  directory=s
  base_url=s
  help
  username=s
  password=s
  timeout=i
  };

pod2usage(0) if $opt{help};

pod2usage(1) if ! -d $opt{directory};


my $schema;
my @args;

upload_to_web($opt{directory});

sub upload_to_web {
    my $dir   = shift;
    my @files = _get_files($dir);

    if (!@files) {
        print "No files ready for processing in dropdir '$dir'\n";
        return;
    }

    my $ua = LWP::UserAgent->new(
        agent                 => "Zaaksysteem simpleton client/$VERSION",
        ssl_opts              => { verify_hostname => 0 },
        requests_redirectable => [qw(POST GET HEAD)],
        cookie_jar            => HTTP::Cookies->new({}),
        protocols_allowed     => [qw(http https)],
        timeout               => $opt{timeout},
    );


    my $url = "$opt{base_url}/auth/login";
    my $response = $ua->post(
        $url,
        Content_Type => 'form-data',
        Content      => {
            username => $opt{username},
            password => $opt{password},
        }
    );
    if (!$response->is_success) {
        die(
            sprintf(
                "Unable to login with username '%s' at %s: %s",
                $opt{username}, $url, $response->status_line
            )
        );
    }

    my @errors;
    $url = "$opt{base_url}/beheer/zaaktypen/import/bulkimport";
    for my $file (@files) {
        my $r = $ua->post(
            $url,
            Content_Type => 'form-data',
            Content      => [
                filename => [$file],
        ]);

        if (!$r->is_success) {
            push(@errors, sprintf("File '%s' could not be uploaded: %s", $file, $r->status_line));
            next;
        }
        print $r->content;
    }

    if (@errors) {
        print "Errors occurred processing files in dropdir:\n";
        print join("\n", @errors);
        print "\n";
    }
}

sub _get_files {
    my $dir = shift;

    my $d;
    opendir $d, $dir;
    my $now = time();

    my @files;
    for my $file (readdir($d)) {
        my $filepath = catfile($dir, $file);
        next if (!-f $filepath);
        push @files, $filepath;
    }
    closedir($d);
    return @files;
}


sub usage {

    print
      qq[Imports files from a certain directory in zaaksysteem documentintake.

--directory     Directory to read files from
--username      DBI username
--password      DBI password
--base_url      The base URL

];
}

1;



__END__

=head1 NAME

bulk-ztb.pl - A bulk zaaktype bulk import script for the command line.

=head1 SYNOPSIS

bulk-ztb.pl OPTIONS

=head1 OPTIONS

=over

=item * directory

=item * username

=item * password

=item * base_url

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

