package ZSTest;

use strict;
use warnings;
use TestSetup;
use Class::Load qw/is_class_loaded/;

initialize_test_globals_ok;

use base 'Test::Class';

INIT {
    if (!$ENV{'Test::Class::Load'}) {
        Test::Class->runtests;
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
