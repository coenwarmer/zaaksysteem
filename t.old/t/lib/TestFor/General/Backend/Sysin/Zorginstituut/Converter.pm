package TestFor::General::Backend::Sysin::Zorginstituut::Converter;
use base 'ZSTest';

use TestSetup;

=head1 NAME

TestFor::General::Backend::Sysin::Zorginstituut::Converter

=cut

use Zaaksysteem::Backend::Sysin::Zorginstituut::Converter;

sub _create_case_with_mapping {
    my %opts = @_;

    my $zt = $opts{casetype} // $zs->create_zaaktype_predefined_ok;

    my $mapping = $zs->set_attribute_mapping_on_interface_ok(
        casetype   => $zt,
        interface  => $opts{interface},
        attributes => $opts{mapping},
    );

    my $case = $zs->create_case_ok(zaaktype => $zt)->discard_changes;
    foreach my $k (values %{$mapping}) {
        my $value;
        if (exists $opts{values}{ $k->magic_string }) {
            my $type = $opts{values}{ $k->magic_string }{type};
            $k->update({ value_type => $type });
            $value = $opts{values}{ $k->magic_string }{value};
            $case->update_attribute(
                bibliotheek_kenmerk => $k,
                value               => $value,
            );
        }
    }
    return $case;
}

sub _test_301 {
    my %opts = @_;

    my $start_case;
    if ($opts{two_messages}) {
        my ($attributes, $attribute_values) = _get_attribute_and_values($opts{type});
        $start_case = _create_case_with_mapping(
            interface => $opts{interface},
            mapping   => $attributes,
            values    => $attribute_values,
        );
    }

    my ($attributes, $attribute_values) = _get_attribute_and_values($opts{type}, $start_case);
    my $case = _create_case_with_mapping(
        interface => $opts{interface},
        mapping   => $attributes,
        values    => $attribute_values,
    );

    my %params = (case_id => $case->id);

    my $code = $opts{interface}->get_interface_config->{berichtcode};

    with_stopped_clock {
        no warnings qw(once redefine);

        my $answer = $opts{interface}->model->instance->instance->spoofmode_302('writer', {});

        local *Zaaksysteem::Backend::Sysin::C2GO::Model::call = sub {
            return $answer;
        };

        lives_ok(
            sub {
                $opts{interface}->model->send_301_message(
                    case          => $case,
                    identificatie => '123456789',
                    berichtcode   => $code,
                );
            },
            "send_301_message"
        );

        my ($pt, $record) = $zs->process_interface_trigger_ok(
            interface => $opts{interface},
            trigger   => 'PostStatusUpdate',
            params    => \%params
        );

        if ($record->is_error) {
            diag $record->output;
            return fail("Something did not go as expected");
        }
        my $output = decode_json($record->output);
        my $ok = cmp_deeply($output, $opts{expect} // { errors => undef, valid => 1, case_id => $case->id, code => $code, type => "$opts{type}301" });
        if (!$ok) {
            diag $output;
        }
        return $ok;
    };
}

sub _zic_ok {

    my $username = Zaaksysteem::TestUtils::generate_random_string;
    my $password = Zaaksysteem::TestUtils::generate_random_string;
    my $o        = Zaaksysteem::Backend::Sysin::Zorginstituut::Converter->new(
        username => $username,
        password => $password,
        endpoint => 'https://localhost/soap/service',
        @_,
    );
    isa_ok($o, 'Zaaksysteem::Backend::Sysin::Zorginstituut::Converter');
    return $o;
}

sub zorginstituut_converter_functions : Tests {

    my @functions = qw(
        to_ascii
        to_xml
        _call_soap
        _build_soap
        _get_converted_data
        _build_endpoints
        _build_soap_envelop
    );

    my $converter = _zic_ok();

    foreach (@functions) {
        can_ok($converter, $_);
    }

    my $req = $converter->_build_soap('foo', 'bar');
    isa_ok($req, 'HTTP::Request');

    {
        my $bericht = "Een bericht voor ZS";
        my $answer  = qq{<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <con:converteerXmlNaarVastLengteResponse xmlns:con="http://www.istandaarden.nl/berichten/conversie/data">
      <con:bericht>$bericht</con:bericht>
    </con:converteerXmlNaarVastLengteResponse>
  </soap:Body>
</soap:Envelope>};
        no warnings qw(redefine once);
        local *LWP::UserAgent::request = sub {
            return HTTP::Response->new(200, 'OK', ['Content-Type' => 'application/xml'], $answer);
        };
        my ($res) = $converter->_call_soap('foo', 'bar')->childNodes;
        is($res->serialize, $bericht, "Converted to correct value");
    }

    {
        my $answer = qq{<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
 <soap:Body>
   <con:converteerVasteLengteNaarXmlResponse xmlns:con="http://www.istandaarden.nl/berichten/conversie/data">
     <con:fouten>
       <con:Fout>
         <con:foutCode>0001</con:foutCode>
         <con:omschrijving>Beschrijving 1</con:omschrijving>
       </con:Fout>
       <con:Fout>
         <con:foutCode>0002</con:foutCode>
         <con:omschrijving>Beschrijving 2</con:omschrijving>
       </con:Fout>
     </con:fouten>
   </con:converteerVasteLengteNaarXmlResponse>
  </soap:Body>
</soap:Envelope>
};

        {
            no warnings qw(redefine once);
            local *LWP::UserAgent::request = sub {
                return HTTP::Response->new(200, 'OK', ['Content-Type' => 'application/xml'], $answer);
            };
            throws_ok(
                sub {
                    $converter->_call_soap('foo', 'bar');
                },
                qr#Conversion went wrong: \Q[{foutCode => '0001',omschrijving => 'Beschrijving 1'},{foutCode => '0002',omschrijving => 'Beschrijving 2'}]\E#,
                "Fails with the correct error message"
            );

        }
    }
}

sub zorginstituut_live_test : Tests {
    my $self = shift;
    if (!$ENV{ZS_DEVELOPER_ZORGINSTITUUT}) {
        $self->builder->skip("ZS_DEVELOPER_ZORGINSTITUUT is not set, skipping tests",);
        return;
    }

    my $converter = _zic_ok(
        username => 'username',
        password => 'password!',
        endpoint => 'https://www.istandaarden.nl/services/soap',
    );

    {
        my $ascii;
        lives_ok(
            sub {
                my $msg = $zs->slurp(qw(share xsd examples iwmo 301.xml));
                $msg =~ s/^<\?xml version="1.0"[^>]*>//;
                $ascii = $converter->to_ascii($msg);
            },
            "live tests work fine with WMO301: xml to ascii",
        );

        lives_ok(
            sub {
                my $xml = $converter->to_xml($ascii);
            },
            "live tests work fine with WMO301: ascii to xml",
        );
    }
    SKIP: {
        skip "Our 302 is not xml-ish correct";

        my $ascii;
        lives_ok(
            sub {
                my $msg = $zs->slurp(qw(share xsd examples ijw ijw_302.xml));
                $msg =~ s/^<\?xml version="1.0"[^>]*>//;
                $ascii = $converter->to_ascii($msg);
            },
            "live tests work fine with JW302: xml to ascii",
        );

        lives_ok(
            sub {
                my $xml = $converter->to_xml($ascii);
            },
            "live tests work fine with JW302: ascii to xml",
        );
    }

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
