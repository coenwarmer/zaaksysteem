package TestFor::General::BrowserDetect;
use base(ZSTest);

use Zaaksysteem::BrowserDetect;
use TestSetup;

sub zs_test_browser_agent_epe : Tests {

    my $b = Zaaksysteem::BrowserDetect->new(useragent =>
            "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; .NET4.0C; .NET4.0E)");

    ok(!$b->check(), "IE9 is not supported");
    throws_ok(
        sub { $b->assert },
        qr#unsupported/browser/version_too_low:#,
        "ie::<9"
    );
}

sub zs_test_browser_agent_ie_minimum : Tests {

    my $b = Zaaksysteem::BrowserDetect->new(useragent =>
            "Mozilla/5.0 (Windows; U; MSIE 9.0; WIndows NT 9.0; en-US))");

    ok(!$b->check(), "IE9 is not supported");
    throws_ok(
        sub { $b->assert },
        qr#unsupported/browser/version_too_low:#,
        "ie::<10"
    );
}

sub zs_test_browser_agent_ie_higher : Tests {
    my $b = Zaaksysteem::BrowserDetect->new(
        useragent =>
            "Mozilla/5.0 (Windows; U; MSIE 9.0; WIndows NT 9.0; en-US))",
        denied => [qw(
            ie::>8
        )],
    );
    throws_ok(
        sub { $b->assert },
        qr#unsupported/browser/version_too_high:#,
        "ie::>8"
    );
}

sub zs_test_browser_agent_ie_no_version : Tests {
    my $b = Zaaksysteem::BrowserDetect->new(
        useragent =>
            "Mozilla/5.0 (Windows; U; MSIE 9.0; WIndows NT 9.0; en-US))",
        denied => [qw(
            ie::9
        )],
    );
    throws_ok(
        sub { $b->assert },
        qr#unsupported/browser#,
        "ie::9"
    );
}

1;

__END__

=head1 NAME

TestFor::General::BrowserDetect - Test ZS::BrowserDetect

=head1 DESCRIPTION

Test browser detection


=head1 SYNOPSIS

    ./zs_prove -v t/lib/TestFor/General/BrowserDetect.pm

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
