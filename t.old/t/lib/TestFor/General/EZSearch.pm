package TestFor::General::EZSearch;
use base qw(ZSTest);

use Zaaksysteem::EZSearch;
use Zaaksysteem::API::v1::Serializer::Reader::EZSearch;

use Moose;
use TestSetup;

=head1 NAME

 TestFor::General::EZSearch - Elastic Zaaksysteem searching

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ./zs_prove -v t/lib/TestFor/General/EZSearch.pm

=head1 DESCRIPTION

These tests prove the L<Zaaksysteem::EZSearch> module and related modules

=head1 USAGE

Usage tests, use these if you would like to know how to interact with the API, it's also useful
for extended documentation of the StUF API.

=head1 IMPLEMENTATION

=head2 ezsearch_instantation

Loading of class/exceptions/etc

=cut

sub ezsearch_instantiation : Tests {
    my $self    = shift;

    my $ezs     = new_ok('Zaaksysteem::EZSearch', [
        schema      => $schema,
        search_type => 'casetype',
    ], 'Succesfully constructed object');

    throws_ok(
        sub {
            Zaaksysteem::EZSearch->new(
                schema      => $schema,
                search_type => 'nonexisting',
            );
        },
        qr/Attribute.*does not pass the type constraint/,
        'Exception: need a valid component'
    );

    throws_ok(
        sub {
            $ezs->search()
        },
        qr/Cannot initiate a search without query set/,
        'Exception: need a valid query'
    );
}

=head2 ezsearch_casetype

Tests for searching casetypes

=cut

sub ezsearch_casetype : Tests {
    $zs->zs_transaction_ok(
        sub {
            $zs->create_zaaktype_predefined_ok(naam => 'With a rrruaarr title');
            $zs->create_zaaktype_predefined_ok(naam => 'With another title');

            my $ezs = Zaaksysteem::EZSearch->new(
                schema      => $schema,
                search_type => 'casetype',
            );

            $zs->num_queries_ok(
                sub {
                    my $results = $ezs->search('rrruaarr');
                    is(@$results, 1, 'Search: got one result for "rrruaarr"');

                    my $first = shift(@$results);

                    is($first->label, 'With a rrruaarr title', 'Got a correct label');
                    is($first->description, 'Dit is een omschrijving', 'Got a correct description');
                    is($first->search_type, 'casetype', 'Got a correct casetype');

                    ok($first->$_, "Found value for key $_") for (qw/reference label description score search_type/);
                },
                1
            );

            $zs->num_queries_ok(
                sub {
                    my $results = $ezs->search('With');
                    is(@$results, 2, 'Search: got two results for "With"');

                    ok($results->[0]->$_, "Found value for key $_") for (qw/reference label description score search_type/);
                },
                1
            );

            $zs->num_queries_ok(
                sub {
                    $ezs->query('rrruaarr');
                    my $results = $ezs->search();

                    is(@$results, 1, 'Search: got one results for "rrruaarr" via attribute setting');
                },
                1
            );
        },
        'Simple search, no filter'
    );

    $zs->zs_transaction_ok(
        sub {
            my $caset1 = $zs->create_zaaktype_predefined_ok(naam => 'With a rrruaarr title');
            my $caset2 = $zs->create_zaaktype_predefined_ok(naam => 'With another title');

            $caset1->zaaktype_node_id->trigger('intern');
            $caset1->zaaktype_node_id->update;

            $zs->num_queries_ok(
                sub {
                    my $ezs = Zaaksysteem::EZSearch->new(
                        schema        => $schema,
                        search_type   => 'casetype',
                        filters       => {
                            trigger => 'intern',
                        }
                    );

                    my $results = $ezs->search('With');
                    is(@$results, 1, 'Search: got one result for "With", trigger intern');

                    is($results->[0]->label, 'With a rrruaarr title', 'Got correct hit with trigger "intern"');
                },
                1
            );

            $zs->num_queries_ok(
                sub {
                    my $ezs = Zaaksysteem::EZSearch->new(
                        schema        => $schema,
                        search_type   => 'casetype',
                        filters       => {
                            betrokkene_type => 'natuurlijk_persoon',
                        }
                    );

                    my $results = $ezs->search('With');
                    is(@$results, 0, 'Search: got no results for query "With", trigger "natuurlijk_persoon"');
                },
                1
            );
        },
        'Simple search, with filters'
    );

    $zs->zs_transaction_ok(
        sub {
            $zs->create_zaaktype_predefined_ok(naam => 'With a rrruaarr title');
            $zs->create_zaaktype_predefined_ok(naam => 'With another title');

            my $ezs = Zaaksysteem::EZSearch->new(
                schema      => $schema,
                search_type => 'casetype',
            );

            my $first;
            $zs->num_queries_ok(
                sub {
                    my $results = $ezs->search('rrruaarr');
                    is(@$results, 1, 'Search: got one result for "rrruaarr"');

                    $first = shift(@$results);
                },
                1
            );

            $zs->num_queries_ok(
                sub {
                    my $reader     = Zaaksysteem::API::v1::Serializer::Reader::EZSearch->new();
                    my $object     = $reader->read_ezsearch(Zaaksysteem::API::v1::Serializer->new(), $first);
                },
                0,
                'Got queries on Zaaksysteem::API::v1::Serializer::Reader::EZSearch: '
            );
        },
        'API/v1/Serializer: Query count checker'
    );
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
