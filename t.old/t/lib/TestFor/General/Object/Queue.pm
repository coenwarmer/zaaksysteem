package TestFor::General::Object::Queue;

# ./zs_prove -v t/lib/TestFor/General/Object/Queue.pm
use base 'ZSTest';

use TestSetup;
use Zaaksysteem::Object::Queue::Model;

sub queue_model : Test(startup) {
    my $self = shift;

    my $rs = $zs->schema->resultset('Queue');

    isa_ok $rs, 'DBIx::Class::ResultSet', 'queue resultset factory';
    can_ok $rs, qw[create_item];

    my $model = Zaaksysteem::Object::Queue::Model->new(
        table => $rs,
        object_model => $zs->object_model,
        subject_model => $zs->betrokkene_model,
        subject_table => $zs->schema->resultset('Subject')
    );

    isa_ok $model, 'Zaaksysteem::Object::Queue::Model', 'model constructor';
    can_ok $model, qw[run run_item];

    $self->{ model } = $model;
    $self->{ queue_rs } = $rs;

    return;
}

sub test_queue_base : Tests {
    my $self = shift;

    $zs->txn_ok(sub {
        my $item = $self->{ queue_rs }->create_item('my_task', {
            label => 'Test task'
        });

        isa_ok $item, 'DBIx::Class::Row', 'queue item row is dbix::class object';

        is $item->status, 'pending', 'queue item row default status';
        is $item->label, 'Test task', 'queue item row label';
        is $item->type, 'my_task', 'queue item row type';
        is_deeply $item->data, {}, 'queue item row default data';
        isa_ok $item->date_created, 'DateTime', 'queue item row default date_created';
        ok !defined $item->date_finished, 'queue item row default date_finished';
        ok !defined $item->date_started, 'queue item row default date_started';
    }, 'basic queue item row tests');
}

sub test_queue_model_base : Tests {
    my $self = shift;

    $zs->txn_ok(sub {
        my $item = $self->{ queue_rs }->create_item('my_task', {
            label => 'Test task'
        });

        $item = $self->{ model }->run($item);

        isa_ok $item, 'DBIx::Class::Row', 'queue item row is dbix::class object';

        ok $item->date_created != $item->date_started, 'date_created != date_started';
        ok $item->date_created != $item->date_finished, 'date_created != date_finished';
        ok $item->date_started != $item->date_finished, 'date_started != date_finished';

        is $item->status, 'failed', 'unsupported task type fails queue item dispatch';
    }, 'basic queue model tests');

    $zs->txn_ok(sub {
        dies_ok {
            $self->{ queue_rs }->create_item;
        } 'create_item type validation';

        dies_ok {
            $self->{ queue_rs }->create_item('my_type');
        } 'create_item label validation';
    }, 'validation failure tests');
}

sub test_queue_model_email : Tests {
    my $self = shift;

    $zs->txn_ok(sub {
        my $case = $zs->create_case_ok;
        my $item = $self->{ queue_rs }->create_item('send_email', {
            label => 'derp',
            object_id => $case->object_data->id,
            data => {
                rcpt => 'overig',
                email => 'alice@example.org',
                body => 'body',
                subject => 'subject',
                sender_address => 'bob@example.org'
            }
        });

        lives_ok {
            $item = $self->{ model }->run($item)
        } 'trigger send_email handler';

        is $item->status, 'finished', 'e-mail queue item row status';

        my $contactmoment = $case->contactmoments->first;

        isa_ok $contactmoment, 'DBIx::Class::Row', 'contact moment row is dbix::class object';

        my $email = $contactmoment->contactmoment_emails->first;

        isa_ok $email, 'DBIx::Class::Row', 'email row is dbix::class object';

        is $email->body, 'body', 'contact moment email row body field';
        is $email->subject, 'subject', 'contact moment email row subject field';
        is $email->recipient, 'alice@example.org', 'contact moment email row recipient field';

        # TODO: Add more tests to verify email was sent (how?)
    }, 'basic queue model email tests');
}

sub test_queue_model_document : Tests {
    my $self = shift;

    $zs->txn_ok(sub {
        my $case = $zs->create_case_ok;

        my $attribute = $zs->create_bibliotheek_kenmerk_ok(
            naam => 'document test',
            magic_string => 'document_test',
            value_type => 'file'
        );

        my $template = $zs->create_zaaktype_sjabloon_ok(
            status => $case->zaaktype_node_id->zaaktype_statuses->first
        );

        my $subject = $zs->create_subject_ok;

        lives_ok {
            $case->case_actions_cine;
        } 'reinitialize case actions';

        my $item = $self->{ queue_rs }->create_item('create_case_document', {
            label => 'derp',
            object_id => $case->object_data->id,
            data => {
                _subject_id => $subject->id,
                bibliotheek_sjablonen_id => $template->get_column('bibliotheek_sjablonen_id'),
                bibliotheek_kenmerken_id => $template->get_column('bibliotheek_kenmerken_id'),
                filename => 'documenty',
                target_format => 'odt'
            }
        });

        lives_ok {
            $item = $self->{ model }->run($item)
        } 'trigger document creation';

        is $item->status, 'finished', 'document create queue item status';

        my $file = $case->files->first;

        isa_ok $file, 'Zaaksysteem::Backend::File::Component',
            'file instance';

        is $file->name, 'documenty', 'generated document name';
        is $file->get_column('created_by'), $subject->betrokkene_identifier,
            'generated document creator';
    });
}

sub test_queue_model_subcase : Tests {
    my $self = shift;

    $zs->txn_ok(sub {
        my $case = $zs->create_case_ok;

        my $subject = $zs->create_subject_ok;

        my $item = $self->{ queue_rs }->create_item('create_case_subcase', {
            label => 'derp',
            object_id => $case->object_data->id,
            data => {
                _subject_id => $subject->id,
                ou_id => $subject->group_ids->[0],
                role_id => $subject->role_ids->[0],
                relatie_type => 'vervolgzaak_datum',
                eigenaar_type => 'aanvrager',
                relatie_zaaktype_id => $case->get_column('zaaktype_id'),
                start_delay => ''
            }
        });

        lives_ok {
            $item = $self->{ model }->run($item)
        } 'trigger subcase creation';

        isa_ok $item, 'Zaaksysteem::Backend::Object::Queue::Component',
            're-fetched subcase create queue item is object';

        is $item->status, 'finished', 'subcase create queue item status';

        my $event = $case->loggings->search({
            event_type => 'case/subcase'
        })->first;

        isa_ok $event, 'Zaaksysteem::DB::Component::Logging',
            'subcase queue item log event';

        my $subcase = $zs->schema->resultset('Zaak')->find(
            $event->data->{ subcase_id }
        );

        isa_ok $subcase, 'Zaaksysteem::Zaken::ComponentZaak',
            'subcase create queue item case';
    });
}

sub test_queue_model_file : Tests {
    my $self = shift;

    $zs->txn_ok(sub {

        my $file = $zs->create_file_ok();
        my $item = $self->{ queue_rs }->create_item('generate_alternative', {
            label => 'testsuite-file',
            data => {
                file_id       => $file->id,
                target_format => 'pdf',
            }
        });

        lives_ok(
            sub {
                $item = $self->{model}->run($item);
            },
            'creating PDF'
        );

        isa_ok $item, 'Zaaksysteem::Backend::Object::Queue::Component',
            're-fetched subcase create queue item is object';

        cmp_deeply(
            {
                file_id        => $file->id,
                target_format  => 'pdf',
            },
            $item->data,
            "Item data is updated"
        );

        is $item->status, 'finished', 'finished generate_pdf item'


    }, "foo");
}

# No infrastructure to test object mutations yet... :(
# sub test_queue_model_object_mutation : Tests {
#     my $self = shift;
# }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
