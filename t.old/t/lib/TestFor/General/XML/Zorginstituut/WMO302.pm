package TestFor::General::XML::Zorginstituut::WMO302;
use base 'ZSTest';

use TestSetup;

use Encode qw(encode_utf8 decode_utf8);
use File::Basename;

use Zaaksysteem::XML::Zorginstituut::WMO302;
use DateTime;

sub wmo_ok {
    my $o = Zaaksysteem::XML::Zorginstituut::WMO302->new(@_);
    isa_ok($o, 'Zaaksysteem::XML::Zorginstituut::WMO302');
    return $o;
}

sub _generate_headers {
    return (
        Header => {
            Berichtspecificatie => {
                Code      => 414,
                Versie    => 3,
                SubVersie => 24,
            },
            Afzender             => 42,
            Ontvanger            => "01234567",
            Berichtidentificatie => {
                Identificatie => "zs-testsuite",
                Dagtekening   => DateTime->now,
                Tekenset      => 1,
            },
        },
        @_,
    );

}

sub test_iwmo_302 : Tests {

    my $xml = $zs->slurp($zs->basedir, qw(share xsd examples iwmo 302.xml));

    my $wmo = wmo_ok(xml => $xml);

    is(
        $wmo->find(
            '/wmo302:Bericht/wmo302:Header/wmo302:RetourCode01',
            )->size,
        1,
        "Found RetourCode01 in wmo302 header"
    );
    is(
        $wmo->find(
            '/wmo302:Bericht/wmo302:Header/wmo302:RetourCode04',
            )->size,
        0,
        "Found RetourCode04 does not exists wmo302 header"
    );

    is($wmo->_is_section_valid('header'), 0, "Header is not correct");
    is($wmo->is_valid, 0, "Message is not correct");
}

sub test_iwmo_302_retourcodes : Tests {

    my %params = (
        message_mapping => {
            header => { xpath  => '/wmo302:Bericht/wmo302:Header', },
            clienten => { xpath  => '/wmo302:Bericht/wmo302:Clienten', optional => 1},
        },
    );

    {
        my $xml = qq{<?xml version="1.0"?>
<x0:Bericht xmlns:iwmo="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0" xmlns:x0="http://www.istandaarden.nl/iwmo/2_0/wmo302/schema/2_0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="x0:Root">
  <x0:Header xsi:type="x0:Header">
    <x0:RetourCode01 xsi:type="xs:string">0200</x0:RetourCode01>
    <x0:RetourCode02 xsi:type="xs:string">0200</x0:RetourCode02>
    <x0:RetourCode03 xsi:type="xs:string">0200</x0:RetourCode03>
    </x0:Header>
  </x0:Bericht>};

        my $wmo = wmo_ok(%params, xml => $xml);
        ok($wmo->_is_section_valid('header'), "Header is OK");
        is($wmo->is_valid, 1, "Message is correct");
    }

    {
        my $xml = qq{<?xml version="1.0"?>
    <x0:Bericht xmlns:iwmo="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0" xmlns:x0="http://www.istandaarden.nl/iwmo/2_0/wmo302/schema/2_0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="x0:Root">
      <x0:Header xsi:type="x0:Header">
        <x0:RetourCode01 xsi:type="xs:string">0001</x0:RetourCode01>
      </x0:Header>
    </x0:Bericht>
};
        my $wmo = wmo_ok(%params, xml => $xml);
        is($wmo->is_valid, 0, "Message isnt correct");

        cmp_deeply(
            $wmo->show_errors,
            {
                header => {
                    RetourCode01 => '0001',
                    RetourCode02 => undef,
                    RetourCode03 => undef
                }
            },
            "Errors are shown"
        );
    }

    {
        my $xml = qq{<?xml version="1.0"?>
    <x0:Bericht xmlns:iwmo="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0" xmlns:x0="http://www.istandaarden.nl/iwmo/2_0/wmo302/schema/2_0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="x0:Root">
      <x0:Header xsi:type="x0:Header">
        <x0:RetourCode01 xsi:type="xs:string">0200</x0:RetourCode01>
      </x0:Header>
    </x0:Bericht>
};
        my $wmo = wmo_ok(%params, xml => $xml);
        is($wmo->is_valid, 1, "Message with single retourcode is correct");
        cmp_deeply($wmo->show_errors, { }, "No errors to be shown are shown");
    }

    {
        my $xml = qq{<?xml version="1.0"?>
    <x0:Bericht xmlns:iwmo="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0" xmlns:x0="http://www.istandaarden.nl/iwmo/2_0/wmo302/schema/2_0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="x0:Root">
      <x0:Header xsi:type="x0:Header">
        <x0:RetourCode02 xsi:type="xs:string">0200</x0:RetourCode02>
      </x0:Header>
    </x0:Bericht>
};
        my $wmo = wmo_ok(%params, xml => $xml);
        throws_ok(
            sub {
                $wmo->is_valid;
            },
            qr#zorginstituut/get/nodelist/no_nodes#,
            "Error message because required field is missing"
        );
    }
}

sub multiple_answers : Tests {

    my %params = (
        message_mapping => {
            header => {
                xpath    => '/wmo302:Bericht/wmo302:Header',
                multiple => 1
            },
            clienten => { xpath  => '/wmo302:Bericht/wmo302:Clienten', optional => 1},
        },
    );
    {
        my $xml = qq{<?xml version="1.0"?>
    <x0:Bericht xmlns:iwmo="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0" xmlns:x0="http://www.istandaarden.nl/iwmo/2_0/wmo302/schema/2_0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="x0:Root">
      <x0:Header xsi:type="x0:Header">
        <x0:RetourCode01 xsi:type="xs:string">0200</x0:RetourCode01>
        <x0:RetourCode02 xsi:type="xs:string">0200</x0:RetourCode02>
      </x0:Header>
      <x0:Header xsi:type="x0:Header">
        <x0:RetourCode01 xsi:type="xs:string">0200</x0:RetourCode01>
        <x0:RetourCode02 xsi:type="xs:string">0200</x0:RetourCode02>
        <x0:RetourCode03 xsi:type="xs:string">0200</x0:RetourCode03>
      </x0:Header>
    </x0:Bericht>
};
        my $wmo = wmo_ok(%params, xml => $xml);

        ok($wmo->is_valid, "Is valid");
        lives_ok(sub { $wmo->show_errors; },
            "Header is error code is optional");
    }

    {
        # Missing RetourCode01
        my $xml = qq{<?xml version="1.0"?>
    <x0:Bericht xmlns:iwmo="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0" xmlns:x0="http://www.istandaarden.nl/iwmo/2_0/wmo302/schema/2_0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="x0:Root">
      <x0:Header xsi:type="x0:Header">
        <x0:RetourCode02 xsi:type="xs:string">0200</x0:RetourCode02>
      </x0:Header>
      <x0:Header xsi:type="x0:Header">
        <x0:RetourCode01 xsi:type="xs:string">0200</x0:RetourCode01>
        <x0:RetourCode02 xsi:type="xs:string">0200</x0:RetourCode02>
      </x0:Header>
    </x0:Bericht>
};
        my $wmo = wmo_ok(%params, xml => $xml);
        throws_ok(
            sub {
                $wmo->is_valid;
            },
            qr#zs/zorginstituut/nodes/missing: RetourCode01 missing#,
            "Invalid XML"
        );
    }

    {
        my $xml = qq{<?xml version="1.0"?>
    <x0:Bericht xmlns:iwmo="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0" xmlns:x0="http://www.istandaarden.nl/iwmo/2_0/wmo302/schema/2_0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="x0:Root">
      <x0:Header xsi:type="x0:Header">
        <x0:RetourCode01 xsi:type="xs:string">0200</x0:RetourCode01>
        <x0:RetourCode02 xsi:type="xs:string">0200</x0:RetourCode02>
      </x0:Header>
      <x0:Header xsi:type="x0:Header">
        <x0:RetourCode01 xsi:type="xs:string">0100</x0:RetourCode01>
        <x0:RetourCode02 xsi:type="xs:string">0100</x0:RetourCode02>
      </x0:Header>
    </x0:Bericht>
};
        my $wmo = wmo_ok(%params, xml => $xml);
        ok(!$wmo->is_valid, "XML is not valid, second product is fail");
        cmp_deeply(
            $wmo->show_errors,
            {
                header => {
                    RetourCode01 => ['0200', '0100'],
                    RetourCode02 => ['0200', '0100'],
                    RetourCode03 => undef,
                },
            },
            "Correct errors"
        );

    }

}

sub ZS_10672 : Tests {

        my $xml = qq{<?xml version="1.0" encoding="utf-8"?>
<Bericht xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.istandaarden.nl/iwmo/2_0/wmo302/schema/2_0">
  <Header>
    <Berichtspecificatie>
      <Code xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">415</Code>
      <Versie xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">2</Versie>
      <SubVersie xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">0</SubVersie>
    </Berichtspecificatie>
    <Afzender>0999</Afzender>
    <Ontvanger>02010009</Ontvanger>
    <Berichtidentificatie>
      <Identificatie xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">135</Identificatie>
      <Dagtekening xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">2016-01-06</Dagtekening>
      <Tekenset xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">1</Tekenset>
    </Berichtidentificatie>
    <IdentificatieRetour>123457</IdentificatieRetour>
    <DagtekeningRetour>2016-02-02</DagtekeningRetour>
    <TekensetRetour>1</TekensetRetour>
  </Header>
  <Clienten>
    <Client>
      <Bsn>788178245</Bsn>
      <GeheimeClient>2</GeheimeClient>
      <Clientnummer>788178245</Clientnummer>
      <Geboortedatum>
        <Datum xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">1995-01-06</Datum>
      </Geboortedatum>
      <Geslacht>1</Geslacht>
      <BurgerlijkeStaat>2</BurgerlijkeStaat>
      <Naam>
        <Geslachtsnaam xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">
          <Naam>Zaaksysteem</Naam>
          <NaamCode>1</NaamCode>
        </Geslachtsnaam>
        <NaamCode xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">1</NaamCode>
      </Naam>
      <RetourCode01>0383</RetourCode01>
      <Adressen>
        <Adres>
          <Soort>01</Soort>
          <Adres>
            <Huis xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">
              <Huisnummer>42</Huisnummer>
            </Huis>
            <Postcode xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">1011PZ</Postcode>
            <Straatnaam xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">Muiderstraat</Straatnaam>
            <Plaatsnaam xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">Amsterdam</Plaatsnaam>
            <LandCode xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">NL</LandCode>
          </Adres>
          <RetourCode01>0001</RetourCode01>
        </Adres>
      </Adressen>
      <Beschikking>
        <Beschikkingnummer>3263</Beschikkingnummer>
        <Afgiftedatum>2016-01-06</Afgiftedatum>
        <Ingangsdatum>2016-01-06</Ingangsdatum>
        <RetourCode01>0001</RetourCode01>
        <BeschiktProducten>
          <BeschiktProduct>
            <Product>
              <Categorie xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">01</Categorie>
              <Code xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">01234</Code>
            </Product>
            <Ingangsdatum>2016-01-06</Ingangsdatum>
            <Einddatum>2016-01-06</Einddatum>
            <Omvang>
              <Volume xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">1</Volume>
              <Eenheid xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">01</Eenheid>
              <Frequentie xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">1</Frequentie>
            </Omvang>
            <RetourCode01>0001</RetourCode01>
          </BeschiktProduct>
        </BeschiktProducten>
        <ToegewezenProducten>
          <ToegewezenProduct>
            <Product>
              <Categorie xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">01</Categorie>
              <Code xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">01234</Code>
            </Product>
            <Aanbieder>02010009</Aanbieder>
            <Gemeentecode>0999</Gemeentecode>
            <Toewijzingsdatum>2016-01-06</Toewijzingsdatum>
            <Toewijzingstijd>00:00:01.0000000+01:00</Toewijzingstijd>
            <Ingangsdatum>2016-01-06</Ingangsdatum>
            <Einddatum>2016-01-06</Einddatum>
            <Omvang>
              <Volume xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">1</Volume>
              <Eenheid xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">01</Eenheid>
              <Frequentie xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">1</Frequentie>
            </Omvang>
            <RetourCode01>0001</RetourCode01>
          </ToegewezenProduct>
        </ToegewezenProducten>
      </Beschikking>
    </Client>
  </Clienten>
</Bericht>
};
        my $wmo = wmo_ok(xml => $xml);
        ok(!$wmo->is_valid, "Is not valid valid");
        lives_ok(sub {  $wmo->show_errors; }, "Header is error code is optional");
}

sub ZS_11584 : Tests {

        my $xml = qq{<?xml version="1.0" encoding="utf-8"?>
<Bericht xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.istandaarden.nl/iwmo/2_0/wmo302/schema/2_0">
  <Header>
    <Berichtspecificatie>
      <Code xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">415</Code>
      <Versie xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">2</Versie>
      <SubVersie xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">0</SubVersie>
    </Berichtspecificatie>
    <Afzender>0999</Afzender>
    <Ontvanger>02010009</Ontvanger>
    <Berichtidentificatie>
      <Identificatie xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">135</Identificatie>
      <Dagtekening xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">2016-01-06</Dagtekening>
      <Tekenset xmlns="http://www.istandaarden.nl/iwmo/2_0/basisschema/schema/2_0">1</Tekenset>
    </Berichtidentificatie>
    <IdentificatieRetour>123457</IdentificatieRetour>
    <DagtekeningRetour>2016-02-02</DagtekeningRetour>
    <TekensetRetour>1</TekensetRetour>
    <RetourCode02>0200</RetourCode02><RetourCode03>0200</RetourCode03>

  </Header>
</Bericht>
};
        my $wmo = wmo_ok(xml => $xml);
        ok($wmo->is_valid, "WMO302 with header only is valid");
        cmp_deeply( $wmo->show_errors, { }, "show errors only shows header information");
    }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
