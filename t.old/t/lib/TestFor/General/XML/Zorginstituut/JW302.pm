package TestFor::General::XML::Generator::Zorginstituut;
use base 'ZSTest';

use TestSetup;

use Zaaksysteem::XML::Zorginstituut::JW302;


sub jw_ok {
    my $o = Zaaksysteem::XML::Zorginstituut::JW302->new(@_);
    isa_ok($o, 'Zaaksysteem::XML::Zorginstituut::JW302');
    return $o;
}

sub test_ijw_302 : Tests {

    my $xml = $zs->slurp(
        $zs->basedir, qw(share xsd examples ijw ijw_302.xml)
    );

    my $jw = jw_ok(xml => $xml);

    is(
        $jw->find(
            '/jw302:Bericht/jw302:Header/jw302:RetourCode01'
            )->size,
        1,
        "Found RetourCode01 in jw302 header"
    );
    is(
        $jw->find(
            '/jw302:Bericht/jw302:Header/jw302:RetourCode04'
            )->size,
        0,
        "Found RetourCode04 does not exists jw302 header"
    );

    is($jw->_is_section_valid('header'), 1, "Header is correct");
    is($jw->is_valid, 1, "Message is correct");
}

sub test_ijw_302_retourcodes : Tests {

    my $xml = qq{<?xml version="1.0"?>
<x0:Bericht xmlns:ijw="http://www.istandaarden.nl/ijw/2_0/basisschema/schema/2_0" xmlns:x0="http://www.istandaarden.nl/ijw/2_0/jw302/schema/2_0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="x0:Root">
  <x0:Header xsi:type="x0:Header">
    <x0:RetourCode01 xsi:type="xs:string">0200</x0:RetourCode01>
    <x0:RetourCode02 xsi:type="xs:string">0200</x0:RetourCode02>
    <x0:RetourCode03 xsi:type="xs:string">0200</x0:RetourCode03>
  </x0:Header>
</x0:Bericht>
};
    my %params = (
        message_mapping => {
            header => {
                xpath    => 'jw302:Bericht/jw302:Header',
            },
            clienten => { xpath => 'jw302:Bericht/jw302:Clienten', optional => 1}
        },
    );

    {
        my $jw = jw_ok(%params, xml => $xml);
        ok($jw->_is_section_valid('header'), "Header is OK");
        is($jw->is_valid, 1, "Message is correct");
    }

    {
        my $xml = qq{<?xml version="1.0"?>
<x0:Bericht xmlns:ijw="http://www.istandaarden.nl/ijw/2_0/basisschema/schema/2_0" xmlns:x0="http://www.istandaarden.nl/ijw/2_0/jw302/schema/2_0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="x0:Root">
  <x0:Header xsi:type="x0:Header">
    <x0:RetourCode01 xsi:type="xs:string">0001</x0:RetourCode01>
  </x0:Header>
</x0:Bericht>
};
        my $jw = jw_ok(%params, xml => $xml);
        is($jw->is_valid, 0, "Message isnt correct");

        cmp_deeply(
            $jw->show_errors,
            {
                header => {
                    RetourCode01 => '0001',
                    RetourCode02 => undef,
                    RetourCode03 => undef
                }
            },
            "Errors are shown"
        );
    }

    {
        my $xml = qq{<?xml version="1.0"?>
<x0:Bericht xmlns:ijw="http://www.istandaarden.nl/ijw/2_0/basisschema/schema/2_0" xmlns:x0="http://www.istandaarden.nl/ijw/2_0/jw302/schema/2_0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="x0:Root">
  <x0:Header xsi:type="x0:Header">
    <x0:RetourCode01 xsi:type="xs:string">0200</x0:RetourCode01>
  </x0:Header>
</x0:Bericht>
};
        my $jw = jw_ok(%params, xml => $xml);
        is($jw->is_valid, 1, "Message with single retourcode is correct");
        cmp_deeply($jw->show_errors, { }, "No errors to be shown are shown");
    }

    {
        my $xml = qq{<?xml version="1.0"?>
<x0:Bericht xmlns:ijw="http://www.istandaarden.nl/ijw/2_0/basisschema/schema/2_0" xmlns:x0="http://www.istandaarden.nl/ijw/2_0/jw302/schema/2_0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="x0:Root">
  <x0:Header xsi:type="x0:Header">
    <x0:RetourCode02 xsi:type="xs:string">0001</x0:RetourCode02>
  </x0:Header>
</x0:Bericht>
};
        my $jw = jw_ok(%params, xml => $xml);
        throws_ok(
            sub {
                $jw->is_valid;
            },
            qr#zorginstituut/get/nodelist/no_nodes#,
            "Error message because required field is missing"
        );
    }

    {
        my $xml = qq{<?xml version="1.0"?>
<x0:Bericht xmlns:ijw="http://www.istandaarden.nl/ijw/2_0/basisschema/schema/2_0" xmlns:x0="http://www.istandaarden.nl/ijw/2_0/jw302/schema/2_0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="x0:Root">
  <x0:Foo xsi:type="x0:Foo">
  </x0:Foo>
</x0:Bericht>
};

        %params = (
            message_mapping => {
                header => {
                    xpath    => 'jw302:Bericht/jw302:Header',
                    optional => 1,
                },
                clienten => { xpath => 'jw302:Bericht/jw302:Clienten', optional => 1}
            },
        );

        my $jw = jw_ok(%params, xml => $xml);
        lives_ok(
            sub {
                $jw->is_valid;
            },
        );
        cmp_deeply($jw->show_errors, { }, "No errors to be shown are shown for optional headers");
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
