package TestFor::General::XML::Generator::iJW;
use base 'ZSTest';

use TestSetup;

use Encode qw(encode_utf8 decode_utf8);
use File::Basename;

sub _generate_headers {
    return (
        Header => {
            Berichtspecificatie => {
                Code      => 436,
                Versie    => 2,
                SubVersie => 00,
            },
            Afzender             => '01234',
            Ontvanger            => "02010009",
            Berichtidentificatie => {
                Identificatie => "zs-testsuite",
                Dagtekening   => DateTime->now,
                Tekenset      => 1,
            },
        },
        @_,
    );
}

sub test_ijw_301 : Tests {

    my $elem = '{http://www.istandaarden.nl/ijw/2_0/jw301/schema/2_0}Bericht';
    my $reader = $zs->jw_schema->compile(READER => "$elem");

    my $np = $zs->create_natuurlijk_persoon_ok();

    my $now = DateTime->now();

    my $end   = $now->clone->add(days => 90);
    my $start = $now->clone->add(days => 30);

    with_stopped_clock {
        my $xml = $zs->ijw_2_0->build_301(
            writer => {
                _generate_headers(),
                Client      => $np,
                Beschikking => {
                    nummer        => 100,
                    afgiftedatum  => $now,
                    ingangsdatum  => $start,
                    einddatum     => $end,
                },
                gemeentecode => '01234',
                producten    => {
                    beschikt => [
                        {
                            ingangsdatum => $start,
                            einddatum    => $end,
                            omvang       => {
                                volume     => 1,
                                eenheid    => '01',
                                frequentie => 1,
                            },
                            code       => 10,
                            categorie  => 32,
                            commentaar => 'x' x 120,
                        },
                    ],
                    toegewezen => [
                        {
                            ingangsdatum      => $start,
                            einddatum         => $end,
                            toewijzingsdatum  => $now,
                            beschikkingsdatum => $now,
                            omvang            => {
                                volume     => 1,
                                eenheid    => '01',
                                frequentie => 1,
                            },

                            # https://www.agbcode.nl/Webzoeker/Snel
                            # https://www.agbcode.nl/Webzoeker/Details/1295395
                            aanbieder => '02010009',    # OLVG apotheek
                            code      => 10,
                            categorie => 32,
                        },
                    ],
                },
                commentaar => {
                    general              => 'x' x 120,
                    toegewezen_producten => 'x' x 120,
                },
            }
        );


        if ($ENV{JW301}) {
            use autodie;
            open my $fh, '>', $ENV{JW301};
            print $fh $xml;
            close $fh;
        }

        $zs->test_generated_xml_ok(
            reader  => $reader,
            xml     => $xml,
        );

    };

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
