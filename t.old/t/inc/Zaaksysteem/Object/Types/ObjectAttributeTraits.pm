package Zaaksysteem::Object::Types::ObjectAttributeTraits;

use Moose;

extends 'Zaaksysteem::Object';

has solo => (
    is => 'rw',
    isa => 'Str',
    traits => [qw[OA]],
    label => 'We must be unique',
    unique => 1
);

has standaard => (
    is => 'rw',
    isa => 'Str',
    traits => [qw[OA]],
    label => 'Default option test',
    default => 'Dope shit ouwe'
);

has duo => (
    is => 'rw',
    isa => 'Str',
    traits => [qw[OA]],
    label => "We aren't unique",
    unique => 0
);

has no_unique => (
    is => 'rw',
    isa => 'Str',
    traits => [qw[OA]],
    label => 'Not unique either'
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
